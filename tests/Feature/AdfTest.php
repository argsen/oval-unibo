<?php

namespace Tests\Feature;

use TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use GuzzleHttp\Client;

use oval\Http\Controllers\AdfController;

class AdfTest extends TestCase
{
    /**
     * Test if a proper token is returned.
     * 
     * @return void
     */
    public function testGetToken()
    {
        $adf = new AdfController;
        $this->assertInstanceOf(Client::class, $adf->client);
        $this->assertNotEmpty($adf->token);
    }

    /**
     * Test Verify in AdfController.
     * 
     * @return void
     */
    public function testVerifyController()
    {
        $response = $this->get('https://covaa.local:8443/adf?contextId=');

        $this->assertDatabaseHas('adf_users', [
            'uuid' => '4a3486ca-8002-497b-8600-ae246062f1a4',
            'email' => 'moe00105t@mail.dev.sls.ufinity.com'
        ]);
        $this->assertDatabaseHas('users', [
            'email' => 'moe00105t@mail.dev.sls.ufinity.com'
        ]);
        $this->assertDatabaseHas('adf_courses', [
            'uuid' => '90c632fa-92e1-11e7-b105-0800279a327c',
            'subject' => 'ADDITIONAL MATHEMATICS'
        ]);
        $this->assertDatabaseHas('courses', [
            'name' => 'ADDITIONAL MATHEMATICS'
        ]);
    }
}
