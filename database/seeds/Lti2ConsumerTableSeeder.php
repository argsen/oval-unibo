<?php

use Illuminate\Database\Seeder;

class Lti2ConsumerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //-- Enter key and secret, then use the same for moodle
        //-- (name can be null)
        // DB::table('lti2_consumer')->insert([
        //     'name' => '',
        //     'consumer_key256' => '',
        //     'secret' => '',
        //     'enabled' => true
        // ]);

        DB::table('lti2_consumer')->insert([
            'name' => 'Moo',
            'consumer_key256' => '7671f502-c8d4-4f35-9cc4-d21e81a2a4ae',
            'secret' => '5t$ZvAh2-zS_M55cDhr675rk',
            'protected'=>false,
            'enabled' => true,
            'created' => date("Y-m-d H:i:s"),
            'updated' => date("Y-m-d H:i:s")
        ]);
        DB::table('lti2_consumer')->insert([
            'name' => 'Noo',
            'consumer_key256' => '#h;E`!dtplT{yy#60Y%3',
            'secret' => 'tTIGGC}UiaAx-2S[D&p8',
            'protected'=>false,
            'enabled' => true,
            'created' => date("Y-m-d H:i:s"),
            'updated' => date("Y-m-d H:i:s")
        ]);
        DB::table('lti2_consumer')->insert([
            'consumer_pk' => 999,
            'name' => 'ADF',
            'consumer_key256' => '3f8c8abc-73a7-11e8-98e2-0800279a327c',
            'secret' => '12345678',
            'protected'=>false,
            'enabled' => true,
            'created' => date("Y-m-d H:i:s"),
            'updated' => date("Y-m-d H:i:s")
        ]);
    }
}
