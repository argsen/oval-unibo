<?php

use Illuminate\Database\Seeder;

class Make20Coders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //--create users--
        DB::table('users')->insert([
			'first_name' => 'Manager',
			'last_name' => '1',
			'email' => 'm1@oval.tk',
			'role' => 'O',
			'password' => bcrypt('nT2177'),
        ]);
        DB::table('users')->insert([
			'first_name' => 'Coder',
			'last_name' => '1',
			'email' => 'c1@oval.tk',
			'role' => 'O',
			'password' => bcrypt('BAqYd3'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '2',
            'email' => 'c2@oval.tk',
            'role' => 'O',
            'password' => bcrypt('Whwas7'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '3',
            'email' => 'c3@oval.tk',
            'role' => 'O',
            'password' => bcrypt('yVgxRt'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '4',
            'email' => 'c4@oval.tk',
            'role' => 'O',
            'password' => bcrypt('zu6bmy'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '5',
            'email' => 'c5@oval.tk',
            'role' => 'O',
            'password' => bcrypt('BjRAVE'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '6',
            'email' => 'c6@oval.tk',
            'role' => 'O',
            'password' => bcrypt('bRwJwB'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '7',
            'email' => 'c7@oval.tk',
            'role' => 'O',
            'password' => bcrypt('5ebhEd'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '8',
            'email' => 'c8@oval.tk',
            'role' => 'O',
            'password' => bcrypt('SQjUAR'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '9',
            'email' => 'c9@oval.tk',
            'role' => 'O',
            'password' => bcrypt('YDMMq6'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '10',
            'email' => 'c10@oval.tk',
            'role' => 'O',
            'password' => bcrypt('vpabK5'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '11',
            'email' => 'c11@oval.tk',
            'role' => 'O',
            'password' => bcrypt('A8HKcB'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '12',
            'email' => 'c12@oval.tk',
            'role' => 'O',
            'password' => bcrypt('MjABK9'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '13',
            'email' => 'c13@oval.tk',
            'role' => 'O',
            'password' => bcrypt('RDprNy'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '14',
            'email' => 'c14@oval.tk',
            'role' => 'O',
            'password' => bcrypt('pZe3n4'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '15',
            'email' => 'c15@oval.tk',
            'role' => 'O',
            'password' => bcrypt('2zXehR'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '16',
            'email' => 'c16@oval.tk',
            'role' => 'O',
            'password' => bcrypt('59rSgq'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '17',
            'email' => 'c17@oval.tk',
            'role' => 'O',
            'password' => bcrypt('tuqTXp'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '18',
            'email' => 'c18@oval.tk',
            'role' => 'O',
            'password' => bcrypt('PBnAVf'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '19',
            'email' => 'c19@oval.tk',
            'role' => 'O',
            'password' => bcrypt('CjjtpF'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'Coder',
            'last_name' => '20',
            'email' => 'c20@oval.tk',
            'role' => 'O',
            'password' => bcrypt('eCZ2eW'),
        ]);

        //--create course--
        $cid = 10000002; 
        DB::table('courses')->insert([
            'id'=> $cid,
        	'name' => 'OVAL Course',
            'created_at' => date("Y-m-d H:i:s")
        ]);
        //--create default group--
        DB::table('groups')->insert([
            'name' => 'Default Group',
            'course_id' => $cid,
        ]);
        $grp = oval\Group::where('course_id', $cid)->first();

        //--enroll & add to group--
        $instructor = oval\User::where('first_name', 'Manager')->first();
        DB::table('enrollments')->insert([
            'course_id'=>$cid, 
            'user_id'=>$instructor->id, 
            'is_instructor'=>true
        ]);
        $grp->addMember($instructor);


        $students = oval\User::where('first_name', 'Coder')->get();
        foreach($students as $s) {
            DB::table('enrollments')->insert([
                'course_id'=>$cid, 
                'user_id'=>$s->id, 
                'is_instructor'=>false
            ]);
            $grp->addMember($s);
        }


        

    }//end run()
}



// DB::table('users')->insert([
//     'first_name' => 'Coder',
//     'last_name' => '1',
//     'email' => 'c1@oval.tk',
//     'role' => 'O',
//     'password' => bcrypt('password'),
// ]);


















