<?php

use Illuminate\Database\Seeder;

class AnnotationPostsTagLabelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '1',
            'name' => 'In my opinion...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '1',
            'name' => 'My view is...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '1',
            'name' => 'I believe...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '1',
            'name' => 'I conclude...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '2',
            'name' => 'My opinion is support by...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '2',
            'name' => 'The evidence of my view is...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '2',
            'name' => 'The data/information that supports my conclusion is...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '3',
            'name' => 'I concur...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '3',
            'name' => 'I have the same idea...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '3',
            'name' => 'I share your/the authors view...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '3',
            'name' => 'That is a good point...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '4',
            'name' => 'I have a different idea...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '4',
            'name' => 'I dont share you/the author view...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '4',
            'name' => 'i beg to differ...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '5',
            'name' => 'Why do you say that...?',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '5',
            'name' => 'I am not sure I understand...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '5',
            'name' => 'Are you sure about...?',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '5',
            'name' => 'What makes you say that...?',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_label')->insert([
            'tag_group_id' => '5',
            'name' => 'How did you come to this conclusion...?',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
