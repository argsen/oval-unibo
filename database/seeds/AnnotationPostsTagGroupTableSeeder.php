<?php

use Illuminate\Database\Seeder;

class AnnotationPostsTagGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('annotation_posts_tag_group')->insert([
            'name' => 'I THINK that...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_group')->insert([
            'name' => 'I think so BECAUSE...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_group')->insert([
            'name' => 'I AGREE...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_group')->insert([
            'name' => 'I DISAGREE...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('annotation_posts_tag_group')->insert([
            'name' => 'I need to ASK...',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
