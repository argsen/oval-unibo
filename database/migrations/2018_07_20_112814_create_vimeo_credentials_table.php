<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVimeoCredentialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vimeo_credentials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vimeo_user_uri')->nullable();
            $table->string('vimeo_user_name')->nullable();
            $table->string('access_token');
            $table->integer('user_id')->length(10)->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('vimeo_credentials', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vimeo_credentials', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('vimeo_credentials');
    }
}
