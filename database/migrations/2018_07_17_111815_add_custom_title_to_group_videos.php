<?php 
 
use Illuminate\Support\Facades\Schema; 
use Illuminate\Database\Schema\Blueprint; 
use Illuminate\Database\Migrations\Migration; 
 
class AddCustomTitleToGroupVideos extends Migration 
{ 
    /** 
     * Run the migrations. 
     * 
     * @return void 
     */ 
    public function up() 
    { 
        Schema::table('group_videos', function (Blueprint $table) { 
            $table->string('custom_title')->nullable(); 
        }); 
    } 
 
    /** 
     * Reverse the migrations. 
     * 
     * @return void 
     */ 
    public function down() 
    { 
        Schema::table('group_videos', function (Blueprint $table) { 
            //simply drop the column 
            $table->dropColumn('custom_title'); 
        }); 
    } 
} 