<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use oval\Keyword;
use oval\GroupVideo;
use oval\Video;

class EditKeywordsToRelateToGroupVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('keywords', function (Blueprint $table) {
            $table->integer('group_video_id')->length(10)->unsigned();
            $table->dropForeign(['videoId']);

            $table->boolean('hide')->default(false);
        });

        $all_kw = oval\Keyword::all();
        foreach ($all_kw as $kw) {
            $group_videos = oval\GroupVideo::where('video_id', '=', $kw->videoId)->get();
            foreach ($group_videos as $gv) {
                $new_kw = new Keyword;
                $new_kw->keyword = $kw->keyword;
                $new_kw->startTime = $kw->startTime;
                $new_kw->endTime = $kw->endTime;
                $new_kw->relevance = $kw->relevance;
                $new_kw->type = $kw->type;
                $new_kw->group_video_id = $gv->id;
                $new_kw->hide = false;
                $new_kw->save();
            }
            $kw->delete();
        }

        Schema::table('keywords', function(Blueprint $table){
            $table->dropColumn('videoId');
            $table->foreign('group_video_id')->references('id')->on('group_videos')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //--'hide' column is simply dropped here, so if it was set to hidden, it shows again
        Schema::table('keywords', function (Blueprint $table) {
            $table->integer('videoId')->length(10)->unsigned();
            $table->dropColumn('hide');
            $table->dropForeign(['group_video_id']);
        });

        $all_kw = oval\Keyword::all();
        $video_id = null;
        foreach ($all_kw as $kw) {
            $gv = GroupVideo::find($kw->group_video_id);
            $vid = $gv->video()->id;
            if ($video_id != $vid) {
                $new_kw = new Keyword;
                $new_kw->videoId = $vid;
                $new_kw->keyword = $kw->keyword;
                $new_kw->startTime = $kw->startTime;
                $new_kw->endTime = $kw->endTime;
                $new_kw->relevance = $kw->relevance;
                $new_kw->type = $kw->type;
                $new_kw->save();
            }
            $kw->delete();
        }

        Schema::table('keywords', function(Blueprint $table) {
            $table->foreign('videoId')->references('id')->on('videos')->onDelete('cascade');
            $table->dropColumn('group_video_id');
        });
    }
}
