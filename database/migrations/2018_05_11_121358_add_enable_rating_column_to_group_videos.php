<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnableRatingColumnToGroupVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_videos', function (Blueprint $table) {
            $table->boolean('enable_recommended_resources_rating')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_videos', function (Blueprint $table) {
            $table->dropColumn('enable_recommended_resources_rating');
        });
    }
}
