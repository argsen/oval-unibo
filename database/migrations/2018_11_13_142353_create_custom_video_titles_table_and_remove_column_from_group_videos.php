<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use oval\GroupVideo;
use oval\Group;
use oval\Course;
use oval\Video;
use oval\CustomVideoTitle;


class CreateCustomVideoTitlesTableAndRemoveColumnFromGroupVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_video_titles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('video_id')->length(10)->unsigned();
            $table->integer('course_id')->length(10)->unsigned();
            $table->string('custom_title');
            $table->timestamps();
        });

        Schema::table('custom_video_titles', function(Blueprint $table) {
            $table->foreign('video_id')
                    ->references('id')->on('videos')
                    ->onDelete('cascade');
            $table->foreign('course_id')
                    ->references('id')->on('courses')
                    ->onDelete('cascade');
        });

        //-- copy existing custom_title 
        $group_videos = oval\GroupVideo::all();
        foreach($group_videos as $gv) {
            if ($gv->custom_title) {
                $course = $gv->course();

                //-- check if record already exists from another group_video
                $cvt = CustomVideoTitle::where([
                        ['video_id', '=', $gv->video_id],
                        ['course_id', '=', $course->id]
                    ])
                    ->first();
                //-- in not exists already, create entry in custom_video_titles table
                if (empty($cvt)) {
                    $cvt = new CustomVideoTitle;
                    $cvt->video_id = $gv->video_id;
                    $cvt->course_id = $course->id;
                    $cvt->custom_title = $gv->custom_title;
                    $cvt->save();
                }
                
            }
        }

        //-- drop column from group_videos
        Schema::table('group_videos', function(Blueprint $table) {
            $table->dropColumn('custom_title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //--add back column to group_videos
        Schema::table('group_videos', function(Blueprint $table) {
            $table->string('custom_title')->nullable();
        });

        //-- put custom_title in group_videos table
        $cvts = oval\CustomVideoTitle::all();
        foreach ($cvts as $cvt) {
            $course = oval\Course::find($cvt->course_id);
            $groups = $course->groups;
            foreach ($groups as $g) {
                $gv = oval\GroupVideo::where([
                        ['group_id', '=', $g->id],
                        ['video_id', '=', $cvt->video_id]
                    ])
                    ->first();
                $gv->custom_title = $cvt->custom_title;
                $gv->save();
            }
        }

        Schema::dropIfExists('custom_video_titles');
    }
}
