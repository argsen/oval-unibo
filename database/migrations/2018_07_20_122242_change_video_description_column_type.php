<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use oval\Video;

class ChangeVideoDescriptionColumnType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->text('description')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('videos', function (Blueprint $table) {
            foreach(Video::all() as $v) {
                if(strlen($v->description) > 255) {
                    $v->description = substr($v->description, 0, 252)."...";
                    $v->save();
                }
            }
            $table->string('description')->change();
        });
    }
}
