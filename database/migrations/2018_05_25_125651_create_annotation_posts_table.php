<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnotationPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annotation_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_video_id')->length(10)->unsigned();
            $table->integer('annotation_id')->length(10)->unsigned();
            $table->integer('parent')->length(10)->unsigned()->nullable();
            $table->longText('content')->nullable();
            $table->longText('pings')->nullable();
            $table->integer('creator')->length(10)->unsigned()->nullable();
            $table->string('fullname', 100)->nullable();
            $table->string('profile_picture_url', 100)->nullable();
            $table->boolean('created_by_admin')->nullable();
            $table->boolean('created_by_current_user')->nullable();
            $table->integer('upvote_count')->length(10)->unsigned()->nullable();
            $table->boolean('user_has_upvoted')->nullable();
            $table->integer('reward_count')->length(10)->unsigned()->default(0);
            $table->integer('trophy_count')->length(10)->unsigned()->default(0);
            //$table->longText('covaaTags')->nullable();;
            $table->timestamps();
        });

        Schema::table('annotation_posts', function(Blueprint $table) {
            $table->foreign('group_video_id')->references('id')->on('group_videos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('annotation_posts', function(Blueprint $table) {
            $table->dropForeign(['group_video_id']);
        });
        Schema::dropIfExists('annotation_posts');
    }
}
