<?php 
 
use Illuminate\Support\Facades\Schema; 
use Illuminate\Database\Schema\Blueprint; 
use Illuminate\Database\Migrations\Migration; 
 
class CreateReceivedNotificationsTable extends Migration 
{ 
    /** 
     * Run the migrations. 
     * 
     * @return void 
     */ 
    public function up() 
    { 
        Schema::create('received_notifications', function (Blueprint $table) { 
            $table->increments('id'); 
            $table->integer('notification_id')->legth(10)->unsigned(); 
            $table->integer('recipient_id')->length(10)->unsigned(); 
            $table->boolean('completed')->default(false); 
            $table->timestamp('completed_at')->nullable(); 
        }); 
 
        Schema::table('received_notifications', function (Blueprint $table) { 
            $table->foreign('notification_id') 
                    ->references('id')->on('notifications') 
                    ->onDelete('cascade'); 
            $table->index('notification_id'); 
 
            $table->foreign('recipient_id') 
                    ->references('id')->on('users') 
                    ->onDelete('cascade'); 
            $table->index('recipient_id'); 
        }); 
    } 
 
    /** 
     * Reverse the migrations. 
     * 
     * @return void 
     */ 
    public function down() 
    { 
        Schema::table('received_notifications', function(Blueprint $table) { 
            $table->dropIndex(['notification_id']); 
            $table->dropForeign(['notification_id']); 
 
            $table->dropIndex(['recipient_id']); 
            $table->dropForeign(['recipient_id']); 
        }); 
        Schema::dropIfExists('received_notifications'); 
    } 
} 