<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupVideoSegmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_video_segments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('group_video_id')->length(10)->unsigned();
            $table->integer('start')->unsigned();
            $table->integer('end')->unsigned();
            $table->timestamps();
        });
        Schema::table('group_video_segments', function(Blueprint $table) {
            $table->foreign('group_video_id')
                    ->references('id')->on('group_videos')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_video_segments', function(Blueprint $table) {
            $table->dropForeign(['group_video_id']);
        });
        Schema::dropIfExists('group_video_segments');
    }
}
