<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdfTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adf_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 36)->unique();
            $table->string('email', 255)->unique();
            $table->string('name', 255);
            $table->string('role', 36);
            $table->string('level', 36)->nullable();
            $table->unsignedInteger('class_serial_no')->nullable();
            $table->unsignedInteger('user_id')->unique();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::create('adf_courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 36)->unique();
            $table->string('code', 36);
            $table->string('subject', 255);
            $table->string('level', 36)->nullable();
            $table->timestamps();
        });
        Schema::create('adf_assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 36)->unique();
            $table->string('title', 255);
            $table->string('type', 36);
            $table->dateTime('start')->nullable();
            $table->dateTime('end')->nullable();
            $table->timestamps();
        });
        Schema::create('adf_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 36)->unique();
            $table->string('title', 255);
            $table->string('subject', 255);
            $table->dateTime('start')->nullable();
            $table->dateTime('end')->nullable();
            $table->string('status', 36)->default('NEW');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adf_users');
        Schema::dropIfExists('adf_courses');
        Schema::dropIfExists('adf_assignments');
        Schema::dropIfExists('adf_tasks');
    }
}
