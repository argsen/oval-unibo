<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use oval\quiz_creation;
use oval\GroupVideo;
use oval\Video;

class RefactorQuizToHaveRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quiz_creation', function (Blueprint $table) {
            $table->dropIndex(['creator_id']);
            $table->integer('group_video_id')->nullable();
        });

        //--copy existing data
        $quizzes = quiz_creation::all();
        $course_wide = config('settings.course_wide.quiz');
        foreach ($quizzes as $q) {
            $video = Video::where('identifier', '=', $q->identifier)->first();
            if(!$video) {
                $q->delete();
                continue;
            }
            $group_videos = collect();
            if($course_wide==1) {
                //--copy only to "default group"s
                $groups = $video->groups->where('moodle_group_id', null);
                foreach($groups as $g) {
                    $gv = GroupVideo::where([
                                            ['group_id', '=', $g->id],
                                            ['video_id', '=', $video->id]
                                        ])
                                        ->first();
                    $group_videos->push($gv);
                }
            }
            else{
                //--copy to all group_videos
                $group_videos = GroupVideo::where('video_id', '=', $video->id)->get();
            }
            foreach ($group_videos as $gv) {
                $new_q = new quiz_creation;
                $new_q->creator_id = $q->creator_id;
                $new_q->quiz_data = $q->quiz_data;
                $new_q->visable = $q->visable;
                $new_q->group_video_id = $gv->id;
                $new_q->identifier = $video->identifier;
                $new_q->save();
            }
            $q->delete();
        }

        //--drop columns & add foreign key connection
        Schema::table('quiz_creation', function (Blueprint $table) {
            $table->integer('group_video_id')->length(10)->unsigned()->change();
            $table->dropColumn('identifier');
            $table->dropColumn('media_type');
            $table->foreign('creator_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->foreign('group_video_id')
                    ->references('id')->on('group_videos')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //--add back columns and drop foreign keys added
        Schema::table('quiz_creation', function (Blueprint $table) {
            $table->string('identifier')->nullable();
            $table->string('media_type')->nullable();
            $table->dropForeign(['group_video_id']);
            
            $table->dropForeign(['creator_id']);
        });

        //--revert data
        //----this will use the first group's quiz to be the video's quiz
        $quizzes = oval\quiz_creation::all();
        $video = null;
        foreach($quizzes as $q) {
            if($video != $q->group_video->video()) {
                $video = $q->group_video->video();
                $new_q = new oval\quiz_creation;
                $new_q->creator_id = $q->creator_id;
                $new_q->quiz_data = $q->quiz_data;
                $new_q->identifier = $video->identifier;
                $new_q->media_type = $video->media_type;
                $new_q->visable = $q->visable;
                $new_q->group_video_id = $q->group_video_id;
                $new_q->save();
            }
            $q->delete();
        }
        

        //--drop column added and add back index
        Schema::table('quiz_creation', function (Blueprint $table) {
            $table->string('identifier')->change();
            $table->string('media_type')->change();
            $table->dropColumn('group_video_id');
            $table->index('creator_id');
        });
    }
}
