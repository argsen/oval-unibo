<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShowRecommendedResourcesToGroupVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_videos', function (Blueprint $table) {
            $table->boolean('show_recommended_resources')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_videos', function (Blueprint $table) {
            $table->dropColumn('show_recommended_resources');
        });
    }
}
