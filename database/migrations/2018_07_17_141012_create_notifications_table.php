<?php 
 
use Illuminate\Support\Facades\Schema; 
use Illuminate\Database\Schema\Blueprint; 
use Illuminate\Database\Migrations\Migration; 
 
class CreateNotificationsTable extends Migration 
{ 
    /** 
     * Run the migrations. 
     * 
     * @return void 
     */ 
    public function up() 
    { 
        Schema::create('notifications', function (Blueprint $table) { 
            $table->increments('id'); 
            $table->string('message'); 
            $table->integer('author_id')->length(10)->unsigned(); 
            $table->timestamp('issue_at')->useCurrent(); 
            $table->timestamp('deadline')->nullable(); 
            $table->integer('recipient_group_id'); 
            $table->string('type', 10)->default('message'); 
            $table->timestamps(); 
        }); 
 
        Schema::table('notifications', function (Blueprint $table) { 
            $table->foreign('author_id') 
                    ->references('id')->on('users') 
                    ->onDelete('cascade'); 
            $table->index('author_id'); 
            $table->foreign('recipient_group_id') 
                    ->references('id')->on('groups') 
                    ->onDelete('cascade'); 
            $table->index('recipient_group_id'); 
        }); 
    } 
 
    /** 
     * Reverse the migrations. 
     * 
     * @return void 
     */ 
    public function down() 
    { 
        Schema::table('notifications', function(Blueprint $table) { 
            $table->dropIndex(['author_id']); 
            $table->dropForeign(['author_id']); 
            $table->dropIndex(['recipient_group_id']); 
            $table->dropForeign(['recipient_group_id']); 
        }); 
        Schema::dropIfExists('notifications'); 
    } 
}