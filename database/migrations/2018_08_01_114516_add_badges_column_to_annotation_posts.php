<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBadgesColumnToAnnotationPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('annotation_posts', function (Blueprint $table) {
            $table->json('covaa_badge_array')->nullable();
        });

        Schema::table('annotation_posts', function(Blueprint $table) {
            $table->dropColumn('reward_count');
            $table->dropColumn('trophy_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('annotation_posts', function(Blueprint $table) {
            $table->dropColumn('covaa_badge_array');
        });

        Schema::table('annotation_posts', function (Blueprint $table) {
            $table->integer('reward_count')->length(10)->unsigned()->default(0);
            $table->integer('trophy_count')->length(10)->unsigned()->default(0);
        });
    }
}
