4<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use oval\quiz_creation;
use oval\quiz_result;
use oval\Video;
use oval\GroupVideo;

class RefactorQuizResultToRelateToGroupVideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quiz_result', function (Blueprint $table) {
            $table->dropIndex(['user_id']);
            $table->integer('group_video_id')->nullable();
        });
        //--reformat the existing data
        $results = quiz_result::all();
        foreach ($results as $result) {
            $video = Video::where('identifier', '=', $result->identifier)->first();
            //-- if video this quiz was attached to is deleted, delete the quiz
            if(!$video) {
                $result->delete();
                continue;
            }
            $group_videos = collect();

            //----if course_wide, attach the result to GroupVideos of default groups
            if (config('settings.course_wide.quiz')==1) {
                $groups = $video->groups->where('moodle_group_id', null);
                foreach($groups as $g) {
                    $gv = GroupVideo::where([
                            ['group_id', '=', $g->id],
                            ['video_id', '=', $video->id]
                        ])
                        ->first();
                    $group_videos->push($gv);
                }
            }
            //----if not, attach it to all GroupVideos whose Video is the identifier
            else{
                $group_videos = GroupVideo::where('video_id', $video->id)->get();
            }

            //--create new records for all GroupVideos that has quiz result
            foreach($group_videos as $gv) {
                $new_r = new quiz_result;
                $new_r->user_id = $result->user_id;
                $new_r->group_video_id = $gv->id;
                $new_r->quiz_data = $result->quiz_data;
                $new_r->identifier = $video->identifier;
                $new_r->save();
            }
            //--delete the old record
            $result->delete();
        }

        Schema::table('quiz_result', function(Blueprint $table){
            $table->dropColumn('identifier');
            $table->dropColumn('media_type');

            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');

            $table->integer('group_video_id')->length(10)->unsigned()->change();
            $table->foreign('group_video_id')
                    ->references('id')->on('group_videos')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quiz_result', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['group_video_id']);
            $table->string('identifier')->nullable();
            $table->string('media_type')->nullable();
        });
        //--revert data
        //------If multiple entries were created on migration up(),
        //------they will stay and will be assigned to the video
        $results = quiz_result::all()->groupBy('user_id');
        foreach ($results->keys() as $uid) {
            foreach ($results[$uid] as $res) {
                $group_video = GroupVideo::find($res->group_video_id);
                $v = $group_video->video();
                $res->identifier = $v->identifier;
                $res->media_type = $v->media_type;
                $res->save();
            }
        }
        Schema::table('quiz_result', function (Blueprint $table) {
            $table->index('user_id');
            $table->string('identifier')->change();
            $table->string('media_type')->change();
            $table->dropColumn('group_video_id');
        });
    }
}
