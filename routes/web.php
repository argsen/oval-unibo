<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// ----------- Auth routes ------------- //
// Auth::routes(); //--to disable password reset route, use below instead..
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register');

Route::get('/reset_password', 'Auth\ResetPasswordController@showResetPasswordForm');
Route::post('/reset_password', 'Auth\ResetPasswordController@resetPassword');

// ----------- adf routes ------------- //
if (env('ADF_ENABLE', false)) {
  Route::get('/adf', 'AdfController@verify');
}

// ----------- lti routes ------------- //
Route::post('/lti', 'LtiController@launch');
Route::get('/lti', function() {
	return redirect()->secure('/view');
});

// ----------- Page routes ------------- //
Route::get('/', function() {
	return redirect()->secure('/view');
});
Route::get('/home', function() {
	return redirect()->secure('/view');
});
Route::get('/course/{course_id}', 'HomeController@course');
Route::get('/group/{group_id}', 'HomeController@group');
Route::get('/year-and-term/{year}/{term}', 'HomeController@year_and_term');
Route::get('/view/{group_video_id?}', 'HomeController@view');
Route::get('/video-management/{course_id?}/{group_id?}', 'HomeController@video_management');
Route::get('/analytics/{course_id?}/{group_id?}/{page?}', 'HomeController@analytics');
Route::get('/content-analysis/{course_id?}/{group_id?}', 'HomeController@content_analysis');
Route::get('/points-details/{group_video_id}', 'HomeController@points_details');
Route::get('/tracking-details/{group_video_id}', 'HomeController@tracking_details');
Route::get('/text-analysis-details/{video_id}', 'HomeController@text_analysis_details');
Route::get('/select-video/{link_id}/{course_id}/{group_video_id?}', 'HomeController@select_video');
Route::get('/manage-analysis-requests', 'HomeController@manage_analysis_requests');
Route::get('/batch-upload', 'HomeController@batch_upload');
Route::get('/manage-lti-connections', 'HomeController@manage_lti_connections');
Route::get('/manage-notifications', 'HomeController@manage_notifications');
Route::get('/manage-groups', 'HomeController@manage_groups');

Route::get('/surveys', 'HomeController@survey');
Route::get('/studentDash', 'StudentdashController@index');
Route::get('/manage-vimeo-tokens', 'HomeController@manage_vimeo_tokens');

/*------ manual sync api -------*/
Route::get('/manualSync', 'AjaxController@manual_sync');
Route::get('/tag-management/{course_id?}/{group_id?}', 'HomeController@tag_management');



// ----------- ajax routes ------------- //
Route::group(['middleware'=>'auth:api'], function() {
	Route::post('/get_annotations', 'AjaxController@get_annotations');
	Route::post('/get_comments', 'AjaxController@get_comments');
	Route::post('/add_comment', 'AjaxController@add_comment');
	Route::post('/add_annotation', 'AjaxController@add_annotation');
	Route::post('/edit_annotation', 'AjaxController@edit_annotation');
	Route::post('/edit_comment', 'AjaxController@edit_comment');
	Route::post('/delete_annotation', 'AjaxController@delete_annotation');
	Route::post('/delete_comment', 'AjaxController@delete_comment');
	Route::post('/helix_media_listing', 'AjaxController@helix_media_listing');
	Route::post('/add_video', 'AjaxController@add_video');
	Route::post('/delete_video', 'AjaxController@delete_video');
	Route::post('/get_groups', 'AjaxController@get_groups');
	Route::post('/save_video_group', 'AjaxController@assign_video_to_groups');
	Route::post('/download_annotations', 'AjaxController@download_annotations');
	Route::post('/save_feedback', 'AjaxController@save_feedback');
	Route::post('/get_group_info_for_video', 'AjaxController@get_group_info_for_video');
	Route::post('/save_confidence_level', 'AjaxController@save_confidence_level');
	Route::post('/get_videos_for_course', 'AjaxController@get_videos_for_course');
	Route::post('/get_groups_for_video', 'AjaxController@get_groups_for_video');
	Route::post('/check_if_course_wide_points', 'AjaxController@check_if_course_wide_points');
	Route::post('/save_points', 'AjaxController@save_points');
	Route::post('/get_points_for_group_video', 'AjaxController@get_points_for_group_video');
	Route::post('/delete_points', 'AjaxController@delete_points');
	Route::post('/add_trackings', 'AjaxController@add_trackings');
	Route::post('/add_analysis_request', 'AjaxController@add_analysis_request');
	Route::post('/get_nominated_students_ids', 'AjaxController@get_nominated_students_ids');
	Route::post('/edit_comment_instruction', 'AjaxController@edit_comment_instruction');
	Route::post('/delete_comment_instruction', 'AjaxController@delete_comment_instruction');
	Route::post('/get_comments_for_tag', 'AjaxController@get_comments_for_tag');
	Route::post('/get_annotations_for_tag', 'AjaxController@get_annotations_for_tag');
	Route::post('/edit_visibility', 'AjaxController@edit_visibility');
	Route::post('/edit_video_order', 'AjaxController@edit_video_order');
	Route::post('/edit_text_analysis_visibility', 'AjaxController@edit_text_analysis_visibility');
	Route::post('/set_lti_resource_link', 'AjaxController@set_lti_resource_link');
	Route::post('/check_student_activity', 'AjaxController@check_student_activity');
	Route::post('/archive_group_video', 'AjaxController@archive_group_video');
	Route::post('/delete_group_video', 'AjaxController@delete_group_video');
	Route::post('/get_groups_with_video', 'AjaxController@get_groups_with_video');
	Route::post('/get_video_info', 'AjaxController@get_video_info');
	Route::post('/delete_lti_connection', 'AjaxController@delete_lti_connection');
	Route::post('/get_lti_connection_detail', 'AjaxController@get_lti_connection_detail');
	Route::post('/edit_lti_connection', 'AjaxController@edit_lti_connection');
	Route::post('/get_keywords_for_group_video', 'AjaxController@get_keywords_for_group_video');
	Route::post('/update_keywords_visibility', 'AjaxController@update_keywords_visibility');
	Route::post('/edit_recommended_resources_visibility', 'AjaxController@edit_recommended_resources_visibility');
	Route::post('/search_related_videos', 'AjaxController@search_related_videos');
	Route::post('/edit_recommended_video_like', 'AjaxController@edit_recommended_video_like');
	Route::get('/get_keyword_autocomplete_list', 'AjaxController@get_keyword_autocomplete_list');//autocomplete suggestion list
	Route::post('/toggle_recommended_resources_rating_ability', 'AjaxController@toggle_recommended_resources_rating_ability');
	Route::post('/update_gv_custom_title', 'AjaxController@update_gv_custom_title'); 
	Route::post('/add_group', 'AjaxController@add_group');
	Route::post('/get_students_in_course', 'AjaxController@get_students_in_course');
	Route::post('/get_groups_info_for_course', 'AjaxController@get_groups_info_for_course');
	Route::post('/edit_group_name', 'AjaxController@edit_group_name');
	Route::post('/edit_group_members', 'AjaxController@edit_group_members');
	Route::post('/delete_group', 'AjaxController@delete_group');
	Route::post('/get_segments_for_group_video', 'AjaxController@get_segments_for_group_video');
	Route::post('/save_segments', 'AjaxController@save_segments');

	
	/*------ quiz API ------*/
	Route::get('/get_quiz', 'AjaxController@get_quiz');
	Route::post('/store_quiz', 'AjaxController@store_quiz');
	Route::post('/submit_quiz_result', 'AjaxController@submit_quiz_result');
	Route::get('/get_video_identifier','AjaxController@get_video_identifier_by_group_video_id');

	/*------ analysis api ------*/
	Route::get('/get_student_view','AjaxController@get_student_view');
	Route::get('/get_annotations_column','AjaxController@get_annotations_column');
	Route::get('/get_comment_column','AjaxController@get_comment_column');
	Route::get('/get_quiz_question','AjaxController@get_quiz_question');
	Route::get('/get_key_point', 'AjaxController@get_key_point');
	Route::get('/change_quiz_visable', 'AjaxController@change_quiz_visable');
	Route::get('/get_quiz_visable_status','AjaxController@get_quiz_visable_status');
	Route::get('/get_all_student_record','AjaxController@get_all_student_record');
	Route::get('/get_all_video_tracking_data','AjaxController@get_all_video_tracking_data');

	/*------ notification ------*/
	Route::post('/add_notification', 'AjaxController@add_notification'); 
	Route::post('/edit_notification', 'AjaxController@edit_notification'); 
	Route::post('/save_notification_status', 'AjaxController@save_notification_status'); 
	Route::post('/refresh_notifications', 'AjaxController@refresh_notifications'); 

	/*------ annotation post api ------*/
	Route::get('/api/comments/count', 'AjaxController@count_annotation_comment');
	Route::get('/api/comments/get', 'AjaxController@get_annotation_comment');
	Route::get('/api/comments/getTop', 'AjaxController@get_top_annotation_comment');
	Route::post('/api/comments/create', 'AjaxController@post_annotation_comment');
	Route::post('/api/comments/edit', 'AjaxController@edit_annotation_comment');
	Route::post('/api/comments/delete', 'AjaxController@delete_annotation_comment');

	Route::post('/api/annonation/updateVote', 'AjaxController@update_annotation_vote');
	Route::post('/api/annonation/updateBadge', 'AjaxController@update_annotation_badge');
	// Route::post('/api/annonation/updateReward', 'AjaxController@update_annotation_reward');
	// Route::post('/api/annonation/updateTrophy', 'AjaxController@update_annotation_trophy');

	/*------ tag group api ------*/
	Route::post('/api/annonation/updateOrCreate', 'AjaxController@create_tag_group');
	Route::get('/api/annonation/get', 'AjaxController@get_tag_group');
	Route::get('/api/annonation/getbygroupvideoid', 'AjaxController@get_tag_group_by_groupvideoid');
	Route::get('/api/annonation/getlatestbygroupvideoid', 'AjaxController@get_latest_tag_group_by_groupvideoid');
	Route::post('/api/annonation/savebygroupvideoid', 'AjaxController@save_tag_group_by_groupvideoid');
	Route::post('/api/annonation/deletebygroupvideoid', 'AjaxController@delete_tag_group_by_groupvideoid');

	/*------ thread api ------*/
	Route::get('/api/thread/get', 'AjaxController@thread_details');

});

// -----------php form processing -----------
Route::post('/upload_transcript', 'FileController@upload_transcript');
Route::post('/request_text_analysis', 'ProcessController@request_text_analysis');
Route::post('/reject_text_analysis_request', 'ProcessController@reject_text_analysis_request');
Route::post('/recover_text_analysis_request', 'ProcessController@recover_text_analysis_request');
Route::post('/delete_text_analysis_request', 'ProcessController@delete_text_analysis_request');
Route::post('/send_all_text_analysis_requests', 'ProcessController@send_all_text_analysis_requests');
Route::post('/reject_all_text_analysis_requests', 'ProcessController@reject_all_text_analysis_requests');
Route::post('/recover_all_rejected_text_analysis_requests', 'ProcessController@recover_all_rejected_text_analysis_requests');
Route::post('/delete_all_rejected_text_analysis_requests', 'ProcessController@delete_all_rejected_text_analysis_requests');
Route::post('/batch_data_insert', 'ProcessController@batch_data_insert');
Route::post('/add_lti_connection', 'ProcessController@add_lti_connection');


// ----------- youtube data api ------------- //
Route::post('/add_google_cred', 'GoogleAPIController@add_google_cred');
Route::get('/youtube_auth_redirect', 'GoogleAPIController@youtube_auth_redirect');
Route::post('/check_youtube_caption', 'GoogleAPIController@check_youtube_caption');

// ----------- vimeo api ------------- //
Route::get('/vimeo_auth_redirect', 'VimeoAPIController@vimeo_auth_redirect');
Route::post('/add_vimeo_token', 'VimeoAPIController@add_vimeo_token');
Route::post('/delete_vimeo_cred', 'VimeoAPIController@delete_vimeo_cred');

Route::post('sendCompletedData','SurveyController@sendCompletedData');


