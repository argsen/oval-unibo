@extends('layouts.app')

@section('title', '')

@section('additional-css')
<link rel="stylesheet" type="text/css" href="{{ URL::secureAsset('css/jquery-comments-modified.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::secureAsset('components/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
@if (count($group_video->keywords)>0)
<link rel="stylesheet" type="text/css" href="{{ URL::secureAsset('components/jquery-autocomplete/autocomplete.css') }}">
@endif
<link rel="stylesheet" type="text/css" href="{{URL::secureAsset('components/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
@endsection

@section ('selection_bar')
	@include('parts.selectionbar')
@endsection

@section('content')

	<div class="container-fuild main-content">
		@if (($user->isInstructorOf($course)) && ($group_video->hide == 1))
		<div class="msg">
			THIS VIDEO IS SET TO "HIDDEN" AND IS NOT VISIBLE TO STUDENTS.<br />
			Set it to "visible" in <a href="/video-management">Video Management page</a> to make it available for student use.
		</div>
		@endif
		@if ($has_quiz)
		<div class="alert alert-warning alert-dismissible fade in space-top" role="alert" style="width:100%;margin:0.5em;">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<i class="fas fa-exclamation-circle"></i>&nbsp;
			If the video pauses while viewing in full screen, it is due to a quiz question appearing that you are asked to answer. 
			When this happens, please click the <strong>Esc</strong> key to come out of full screen mode to see the quiz question.
		</div>
		@endif

		<div class="row">
			<div id="left-side" class="col-md-7">
				@if ($group_video->segments->count() > 0)
				<div id="segment-control" class="video-width pb-2 form-group row">
					<label for="video-segment-select" class="col-sm-3 col-md-4 col-lg-3 col-form-label px-0">Select a segment</label>
					<select id="video-segment-select" class="selectpicker form-control col-sm-9 col-md-8 col-lg-9" title="Select" data-style="dark-outline-form-control">
						@foreach ($group_video->segments->sortBy('start') as $seg)
						@if (empty($seg->title))
						<option value="{{$seg->id}}">{{$seg->formattedStartTime()}} to {{$seg->formattedEndTime()}}</option>
						@else
						<option value="{{$seg->id}}">{{$seg->title}}</option>
						@endif
						@endforeach
					</select>
				</div>
				@endif
				
				<div id="video" class="video-width">
					<div id="player"></div>
				</div><!-- .video -->
				
				<div class="video-width">

					<div class="tab-content">

						<div class="annotations-buttons">
							<div class="btn btn-link" style="float:left;margin:0.5em;">
								<ul class="nav nav-tabs content-tabs" role="tablist">
									@if (($group_video->show_analysis == true) && (count($group_video->keywords)>0))
									<li role="presentation" class="active col-xs-6"><a href="#annotations" aria-controls="annotations" role="tab" data-toggle="tab">ANNOTATIONS</a></li>
									<li role="presentation" class="col-xs-6"><a href="#content-analysis" aria-controls="current topics" role="tab" data-toggle="tab">CURRENT TOPICS</a></li>
									@endif
								</ul>
							</div>

							<button type="button" class="btn btn-link download-comments" title="Download annotations and comments">
								<!-- Download -->
								Details
								<i class="fas fa-download"></i>
							</button>
							<button type="button" class="btn btn-link add-annotation" title="Add an annotation">
								New Annotation Thread
								<i class="fas fa-plus-circle"></i>
							</button>
						</div><!-- .annotations-buttons -->
						
						<div role="tabpanel" class="tab-pane active" id="annotations">


							<div class="">
								<canvas id="trends"></canvas>
								<div id="annotations-list">
									<div id="instructor-annotations">
										<div class="section"></div>
									</div>
									<div id="student-annotations">
										<div class="section"></div>
									</div>
								</div>
							</div><!-- .horizontal-scroll -->

							<div id="annotation-filter" class="row" data-toggle="buttons">
								<div class="col-xs-6 col-sm-3">
									<label class="btn active" title="Show all Annotations">
										<input type="radio" name="filter" value="4" checked><i class="far fa-eye green"></i></i>All
									</label>
								</div>
								<div class="col-xs-6 col-sm-3">
									<label class="btn" title="Show only Annotations by me">
										<input type="radio" name="filter" value="1"><i class="far fa-dot-circle"></i>Mine
									</label>
								</div>
								<div class="col-xs-6 col-sm-3">
									<label class="btn" title="Show only Annotations by Student">
										<input type="radio" name="filter" value="3"><i class="far fa-user-circle pink"></i></i>Students
									</label>
								</div>
								<div class="col-xs-6 col-sm-3">
									<label class="btn" title="Show only Annotations by Instructors">
										<input type="radio" name="filter" value="2"><i class="fas fa-info-circle blue"></i>Instructors
									</label>
								</div>
							</div><!-- #annotation-filter -->
						</div><!-- #annotations -->
						
						@if (($group_video->show_analysis == true) && (count($group_video->keywords)>0))
						<div role="tabpanel" class="tab-pane" id="content-analysis">
							<h4>Current topic</h4>
							<div class="panel panel-default">
								<div class="panel-body" id="current-keywords">
									&nbsp;
								</div><!-- panel-body -->
							</div><!-- panel -->
							<div class="content-analysis-body">
								<h4>List of topics covered in this video</h4>
								<div class="keyword-ul-box vertical-scroll" id="keyword-list">
									<ul id="keyword-ul">	
									</ul>
									<div class="no-keyword-msg"></div>
								</div><!-- keyword-ul-box -->
							</div><!-- content-analysis-body -->
						</div><!-- #content-analysis -->
						@endif
					</div><!-- tab-content -->
				</div>


			</div><!-- left column .col-md-8 -->

			<div id="right-side" class="col-md-5">
				<ul class="nav nav-tabs nav-fill" id="right-side-tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="comments-tab" data-toggle="tab" href="#plain-comments" role="tab" aria-controls="plain-comments" aria-selected="true">Comments</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="discussion-tab" data-toggle="tab" href="#discussion" role="tab" aria-controls="discussion" aria-selected="false">Annotation Discussion</a>
					</li>
				</ul>

				<div class="tab-content" id="right-side-tab-content">
					<div id="plain-comments" class="tab-pane fade show active" role="tabpanel" aria-labelledby="">
						<div class="header">
							<button type="button" class="btn btn-link add-comment float-right" title="Add a comment">
								New Comment	
								<i class="fa fa-plus-circle"></i>
							</button><!-- .add-comment -->
						</div><!-- .header -->
						<div class="comments-box vertical-scroll">
						</div><!-- .comments-box -->
					</div><!-- plain-comments -->
					
					<div id="discussion" class="tab-pane fade" role="tabpanel" aria-labelledby="">
						<!-- insert jquery Comment plugin -->
						<div id="comments-header" class="comments-header container-fuild"></div>
						<div id="comments-container" class="comments-container"></div>
						<!-- add pagination to control comment length -->
						<div class="comments-pagination-container">
							<ul id="covaa-pagination" class="pagination-sm"></ul>
						</div>	
					</div><!-- #discussion -->
				</div><!-- #right-side-tab-content -->

			</div><!-- #right-side -->
		</div>
	</div>

@endsection

@section('javascript')

	<!-- load plugin -->
	<script type="text/javascript" src="{{ URL::secureAsset('components/jquery-textcomplete/dist/jquery.textcomplete.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::secureAsset('components/multiselect/js/jquery.multi-select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::secureAsset('components/bootstrap-validator/dist/validator.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::secureAsset('components/twbs-pagination/jquery.twbsPagination.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::secureAsset('components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
	@if (count($group_video->keywords)>0)
	<script type="text/javascript" src="{{ URL::secureAsset('components/jquery-autocomplete/jquery.autocomplete.js') }}"></script>
	@endif
	<script type="text/javascript" src="{{URL::secureAsset('components/popper.js/dist/popper.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::secureAsset('components/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>

	<!-- javascript customise -->
	<script type="text/javascript" src="{{ URL::secureAsset('js/plugin/jquery-comments-modified.js') }}"></script>
	@if($video->media_type == 'vimeo')
	<script src="https://player.vimeo.com/api/player.js"></script>
	@endif
	<script type="text/javascript" src="{{URL::secureAsset('js/media/'.config('settings.video_source')[$video->media_type]['js'])}}"></script>
	<script type="text/javascript" src="{{ URL::secureAsset('js/home.js') }}"></script>

@endsection

@section('modal')
	@include('parts.home-modal')
@endsection
