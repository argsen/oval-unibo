@extends('layouts.app') 
 
@section('title', 'OVAL Admin Page') 

@section('additional-css') 
<link rel="stylesheet" type="text/css" href="{{ URL::secureAsset('components/trumbowyg/dist/ui/trumbowyg.min.css') }}"> 
@endsection
 
@section('content') 
<div class="container-fluid"> 
    <div class="page-title"> 
        <i class="fa fa-laptop"></i> 
        MANAGE NOTIFICATIONS 
    </div> 
 
    <div class="admin-page-section-header"> 
        <h2>CREATE NEW NOTIFICATION</h2> 
    </div><!-- admin-page-section-header --> 
 
    <div class="admin-page-section"> 
        <form id="new-notification-form"action="" method="POST" role="form" data-toggle="validator"> 
            <fieldset class="form-group"> 
                <legend>
                    Notification Type
                    <i class="fa fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="'Message' type is just message. 'Task' type can have deadline and allows students to mark the task as 'done'."></i> 
                </legend> 
                <div class="form-check form-check-inline"> 
                    <label class="form-check-label"><input class="form-check-input" type="radio" name="type-radio" value="message" checked>Message</label> 
                </div><!-- .radio-inline --> 
                <div class="form-check form-check-inline"> 
                    <label class="form-check-label"><input class="form-check-input" type="radio" name="type-radio" value="task">Task</label> 
                </div><!-- .radio-inline --> 
            </fieldset> 
            <fieldset class="form-group"> 
                <legend>
                    Notification Content
                    <i class="fa fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Use text editor to add text decoration of bold/italic/crossed or link to url."></i> 
                </legend> 
                <textarea class="form-control gray-textbox" id="new-notification-content" required></textarea>
                <div class="help-block with-errors"></div> 
            </fieldset> 
            <fieldset class="form-group"> 
                <legend>
                    Recipients
                    <i class="fa fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Select which group to send notification to."></i> 
                </legend> 
                <label for="new-notification-course">Course</label> 
                    <select id="new-notification-course"> 
                        @foreach ($user->coursesTeaching() as $c) 
                        <option value="{{$c->id}}">{{$c->name}}</option> 
                        @endforeach 
                    </select> 
                    &nbsp;&nbsp;&nbsp; 
                    <label for="new-notification-group" class="">Group</label> 
                    <select id="new-notification-group"> 
                    </select> 
            </fieldset> 
            <fieldset class="form-group"> 
                <legend> 
                    Issue Date 
                    <i class="fa fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="This is the date notification will be sent out on. Leave blank if you would like it to send it now."></i> 
                </legend> 
                <input type="date" class="form-control gray-textbox" id="new-notification-issue"> 
            </fieldset> 
            <fieldset class="form-group" id="deadline-fieldset"> 
                <legend>
                    Deadline
                    <i class="fa fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Date this task should be completed by."></i> 
                </legend> 
                <span class="space-right">Does this item have deadline?</span> 
                <div class="form-check form-check-inline"> 
                    <label class="form-check-label"><input class="form-check-input" type="radio" name="deadline-radio" value="true" checked>Yes</label> 
                </div><!-- .radio-inline --> 
                <div class="form-check form-check-inline">  
                    <label class="form-check-label"><input class="form-check-input" type="radio" name="deadline-radio" value="false">No</label> 
                </div><!-- .radio-inline --> 
                <input type="date" class="form-control gray-textbox" id="new-notification-deadline"> 
            </fieldset> 
             
 
            <div class=""> 
                <button type="button" id="save-new-notification-button" class="rectangle-button" title="Save">SAVE<i class="fa fa-save"></i></button> 
                <div id="loading-hud"> 
                    <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i> 
                    <span class="sr-only">Loading...</span> 
                </div> 
            </div> 
        </form> 
 
    </div><!-- admin-page-section --> 
 
 
    <div class="admin-page-section-header"> 
        <h2>SENT NOTIFICATIONS</h2> 
    </div><!-- admin-page-section-header --> 
 
    <div class="admin-page-section"> 
        <div class="table-responsive"> 
            <table class="table table-striped"> 
                <thead> 
                    <tr> 
                        <th>Date</th> 
                        <th>Type</th> 
                        <th>Group Sent To</th> 
                        <th>Message</th> 
                        <th>Deadline</th> 
                        <th>Action</th> 
                    </tr> 
                </thead> 
                <tbody> 
                    @foreach ($user->authored_notification->sortByDesc('issue_at') as $n) 
                    <tr> 
                        <td class="issue-td" data-val="{{date('Y-m-d', strtotime($n->issue_at))}}">{{date("d-M-y", strtotime($n->issue_at))}}</td> 
                        <td class="type-td">{{$n->type}}</td> 
                        <td class="grp-td">{{$n->recipient_group->name}} of {{$n->recipient_group->course->name}}</td> 
                        <td class="msg-td">{{htmlspecialchars_decode($n->message)}}</td> 
                        <td class="deadline-td" data-val="{{empty($n->deadline) ? '' : date('Y-m-d', strtotime($n->deadline))}}">@if(!empty($n->deadline)) {{date("d-M-y", strtotime($n->deadline))}} @endif</td> 
                        <td> 
                            <button type='button' class="btn btn-link" data-nid="{{$n->id}}"  data-toggle="modal" data-target="#edit-notification-modal" title="Edit notification"> 
                                Edit 
                                <i class="fa fa-edit"></i> 
                            </button> 
                        </td> 
                    </tr> 
                    @endforeach 
                </tbody> 
            </table> 
        </div><!-- table-responsive --> 
    </div><!-- admin-page-section --> 
 
</div><!-- container-fluid --> 
 
@endsection 
 
@section('modal') 
  @include('parts.manage-notification-modal') 
@endsection 
 
@section('javascript') 
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script> 
    <script type="text/javascript" src="{{ URL::secureAsset('components/trumbowyg/dist/trumbowyg.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::secureAsset('js/manage-notifications.js') }}"></script> 
@endsection 