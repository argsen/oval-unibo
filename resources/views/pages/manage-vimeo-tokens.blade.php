@extends('layouts.app')

@section('title', 'Manage Vimeo Tokens')


@section('content')
<div class="container-fluid">
    <div class="page-title">
        <i class="fa fa-laptop"></i>
        MANAGE VIMEO TOKENS
    </div>
    @if (!empty(session('msg')))
    <div class="msg">
        {{ session('msg') }}
    </div>
    @endif

    <div class="admin-page-section-header">
        <h2>EXISTING TOKENS</h2>
    </div><!-- admin-page-section-header -->

    <div class="admin-page-section">
        @if (count($creds) == 0)
        There are no Vimeo tokens stored.
        @else
        <div class="table-responsive">
            <table class="table table-striped" id="vimeo-creds-table">
                <thead>
                    <tr>
                        <th>VIMEO USER URI</th>
                        <th>VIMEO USER NAME</th>
                        <th>TOKEN</th>
                        <th>SAVED BY</th>
                        <th>DELETE</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($creds as $c)
                    <tr>
                        <td>{{$c->vimeo_user_uri}}</td>
                        <td>{{$c->vimeo_user_name}}</td>
                        <td>{{$c->access_token}}</td>
                        <td>
                            @if (!empty($c->owner))
                            {{$c->owner->fullName()}}
                            @else
                            N/A
                            @endif
                        </td>
                        <td>
                            <button type="button" class="delete-vimeo-cred-button" data-id="{{$c->id}}">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- table-responsive -->
        @endif
    </div><!-- admin-page-section -->

    <div class="admin-page-section-header">
        <h2>ADD NEW TOKEN</h2>
    </div><!-- admin-page-section-header -->

    <div class="admin-page-section">
        <div class="space-left-right">
            <form id="add-lti-form" method="POST" action="/add_vimeo_token" role="form" data-toggle="validator">
                {{ csrf_field() }}

                <fieldset class="form-group">
                    <legend>Access Token</legend>
                    <input type="text" id="vimeo-token" class="form-control gray-textbox" name="token" required>
                </fieldset>

                <button type="submit" class="rectangle-button btn">
                    SAVE
                    <i class="far fa-save" aria-hidden="true"></i>
                </button>

            </form>
        </div><!-- space-left-right -->
    </div><!-- admin-page-section -->



        
@endsection




@section('javascript')


		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
		
		<script type="text/javascript" src="{{ URL::secureAsset('js/manage-vimeo-token.js') }}"></script>

@endsection