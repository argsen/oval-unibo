@extends('layouts.app')

@section('title', '{{ $title }}')


@section('content')
	<div class="container">
		<div class="page-title">
			@if (strtoupper($title)=="ERROR")
			<i class="fa fa-exclamation-triangle"></i>
			@elseif (strtoupper($title)=="NOT AUTHORIZED" || strtoupper($title)=="NOT AUTHORISED")
			<i class="fa fa-ban"></i>
			@else
			<i class="fa fa-info-circle"></i>
			@endif
			{{ $title }}
		</div>
			
		<div class="text-content">
			<p class="error-message">{!! html_entity_decode($message) !!}</p> 
			@if (!empty(Auth::user()) && (Auth::user()->isAStudent()) ) 
			<p class="error-message"><a href="/view">Go back to home page</a></p> 
			@endif
		</div><!-- .text-content" -->
	</div>
@endsection

@if (!empty(Auth::user()) && !Auth::user()->isAStudent() )
@section('javascript')
	<script type="text/javascript">
		$("#menu-button").click();
	</script>
@endsection 
@endif