@extends('layouts.app')

@section('title', '{{ $title }}')

@section('content')
  {{-- <div id="container" class="container-fluid">
    <div>
      <label id="subjectLabel">Course:</label><select id="subjectSelect">
        <option value="">Choose a Course</option>
      </select>
      <label id="classLabel">Group:</label><select id="classSelect">
        <option value="">Choose a Group</option>
      </select>
      <label id="videoLabel">Video:</label>
      <select id="videoSelect"></select>
    </div>
    <div id="content" class="container-fluid ">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div id="sidenav">
           <ul class="nav nav-pills" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="pill" href="#data">MY COVAA LEARNING PROFILE 3</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="pill" href="#21cc">MY 21CC PROFILE</a>
            </li>
          </ul>
          </div>
        </div>
      </div>

          <!-- Tab panes -->
          <div class="dataContainer">
            <div class="tab-content">
              <div id="data" class="container tab-pane active">
                  <div class="row">
                    <div class="center-block" style="width: 470;float: left;">
                      <div class="grid_tile grid_tile_bar1">
                        <div class="grid_chart" id="TS_chart"></div>
                        <div id="goals" style="padding-top: 5px;">
                          <label data-seq="discussion" data-title="Discussion Frames" class="reflection">Reflect & Set Goals</label>
                        </div>
                      </div>
                    </div>
                    <div class="center-block" style="width: 470;margin-left: 490;">
                      <div class="grid_tile grid_tile_sna">
                        <div class="grid_title">MY COVAA DISCUSSION NETWORK</div>
                        <div id="sna_container"></div>
                        <div>
                          <label id="sna_info" data-toggle="modal" data-target="#myModal">SNA Info</label>
                          <label id="btn_sna_generate">Reset View</label>
                          <label data-seq="network" data-title="Discussion Network" class="reflection">Reflect & Set Goals</label>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div id="21cc" class="container tab-pane fade">
                <div class="row">
                  <div class="center-block" style="width:520;float:left">
                    <div class="grid_tile grid_tile_bar">
                      <div class="grid_chart21cc" id="attitudes_chart"></div>
                      <div id="goals">
                        <label data-seq="learningSS" data-title="My Attitudes Towards Learning SS" class="reflection">Reflect & Set Goals</label>
                      </div>
                    </div>
                    <div class="grid_tile grid_tile_bar">
                      <div class="grid_chart21cc" id="feel_chart"></div>
                      <div id="goals">
                        <label data-seq="motivation" data-title="My Learning Motivation" class="reflection">Reflect & Set Goals</label>
                      </div>
                    </div>
                  </div>
                  <div class="center-block" style="width: 520;margin-left: 540;">
                    <div class="grid_tile grid_tile_bar">
                      <div class="grid_chart21cc" id="21cc_chart"></div>
                      <div id="goals">
                        <label data-seq="21CC" data-title="My 21st Century Skills Profile" class="reflection">Reflect & Set Goals</label>
                      </div>
                    </div>
                    <div class="grid_tile grid_tile_bar">
                      <div id="learning_mindset_container"  style="text-align: center;">
                        <text style="font-size:12px;font-weight:bold">
                          <label style="padding-bottom:5px;padding-top:7px">MY LEARNING STRATEGIES</label>
                        </text>
                      </div>
                      <div id="goals">
                        <label data-seq="strategies" data-title="My Learning Strategies" class="reflection">Reflect & Set Goals</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

    </div>
  </div>
</div> --}}



<div class="container">
  <div>
    <label id="subjectLabel">Course:</label><select id="subjectSelect">
      <option value="">Choose a Course</option>
    </select>
    <label id="classLabel">Group:</label><select id="classSelect">
      <option value="">Choose a Group</option>
    </select>
    <label id="videoLabel">Video:</label>
    <select id="videoSelect"></select>
  </div>
  <div class="container-fuild">
    <div class="col-md-12">
      <button class="btn btn-outline-info" id="manual-sync"><i class="fas fa-sync-alt"></i> Manually Synchronise </button>
    <div>
  </div>
  <br>
  <div id="content" class="container-fluid ">
    <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="row">
            <div id="sidenav">
    <ul class="nav nav-pills">
      <li><a data-toggle="pill" href="#home" class="active show">MY COVAA LEARNING PROFILE</a></li>
      <li><a data-toggle="pill" href="#menu1">MY 21CC PROFILE</a></li>
    </ul>
  </div>
</div>
</div>

<div class="dataContainer">
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div class="row">
        <div class="center-block" style="width: 470;float: left;">
          <div class="grid_tile grid_tile_bar1">
            <div class="grid_chart" id="TS_chart"></div>
            <div id="goals" style="padding-top: 5px;">
              <label data-seq="discussion" data-title="Discussion Frames" class="reflection">Reflect & Set Goals</label>
            </div>
          </div>
        </div>
        <div class="center-block" style="width: 470;margin-left: 490;">
          <div class="grid_tile grid_tile_sna">
            <div class="grid_title">MY COVAA DISCUSSION NETWORK</div>
            <div id="sna_container"></div>
            <div>
              <label id="sna_info" data-toggle="modal" data-target="#myModal">SNA Info</label>
              <label id="btn_sna_generate">Reset View</label>
              <label data-seq="network" data-title="Discussion Network" class="reflection">Reflect & Set Goals</label>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="menu1" class="tab-pane fade">
      <div class="row">
        <div class="center-block" style="width:520;float:left">
          <div class="grid_tile grid_tile_bar">
            <div class="grid_chart21cc" id="attitudes_chart"></div>
            <div id="goals">
              <label data-seq="learningSS" data-title="My Attitudes Towards Learning SS" class="reflection">Reflect & Set Goals</label>
            </div>
          </div>
          <div class="grid_tile grid_tile_bar">
            <div class="grid_chart21cc" id="feel_chart"></div>
            <div id="goals">
              <label data-seq="motivation" data-title="My Learning Motivation" class="reflection">Reflect & Set Goals</label>
            </div>
          </div>
        </div>
        <div class="center-block" style="width: 520;margin-left: 540;">
          <div class="grid_tile grid_tile_bar">
            <div class="grid_chart21cc" id="21cc_chart"></div>
            <div id="goals">
              <label data-seq="21CC" data-title="My 21st Century Skills Profile" class="reflection">Reflect & Set Goals</label>
            </div>
          </div>
          <div class="grid_tile grid_tile_bar">
            <div id="learning_mindset_container"  style="text-align: center;">
              <text style="font-size:12px;font-weight:bold">
                <label style="padding-bottom:5px;padding-top:7px">MY LEARNING STRATEGIES</label>
              </text>
            </div>
            <div id="goals">
              <label data-seq="strategies" data-title="My Learning Strategies" class="reflection">Reflect & Set Goals</label>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>



<!-- Modal -->

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content container-fluid">
      <div class="modal-header">
        <h4 id="modal-title" class="modal-title">Information on SNA</h4>
      </div>
      <div id="modal-body" class="modal-body row">
        <p>
          - The network map is based on you/your classmates’ replies to one another’s comments.<br>
          - Nodes of people who have replied one another are connected by lines.<br>
          - Arrows point in the direction of replies.<br>
          - Thicker lines mean more replies between nodes.<br>
          - The more you post, the bigger your node.<br>
          - The more you reply, the darker your node.<br>
          - Your node will show as a grey square until you make a post.<br>
          - Your node will be a skewed rectange if you have only posted replies.<br>
          - Click the Reset View button to reset your CoVAA Discussion Network Map.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- lightbox -->
<div id="lightbox-bg"></div>
<div id="lightbox-popup">
  <img id="lightbox-close" src="icons/close2.png" alt="Close" height="38" width="38">

  <h3 id="reflect_title">Reflect & Goal Set</h3>
  <p id="reflect_mini_title"></p>

  <div id="lightbox-content">
    <form action="reflection_handler.php" method="post">
      <input type="hidden" name="request" id="request" value="insertReflection">
      <input type="hidden" name="chart_id" id="chart_id">
      <input type="hidden" name="user_id" value="<?php echo 1;?>">
      <input type="hidden" name="video_id" id="video_id">

      <label class='nonSNA'>What are my strengths?</label>
      <textarea class='nonSNA' maxlength="140" id="good" name="good" placeholder="Write something.." style="height:40px"></textarea>

      <label id='label_helpOthers'>How can I help others?</label>
      <textarea maxlength="140" id="helpOthers" name="helpOthers" placeholder="Write something.." style="height:40px"></textarea>

      <label id='label_improve'>How can I improve?</label>
      <textarea maxlength="140" id="improve" name="improve" placeholder="Write something.." style="height:40px"></textarea>

      <label class='nonSNA'>How can others help me?</label>
      <textarea class='nonSNA' maxlength="140" id="helpMe" name="helpMe" placeholder="Write something.." style="height:40px"></textarea>

      <input id="reflection-submit" type="submit" value="Submit">
    </form>
  </div>
</div>
@endsection


@section('javascript')
<!--Load the AJAX API-->
   <script
   src="https://code.jquery.com/jquery-3.3.1.min.js"
   integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
   crossorigin="anonymous"></script>
   <!--Load JQueryUI for draggable function-->
   {{-- <script
   src="https://code.jquery.com/jquery-3.3.1.js"
   integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
   crossorigin="anonymous"></script> --}}

   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
   {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> --}}

   <!--Load Highcharts-->
   <script src="https://code.highcharts.com/highcharts.js"></script>
   <script src="https://code.highcharts.com/highcharts-more.js"></script>
   <script src="https://code.highcharts.com/modules/exporting.js"></script>

   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <script src="components/jqueryui-touch-punch/jquery.ui.touch-punch.min.js"></script>

   <!--Load scripts for any visualizations library, + oval related, + click stream related etc-->

   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <script type="text/javascript" src="js/cytoscape.js-2.4.8/cola.v3.min.js"></script>
   <script type="text/javascript" src="js/cytoscape.js-2.4.8/cytoscape.min.js"></script>
   <script type="text/javascript" src="js/cytoscape-cose-bilkent/cytoscape-cose-bilkent.js"></script>
   <script type="text/javascript" src="js/covaa_sna.js"></script>
   <script type="text/javascript">
        //var userID = <?php //echo $userID;?>;
        //var role = <?php //echo $_SESSION['role']?>;
    </script>
    <script>
        var cal = {!! json_encode($ownPreData) !!};
        var postcal = {!! json_encode($ownPostData) !!};
        var allsurvey = {!! json_encode($allPreData) !!};
        var allpostsurvey = {!! json_encode($allPostData) !!};
        var overall_data = {!! json_encode($overall_data) !!};
        console.log(allsurvey);
        console.log(overall_data);
        var is_admin = {!! json_encode($is_admin) !!};
        var userID = {!! json_encode($current_logged_in_user_id) !!}
    </script>
    <script type="text/javascript" src="js/student_dashboard.js"></script>
@endsection
