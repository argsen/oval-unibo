@extends('layouts.app')

@section('title', 'Group Management')

@section('additional-css')
<link rel="stylesheet" type="text/css" href="{{ URL::secureAsset('components/multiselect/css/multi-select.css') }}">
@endsection

@section('content')
	<div class="container-fluid">
    	<div class="page-title"><i class="fas fa-users-cog"></i>GROUP MANAGEMENT</div>

		<div class="msg">
		@if (!empty(session('msg')))
			{{session('msg')}}
		@endif
		</div>

        <div class="admin-page-section-header">
            <h2>ADD NEW GROUP</h2>
        </div><!-- .admin-page-section-header -->

        <div class="admin-page-section">
            <form id="new-group-form"  action="" method="POST" role="form" data-toggle="validator">
                <fieldset class="form-group">
                    <legend>Course</legend>
                    <select id="course-select" name="course_select">
                        @foreach ($courses as $c)
                        <option value="{{$c->id}}">{{$c->name}}</option>
                        @endforeach
                    </select>
                </fieldset>
                <fieldset class="form-group">
                    <legend>Group Name</legend>
                    <input id="group-name" class="form-control gray-textbox" required>
                    <div class="help-block with-errors"></div>
                </fieldset>
                <fieldset class="form-group">
                    <legend>Group Members</legend>
                    <select id="member-select" name="member_select" multiple="multiple">
                        @foreach ($course_members as $s)
                        <option value="{{$s->id}}">{{$s->fullName()}}</option>
                        @endforeach
                    </select>
                </fieldset>
                <button type="button" id="add-group-button" class="rectangle-button" title="Save">SAVE<i class="fas fa-plus"></i></button>
                <div id="loading-hud">
                    <i class="fas fa-spinner fa-pulse fa-2x fa-fw"></i>
                    <span class="sr-only">Loading...</span>
                </div>
            </form>
        </div><!-- admin-page-section -->
        
        <div class="admin-page-section-header">
            <h2>EXISTING GROUPS</h2>
        </div><!-- .admin-page-section-header -->

        <div class="search-bar filter-bar container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-2 admin-bar-text white-text">Filter by Course</div>
                <div class="col-xs-12 col-sm-10 admin-bar-text">
                    <select id="table-filter">
                        @foreach ($courses as $c)
                        <option value="{{$c->id}}">{{$c->name}}</option>
                        @endforeach
                    </select>
                </div><!-- col -->
            </div><!-- row -->
        </div><!-- search-bar -->

        <div class="admin-page-section">
            <div class="table-responsive">
				<table class="table table-striped" id="groups-table">
                    <thead>
                        <tr>
                            <th>Group Name</th>
                            <th>Members</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($courses->first()->groups as $g)
                        <tr>
                            <td>
                                <span>{{$g->name}}</span><br />
                                <button class="btn btn-link edit-name-button" data-toggle="modal" data-target="#edit-name-modal" data-gid="{{$g->id}}" title="Edit group name">Edit&nbsp;<i class="fas fa-pen-square"></i></button>   
                            </td>
                            <td>
                                <div>
                                    @foreach($g->members as $m)
                                    {{$m->fullName()}}<br />
                                    @endforeach
                                </div>
                                <button class="btn btn-link edit-members-button" data-toggle="modal" data-target="#edit-members-modal" data-gid="{{$g->id}}" title="Edit group members">Edit&nbsp;<i class="fas fa-pen-square"></i></button>   
                            </td>
                            <td>
                                <button class="btn btn-link delete-button" data-gid="{{ $g->id }}" title="Delete">
									<i class="far fa-trash-alt"></i>
								</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- table-responsive -->
        </div><!-- admin-page-section -->
    

    </div><!-- container-fluid -->
@endsection



@section('modal')
	@include('parts.manage-groups-modal')
@endsection

@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
    <script type="text/javascript" src="{{ URL::secureAsset('components/multiselect/js/jquery.multi-select.js') }}"></script>
    <script type="text/javascript" src="{{ URL::secureAsset('js/manage-groups.js') }}"></script>
@endsection