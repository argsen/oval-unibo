@extends('layouts.app')

@section('title', 'Analytics')


@section('content')
	<div class="container-fluid">
    	@if ($user->coursesTeaching()->count() == 0)
    	<p>There are no courses associated with your account</p>
    	@else
		<div class="page-title"><i class="fa fa-list-alt" aria-hidden="true"></i> VIDEO USAGE ANALYTICS</div>
			@if ($user->enrolledTerms()->count()>0)
			<div class="search-bar container-fluid">
			@else
			<div class="search-bar filter-bar container-fluid">
			@endif
				<div class="row">
					@if ($user->enrolledTerms()->count()>0)
					<div class="col-xs-12 space-top-10 white-text">Filter videos</div>
					@else
					<div class="col-xs-12 col-sm-2 admin-bar-text white-text">Filter videos</div>
					@endif

					@if ($user->enrolledTerms()->count()>0)
					<div class="btn-group col-md-4 col-xs-12">
						<a class="btn dropdown-button dropdown-left" href="#">Term</a>
						<a class="btn dropdown-button dropdown-center"  data-toggle="dropdown" href="#" id="term-name"></a>
						<a class="btn dropdown-button dropdown-toggle" data-toggle="dropdown" href="#">
						<!-- <span class="fa fa-caret-down"></span> -->
						<span class="sr-only">Toggle Dropdown</span>
						</a>
						<ul class="dropdown-menu">	
							@foreach ($user->enrolledTerms() as $t)
							<li><a href="/analytics?year={{$t->year}}&term={{$t->term}}">{{ $t->term_name }}</a></li>
							@endforeach
						</ul>
					</div><!-- btn-group -->
					@endif
					
					@if ($user->enrolledTerms()->count()>0)
					<div class="btn-group col-md-4 col-xs-12">
					@else
					<div class="btn-group col-sm-5 col-xs-12">
					@endif
						<a class="btn dropdown-button dropdown-left non-functional-button disabled" href="#">Course</a>
						<a class="btn dropdown-button dropdown-center" data-toggle="dropdown" href="#" id="course-name" href=""></a>
						<a class="btn dropdown-button dropdown-toggle" data-toggle="dropdown" href="#">
						<!-- <span class="fa fa-caret-down"></span> -->
						<span class="sr-only">Toggle Dropdown</span>
						</a>
						<ul class="dropdown-menu">
						@if (!empty($course->term) && !empty($course->year))
							@foreach ($user->coursesTeachingForTermAndYear($course->term, $course->year) as $c)
							<li><a href="/analytics/{{$c->id}}">{{ $c->name }}</a></li>
							@endforeach
						@else
							@foreach ($user->coursesTeaching() as $c)
							<li><a href="/analytics/{{$c->id}}">{{ $c->name }}</a></li>
							@endforeach
						@endif
						</ul>
					</div><!-- .btn-grp -->
					
					@if ($user->enrolledTerms()->count()>0)
					<div class="btn-group col-md-4 col-xs-12">
					@else
					<div class="btn-group col-sm-5 col-xs-12">
					@endif			
						<a class="btn dropdown-button dropdown-left non-functional-button disabled" href="#">Group</a>
						<a class="btn dropdown-button dropdown-center" data-toggle="dropdown" id="group-name" href="#"></a>
						<a class="btn dropdown-button dropdown-toggle" data-toggle="dropdown" href="#">
						<!-- <span class="fa fa-caret-down"></span> -->
						<span class="sr-only">Toggle Dropdown</span>
						</a>
						<ul class="dropdown-menu">
							@if (count($course->groups)==0)
							<li>NO GROUPS</li>
							@else
							@foreach ($course->groups as $g)
							<li><a href="/analytics/{{$course->id}}/{{$g->id}}">{{$g->name}}</a></li>
							@endforeach
							@endif
						</ul>
					</div><!-- .btn-group -->

				</div><!-- .row -->
			</div><!-- .search-bar -->

			@if (count($group_videos)==0)
			<div class="admin-page-section">
				No Videos in this Course and Group
			</div>
			@else
			<div class="admin-page-section">
				<table class="table table-striped">
					<thead>
						<tr style="border-bottom:1px solid black">
							<th>Video</th>
							<th>Student Views</th>
							<th>Annotations</th>
							<th>Key Points</th>
							<!-- <th>User Actions</th> -->
							<th>Quiz Questions</th>
							<th>video event records</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($group_videos as $gv)
						<tr>
							<td class="img-cel">
								<a href="/view/{{$gv->id}}">
									<img class="video-thumbnail" src="{{ $gv->video()->thumbnail_url }}">
									<br />
									{{ $gv->video()->title }}
								</a>
							</td>
							<td>
								Views: {{$gv->numViews()}}<br />
								<span class="tooltip_class" data-tt="Course Enrolment : Number of unique viewers as a percentage ">Course engagement: {{$gv->percentageUsersViewed()}}%</span><br />
								<button class="student_views analytics_btn" groupvideoid = {{$gv->id}}> Unique students &nbsp;&nbsp; <i class="fa fa-info-circle" aria-hidden="true"></i></button>
								<div class="loading-hud">
									<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
									<span class="sr-only">Loading...</span>
								</div>
							</td>
							<td>
								Total: {{count($gv->annotations)}}<br />
								Average per user: {{ceil($gv->aveAnnotationsPerUser())}}<br />
								<button class='annotations_column analytics_btn' groupvideoid = {{$gv->id}}> Unique students &nbsp;&nbsp; <i class="fa fa-info-circle" aria-hidden="true"></i></button>
								<div class="loading-hud">
									<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
									<span class="sr-only">Loading...</span>
								</div>
							</td>
							<td>
								@if (count($gv->points) == 0) 
								N/A<br /><br /><br /><br />
								@else
								Total: {{count($gv->points)}}<br /><br />
								<!-- <a href="../../points-details/{{$gv->id}}" class="btn btn-default btn-xs">
									<i class="fa fa-info-circle" aria-hidden="true"></i>
									Details
								</a> -->
								<button class='key_point analytics_btn' groupvideoid = {{$gv->id}}> Details &nbsp;&nbsp; <i class="fa fa-info-circle" aria-hidden="true"></i></button>
								<div class="loading-hud">
									<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
									<span class="sr-only">Loading...</span>
								</div>
								@endif
							</td>
							<!-- <td>
								Annotation viewed: {{$gv->numTimesAnnotationViewed()}}<br />
								Annotation downloads: {{$gv->numAnnotationDownloads()}}<br />
								<a href="../../tracking-details/{{$gv->id}}" class="btn btn-default btn-xs">
									<i class="fa fa-info-circle" aria-hidden="true"></i>
									Details
								</a>
							</td> -->
							<td>
								@if($gv->quizzes->count()==0)
								N/A<br /><br /><br /><br />
								@else
								<br /><br />
								<button class='quiz_question analytics_btn' groupvideoid = {{$gv->id}} > Details <i class="fa fa-info-circle" aria-hidden="true"></i></button>
								<div class="loading-hud">
									<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
									<span class="sr-only">Loading...</span>
								</div>
								@endif
							</td>
							<td>
								<br /><br />
								<button class='video_event analytics_btn' groupvideoid = {{$gv->id}}> Details <i class="fa fa-info-circle" aria-hidden="true"></i></button>
								<div class="loading-hud">
									<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
									<span class="sr-only">Loading...</span>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>

				@if ($num_pages > 1)
				<div class="page-filter">
					<ul class="">
						<li><a href="/analytics/{{$course->id}}/{{$group->id}}/1"><i class="fa fa-step-backward"></i></a></li>	
					@for ($i=0; $i<$num_pages; $i++)
						@if ($i+1 == $current_page)
						<li class="current-page">{{$i+1}}</li>
						@else
						<li><a href="/analytics/{{$course->id}}/{{$group->id}}/{{$i+1}}">{{$i+1}}</a></li>
						@endif
					@endfor
						<li><a href="/analytics/{{$course->id}}/{{$group->id}}/{{$num_pages}}"><i class="fa fa-step-forward"></i></a></li>
					</ul>
				</div>
				@endif
			</div><!-- .admin-page-section -->
			@endif {{-- endelse no group_video--}}
    	@endif
	</div><!-- container-fluid -->
@endsection


@section('javascript')
		<!-- <script type="text/javascript" src="{{ URL::secureAsset('js/plugin/d3.min.js') }}"></script> -->
		<script type="text/javascript" src="{{ URL::secureAsset('js/plugin/jquery.tooltip.js') }}"></script>
    	<script type="text/javascript" src="{{ URL::secureAsset('js/covaa-stats.js') }}"></script>
@endsection


@section('modal')
	@include('parts.analytics_modal')
@endsection