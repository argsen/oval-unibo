@extends('layouts.app')

@section('title', 'Tag Group Management')

@section('additional-css')
    <link rel="stylesheet" type="text/css" href="{{ URL::secureAsset('components/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::secureAsset('components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <i class="fas fa-tags"></i>TAG GROUP MANAGEMENT
        </div>

        <!-- <div class="msg"></div> -->

        <div class="container-fuild">
			<form>
                <div class="admin-page-section">
                    <fieldset class="form-group no-border covaa-set-tags">
                    <!-- <legend>Set tags
                        <i class="far fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Set tag for annnotation."></i>
                    </legend> -->
                    
                        <div class="row">
                            <div class="col-md-6" >
                                <h4>Tag Group</h4>
                                <span id="covaa-set-tag-group"></span>
                            </div>
                            <div class="col-md-6">
                                <h4>Tag Label</h4>
                                <span id="covaa-set-tag-label"></span>
                            </div>
                        </div><!-- row -->
                    </fieldset>
                </div><!-- admin-page-section -->
                <div class="admin-page-section">
                        <div class="row form-group">
                            <div class="col">
                                To add a new Tag Group and Label(s), enter name and label(s). To add label(s) to existing Tag Group, select a group from above list.
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-md-4" >
                                <div class="input-group mb-3" id="covaa-set-tag-group-input">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Tags Group</span>
                                    </div>
                                    <input type="text" id="covaa-tag-group-textbox" covaa-tag-group=''/> 
                                </div>	
                            </div>
                            <div class="col-md-4">
                                <div class="input-group mb-3" id="covaa-set-tag-label-input">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Labels</span>
                                    </div>
                                    <input type="text" covaa-tag-group-label=''/> 
                                </div>	
                            </div>
                            <div class="col-md-4" id="covaa-set-tag-label">
                                <button class="rectangle-button" id="covaa-add-tag-group">Add<i class="fas fa-plus"></i></button>
                                <div id="covaa-tag-loading">
                                    <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>

                        </div>
                </div>
                
            </form>
        </div>
    </div>
@endsection

@section('modal')
	@include('parts.tag-management-modal')
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ URL::secureAsset('components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::secureAsset('js/tag-group-management.js') }}"></script>
@endsection
