@extends('layouts.app')

@section('title', 'Video Management')

@section('additional-css')
<link rel="stylesheet" type="text/css" href="{{ URL::secureAsset('components/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::secureAsset('components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endsection

@section('content')
	<div class="container-fluid">
    	<div class="page-title"><i class="fas fa-video"></i>VIDEO MANAGEMENT</div>

		<div class="msg">
		@if (!empty(session('msg')))
			{{session('msg')}}
		@endif
		</div>

			<div class="admin-page-section-header">
					<h2>ADD VIDEO</h2>
			</div><!-- .admin-page-section-header -->

			<div class="admin-page-section">
				<form id="add-video-form" name="add-video-form" action="" method="POST" role="form">
					<fieldset class="form-group">
						<legend>Video Source</legend>
						@foreach (config("settings.video_source") as $key=>$src)
						<div class="form-check form-check-inline">
							<label class="form-ckeck-label"><input class="form-check-input" type="radio" name="source-radio" id="{{$key}}" value="{{$key}}" data-url="{{$src['url']}}">{{$src['label']}}</label>
						</div><!-- .radio-inline -->

						@endforeach
						<span class="space-left-right">
							<a href="" target="_blank" id="open-link" class="btn btn-link" title="Open video page in new window">
								<i class="fas fa-link"></i>
								Open
							</a>
						</span>
					</fieldset>

					<fieldset class="form-group">
						<legend>Video URL</legend>
						<input class="form-control gray-textbox" type="url" id="video-url" required>
						<div class="instruction">Please enter full URL of the video</div>
						<div class="help-block with-errors"></div>
					</fieldset>

					 @if (config('settings.custom_video_title')==1) 
					<fieldset class="form-group"> 
						<legend> 
						Custom Video Title 
						<i class="fa fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Optional. This is only if you want to display video title that is different from its original source's."></i> 
						</legend> 
						<input class="form-control gray-textbox" id="custom-title"> 
					</fieldset> 
					@endif 
					<fieldset class="form-group vimeo-only">
						<legend>Video Password</legend>
						<input class="form-control gray-textbox" type="text" id="video-password">
						<div class="instruction">If the video is password protected, please enter the password</div>
					</fieldset>

					<fieldset class="form-group" id="segments">
						<legend>
							Video Segments
							<i class="fas fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Segments with start and end time can be set, however the whole video will be accessible."></i>
						</legend>
						<div>
							<span class="blue-text">Do you want to specify segments?</span>
							<span class="space-left-right">
								<div class="form-ckeck form-check-inline">
									<label class="form-check-label"><input class="form-check-input" type="radio" name="segments-radio" id="yes" value="true">Yes</label>
								</div><!-- .radio-inline -->
								<div class="form-check form-check-inline">
									<label class="form-check-lable"><input class="form-check-input" type="radio" name="segments-radio" id="no" value="false">No</label>
								</div><!-- .radio-inline -->
							</span>
						</div>
						<div id="segments-controls">
							<div class="form-group" id="segments-inputs">
								<fieldset class="row form-group mb-2 no-border">
									<div class="col-12 col-md-4 mb-1">
										<legend>Title</legend>
										<input class="form-control gray-textbox segment-title" type="text" placeholder="Title">
									</div>
									<div class="col-5 col-md-3">
										<legend>Start Time</legend>
										<input class="form-control gray-textbox segment-start" type="text" placeholder="0:00" data-start="start" required data-required-error="Please fill in start time." pattern="^(\d?\d:)?\d?\d:\d\d$" data-pattern-error="Please match the time format required.">
										<div class="instruction">mm:ss or hh:mm:ss</div>
									</div>
									<div class="col-5 col-md-3">
										<legend>End Time</legend>
										<input class="form-control gray-textbox segment-end" type="text" placeholder="1:00" data-end="end" required data-required-error="Please fill in end time."  pattern="^(\d?\d:)?\d?\d:\d\d$" data-pattern-error="Please match the time format required.">
										<div class="instruction">mm:ss or hh:mm:ss</div>
									</div>
									<div class="col-2 mt-4">
										<button type="button" class="delete-segment outline-button btn-sm full-width" title="delete"><span class="d-none d-md-inline-block">Delete</span><i class="fa fa-minus-circle"></i></button>
									</div>
									<div class="col-12 help-block with-errors"></div>
								</fieldset><!-- row -->
							</div>
							<button type="button" id="another-segment-button" class="outline-button btn-sm" title="Add another segment">Add another segment<i class="fa fa-plus-circle"></i></button>
						</div><!-- points-controls -->
					</fieldset>

					<fieldset class="form-group">
						<legend>Group</legend>
						<span class="blue-text">Assign this video to the default group of</span>
						<select id="course-to-assign" name="course-to-assign">
							@foreach ($user->coursesTeaching() as $c)
							<option value="{{$c->id}}">{{$c->name}}</option>
							@endforeach
						</select>
					</fieldset>

					<fieldset class="form-group" id="points">
						<legend>
							Key Points
							<i class="fas fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Key Points refers to an activity where students are asked to write a comment related to this video and then self-check whether their comment includes key points raised in the video. It also asks them to rate their confidence on whether their comment captures the key points of the video."></i>
						</legend>
						<div>
							<span class="blue-text">Do you want to add points to your video?</span>
							<span class="space-left-right">
								<div class="form-ckeck form-check-inline">
									<label class="form-check-label"><input class="form-check-input" type="radio" name="points-radio" id="yes" value="true">Yes</label>
								</div><!-- .radio-inline -->
								<div class="form-check form-check-inline">
									<label class="form-check-lable"><input class="form-check-input" type="radio" name="points-radio" id="no" value="false">No</label>
								</div><!-- .radio-inline -->
							</span>
						</div>
						<div id="points-controls">
							<div class="form-group">
								<input id="point-instruction-textbox" class="form-control gray-textbox" type="text" placeholder="Instruction for students here...">
							</div>
							<div class="form-group" id="points-inputs">
								<div class="row space-bottom">
									<div class="col-9">
										<input class="form-control gray-textbox" type="text" placeholder="Point text here...">
									</div>
									<div class="col-3">
										<button type="button" class="delete-point outline-button btn-sm full-width" title="delete"><span class="d-none d-sm-inline-block">Delete</span><i class="fa fa-minus-circle"></i></button>
									</div>
								</div><!-- row -->
							</div>
							<button type="button" id="another-point-button" class="outline-button btn-sm" title="Add another point">Add another point<i class="fa fa-plus-circle"></i></button>
						</div><!-- points-controls -->
					</fieldset>
					
					<fieldset class="form-group" id="text-analysis">
						<legend>Content Analysis</legend>
						<div class="form-inline">
							<span class="space-right">Request content analysis?</span>
							<div class="radio-inline">
								<label><input type="radio" name="analysis-radio" id="request-analysis" value="request-analysis">Yes</label>
							</div><!-- .radio-inline -->
							<div class="radio-inline">
								<label><input type="radio" name="analysis-radio" id="no-analysis" value="no-analysis" checked>No</label>
							</div><!-- .radio-inline -->
						</div>
					</fieldset>

					<button type="button" id="add-video-button" class="rectangle-button" title="Save">SAVE<i class="fas fa-video"></i></button>
					<div id="loading-hud">
						<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</div>
					<div class="instruction vimeo-only">
						You will be redirected to authenticate with Vimeo if metadata for the video is not available without authenticating with existing tokens.
					</div>
				</form><!-- #add-video-form -->
			</div><!-- .admin-page-section -->

			<div class="admin-page-section-header">
				<h2>UPLOADED VIDEOS</h2>
			</div><!-- admin-page-section-header -->

			@if ($user->enrolledTerms()->count()>0)
			<div class="search-bar container-fluid">
			@else
			<div class="search-bar filter-bar container-fluid">
			@endif
				<div class="row">
					<a id="filter_controls"></a>
					@if ($user->enrolledTerms()->count()>0)
					<div class="col-xs-12 space-top-10 white-text">Filter videos</div>
					@else
					<div class="col-xs-12 col-sm-2 admin-bar-text white-text">Filter videos</div>
					@endif

					@if ($user->enrolledTerms()->count()>0)
					<div class="btn-group col-sm-5 col-xs-12">
						<a class="btn dropdown-button dropdown-left" href="#">Term</a>
						<a class="btn dropdown-button dropdown-center"  data-toggle="dropdown" href="#" id="term-name"></a>
						<a class="btn dropdown-button dropdown-toggle" data-toggle="dropdown" href="#">
						<!-- <span class="fa fa-caret-down"></span> -->
						<span class="sr-only">Toggle Dropdown</span>
						</a>
						<ul class="dropdown-menu">	
							@foreach ($user->enrolledTerms() as $t)
							<li><a href="/video-management?year={{$t->year}}&term={{$t->term}}">{{ $t->term_name }}</a></li>
							@endforeach
						</ul>
					</div><!-- btn-group -->
					@endif
					<form id="filter-by-class" name="filter-by-class" method="POST" action="" class="col-sm-5 col-xs-12">
						@if ($user->enrolledTerms()->count()>0)
						<div class="btn-group">
						@else
						<div class="btn-group">
						@endif
							<a class="btn dropdown-button dropdown-left" href="#">Course</a>
							<a class="btn dropdown-button dropdown-center"  data-toggle="dropdown" href="#" id="course-name"></a>
							<a class="btn dropdown-button dropdown-toggle" data-toggle="dropdown" href="#">
							<!-- <span class="fa fa-caret-down"></span> -->
							<span class="sr-only">Toggle Dropdown</span>
							</a>
							<ul class="dropdown-menu">
								@if ($user->enrolledCourses->count() == 0)
								<li>NO COURSES</li>
								@else
									@if (!empty($course->term) && !empty($course->year))
										@foreach ($user->coursesTeachingForTermAndYear($course->term, $course->year) as $c)
										<li><a href="/video-management/{{$c->id}}">{{ $c->name }}</a></li>
										@endforeach
									@else
										@foreach ($user->coursesTeaching() as $c)
										<li><a href="/video-management/{{$c->id}}">{{ $c->name }}</a></li>
										@endforeach
									@endif
								@endif
							</ul>
						</div><!-- btn-group -->
					</form><!-- filter-by-class -->

					<form id="filter-by-group" method="POST" action="" class="col-sm-5 col-xs-12">
						@if ($user->enrolledTerms()->count()>0)
						<div class="btn-group">
						@else
						<div class="btn-group">
						@endif
							<a class="btn dropdown-button dropdown-left" href="#">Group</a>
							<a class="btn dropdown-button dropdown-center" data-toggle="dropdown" href="#" id="group-name"></a>
							<a class="btn dropdown-button dropdown-toggle" data-toggle="dropdown" href="#">
							<!-- <span class="fa fa-caret-down"></span> -->
							<span class="sr-only">Toggle Dropdown</span>
							</a>
							<ul class="dropdown-menu">
								@if ($user->groupMemberOf->count() == 0)
								<li>NO GROUPS</li>
								@elseif ($course)
									@foreach ($user->groupMemberOf->where('course_id', $course->id) as $g)
									<li><a href="/video-management/{{ $course->id }}/{{ $g->id }}">{{ $g->name }}</a></li>
									@endforeach
								@else
									<li>Select A Course</li>
								@endif
							</ul>
						</div><!-- btn-group -->
					</form><!-- filter-by-group -->

				</div><!-- .row -->
			</div><!-- .search-bar (filter bar) -->


			@if (count($group_videos)==0)
			<div class="admin-page-section">
				No Videos in this Course and Group
			</div>
			@else
			<a id="assigned"></a>
			<div class="admin-page-section">
				<div class="table-responsive">
				<table class="table table-striped" name="assiged">
					<thead>
						<tr>
							<th>TITLE</th>
							<th>
								DESCRIPTION&nbsp;
								<i class="far fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" title="This is the description of the video provided by YouTube or UniSA Media Library"></i>
							</th>
							<th>
								VIDEO ACCESS CONTROL&nbsp;
								<i class="far fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" title="Use the 'visible' setting to make the video visible/hidden to your students. By default the video is 'hidden'. Use the 'Order: default' setting to edit the order in which the videos display to your students in the OVAL 'Home' page."></i>
							</th>
							<th>
								CONTENT ANALYSIS&nbsp;
								<i class="far fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" title="Use the 'visible' setting to display a list of 'current topics' discussed in the video with hyperlinked time codes so students can jump to particular key topics mentioned in the video and a set of recommended videos. Note: this is only available for videos that are hosted on the UniSA UO YouTube Channel that have transcripts."></i>
							</th>
							<th>
								RECOMMENDED RESOURCES&nbsp;
								<i class="far fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" title="Use the 'visible' setting to display a list of 'recommended resources' based on topics discussed in the video. Note: this is only available for videos that are hosted on the UniSA UO YouTube Channel that have transcripts."></i>
							</th>
							<th>
								DURATION&nbsp;
								<i class="far fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" title="This shows the total duration of the video."></i>
							</th>
							<th>
								GROUP ACCESS&nbsp;
								<i class="far fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" title="By default, the video will be available to all students in the specific course you have selected with the group title 'Course Group'. You can make a video available to specific groups of students by clicking on the group icon below."></i>
							</th>
							<th>
								KEY POINTS&nbsp;
								<i class="far fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" title="Click on the 'edit' icon below to add or modify instructions for students to make general comments and self-check their comments against correct key points raised in the video."></i>
							</th>
							<th>
								QUIZ&nbsp;
								<i class="far fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" title="If you want in-video quiz questions, use this column to setup quizzes for each video. Follow the instruction prompts. You can also make the quizzes hidden or visible to students."></i>
							</th>
							<th>
								DATE UPLOADED&nbsp;
								<i class="far fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" title="This is the date and time when the video was added to OVAL."></i>
							</th>
							<th>
								TAG GROUP&nbsp;
								<i class="far fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" title="Use this to set up tag group."></i>
							</th>
							<th>
								TIME SEGMENTS&nbsp;
								<i class="far fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" title="Segments can be created for playback of set start and end time."></i>
							</th>
							<th>
								DELETE&nbsp;
								<i class="far fa-question-circle icon-for-tooltip" aria-hidden="true" data-toggle="tooltip" title="By clicking on the delete icon, you will make the video unavailable to the selected Course. It does not delete the video or its associated data from OVAL."></i>
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($group_videos as $gv)

						<tr id="v-{{$gv->video()->id}}">
							<td class="img-cel">
								<a href="/view/{{$gv->id}}">
									<img class="video-thumbnail" src="{{ $gv->video()->thumbnail_url }}">
									@if(config('settings.custom_video_title')==0) 
									<br />
									{{ $gv->video()->title }}
									@endif
								</a>
								@if(config('settings.custom_video_title')==1) 
							 
								<button type="button" class="btn btn-link gv-title-button" data-id="{{$gv->video()->id}}" data-gvid="{{$gv->id}}" data-val='{{$gv->displayTitle()}}' data-toggle="modal" data-target="#edit-title-modal" title="Edit video title"> 
									<div class="title-button-div"> 
										<span class="full-width">{{ htmlspecialchars_decode($gv->displayTitle()) }}&nbsp;</span> 
										<i class="fas fa-pencil-alt  left-indent"></i>
									</div>
								</button> 
								@endif
							</td>
							<td> 
								<div class="clamp-this" data-full="{{$gv->video()->description}}"> 
									{{ $gv->video()->description }} 
								</div> 
							</td>
							<td>
								<button type="button" class="btn btn-link visibility-button" data-toggle="modal" data-target="#visibility-modal" data-id="{{$gv->id}}" data-hidden="{{$gv->hide}}" title="Edit video visibility">
									@if ($gv->hide == true) 
									Hidden
									<i class="fas fa-eye-slash" aria-hidden="true"></i>
									@else
									Visible
									<i class="fas fa-eye" aria-hidden="true"></i>
									@endif
								</button>
								<br />
								<button type="button" class="btn btn-link video-order-button" data-toggle="modal" data-target="#order-modal" data-id="{{ $gv->id }}" data-type="" title="Edit video order">
									Order: 
									@if($gv->order != 1000)
									{{$gv->order}}
									@else
									default
									@endif
									&nbsp;
									<i class="fas fa-pen-square" aria-hidden="true"></i>
								</button>
							</td>
							<td>
							@if($gv->video()->media_type == "helix")
								<div class="text-center">N/A</div>
							@else
								<button type="button" class="btn btn-link text-analysis-button toggle-visibility" data-toggle="modal" data-target="#text-analysis-modal" data-id="{{ $gv->id }}"  data-visible="{{$gv->show_analysis}}" title="Edit visibility of text analysis">
								@if ($gv->show_analysis == true)
									Visible
									<i class="fas fa-eye" aria-hidden="true"></i>
								@else
									Hidden
										<i class="fas fa-eye-slash" aria-hidden="true"></i>
								@endif
								</button>
								<br />
								
								@if ($gv->keywords->count() > 0)
								<button type="button" class="btn btn-link text-analysis-button" data-toggle="modal" data-target="#edit-keywords-modal" data-gvid="{{$gv->id}}" title="Edit keywords">
									<i class="fa fa-info-pencil" aria-hidden="true"></i>
									Edit
								</button>
								<br />	
								@endif
							@endif {{-- if helix/youtube --}}
							</td>
							<td>
							@if($gv->video()->media_type == "helix")
								<div class="text-center">N/A</div>
							@else
								<button type="button" class="btn btn-link recommended-resources-button toggle-visibility" data-toggle="modal" data-target="#text-analysis-modal" data-id="{{ $gv->id }}"  data-visible="{{$gv->show_recommended_resources}}" title="Edit visibility of recommended resources">
								@if ($gv->show_recommended_resources == true)
									Visible
									<i class="fas fa-eye" aria-hidden="true"></i>
								@else
									Hidden
									<i class="fas fa-eye-slash" aria-hidden="true"></i>
								@endif
								</button>
								<br />
								<button type="button" class="btn btn-link rating-button toggle-visibility" data-toggle="modal" data-target="#text-analysis-modal" data-id="{{ $gv->id }}"  data-visible="{{$gv->enable_recommended_resources_rating}}" title="Enable/Disable recommended resources">
								@if ($gv->enable_recommended_resources_rating == true)
									Rating Enabled
									<i class="fa fa-check" aria-hidden="true"></i>
								@else
									Rating Disabled
									<i class="fa fa-ban" aria-hidden="true"></i>
								@endif
								</button>
							@endif {{-- if helix/youtube --}}
							</td>
							<td>{{ $gv->video()->formattedDuration() }}</td>
							<td>
								<button type="button" class="btn btn-link" data-toggle="modal" data-target="#modal-assigned-group-list" data-id="{{ $gv->video()->id }}" title="List assigned groups">
									Show&nbsp;<i class="fa fa-list-alt" aria-hidden="true"></i>
								</button>
								<br />
								<button type="button" class="btn btn-link group-mng-button" data-toggle="modal" data-target="#modal-form" data-id="{{ $gv->video()->id }}" data-type="manage" title="Assign to another group">
									Add&nbsp;<i class="fa fa-user-plus" aria-hidden="true"></i>
								</button>
							</td>
							<td>
								<button type="button" class="btn btn-link edit-points-button" data-toggle="modal" data-target="#modal-points-form" data-id="{{ $gv->video()->id }}" title="Manage points"><i class="fas fa-edit group-icon"></i></button>
							</td>
							<td>
								<button type="button" videoid="{{ $gv->video()->id}}" class="quiz btn btn-link" title="Set quiz" data-group-video-id="{{$gv->id}}"><i class="fas fa-comments"></i></button>
								<br />
								<label class="switch">
									@if (!empty($gv->relatedQuiz()) && $gv->relatedQuiz()->visable==1)
									<input type="checkbox" checked videoid="{{ $gv->video()->id}}" data-group-video-id="{{ $gv->id }}">
									<!-- <div class="slider-div"> -->
										<span class="slider round"></span>
										<br/>
										<p class="visibility">visible</p>
									<!-- </div> -->
									@elseif (!empty($gv->relatedQuiz()))
									<input type="checkbox" videoid="{{ $gv->video()->id}}" data-group-video-id="{{ $gv->id }}">
									<!-- <div class="slider-div"> -->
										<span class="slider round"></span>
										<br/>
										<p class="visibility">hidden</p>
									<!-- </div> -->
									@else
									<input type="checkbox" videoid="{{ $gv->video()->id}}" data-group-video-id="{{ $gv->id }}" disabled>
									<!-- <div class="slider-div hidden"> -->
										<span class="slider round hidden"></span>
										<br/>
										<p class="visibility"></p>
									<!-- </div> -->
									@endif
									
								</label>
							</td>
							<td>{{ $gv->created_at }}</td>
							<td>
								<button type="button" videoid="{{ $gv->video()->id}}" class="covaa-set-tag-group btn btn-link" title="Assign tag groups" data-group-video-id="{{$gv->id}}"><i class="fas fa-tags"></i></button>
								<br />
							</td>
							<td>
								<button type="button" class="btn btn-link edit-segments-button" data-toggle="modal" data-target="#modal-segments-form" data-gvid="{{$gv->id}}" data-vid="{{$gv->video()->id}}" data-gvsid="{{ $gv->id }}" title="Manage time segments"><i class="fas fa-edit group-icon"></i></button>
							</td>
							<td>
								<button class="btn btn-link archive-button" data-id="{{ $gv->id }}" title="Delete">
									<i class="far fa-trash-alt" aria-hidden="true"></i>
								</button>
							</td>

						  </tr>
						@endforeach
					</tbody>
				</table>
				</div><!-- table-responsive -->
			@endif
			</div><!-- .admin-page-section (Videos by course and groups) -->

		</div><!-- container-fluid -->
@endsection

@section('modal')
	@include('parts.video-management-modal')
@endsection


@section('javascript')

		<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5sortable/0.6.3/html.sortable.min.js"></script> -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5sortable/0.9.8/html5sortable.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
		<script type="text/javascript" src="{{ URL::secureAsset('components/multiselect/js/jquery.multi-select.js') }}"></script>
		<script src="https://player.vimeo.com/api/player.js"></script>
			
		<script type="text/javascript" src="{{ URL::secureAsset('components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
		<script type="text/javascript" src="{{ URL::secureAsset('js/video-management.js') }}"></script>
		<script type="text/javascript" src="{{ URL::secureAsset('js/plugin/oval_quiz_creation.js') }}"></script>
		<script type="text/javascript" src="{{ URL::secureAsset('components/clamp-js/clamp.min.js') }}"></script>
@endsection



