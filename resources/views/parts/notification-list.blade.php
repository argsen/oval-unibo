
<div id="notifications-container"> 
    <div class="navmenu-title">NOTIFICATIONS</div>
    <div class="row"> 
        <div class="col-xs-8"> 
            <div class="pull-right space-bottom"> 
                <button id="refresh-notification-button" type="button" class="btn btn-link"> 
                    Refresh 
                    <i class="fas fa-sync-alt"></i> 
                </button> 
            </div> 
        </div> 
    </div> 
 
<div id="notifications-display" class="panel-group"> 
    <div class="panel"> 
        <div class="panel-heading"> 
            <h4 class="panel-title"> 
                <a data-toggle="collapse" data-parent="#notifications-display" href="#current-notifications"> 
                    <i class="fas fa-caret-down"></i> 
                    NEW 
                </a> 
            </h4> 
        </div><!-- panel-heading --> 
        <div id="current-notifications" class="panel-collapse collapse in"> 
            @if (!empty($received_notifications) && $received_notifications->where('completed', '=', 0)->count() > 0) 
            @foreach ($received_notifications->where('completed', '=', 0) as $rn) 
            <div class="notification"> 
                @if ($rn->notification->type == "message") 
                <button type="button" class="btn btn-link read-button pull-right" data-rnid="{{$rn->id}}"> 
          <i class="fas fa-times" aria-hidden="true"></i> 
                </button> 
                @else 
                <button type="button" class="btn btn-link status-button task-not-done pull-right" data-rnid="{{$rn->id}}"> 
                    Completed&nbsp;<i class="fa fa-square-o"></i> 
                </button> 
                @endif 
                <div class="notification-type-container"> 
                    <span class="notification-type">{{$rn->notification->type}}</span>  
                </div>     
                <div class="notification-msg"> 
                    {!! html_entity_decode($rn->notification->message) !!}
                </div><!-- notification-msg --> 
                @if(!empty($rn->notification->deadline)) 
                <div class="notification-deadline">Due on <strong>{{date('d-M-Y', strtotime($rn->notification->deadline))}}</strong></div> 
                @endif 
            </div><!-- notification --> 
            @endforeach 
            @else 
            <p class="white-text">There are no new notifications.</p> 
            @endif 
        </div><!-- #current-notifications --> 
    </div><!-- panel --> 
 
 
    <div class="panel"> 
        <div class="panel-heading"> 
            <h4 class="panel-title"> 
                <a data-toggle="collapse" data-parent="#notifications-display" href="#previous-notifications"> 
                    <i class="fas fa-caret-down"></i> 
                    READ 
                </a> 
            </h4> 
        </div><!-- panel-heading --> 
        <div id="previous-notifications" class="panel-collapse collapse"> 
            @if (!empty($received_notifications) && $received_notifications->where('completed', '=', 1)->count() > 0)
            @foreach ($received_notifications->where('completed', '=', 1) as $rn) 
            <div class="notification"> 
                @if ($rn->notification->type == "task") 
                <button type="button" class="btn btn-link status-button task-done pull-right" data-rnid="{{$rn->id}}"> 
                    done&nbsp;<i class="fas fa-check-square-o"></i> 
                </button> 
                @endif 
                <div class="notification-type-container"> 
                    <span class="notification-type">{{$rn->notification->type}}</span>  
                </div>     
                <div class="notification-msg"> 
                    {!! html_entity_decode($rn->notification->message) !!}
                </div><!-- notification-msg --> 
                @if(!empty($rn->notification->deadline)) 
                <div class="notification-deadline">Due on <strong>{{date('d-M-Y', strtotime($rn->notification->deadline))}}</strong></div> 
                @endif 
            </div><!-- notification --> 
            @endforeach 
            @else 
            <p class="white-text">There are no previous notifications.</p> 
            @endif 
        </div><!-- #current-notifications --> 
    </div><!-- panel --> 
     
</div><!-- #notifications-display --> 
 
</div>