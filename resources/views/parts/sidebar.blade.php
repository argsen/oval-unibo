	<div class="navmenu navmenu-default navmenu-fixed-left vertical-scroll">
		<i class="far fa-times-circle white" id="close-nav"></i>
	@if(!empty(Auth::user())) 
  	@if(Auth::user()->isAnInstructor() || Auth::user()->role=="A")
  		<!-- <div class="navmenu-title">MENU</div>
  		<ul class="nav navmenu-nav side-bar-nav">
			<li><a href="/">Home</a></li>
			@if(Auth::user()->isAnInstructor())
  			<li><a href="/video-management">Video Management</a></li>
			<li><a href="/manage-notifications">Notification Management</a></li>
			@endif
			@if(Auth::user()->role=="A") 
			<li><a href="/manage-analysis-requests">Manage Content Analysis Requests</a></li>
			<li><a href="/register">Add an admin user</a></li>
			@endif
			@if(Auth::user()->isAnInstructor())
			<li><a href="/analytics">Analytics</a></li>

			@endif
			@if(Auth::user()->role=="A") 
			<li><a href="/batch-upload">Batch Upload</a></li>
			<li><a href="/manage-lti-connections">Manage LTI Connections</a></li>
  			<li class="space-top"><a href="/logout">Logout</a></li>
			@endif
  		</ul> -->
	@else 
		<!-- <div class="navmenu-title">MENU</div>
		<ul class="nav navmenu-nav side-bar-nav">
			<li><a href="/">Home</a></li>
			<li><a href="/survey">Survey</a></li>
		</ul> -->
    	@include('parts.notification-list') 
	@endif 
	@endif
  	</div><!-- .navmenu -->