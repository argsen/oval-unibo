		<div class="courses-bar container-fluid">
			<div class="row">
				<div class="btn-group col-md-4 col-xs-12">
					<a class="btn dropdown-button dropdown-left non-functional-button disabled" href="#">Course</a>
					<a class="btn dropdown-button dropdown-center" id="course-name" href="#" data-toggle="dropdown"></a>
					<a class="btn dropdown-button dropdown-toggle" data-toggle="dropdown" href="#">
					<span class="" title="Toggle dropdown menu"></span>
					</a>
					<ul class="dropdown-menu">
					@if (count($user->enrolledCourses) == 0)
						<li class="dropdown-item">NO COURSES</li>
					@else
						@if (!empty($course->term) && !empty($course->year))
							@foreach ($user->enrolledCoursesForTermAndYear($course->term, $course->year) as $c)
							<li class="dropdown-item"><a href="/course/{{$c->id}}">{{ $c->name }}</a></li>
							@endforeach
						@else
							@foreach ($user->enrolledCourses as $c)
							<li class="dropdown-item"><a href="/course/{{$c->id}}">{{ $c->name }}</a></li>
							@endforeach
						@endif
					@endif
				  </ul>
				</div><!-- .btn-group --> 

				<div class="btn-group col-md-4 col-xs-12">
					<a class="btn dropdown-button dropdown-left non-functional-button disabled" href="#">Group</a>
					<a class="btn dropdown-button dropdown-center" id="group-name" href="#" data-toggle="dropdown"></a>
					<a class="btn dropdown-button dropdown-toggle" data-toggle="dropdown" href="#">
					<span class="" title="Toggle dropdown menu"></span>
					</a>
					<ul class="dropdown-menu">
					@if (count($user->groupMemberOf) == 0)
						<li class="dropdown-item">NO GROUPS</li>
					@else
						@foreach ($user->groupMemberOf->where('course_id', $course->id) as $g)
						<li class="dropdown-item"><a href="/group/{{$g->id}}">{{ $g->name }}</a></li>
						@endforeach
					@endif
					</ul>
				</div><!-- .btn-group -->

				<div class="btn-group col-md-4 col-xs-12">
				  <a class="btn dropdown-button dropdown-left  non-functional-button disabled" href="#">Video</a>
				  <a class="btn dropdown-button dropdown-center" id="video-name" href="#" data-toggle="dropdown"></a>
				  <a class="btn dropdown-button dropdown-toggle" data-toggle="dropdown" href="#">
					<span class="" title="Toggle dropdown menu"></span>
				  </a>
					<ul class="dropdown-menu">
					@if (count($user->viewableGroupVideos()) == 0)
						<li class="dropdown-item">NO VIDEOS</li> 
					@else
					
					@foreach ($group->availableGroupVideosForUser($user) as $gv)  
					<li class="dropdown-item"><a href="/view/{{$gv->id}}">{{ htmlspecialchars_decode($gv->displayTitle()) }}</a></li>
					@endforeach
					@endif

				  </ul>
				</div><!-- .btn-group -->
			</div><!-- .row -->
		</div><!-- .courses-bar -->