		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="_token" content="{{ csrf_token() }}" />
		<meta name=viewport content="width=device-width, initial-scale=1">

		<title>OVAL - Online Video Annotation for Learning | @yield('title')</title>

		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700">
		<link rel="stylesheet" type="text/css" href="{{ URL::secureAsset('components/font-awesome/css/all.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::secureAsset('components/bootstrap/dist/css/bootstrap.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::secureAsset('css/navmenu-reveal.css') }}">
		@yield('additional-css')
		<link rel="stylesheet" type="text/css" href="{{ URL::secureAsset('css/style.css') }}">
		
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
