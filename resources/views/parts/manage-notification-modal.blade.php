<div class="modal fade" id="edit-notification-modal" tabindex="-1" role="dialog" aria-labelledby="edit-notification-modal-label" aria-hidden="true"> 
  <div class="modal-dialog" role="document"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
          <i class="fa fa-close" aria-hidden="true"></i> 
        </button> 
        <h4 class="modal-title" id="edit-notification-modal-label">EDIT NOTIFICATION</h4> 
      </div><!-- .modal-header --> 
       
      <div class="modal-body container-fluid"> 
        <form id="edit-notification-form" action="" method="POST" role="form" data-toggle="validator"> 
          <fieldset class="form-group"> 
            <legend>Notification Type</legend> 
            <div class="radio-inline"> 
              <label><input type="radio" name="edit-type-radio" value="message" checked>Message</label> 
            </div><!-- .radio-inline --> 
            <div class="radio-inline"> 
              <label><input type="radio" name="edit-type-radio" value="task">Task</label> 
            </div><!-- .radio-inline --> 
          </fieldset> 
          <div class="form-group"> 
            <label for="edit-notification-msg">Notification Content</label> 
            <textarea id="edit-notification-msg" type="text" class="form-control" required></textarea> 
            <div class="help-block with-errors"></div> 
          </div> 
          <div class="form-group"> 
            <label for="edit-notification-issue">Issue Date</label> 
            <input id="edit-notification-issue" type="date" class="form-control"> 
          </div> 
          <div class="form-group" id="edit-deadline-div"> 
            <label for="edit-notification-deadline">Deadline</label> 
            <input id="edit-notification-deadline" type="date" class="form-control"> 
          </div> 
 
          <div class="form-buttons"> 
            <button type="button" class="btn btn-link d-block mx-auto" id="save-notification-button" data-nid=""><i class="fa fa-save"></i></button> 
          </div><!-- form-group --> 
        </form> 
             
             
            </div><!-- modal-body --> 
 
    </div><!-- .modal-content --> 
  </div><!-- .modal-dialog --> 
</div><!-- .#modal-form --> 