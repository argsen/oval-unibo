		<!-- jQuery & jQuery migrat -->
		<script type="text/javascript" src="{{ URL::secureAsset('components/jquery/dist/jquery.min.js') }}"></script>
		<script type="text/javascript" src="{{ URL::secureAsset('components/jquery-migrate/jquery-migrate.min.js') }}"></script>
		<!-- Bootstrap update to 4.0 -->
		<script type="text/javascript" src="{{ URL::secureAsset('components/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
		<!-- NiceScroll -->
		<script type="text/javascript" src="{{ URL::secureAsset('components/jquery.nicescroll/jquery.nicescroll.min.js') }}"></script>

		@if (!empty(Auth::user()) && (!Auth::user()->isAnInstructor() || Auth::user()->role!='A')) 
    		<script type="text/javascript" src="{{ URL::secureAsset('js/sidebar.js') }}"></script> 
    	@endif

		@yield('additional-js')

		<!-- font-awesome -->
		<!-- <script defer src="components/font-awesome/svg-with-js/js/fontawesome-all.min.js"></script> -->

		<script type="text/javascript">

			$(document).ready(function () {
				$("#logout").on("click", function() {
					$.ajax({
						type: "get",
						url: "{{ secure_url("logout") }}",
						success: function(data) {
							location.reload();
						},
						async: false
					});
				});
			});
		</script>

		@if (!empty($user))<script type="text/javascript">
			$.ajaxSetup({
			   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
			   beforeSend: function(request){
			   	request.setRequestHeader("Authorization", "Bearer {{$user->api_token}}");
			   },
			});
		</script>
		@endif
