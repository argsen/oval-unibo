<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<h4 class="modal-title" id="modalLabel">ASSIGN VIDEO TO GROUP</h4>
				<div class="row">
					<div id="modal-video-title" class="col"></div>
					<div class="col-3 space-right">
						<img id="modal-video-thumbnail" src="" class="video-thumbnail">
					</div>
				</div><!-- row -->				
			</div><!-- .modal-header -->
			
			<div class="modal-body container-fluid">
				<form id="assign-video-to-group-form" role="form" data-toggle="validator">
							<div class="semi-bold col">Assign to:</div>

							<div id="assign-to-course" class="btn-group col">
								<button type="button" class="btn dropdown-button dropdown-left">Course</button>
								<button type="button" class="btn dropdown-button dropdown-center" id="modal-course-name" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false"></button>
								<button type="button" class="btn dropdown-button dropdown-toggle" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
									<!-- <span class="fa fa-caret-down"></span> -->
									<span class="sr-only">Toggle Dropdown</span>
								</button>
								<ul class="dropdown-menu" id="course-dropdown">
									@if (count($user->coursesTeaching())==0)
									<li>NO COURSES</li>
									@else
									@foreach ($user->coursesTeaching() as $c)
									<li id="{{$c->id}}">{{ $c->name }}</li>
									@endforeach
									@endif
								</ul><!-- .dropdown-menu -->
							</div><!-- btn-group -->

					<div class="container space-top">
						<div class="col-xs-12">
							<div class="col-xs-12 form-group">
								<label for="modal-group-list">Available groups</label>
								<select multiple id="modal-group-list" class="form-control" required>
								</select>
								<div class="help-block with-errors"></div>
								<div class="instruction"><strong>Note:</strong> Hold down ctrl to select multiple.</div>
							</div><!-- col-xs-12 -->
						</div><!-- colxs-12 -->
					</div><!-- row -->

					<div class="container space-top">
						<div class="col-xs-12">
							<span class="space-right">Would you like to copy existing contents?</span>
							<span class="">
								<label class="radio-inline">
									<input type="radio" name="copy-contents" id="copy-contents-yes" value="true">Yes
								</label>
								<label class="radio-inline left-indent">
									<input type="radio" name="copy-contents" id="copy-contents-no" value="false" checked>No
								</label>
							</span>
						</div><!-- col-xs-12 -->
						<div id="copy-from" class="row space-top-10">
							<div class="col-sm-7">
								<div class="container">
									<div class="row form-group">
										<label for="copy-from-course">Course</label>
										<select id="copy-from-course" class="form-control">
										</select>
									</div><!-- row -->
									<div class="row form-group">
										<label for="copy-from-group">Group</label>
										<select id="copy-from-group" class="form-control">
										</select>
									</div><!-- row -->
								</div><!-- container -->
							</div><!-- col-sm-7 -->
							<div class="col-sm-5" id="copy-options">
								<fieldset class="no-border">
									<legend>Options:</legend>
									<div class="checkbox" id="copy-comment-instruction-checkbox" >
										<label>
											<input type="checkbox" value="" id="comment-instruction-cb">
											Comment Instruction
										</label>
									</div><!-- checkbox -->
									<div class="checkbox" id="copy-points-checkbox">
										<label>
											<input type="checkbox" value="" id="points-cb">
											Key Points
										</label>
									</div><!-- checkbox -->
									<div class="checkbox" id="copy-quiz-checkbox">
										<label>
											<input type="checkbox" value="" id="quiz-cb">
											Quiz
										</label>
									</div><!-- checkbox -->
								</fieldset>
							</div><!-- #copy-options -->
						</div><!-- #copy-from -->
					</div><!-- row -->


					<div class="form-buttons">
						<button type="button" class="btn btn-link d-block mx-auto" id="assign-to-group"><i class="far fa-save"></i></button>
					</div><!-- form-buttons -->
				
				</form>

			</div><!-- .modal-body -->
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .#modal-form -->


<div class="modal fade" id="modal-points-form" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<h4 class="modal-title" id="modal-points-title">Edit Points</h4>
			</div><!-- .modal-header -->
			
			<div class="modal-body container-fluid">
				<form>
					<div class="row form-group">
						<div class="col-sm-8">
							<div class="row">
								<div class="col-2 bold">Video:</div>
								<div class="col" id="points-form-video-title"></div>
							</div><!-- row -->
							@if (config("settings.course_wide.point")==1)
							<div class="row">
								<div class="col-2 bold">Course:</div>
								<div class="col" id="points-form-course-name"></div>
							</div><!-- row -->
							@else
							<div class="row">
								<div class="col-2 bold">Course:</div>
								<div class="col" id="points-form-course-name"></div>
							</div><!-- row -->
							<div class="row">
								<div class="col-2 bold">Group:</div>
								<div class="col" id="points-form-course-name"></div>
							</div><!-- row -->
							@endif

						</div><!-- col -->
						<div class="col-sm-4">
							<img id="points-form-thumbnail-img" src="" class="video-thumbnail">
						</div><!-- col -->
					</div><!-- row -->
					
					<div class="container form-group space-top-30">
							<fieldset class="space-bottom no-border">
								<label for="modal-point-instruction">Instruction for students:</label>
								<input id="modal-point-instruction" class="form-control" placeholder="Enter instruction for students..." type="text">
							</fieldset>
							<fieldset id="modal-points" class="no-border">
								<label>Points:</label>
								<div class="row">
									<div class="col-9">
										<input class="form-control new-point" type="text" placeholder="Enter Video Key Point text...">
									</div><!-- col-xs-10 -->
									<div class="col-3">
										<button class="modal-delete-point btn-sm outline-button full-width" type="button" title="delete">
											<span class="hidden-sm-down">Delete</span>
											<i class="fa fa-minus-circle"></i>
										</button>
									</div><!-- col-xs-2 -->
								</div><!-- row -->
							</fieldset>
							<button id="modal-another-point" class="btn-sm outline-button ml-2" type="button" title="Add another point">
								Add another point
								<i class="fa fa-plus-circle"></i>
							</button>
					</div><!-- row -->		
					
						
										
					<div class="form-buttons">
						<button type="button" class="btn btn-link" id="save-points"><i class="far fa-save"></i></button>
						<button type="button" class="btn btn-link pull-right" id="delete-points"><i class="fa fa-trash"></i></button>
					</div><!-- form-group -->
					
				</form>
				
			</div><!-- .modal-body -->
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .#modal-form -->


<!-- Quiz Creation Modal content-->
<div class="modal fade" id="quiz_create_modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document" style="width:70%;">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<h4 class="modal-title">SETUP QUIZ</h4>

				<div class="pull-right">
					<button type="button" class="rectangle-button" title="Show tutorial" id="show-tutorial-button">
						Tutorial
						<i class="fa fa-information" aria-hidden="true"></i>
					</button>
				</div><!-- pull-right -->
			</div><!-- .modal-header -->

			
			
			<div class="modal-body container-fluid">
				<div class="player_wrap">
					<div id="player">
					</div>
					<br>
					<p class="quiz_hint"><i class="fa fa-info-circle" aria-hidden="true"></i> Play and pause the video to create a new quiz at the desired time point</p>
				</div>

				<div class="player_wrap">
					<div id="vimeoPlayer">
					</div>
					<br>
					<p class="quiz_hint"><i class="fa fa-info-circle" aria-hidden="true"></i> Play and pause the video to create a new quiz at the desired time point</p>
				</div>



				<br>
				<div class="container-fluid create_new_quiz_wrap" style="display:none;">
					<h2><i class="fa fa-plus-square" aria-hidden="true"></i> <span id='create_quiz_time_title'>CREATE NEW QUIZ @</span> <span id='create_quiz_time'></span></h2>

					<div class="row">
						<div class="container-fluid col-md-4 question_warp">
							<ul>
								<div><span class="title">Question List</span><i class="fa fa-caret-down" aria-hidden="true" id="quiz_list_toggle_btn"></i></div>
								<ul> 
									<!-- <li>Question 01<i class="fa fa-trash-o" aria-hidden="true"></i></li>
									<li>Question 20<i class="fa fa-trash-o" aria-hidden="true"></i></li> -->
								</ul>
							</ul>	
						</div>

						<div class="container-fluid col-md-8 create_block_warp">	
							<div class="form-heading">Select which type of question you wish to create. </div>
							<ul  class="nav nav-pills">
								<li class="active">
									<a  href="#1a" data-toggle="tab">MULTIPLE CHOICE</a>
								</li>
								<li>
									<a href="#2a" data-toggle="tab">Short Answer Question</a>
								</li>
							</ul>
							<div class="">
								Note: multiple choice questions will be automatically marked and short answer questions require students to self-check their answer with the correct answer that you will provide.
							</div>

							<div class="tab-content clearfix">
								<div class="tab-pane active" id="1a">
									<div class="form-group">
										<div class="form-heading">Write your question text below.</div>
										<textarea class="quiz_text_area" rows="2"></textarea>
									</div><!-- form-group -->
									<div class="form-group">
										<div class="form-heading">Select 'Add Answer' to write answer options to the question.</div>
										<div class="">Note: you can have any number of answers for each question. Only one answer can be correct.</div>

										<table class="quiz_options_wrap" id="quiz_options_wrap">
											<tbody>
												<!-- <tr>
													<td>&nbsp;&nbsp; A : &nbsp;&nbsp; </td>
													<td><input type="text" value=""></input></td>
												</tr> -->
											</tbody>
										</table>

										<button class="quiz_options_btn" id="quiz_options_add">Add Answer <i class="fa fa-plus-circle" aria-hidden="true"></i></button>
										<button class="quiz_options_btn" id="quiz_options_remove">Remove Answer <i class="fa fa-minus-circle" aria-hidden="true"></i></button>
									</div><!-- form-group -->

									<br />

									<button type="button" class="btn btn-link pull-left add_question_list_btn" id="1a_btn" blockType="multiple_choice">
										Add to question list <i class="fa fa-plus" aria-hidden="true"></i>
									</button>
									<button type="button" class="btn btn-link pull-left add_question_list_btn" id="1a_edit_btn" blockType="text" style="display:none;">
										Finish & Add to list <i class="fas fa-pen-square" aria-hidden="true"></i>
									</button>
									<button type="button" class="btn btn-link pull-left add_question_list_btn" id="1a_cancel_btn" blockType="text" style="display:none;">
										Cancel Edit <i class="fa fa-ban" aria-hidden="true"></i>
									</button>
								</div>
								<div class="tab-pane" id="2a">
									<div class="form-group">
										<div class="form-heading">Write your question text below.</div>
										<textarea class="quiz_text_area" rows="2" id="quiz_text_area_question"></textarea>
									</div><!-- form-group -->
									<div class="form-group">
										<div class="form-heading">Write the correct answer below.</div>
										<textarea class="quiz_text_area" rows="2" id="quiz_text_area_answer"></textarea>
									</div><!-- form-group -->
									<button type="button" class="btn btn-link pull-left add_question_list_btn" id="2a_btn" blockType="text">
										Add to question list <i class="fa fa-plus" aria-hidden="true"></i>
									</button>
									<button type="button" class="btn btn-link pull-left add_question_list_btn" id="2a_edit_btn" blockType="text" style="display:none;">
										Finish & Add to list <i class="fas fa-pen-square" aria-hidden="true"></i>
									</button>
									<button type="button" class="btn btn-link pull-left add_question_list_btn" id="2a_cancel_btn" blockType="text" style="display:none;">
										Cancel Edit <i class="fa fa-ban" aria-hidden="true"></i>
									</button>
								</div>
							</div>

						</div>
					</div>
	

				</div>

				<div class="container-fluid">
					<div class="question_preview_wrap" style="display:none;">
						<h2>Question preview</h2>
						<table>
							<tr>
								<th>Question type: </th>
								<th id="question_preview_type"></th>
							</tr>
							<tr>
								<th>Question: </th>
								<th id="question_preview_question"></th>
							</tr>
							<tr>
								<th>Choice options: </th>
								<th id="question_preview_option"></th>
							</tr>
							<tr>
								<th>Answer: </th>
								<th id="question_preview_answer"></th>
							</tr>
						</table>
					</div>
				</div>


				<div class="container-fluid">
					<div class="quiz_save_btn_wrap" style="display:none;"> 
						<div class="button_warp d-flex justify-content-center">
							<div class="pull-right tooltip-in-button">
								<i class="fa fa-question-circle gray-tooltip" data-toggle="tooltip" data-placement="left" title="By clicking on this button, you are saving your quiz to the quiz list but you must click on the Submit to OVAL button further below to save the quiz to OVAL as well."></i>
							</div>
							<button type="button" class="btn btn-link d-block mx-auto quiz_save_btn" id="quiz_save_btn" >
							</button>
						</div>
					</div><!-- button_warp -->
				</div><!-- container-fluid -->
				

				<div class="container-fluid quiz_warp">
					<h2><i class="fa fa-list" aria-hidden="true"></i> CURRENT QUIZ LIST </h2>
					<ul>
						<div><span class="title"> Quiz List &nbsp;&nbsp;</span><i class="fa fa-caret-down" aria-hidden="true" id="question_list_toggle_btn"></i></div>
						<ul> 

						</ul>
					</ul>
				</div>

				<div class="container-fluid">
					<div class="quiz_preview_wrap" style="display:none;">
						<h2>Quiz list preview</h2>
						<div class="quiz_preview_inner">
							<div class="row">
								<div class="col-xs-12 col-md-2"><h4>Quiz Name</h4></div>
								<div class="col-xs-12 col-md-10" id="quiz_preview_name">right</div>
							</div><!-- row -->
							<div class="row">
								<div class="col-xs-12 col-md-2"><h4>Quiz Stop Time Point</h4></div>
								<div class="col-xs-12 col-md-10" id="quiz_preview_stop">right</div>
							</div><!-- row -->
							<div class="row">
								<div class="col-xs-12"><h4>Quiz Details</h4></div>
								<div class="table-responsive col-xs-12">
									<table border="1" cellpadding="20" id="quiz_preview_details">
									</table>
								</div><!-- table-responsive -->
							</div><!-- row -->

							<!-- table-responsive -->
						</div><!-- quiz_preview_inner -->
					</div><!-- quiz_preview_wrap -->
				</div><!-- container-fluid -->

				<div class="container-fluid">
					<div class="button_warp d-flex justify-content-center">
						<div class="pull-right tooltip-in-button">
							<i class="fa fa-question-circle gray-tooltip" data-toggle="tooltip" data-placement="left" title="By clicking on the Submit to OVAL button, you are submitting your quiz and quiz questions to OVAL. Without clicking on this button your quiz will not be saved to OVAL."></i>
						</div>
						<button type="button" class="btn btn-link d-block mx-auto quiz_submit_btn" id="quiz_submit_btn">
							SUBMIT TO OVAL <i class="fa fa-upload" aria-hidden="true"></i> 
						</button>
					</div>
				</div>

			</div><!-- .modal-body -->
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .#modal-form -->

<div id="pop_out_dialog" class="modal fade" tabindex="-1" style="display: none;">
	<div class="modal-dialog" role="document" style="width:80%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" id="close_to_use">×</button>
				<h4 class="modal-title">Quiz Setup Tutorial</h4>
			</div>

			<div>
				<button type="button" class="btn btn-link d-block mx-auto quiz_submit_btn" id="skip_tutorial">Skip Tutorial <i class="fa fa-ban" aria-hidden="true"></i></button>
			</div>
			
			<div class="modal-body" id="pop_out_dialog_body">
                <h4 style='font-weight:bolder'>You can have multiple quizzes per video and multiple quiz questions per quiz. The instructions below are to setup each quiz.</h4>
				
				<h4 style='font-weight:bolder'>1. Play and then pause the video to setup each quiz</h4>
                <p>SECTION A: shows the video preview window, SECTION B: shows the quiz creation window</p>
                <p>Please pause the video at the specific time you wish the quiz to be displayed to students, (such as at 10th second), then, SECTION B will automatically display.</p>
                <p>
					In SECTION B, you are able to setup two types of questions for each quiz, 
					multiple choice (automatically marked) 
					and short answer (students can self-check against a correct answer that you provide). 
					<br />
					Note: these quizzes can only be used as formative assessment as their scores are not returned to the Moodle Gradebook. 
					Students also have an unlimited number of attempts to complete the quizzes and without any time restriction. 
					Teaching staff can view students' scores and the number of attempts by going to the OVAL Analytics page.
				</p>
				<div class='img_wrap'><img src='{{ URL::secureAsset('img/instruction_1.png') }}'/></div>
				
				<h4 style='font-weight:bolder'>2. Submit Quiz at a specified time </h4>
                <p>SECTION C: shows the question list, SECTION D: shows the Save Quiz button</p>
                <p>You can use the question list displayed in SECTION C to preview the current list of questions and make any edits.</p>
				<p>
					Press the Save Quiz button displayed in SECTION D to save the quiz questions and answers. 
					<br />
					Note: This does not submit or save the quiz to OVAL. 
					To submit the quiz to OVAL, make sure to click on the Submit to OVAL button after you have selected the Save Quiz button.
				</p>
				<div class='img_wrap'><img src='{{ URL::secureAsset('img/instruction_2.png') }}' /></div>
				
				<h4 style='font-weight:bolder'>3. Submit the Quiz to OVAL</h4>
				<p>SECTION E: shows the quiz list, SECTION F: shows the Submit to OVAL button</p>
				<p>You can use the quiz list displayed in SECTION E to review each quiz associated with the video.</p>
				<p>
					<strong>IMPORTANT:</strong>
					Do not forget to press the Submit to OVAL button displayed in SECTION F to submit your quiz to OVAL, 
					if you do not press this button, you may lose your quiz or quiz questions.
				</p>
				<div class='img_wrap'><img src='{{ URL::secureAsset('img/instruction_3.png') }}' /></div>
			</div>
			<div>
				<button type="button" id="close_to_use" class="btn btn-link d-block mx-auto quiz_submit_btn" >
					OK, start to use &nbsp;&nbsp;&nbsp;<i class="fa fa-play" aria-hidden="true"></i> 
				</button>
			</div>
		</div>
	</div>
</div>
<!-- End Quiz Creation Modal content-->

<div id="alert_dialog" class="modal fade" tabindex="-1" style="display: none;">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Notification</h4>
			</div>
			<div class="modal-body" id="alert_dialog_content">
				
			</div>
			<div class="d-flex justify-content-center">
				<button type="button" class="btn btn-link d-block mx-auto quiz_submit_btn" onclick="$('#alert_dialog').modal('hide');">
					OK, I got it. &nbsp;&nbsp;&nbsp;<i class="fa fa-thumbs-up" aria-hidden="true"></i>
				</button>
			</div>
		</div>
	</div>
</div>

<div id="confirm_dialog" class="modal fade" tabindex="-1" style="display: none;">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Notification</h4>
			</div>
			<div class="modal-body" id="confirm_dialog_content">
				
			</div>
			<div class="modal-body">
				<button type="button" class="btn btn-link d-block mx-auto" id="confirm_delete" onclick="$('#confirm_dialog').modal('hide');">
					Continue
				</button>
				<button type="button" class="btn btn-link d-block mx-auto" onclick="$('#confirm_dialog').modal('hide');">
					Cancel
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="visibility-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<h4 class="modal-title" id="modalLabel">EDIT VIDEO VISIBILITY</h4>
			</div><!-- .modal-header -->
			
			<div class="modal-body container-fluid">
				<form id="visibility-form">
					<input type="hidden" id="group-video-id">
					<div class="form-group">
						<div>Set visibility to</div>
						<label class="radio-inline">
							<input type="radio" name="visibility-radio" id="visible-radio" value="0">Visible
						</label>
						<label class="radio-inline left-indent">
							<input type="radio" name="visibility-radio" id="hidden-radio" value="1">Hidden
						</label>
					</div><!-- form-group -->
					<div class="form-buttons">
						<button type="submit" class="btn btn-link d-block mx-auto" id="save-visibility"><i class="far fa-save" aria-hidden="true"></i></button>
					</div><!-- form-buttons -->
				</form>
			</div><!-- modal-body -->
		</div><!-- modal-content -->
	</div><!-- modal-dialog -->
</div><!-- #visibility-form -->

<div class="modal fade" id="order-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<h4 class="modal-title" id="modalLabel">EDIT VIDEO LIST</h4>
			</div><!-- .modal-header -->
			
			<div class="modal-body container-fluid">
				<div class="col">
					Edit the order videos appear in the list.
				</div>
				<form id="order-form">
					<ul id="video-order-list" class="sortable-list">
						@foreach ($group_videos->sortBy('order') as $gv)
						<li data-id="{{$gv->id}}">{{$gv->video()->title}}</li>
						@endforeach
					</ul>
					<div class="form-buttons">
						<button type="submit" class="btn btn-link d-block mx-auto" id="save-order"><i class="far fa-save" aria-hidden="true"></i></button>
					</div><!-- form-buttons -->
				</form>
			</div><!-- modal-body -->
		</div><!-- modal-content -->
	</div><!-- modal-dialog -->
</div><!-- #order-form -->

<div class="modal fade" id="text-analysis-modal" tabindex="-1" role="dialog" aria-labelledby="text-analysis-title" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<h4 class="modal-title" id="text-analysis-title">EDIT VISIBILITY OF RELATED CONTENTS</h4>
			</div><!-- .modal-header -->
			
			<div class="modal-body container-fluid">
				<form id="text-analysis-form">
					<input type="hidden" class="group-video-id">
					<div class="form-group">
						<div class="instruction">Set visibility to</div>
						<label class="radio-inline" for="show-radio">
							<input type="radio" name="analysis-vis-radio" id="show-radio" value="1">
							<span class="radio-text">Visible</span>
						</label>
						<label class="radio-inline left-indent" for="hide-radio">
							<input type="radio" name="analysis-vis-radio" id="hide-radio" value="0">
							<span class="radio-text">Hidden</span>
						</label>
					</div><!-- form-group -->
					<div class="form-buttons">
						<button type="submit" class="btn btn-link d-block mx-auto" id="save-analysis-vis"><i class="far fa-save" aria-hidden="true"></i></button>
					</div><!-- form-buttons -->
				</form>
			</div><!-- modal-body -->
		</div><!-- modal-content -->
	</div><!-- modal-dialog -->
</div><!-- #visibility-form -->

<div class="modal fade" id="transcript-form" tabindex="-1" role="dialog" aria-labelledby="transcript-modal-title" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<h4 class="modal-title" id="transcript-modal-title">UPLOAD TRANSCRIPT</h4>
			</div><!-- .modal-header -->
			
			<div class="modal-body container-fluid">
				<form id="upload-transcript-form" method="POST" action="/upload_transcript" enctype="multipart/form-data" role="form" data-toggle="validator">
					{{ csrf_field() }}
					<input type="hidden" name="video_id" />
					<div class="form-group">
						<label for="transcript-file">Please select file to upload...</label>
						<input type="file" id="transcript-file" name="file" data-filetype="srt" data-required-error="Please select a file in .srt format" required>
						<div class="help-block with-errors"></div>
					</div><!-- form-group -->
					
					<div class="instruction space-top">
						<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
						If a transcript file already exists for this video, it will be overwritten.
					</div>
					<div class="form-buttons">
						<button type="submit" class="btn btn-link d-block mx-auto" id="upload">
							<i class="fa fa-upload" aria-hidden="true"></i>
						</button>
					</div><!-- form-buttons -->
				</form>
				
			</div><!-- modal-body -->				
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .#modal-form -->

<div class="modal fade" id="edit-keywords-modal" tabindex="-1" role="dialog" aria-labelledby="edit-keywords-modal-title" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<h4 class="modal-title" id="edit-keywords-modal-title">EDIT KEYWORDS</h4>
			</div><!-- .modal-header -->
			
			<div class="modal-body container-fluid">
				<form id="edit-keywords-form">
					<div class="form-group">
						<table class="col-sm-10 col-sm-offset-1 table-hover">
							<thead>
								<tr>
									<th>Word / Phrase</th>
									<th class="text-center">Show</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div><!--form-group-->

					<div class="form-buttons">
						<button type="submit" class="btn btn-link d-block mx-auto" id="update-keywords">
							<i class="far fa-save" aria-hidden="true"></i>
						</button>
					</div><!-- form-buttons -->
				</form>
				
			</div><!-- modal-body -->				
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .#modal-form -->

<div class="modal fade" id="modal-assigned-group-list" tabindex="-1" role="dialog" aria-labelledby="assigned-groups-modal-title" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<h4 class="modal-title" id="assigned-groups-modal-title">ASSIGNED GROUPS</h4>
			</div><!-- .modal-header -->
			
			<div class="modal-body container-fluid">

				<div class="row">
					<div class="btn-group col form-group">
						<button type="button" class="btn dropdown-button dropdown-left">Course</button>
						<button type="button" class="btn dropdown-button dropdown-center" id="assigned-groups-course-name" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false"></button>
						<button type="button" class="btn dropdown-button dropdown-toggle" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
							<!-- <span class="fa fa-caret-down"></span> -->
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu" id="assigned-groups-course-ul">
							@if (count($user->coursesTeaching())==0)
							<li>NO COURSES</li>
							@else
							@foreach ($user->coursesTeaching() as $c)
							<li data-id="{{$c->id}}">{{ $c->name }}</li>
							@endforeach
							@endif
						</ul><!-- .dropdown-menu -->
					</div><!-- btn-group -->
				</div><!-- row -->

				<table id="assigned-group-table" class="table table-hover">
					<thead>
						<tr>
							<th class="col-xs-6">Group Name</th>
							<th class="col-xs-2 text-center">Comment Instruction</th>
							<th class="col-xs-2 text-center">Points</th>
							<th class="col-xs-2 text-center">Quiz</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>


			</div><!-- modal-body -->				
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .#modal-form -->

<!-- add tag management group -->
<div id="covaa-dialog" class="modal fade" tabindex="-1" style="display: none;">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<h4 class="modal-title" id="modal-covaa-tag-title">Tag Groups</h4>
			</div>

			<div class="modal-body container-fluid">
				<form>
					<div id="my-select-root">
						<select multiple="multiple" id="my-select">
						</select>
					</div>
													
					<div class="form-buttons">
						<button type="button" class="btn btn-link" id="save-tag-group"><i class="far fa-save"></i></button>
						<button type="button" class="btn btn-link pull-right" id="delete-tag-group"><i class="fas fa-redo"></i></button>
					</div>
				</form>			
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-points-form" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<h4 class="modal-title" id="modal-points-title">Edit Points</h4>
			</div><!-- .modal-header -->
			
			
			<div class="modal-body container-fluid">
				<form>
					
					
					
					
					<div class="container form-group">
						<div class="col-xs-9">
							<div class="row">
								<div class="col-xs-2 bold">Video:</div>
								<div class="col-xs-10" id="points-form-video-title"></div>
							</div><!-- row -->
							@if (config("settings.course_wide.point")==1)
							<div class="row">
								<div class="col-xs-2 bold">Course:</div>
								<div class="col-xs-10" id="points-form-course-name"></div>
							</div><!-- row -->
							@else
							<div class="row">
								<div class="col-xs-2 bold">Course:</div>
								<div class="col-xs-10" id="points-form-course-name"></div>
							</div><!-- row -->
							<div class="row">
								<div class="col-xs-2 bold">Group:</div>
								<div class="col-xs-10" id="points-form-course-name"></div>
							</div><!-- row -->
							@endif

						</div><!-- col -->
						<div class="col-xs-3">
							<img id="points-form-thumbnail-img" src="" class="video-thumbnail">
						</div><!-- col -->
					</div><!-- row -->
					
					
					
					<div class="container form-group space-top-30">
						<div class="col-xs-12">
							<fieldset class="space-bottom">
								<label for="modal-point-instruction">Instruction for students:</label>
								<input id="modal-point-instruction" class="form-control" placeholder="Enter instruction for students..." type="text">
							</fieldset>
							<fieldset id="modal-points">
								<label>Points:</label>
								<div class="row">
									<div class="col-xs-10">
										<input class="form-control new-point" type="text" placeholder="Enter Video Key Point text...">
									</div><!-- col-xs-10 -->
									<div class="col-xs-2">
										<button class="modal-delete-point btn-sm outline-button full-width" type="button" title="delete">
											<span class="hidden-xs">Delete</span>
											<i class="fa fa-minus-circle"></i>
										</button>
									</div><!-- col-xs-2 -->
								</div><!-- row -->
							</fieldset>
							<button id="modal-another-point" class="btn-sm outline-button" type="button" title="Add another point">
								Add another point
								<i class="fa fa-plus-circle"></i>
							</button>
						</div><!-- points-controls -->
					</div><!-- row -->		
					
						
										
					<div class="form-buttons">
						<button type="button" class="btn btn-link" id="save-points"><i class="far fa-save"></i></button>
						<button type="button" class="btn btn-link pull-right" id="delete-points"><i class="fa fa-trash"></i></button>
					</div><!-- form-group -->
					
				</form>
				
			</div><!-- .modal-body -->
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .#modal-form -->

<div class="modal fade" id="edit-title-modal" tabindex="-1" role="dialog" aria-labelledby="edit-title-modal-title" aria-hidden="true"> 
  <div class="modal-dialog" role="document"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
          <i class="fa fa-close" aria-hidden="true"></i> 
        </button> 
        <h4 class="modal-title" id="edit-title-modal-title">EDIT VIDEO TITLE</h4> 
 
        <div class="row"> 
          <div class="col-xs-9"> 
            <div class="row"> 
              <div class="col-xs-2 bold">Video:</div> 
              <div class="col-xs-10" id="title-modal-video-title"></div> 
            </div><!-- row --> 
            <div class="row"> 
              <div class="col-xs-2 bold">Course:</div> 
              <div class="col-xs-10" id="title-modal-course-name"></div> 
            </div><!-- row --> 
          </div><!-- col --> 
          <div class="col-xs-3"> 
            <img id="title-modal-thumbnail-img" src="" class="video-thumbnail"> 
          </div><!-- col --> 
        </div><!-- row --> 
      </div><!-- .modal-header --> 
       
      <div class="modal-body container-fluid"> 
        <form id="edit-title-form"> 
          <div class="form-group"> 
            Enter title for this video. <br /> 
            This will be shown as the title for this course only, and different course using same video will not be affected. 
          </div> 
          <div class="form-group"> 
            <label for="edit-custom-title">Title</label> 
            <input type="text" class="form-control" id="edit-custom-title">  
          </div><!--form-group--> 
 
          <div class="form-buttons"> 
            <button type="submit" class="btn btn-link d-block mx-auto" id="save-title" data-gvid=""> 
              <i class="fa fa-save" aria-hidden="true"></i> 
            </button> 
          </div><!-- form-buttons --> 
        </form> 
         
      </div><!-- modal-body -->         
    </div><!-- .modal-content --> 
  </div><!-- .modal-dialog --> 
</div><!-- .#modal-form -->



<div class="modal fade" id="modal-segments-form" tabindex="-1" role="dialog" aria-labelledby="modal-segments-title" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<h4 class="modal-title" id="modal-segments-title">Edit Segments</h4>
			</div><!-- .modal-header -->
			
			<div class="modal-body container-fluid">
				<form id="m-segments-form" data->
					<div class="row form-group">
						<div class="col-sm-8">
							<div class="row">
								<div class="col-2 bold">Video:</div>
								<div class="col" id="segments-form-video-title"></div>
							</div><!-- row -->
							
							<div class="row">
								<div class="col-2 bold">Course:</div>
								<div class="col" id="segments-form-course-name"></div>
							</div><!-- row -->
							<div class="row">
								<div class="col-2 bold">Group:</div>
								<div class="col" id="segments-form-group-name"></div>
							</div><!-- row -->
							

						</div><!-- col -->
						<div class="col-sm-4">
							<img id="segments-form-thumbnail-img" src="" class="video-thumbnail">
						</div><!-- col -->
					</div><!-- row -->
					
					<div class="container form-group space-top-30">
						<div id="m-segments-inputs">
							<fieldset class="row form-group mb-2">
									<div class="col-12 col-md-4 mb-1">
										<legend>Title</legend>
										<input class="form-control m-segment-title" type="text" placeholder="Title">
									</div>
									<div class="col-5 col-md-3">
										<legend>Start Time</legend>
										<input class="form-control m-segment-start" type="text" placeholder="0:00" data-start="start" required data-required-error="Please fill in start time.">
									</div>
									<div class="col-5 col-md-3">
										<legend>End Time</legend>
										<input class="form-control m-segment-end" type="text" placeholder="1:00" data-end="end" required data-required-error="Please fill in end time.">
									</div>
									<div class="col-2 mt-4">
										<button type="button" class="m-delete-segment outline-button btn-sm full-width" title="delete"><i class="fa fa-minus-circle"></i></button>
									</div>
									<div class="col-12 help-block with-errors"></div>
							</fieldset><!-- row -->
						</div><!-- #m-segments-inputs -->
							
							<button id="m-another-segment" class="btn-sm outline-button ml-2" type="button" title="Add another segment">
								Add another segment
								<i class="fa fa-plus-circle"></i>
							</button>
					</div><!-- row -->		
					
						
										
					<div class="form-buttons text-center">
						<button type="button" class="btn btn-link" id="save-segments"><i class="far fa-save"></i></button>
						<!-- <button type="button" class="btn btn-link pull-right" id="delete-segments"><i class="fa fa-trash"></i></button> -->
					</div><!-- form-group -->
					
				</form>
				
			</div><!-- .modal-body -->
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .#modal-form -->