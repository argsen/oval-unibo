
<div class="modal fade" id="edit-name-modal" tabindex="-1" role="dialog" aria-labelledby="edit-name-modal-title" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<h4 class="modal-title" id="edit-name-modal-title">EDIT GROUP NAME</h4>
			</div><!-- .modal-header -->
			
			<div class="modal-body container-fluid">
				<form id="edit-name-form" role="form" data-toggle="validator">
					<fieldset class="form-group no-border">
                        <label for="edit-grp-name-textbox">Group Name</label>
                        <input id="edit-grp-name-textbox" type="text"  class="form-control" required>
                        <div class="help-block with-errors"></div>
                    </fieldset>

					<div class="form-buttons">
						<button type="submit" class="btn btn-link d-block mx-auto" id="update-group-name">
							<i class="far fa-save" aria-hidden="true"></i>
						</button>
					</div><!-- form-buttons -->
				</form>
				
			</div><!-- modal-body -->				
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .#modal-form -->



<div class="modal fade" id="edit-members-modal" tabindex="-1" role="dialog" aria-labelledby="edit-members-modal-title" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
                <h4 class="modal-title" id="edit-members-modal-title">EDIT GROUP MEMBERS</h4>
                <div>Edit the members of Group "<span class="group-being-edited"></span>"</div>
			</div><!-- .modal-header -->
			
			<div class="modal-body container-fluid">
				<form id="edit-members-form">
					<fieldset class="form-group no-border">
                        <select id="edit-grp-members-select" multiple>
                        </select>
                    </fieldset>

					<div class="form-buttons">
						<button type="submit" class="btn btn-link d-block mx-auto" id="update-group-members">
							<i class="far fa-save" aria-hidden="true"></i>
						</button>
					</div><!-- form-buttons -->
				</form>
				
			</div><!-- modal-body -->				
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .#modal-form -->