<div class="navbar">


	<div class="d-flex">
		<div class="mr-auto covaa-logo"><a href="/"><img src="/img/OVAL-IconSmallNoFill01.png" alt="logo"></a></div>	
		<div class="covaa-logo-title"></div>
	</div>

	<div class="d-flex">
		<div class="">
			<div class="dropdown">
				@if (Auth::user()) 

					@if (Auth::user()->isAnInstructor() || Auth::user()->role=='A')
					@else 
					@include('parts.menu-icon-with-badge') 
					@endif

					<button class="navbar-button" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fas fa-bars"></i>
					</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<li class= "dropdown-item"><a href="/"> <i class="fas fa-home"></i>  Home</a></li>
						<!-- <li class= "dropdown-item"><a href="/surveys"> <i class="fas fa-question"></i>  Survey</a></li> -->
						<!-- <li class= "dropdown-item"><a href="/studentDash"> <i class="fas fa-tachometer-alt"></i>  Dashbroad</a></li> -->
					@if (!empty(Auth::user()))	
						
						@if(Auth::user()->isAnInstructor())
						<li class= "dropdown-item"><a href="/video-management"><i class="fas fa-video"></i>  Video Management</a></li>
						<li class= "dropdown-item"><a href="/tag-management"><i class="fas fa-tags"></i>  Tag Management</a></li>
						<li class= "dropdown-item"><a href="/manage-notifications"><i class="fas fa-bell"></i> Notification Management</a></li>
						@endif
						@if(Auth::user()->isAnInstructor() || Auth::user()->role==="A")
						<li class= "dropdown-item"><a href="/manage-groups"><i class="fas fa-users-cog"></i> Manage Groups</a></li>
						@endif
						@if(Auth::user()->role=="A") 
						<li class= "dropdown-item"><a href="/manage-analysis-requests"><i class="fab fa-telegram-plane"></i> Manage Content Analysis Requests</a></li>
						<li class="dropdown-item"><a href="/manage-vimeo-tokens"><i class="fab fa-vimeo-square"></i> Manage Vimeo Token</li>
						<li class= "dropdown-item"><a href="/register"><i class="far fa-plus-square"></i>  Add an admin user</a></li>
						@endif
						@if(Auth::user()->isAnInstructor())
						<li class= "dropdown-item"><a href="/analytics"><i class="fas fa-chart-line"></i>  Analytics</a></li>
						@endif
						@if(Auth::user()->role=="A") 
						<li class= "dropdown-item"><a href="/batch-upload"><i class="fas fa-upload"></i>  Batch Upload</a></li>
						<li class= "dropdown-item"><a href="/manage-lti-connections"><i class="fas fa-sitemap"></i> Manage LTI Connections</a></li>
						@endif


						<li class= "dropdown-item"><a href="/logout"><i class="fas fa-sign-out-alt"></i>  Logout</a></li>
					@endif
					</div>


				@endif

			</div>
		</div>
		<div class="">
			@if (Auth::user())
				<span class="greetings" userid="{{isset($user) ? $user->id() : ''}}" isinstructor = "{{isset($user) ? $user->isAnInstructor() : ''}}">{{ Auth::user()->fullName() }}</span>
			@if (strpos(url()->current(), "/view")!==false && !empty($user) && $user->enrolledTerms()->count()>0)
				<select id="term-select">
					@foreach($user->enrolledTerms() as $t)
					<option value="{{$t->term_name}}" data-year="{{$t->year}}" data-term="{{$t->term}}">{{$t->term_name}}</option>
					@endforeach
				</select>
			@endif {{-- enrolledTerms --}}
			@endif {{-- if auth --}}
		</div>
	</div>




</div><!-- .navbar -->