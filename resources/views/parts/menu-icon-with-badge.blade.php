<button class="icon-wrap navbar-button" id="covaa-notification-show"> 
    <i class="far fa-bell"></i>

    @if(!empty($received_notifications) && $received_notifications->where('completed', '=', false)->count() > 0) 
    <span class="badge covaa-notification-badge"> 
        {{$received_notifications->where('completed', '=', false)->count()}}  
    </span> 
    @endif
</button><!-- icon-wrap -->