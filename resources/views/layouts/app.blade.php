<!DOCTYPE html>
<html lang="en">
	<head>
		@include('parts.head')
	</head>

	<body>

		@include('parts.sidebar')
		
		<div class="canvas">
			@include('parts.navbar')  

			@yield('selection_bar')    

			@yield('content')

			@include('parts.loading')
			
			@include('parts.footer')

			@include('parts.scripts')

		</div>

		@yield('modal')
	
		@yield('javascript')
    
		<script>
			$(document).ready(function(){
				var is_home_page = window.location.href.split("/")[3] === "view" ? true : false;
				if(is_home_page){
					$("#mask-loading").hide();
				}else{
					$("#mask-loading").hide();

					$(document).ajaxStart(function(){
						$("#mask-loading").show();
					});

					$(document).ajaxStop(function(){
						$("#mask-loading").hide();
					});
				}
			});
		</script>
	
</html>





