<h1>Welcome to CoVAA</h1>
<br />

<p>Your account has been automatically activatied on <a href="{{ env('APP_URL') }}">CoVAA</a> with your current Email address and the password below.</p>
<p>{{ $password }}</p>

<p>You can always change your password <a href="{{ env('APP_URL') . '/reset_password' }}">here</a>.

<p>Enjoy and have fun!</p>

<br />
<p>CoVAA Team</p>