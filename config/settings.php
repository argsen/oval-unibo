<?php
/** 
 * This file contains configuration values for OVAL.
 * 
 * Settings for OVAL application is stored here. 
 * Values that depends on server environment or sensitive data are placed in .env file.
 * 
 **/

return [

    /** -----------------------------------------------------
 * Application default settings.
 * -----------------------------------------------------
 * group_video_hide : default value for group_video.hide (if set to true, it is not visible to students)
 * group_video_show_analysis : default value for group_video.show_analysis (if set to true, shows text analysis result)
 * 
 */
    "defaults" => [
        "group_video_hide" => 1,
        "group_video_show_analysis" => 1,

    ],

    /**
     * Course wide objects
     * 
     * Objects set to "true" are course-wide. (All groups in same course with same video share the course-wide objects)
     */
    "course_wide" => [
        "comment_instruction" => 1,
        "point" => 1,
        "quiz" => 1,
    ],

    /**
     * Media source types
     * 
     * Origin of videos (eg. youtube) to be used.
     * key: "media_type" of "video", value: label is for display, js is the name of file
     * Need JS file in public/js/media to handle the controls of video player etc
     */
    "video_source" => [
        "youtube"=>["label"=>"YouTube", "js"=>"youtube-functions.js", "url"=>"https://youtube.com"],
        "vimeo"=>["label"=>"Vimeo", "js"=>"vimeo-functions.js", "url"=>"https://vimeo.com/watch"],
        // "helix"=>["label"=>"UniSA Media Library", "js"=>"jwplayer-functions.js", "url"=>env('HELIX_SERVER_HOST')]
    ],

    /**
     * Languages to check for Youtube transcript existance
     * 
     * When checking for publicly obtainable transcript of YouTube video, 
     * we will check for languages listed here.
     */
    "youtube_transcript_lang" => ['en', 'en-GB'],


    /**
     * Rating Ratio
     * 
     * For calcuration of ratings for recommended resources 
     * so that instructor and student votes can have different rates.
     */
    "rating_ratio" => [
        "instructor"=> 2,
        "student"=> 1,
    ],

    /** 
     * Custom title for GroupVideo 
     *  
     * Allow instructor to set title different from the one the video has  
     * on its original source if set to "true". 
     */ 
    "custom_video_title" => true,

    /** 
     * HTML tags allowed for notification text 
     *  
     * html_allow_tags is used as parameter for strip_tag() function(php). 
     * trumbowyg_btns is used as parameter for setting up trubowyg editor's buttons. 
     * @see https://alex-d.github.io/Trumbowyg/ 
     */ 
    "html_allow_tags"=>"<a><i><b><strong>", 
    "notification_allow_html" => [ 
        "html_allow_tags"=>"<a><i><b><strong><em><del>", 
        "trumbowyg_btns" => [['viewHTML'], ['strong', 'em', 'del'], ['link']] 
    ],
 
    "google_api_key"=>env('GOOGLE_API_KEY')
];