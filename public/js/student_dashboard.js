function round(value, decimals) {
	return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

var selectedSubject = "";
var selectedClass = "";
var selectedClassName = "";
//var userID = "<?php echo $userID;?>";
//var userID = 12; //hardcoded for testing.
var selectedStudents;
var selectedVideo;
var sna_links;
var sna_weights;
var sna_strengths;
var total_students_count = 0;
var logged_in = 0;
var never_logged_in = 0;

var watched_video = 0;
var never_watched_video = 0;
var comments = 0;
var annotations = 0;
var replies = 0;

//thinking skills
var ideate =0;
var justify =0;
var agree = 0;
var disagree = 0
var inquire = 0
var totalqnsNo=0;
var completedQnsTracker = [];

/* 21CC Profile variables*/
var SelfEfficacy = 0;
var TaskValues = 0;
var Behave_Cog_EmoEngagement = 0;
var AcadSelfConcept = 0;
var TopSelfEfficacy = 0;
var TopTaskValues = 0;
var TopBehave_Cog_EmoEngagement = 0;
var TopAcadSelfConcept = 0;
var AvgSelfEfficacy = 0;
var AvgTaskValues = 0;
var AvgBehave_Cog_EmoEngagement = 0;
var AvgAcadSelfConcept = 0;
var Creativity = 0;
var Curiosity = 0;
var CriticalThinking = 0;
var Collaboration = 0;
var SelfDirectedLearning = 0;
var TopCreativity = 0;
var TopCuriosity = 0;
var TopCriticalThinking = 0;
var TopCollaboration = 0;
var TopSelfDirectedLearning = 0;
var AvgCreativity = 0;
var AvgCuriosity = 0;
var AvgCriticalThinking = 0;
var AvgCollaboration = 0;
var AvgSelfDirectedLearning = 0;
var PerformanceGoals = 0;
var LearningGoals = 0;
var SocialGoal = 0;
var SurfaceLearning =0;
var DeepLearning =0;
var AvgPerformanceGoals =0;
var TopSurfaceLearning =0;
var AvgLearningGoals =0;
var AvgSocialGoal =0;
var TopDeepLearning =0;
var AvgDeepLearning =0;
var TopPerformanceGoals =0;
var TopLearningGoals =0;
var TopSocialGoal =0;

var PostSelfEfficacy = 0;
var PostTaskValues = 0;
var PostBehave_Cog_EmoEngagement = 0;
var PostAcadSelfConcept = 0;
var PostTopSelfEfficacy = 0;
var PostTopTaskValues = 0;
var PostTopBehave_Cog_EmoEngagement = 0;
var PostTopAcadSelfConcept = 0;
var PostAvgSelfEfficacy = 0;
var PostAvgTaskValues = 0;
var PostAvgBehave_Cog_EmoEngagement = 0;
var PostAvgAcadSelfConcept = 0;
var PostCreativity = 0;
var PostCuriosity = 0;
var PostCriticalThinking = 0;
var PostCollaboration = 0;
var PostSelfDirectedLearning = 0;
var PostTopCreativity = 0;
var PostTopCuriosity = 0;
var PostTopCriticalThinking = 0;
var PostTopCollaboration = 0;
var PostTopSelfDirectedLearning = 0;
var PostAvgCreativity = 0;
var PostAvgCuriosity = 0;
var PostAvgCriticalThinking = 0;
var PostAvgCollaboration = 0;
var PostAvgSelfDirectedLearning = 0;
var PostTopPerformanceGoals = 0;
var PostAvgPerformanceGoals = 0;
var PostPerformanceGoals = 0;
var PostTopLearningGoals = 0;
var PostAvgLearningGoals = 0;
var PostLearningGoals = 0;
var PostAvgSocialGoal = 0;
var PostTopSocialGoal = 0;
var PostSocialGoal = 0;
var PostTopSurfaceLearning =0;
var PostAvgSurfaceLearning =0;
var PostSurfaceLearning =0;
var PostDeepLearning =0;
var PostTopDeepLearning =0;
var PostAvgDeepLearning =0;

var role ='O'; //testing purpose
/* For 21cc slider chart look and feel */
var twoColComp = {
  init: function (){
    var tables = document.getElementsByClassName('slider-chart');
    // for each table
    for(var i = 0; i < tables.length; i++) {
      // don't process one that's already been done (has class two-column-comp)
      if (new RegExp('(^| )two-column-comp( |$)', 'gi').test(tables[i].className)){
         return;
      }
      //TODO: need to verify cross-browser support of these vars
      var h = tables[i].clientHeight,
          t = tables[i].getBoundingClientRect().top,
          wT = window.pageYOffset || document.documentElement.scrollTop,
          wH = window.innerHeight;
      if(wT + wH > t + h/2){
         this.make(tables[i]);
       }
    }
  },

  make : function(el){
    var rows = el.getElementsByTagName('tr'),
        vals = [],
        max,
        percent;
    // for each row in the table, get vals
    for(var x = 0; x < rows.length; x++) {
      var cells = rows[x].getElementsByTagName('td');
      for(var y = 1; y < cells.length; y++){
        vals.push(parseFloat(cells[y].innerHTML, 10));
      }
    }
    //max = Math.max.apply( Math, vals );
    max = 7;
    percent = 100/max;
    //for each row in the table, apply vals
    for(x = 0; x < rows.length; x++) {
      var cells = rows[x].getElementsByTagName('td');
      for(var y = 1; y < cells.length; y++){
        var currNum = parseFloat(cells[y].innerHTML, 10);
        cells[y].style.backgroundSize = percent * currNum + "% 100%";
        cells[y].style.transitionDelay = x/20 + "s";
      }
    }
    //add a class so you dont process it a bunch of times
    el.className = "two-column-comp";
  } // end make
}
/* End For 21cc slider chart look and feel */
$(document).ready(function(){

	//add manually sync button
	if(is_admin){
		$('#manual-sync').show();
		$('#manual-sync').on('click', function(){
			$.get('/manualSync')
			 .done(function(data){

				if(data === 'success'){
					alert('Thanks, the covaa submission table synchronise start.');
					$('#manual-sync').attr("disabled", "disabled");
					$('#manual-sync').text("Synchronise Completed!");

				}else{
					window.location.reload();
				}
			 });
		});
	}else{
		$('#manual-sync').hide();
		$('#manual-sync').off('click')
	}

	//21CC Profile Variables
		SelfEfficacy =0;
		TaskValues = 0;
		Behave_Cog_EmoEngagement = 0;
		AcadSelfConcept = 0;
		TopSelfEfficacy = 0;
		TopTaskValues = 0;
		TopBehave_Cog_EmoEngagement = 0;
		TopAcadSelfConcept = 0;
		AvgSelfEfficacy = 0;
		AvgTaskValues = 0;
		AvgBehave_Cog_EmoEngagement = 0;
		AvgAcadSelfConcept = 0;
		Creativity = 0;
		Curiosity = 0;
		CriticalThinking =0;
		Collaboration = 0;
		SelfDirectedLearning = 0;
		TopCreativity = 0;
		TopCuriosity = 0;
		TopCriticalThinking=0;
		TopCollaboration = 0;
		TopSelfDirectedLearning = 0;
		AvgCreativity = 0;
		AvgCuriosity = 0;
		AvgCriticalThinking=0;
		AvgCollaboration = 0;
		AvgSelfDirectedLearning = 0;
		PerformanceGoals = 0;
		LearningGoals = 0;
		SocialGoal = 0;
		SurfaceLearning =0;
		DeepLearning =0;
		TopPerformanceGoals = 0;
		TopLearningGoals = 0;
		TopSocialGoal = 0;
		TopSurfaceLearning =0;
		TopDeepLearning =0;
		AvgPerformanceGoals = 0;
		AvgLearningGoals = 0;
		AvgSocialGoal = 0;
		AvgSurfaceLearning =0;
		AvgDeepLearning =0;

		PostSelfEfficacy = 0;
		PostTaskValues = 0;
		PostBehave_Cog_EmoEngagement = 0;
		PostAcadSelfConcept = 0;
		PostTopSelfEfficacy = 0;
		PostTopTaskValues = 0;
		PostTopBehave_Cog_EmoEngagement = 0;
		PostTopAcadSelfConcept = 0;
		PostAvgSelfEfficacy = 0;
		PostAvgTaskValues = 0;
		PostAvgBehave_Cog_EmoEngagement = 0;
		PostAvgAcadSelfConcept = 0;
		PostCreativity = 0;
		PostCuriosity = 0;
		PostCriticalThinking = 0;
		PostCollaboration = 0;
		PostSelfDirectedLearning = 0;
		PostTopCreativity = 0;
		PostTopCuriosity = 0;
		PostTopCriticalThinking = 0;
		PostTopCollaboration = 0;
		PostTopSelfDirectedLearning = 0;
		PostAvgCreativity = 0;
		PostAvgCuriosity = 0;
		PostAvgCriticalThinking = 0;
		PostAvgCollaboration = 0;
		PostAvgSelfDirectedLearning = 0;
		PostPerformanceGoals = 0;
		PostLearningGoals = 0;
		PostSocialGoal = 0;
		PostSurfaceLearning =0;
		PostDeepLearning =0;

		var length= 0;
		var length1= 0;
	$.each(allsurvey, function( i, l ){

		if(selectedClassName==allsurvey[i]["class"]){
			length++;

			if (parseFloat(allsurvey[i]["cal"]["SelfEfficacy"]) > TopSelfEfficacy){
				TopSelfEfficacy = parseFloat(allsurvey[i]["cal"]["SelfEfficacy"]);
			}
			if (parseFloat(allsurvey[i]["cal"]["TaskValues"]) > TopTaskValues){
				TopTaskValues = parseFloat(allsurvey[i]["cal"]["TaskValues"]);
			}
			if (parseFloat(allsurvey[i]["cal"]["Behave_Cog_EmoEngagement"])> TopBehave_Cog_EmoEngagement){
				TopBehave_Cog_EmoEngagement = parseFloat(allsurvey[i]["cal"]["Behave_Cog_EmoEngagement"]);
			}
			if (parseFloat(allsurvey[i]["cal"]["AcadSelfConcept"])> TopAcadSelfConcept){
				TopAcadSelfConcept = parseFloat(allsurvey[i]["cal"]["AcadSelfConcept"]);
			}
			if (parseFloat(allsurvey[i]["cal"]["Creativity"]) > TopCreativity){
				TopCreativity = parseFloat(allsurvey[i]["cal"]["Creativity"]);
			}
			if (parseFloat(allsurvey[i]["cal"]["Curiosity"]) > TopCuriosity){
				TopCuriosity = parseFloat(allsurvey[i]["cal"]["Curiosity"]);
			}
			if (parseFloat(allsurvey[i]["cal"]["CriticalThinking"])> TopCriticalThinking){
				TopCriticalThinking = parseFloat(allsurvey[i]["cal"]["CriticalThinking"]);
			}
			if (parseFloat(allsurvey[i]["cal"]["Collaboration"])> TopCollaboration){
				TopCollaboration = parseFloat(allsurvey[i]["cal"]["Collaboration"]);
			}
			if (parseFloat(allsurvey[i]["cal"]["SelfDirectedLearning"])> TopSelfDirectedLearning){
				TopSelfDirectedLearning = parseFloat(allsurvey[i]["cal"]["SelfDirectedLearning"]);
			}
			if (parseFloat(allsurvey[i]["cal"]["PerformanceGoals"]) > TopPerformanceGoals){
				TopPerformanceGoals = parseFloat(allsurvey[i]["cal"]["PerformanceGoals"]);
			}
			if (parseFloat(allsurvey[i]["cal"]["LearningGoals"]) > TopLearningGoals){
				TopLearningGoals = parseFloat(allsurvey[i]["cal"]["LearningGoals"]);
			}
			if (parseFloat(allsurvey[i]["cal"]["SocialGoal"])> TopSocialGoal){
				TopSocialGoal = parseFloat(allsurvey[i]["cal"]["SocialGoal"]);
			}
			if (parseFloat(allsurvey[i]["cal"]["SurfaceLearning"])> TopSurfaceLearning){
				TopSurfaceLearning = parseFloat(allsurvey[i]["cal"]["SurfaceLearning"]);
			}
			if (parseFloat(allsurvey[i]["cal"]["DeepLearning"])> TopDeepLearning){
				TopDeepLearning = parseFloat(allsurvey[i]["cal"]["DeepLearning"]);
			}


			AvgSelfEfficacy += parseFloat(allsurvey[i]["cal"]["SelfEfficacy"]);
			AvgTaskValues += parseFloat(allsurvey[i]["cal"]["TaskValues"]);
			AvgBehave_Cog_EmoEngagement += parseFloat(allsurvey[i]["cal"]["Behave_Cog_EmoEngagement"]);
			AvgAcadSelfConcept += parseFloat(allsurvey[i]["cal"]["AcadSelfConcept"]);

			AvgCreativity += parseFloat(allsurvey[i]["cal"]["Creativity"]);
			AvgCuriosity += parseFloat(allsurvey[i]["cal"]["Curiosity"]);
			AvgCriticalThinking += parseFloat(allsurvey[i]["cal"]["CriticalThinking"]);
			AvgCollaboration += parseFloat(allsurvey[i]["cal"]["Collaboration"]);
			AvgSelfDirectedLearning += parseFloat(allsurvey[i]["cal"]["SelfDirectedLearning"]);

			AvgPerformanceGoals += parseFloat(allsurvey[i]["cal"]["PerformanceGoals"]);
			AvgLearningGoals += parseFloat(allsurvey[i]["cal"]["LearningGoals"]);
			AvgSocialGoal+= parseFloat(allsurvey[i]["cal"]["SocialGoal"]);
			AvgSurfaceLearning += parseFloat(allsurvey[i]["cal"]["SurfaceLearning"]);
			AvgDeepLearning += parseFloat(allsurvey[i]["cal"]["DeepLearning"]);

		}

	});

	AvgSelfEfficacy = AvgSelfEfficacy/length;
	AvgTaskValues = AvgTaskValues/length;
	AvgBehave_Cog_EmoEngagement = AvgBehave_Cog_EmoEngagement/length;
	AvgAcadSelfConcept = AvgAcadSelfConcept/length;
	AvgCreativity = AvgCreativity/length;
	AvgCuriosity = AvgCuriosity/length;
	AvgCriticalThinking = AvgCriticalThinking/length;
	AvgCollaboration = AvgCollaboration/length;
	AvgSelfDirectedLearning = AvgSelfDirectedLearning/length;

	AvgPerformanceGoals = AvgPerformanceGoals/length;
	AvgLearningGoals = AvgLearningGoals/length;
	AvgSocialGoal= AvgSocialGoal/length;
	AvgSurfaceLearning = AvgSurfaceLearning/length;
	AvgDeepLearning = AvgDeepLearning/length;

	//post result
	$.each(allpostsurvey, function( i, l ){
		if(selectedClassName==allpostsurvey[i]["class"]){
			length1++;
			if (parseFloat(allpostsurvey[i]["cal"]["SelfEfficacy"]) > PostTopSelfEfficacy){
				PostTopSelfEfficacy = parseFloat(allpostsurvey[i]["cal"]["SelfEfficacy"]);
			}
			if (parseFloat(allpostsurvey[i]["cal"]["TaskValues"]) > PostTopTaskValues){
				PostTopTaskValues = parseFloat(allpostsurvey[i]["cal"]["TaskValues"]);
			}
			if (parseFloat(allpostsurvey[i]["cal"]["Behave_Cog_EmoEngagement"])> PostTopBehave_Cog_EmoEngagement){
				PostTopBehave_Cog_EmoEngagement = parseFloat(allpostsurvey[i]["cal"]["Behave_Cog_EmoEngagement"]);
			}
			if (parseFloat(allpostsurvey[i]["cal"]["AcadSelfConcept"])> PostTopAcadSelfConcept){
				PostTopAcadSelfConcept = parseFloat(allpostsurvey[i]["cal"]["AcadSelfConcept"]);
			}
			if (parseFloat(allpostsurvey[i]["cal"]["Creativity"]) > PostTopCreativity){
				PostTopCreativity = parseFloat(allpostsurvey[i]["cal"]["Creativity"]);
			}
			if (parseFloat(allpostsurvey[i]["cal"]["Curiosity"]) > PostTopCuriosity){
				PostTopCuriosity = parseFloat(allpostsurvey[i]["cal"]["Curiosity"]);
			}
			if (parseFloat(allpostsurvey[i]["cal"]["CriticalThinking"])> PostTopCriticalThinking){
				PostTopCriticalThinking = parseFloat(allpostsurvey[i]["cal"]["CriticalThinking"]);
			}
			if (parseFloat(allpostsurvey[i]["cal"]["Collaboration"])> PostTopCollaboration){
				PostTopCollaboration = parseFloat(allpostsurvey[i]["cal"]["Collaboration"]);
			}
			if (parseFloat(allpostsurvey[i]["cal"]["SelfDirectedLearning"])> PostTopSelfDirectedLearning){
				PostTopSelfDirectedLearning = parseFloat(allpostsurvey[i]["cal"]["SelfDirectedLearning"]);
			}
			if (parseFloat(allpostsurvey[i]["cal"]["PerformanceGoals"]) > PostTopPerformanceGoals){
				PostTopPerformanceGoals = parseFloat(allpostsurvey[i]["cal"]["PerformanceGoals"]);
			}
			if (parseFloat(allpostsurvey[i]["cal"]["LearningGoals"]) > PostTopLearningGoals){
				PostTopLearningGoals = parseFloat(allpostsurvey[i]["cal"]["LearningGoals"]);
			}
			if (parseFloat(allpostsurvey[i]["cal"]["SocialGoal"])> PostTopSocialGoal){
				PostTopSocialGoal = parseFloat(allpostsurvey[i]["cal"]["SocialGoal"]);
			}
			if (parseFloat(allpostsurvey[i]["cal"]["SurfaceLearning"])> PostTopSurfaceLearning){
				PostTopSurfaceLearning = parseFloat(allpostsurvey[i]["cal"]["SurfaceLearning"]);
			}
			if (parseFloat(allpostsurvey[i]["cal"]["DeepLearning"])> PostTopDeepLearning){
				PostTopDeepLearning = parseFloat(allpostsurvey[i]["cal"]["DeepLearning"]);
			}

			PostAvgSelfEfficacy += parseFloat(allpostsurvey[i]["cal"]["SelfEfficacy"]);
			PostAvgTaskValues += parseFloat(allpostsurvey[i]["cal"]["TaskValues"]);
			PostAvgBehave_Cog_EmoEngagement += parseFloat(allpostsurvey[i]["cal"]["Behave_Cog_EmoEngagement"]);
			PostAvgAcadSelfConcept += parseFloat(allpostsurvey[i]["cal"]["AcadSelfConcept"]);

			PostAvgCreativity += parseFloat(allpostsurvey[i]["cal"]["Creativity"]);
			PostAvgCuriosity += parseFloat(allpostsurvey[i]["cal"]["Curiosity"]);
			PostAvgCriticalThinking += parseFloat(allpostsurvey[i]["cal"]["CriticalThinking"]);
			PostAvgCollaboration += parseFloat(allpostsurvey[i]["cal"]["Collaboration"]);
			PostAvgSelfDirectedLearning += parseFloat(allpostsurvey[i]["cal"]["SelfDirectedLearning"]);

			PostAvgPerformanceGoals += parseFloat(allpostsurvey[i]["cal"]["PerformanceGoals"]);
			PostAvgLearningGoals += parseFloat(allpostsurvey[i]["cal"]["LearningGoals"]);
			PostAvgSocialGoal+= parseFloat(allpostsurvey[i]["cal"]["SocialGoal"]);
			PostAvgSurfaceLearning += parseFloat(allpostsurvey[i]["cal"]["SurfaceLearning"]);
			PostAvgDeepLearning += parseFloat(allpostsurvey[i]["cal"]["DeepLearning"]);



		}

	});
	PostAvgSelfEfficacy = PostAvgSelfEfficacy/length1;
	PostAvgTaskValues = PostAvgTaskValues/length1;
	PostAvgBehave_Cog_EmoEngagement = PostAvgBehave_Cog_EmoEngagement/length1;
	PostAvgAcadSelfConcept = PostAvgAcadSelfConcept/length1;
	PostAvgCreativity = PostAvgCreativity/length1;
	PostAvgCuriosity = PostAvgCuriosity/length1;
	PostAvgCriticalThinking = PostAvgCriticalThinking/length1;
	PostAvgCollaboration = PostAvgCollaboration/length1;
	PostAvgSelfDirectedLearning = PostAvgSelfDirectedLearning/length1;

	PostAvgPerformanceGoals = PostAvgPerformanceGoals/length1;
	PostAvgLearningGoals = PostAvgLearningGoals/length1;
	PostAvgSocialGoal= PostAvgSocialGoal/length1;
	PostAvgSurfaceLearning = PostAvgSurfaceLearning/length1;
	PostAvgDeepLearning = PostAvgDeepLearning/length1;

	/*
	|
	| SNA and Thinking Skills Chart stuff
	|
	*/
	//Event listener to identify clicking of the SNA tab.
	$("#sidebar").on('shown.bs.tab', function (e) {
		console.log("target: " + e.target.id);
		//if SNA tab is clicked, only then #sna_container "exists", then sna can be drawn.
		if(e.target.id == "sna_tab")
			drawSNAChart();
	});

	$("#btn_sna_generate").click(function(){
		$('#sna_container').html("<h3 style='text-align:center;margin:50% 0'>Redrawing...</h3>");
		drawSNAChart();
	});

	//Populating Subject/Class/Video Dropdown on default ========================================================
	var subjectOptions = '';

	$.each( overall_data, function( i, l ){
		subjectOptions += '<option value="'+ overall_data[i]["id"] + '">' + overall_data[i]["name"] + '</option>';
	});
	$('#subjectSelect').append(subjectOptions);
	$("#subjectSelect option:last").prop("selected", "selected");

	selectedSubject = $("#subjectSelect").val();
	var classOptions = '<option value="">Choose a Group</option>';

	$.each( overall_data[selectedSubject]["groups"], function( i, l ){
		classOptions += '<option value="'+ overall_data[selectedSubject]["groups"][i]["id"] + '">' + overall_data[selectedSubject]["groups"][i]["name"] + '</option>';
	});
	$('#classSelect').empty().append(classOptions);
	$("#classSelect option:last").prop("selected", "selected");

	selectedClass = $('#classSelect').val();
	var videoOptions = '<option value="">Choose a Video</option><option value="overall">Overall</option>';

	$.each( overall_data[selectedSubject]["groups"][selectedClass]["videos"], function( i, l ){
		videoOptions += '<option value="'+ overall_data[selectedSubject]["groups"][selectedClass]["videos"][i]["id"] + '">' + overall_data[selectedSubject]["groups"][selectedClass]["videos"][i]["name"] + '</option>';
	});

	$('#videoSelect').empty().append(videoOptions);
	$("#videoSelect option:eq(1)").prop("selected", "selected");
	drawAllCharts();
	//End of populated subject/class/video dropdowns on default====================================================

	//Populate Class Dropdown when Subject chosen==================================================================
	$('#subjectSelect').change(function(){
		selectedSubject = $("#subjectSelect").val();

		$('#videoLabel').fadeOut();
			$('#videoSelect').fadeOut();

		if(selectedSubject === "")
		{
			$('#classLabel').fadeOut();
			$('#classSelect').fadeOut();
		}
		else
		{
			var classOptions = '<option value="">Choose a Group</option>';

			$.each( overall_data[selectedSubject]["groups"], function( i, l ){
				classOptions += '<option value="'+ overall_data[selectedSubject]["groups"][i]["id"] + '">' + overall_data[selectedSubject]["groups"][i]["name"] + '</option>';
			});
			$('#classSelect').empty().append(classOptions);

			$('#classLabel').fadeIn();
			$('#classSelect').fadeIn();
		}
	});
	//End of Populate Class Dropdown when Subject chosen==================================================================

	//Populate Video Dropdown when class chosen ==========================================================================
	$('#classSelect').change(function(){
		selectedClass = $('#classSelect').val();

		if(selectedClass === "")
		{
			$('#videoLabel').fadeOut();
			$('#videoSelect').fadeOut();
		}
		else
		{
			var videoOptions = '<option value="">Choose a Video</option><option value="overall">Overall</option>';

			$.each( overall_data[selectedSubject]["groups"][selectedClass]["videos"], function( i, l ){
				videoOptions += '<option value="'+ overall_data[selectedSubject]["groups"][selectedClass]["videos"][i]["id"] + '">' + overall_data[selectedSubject]["groups"][selectedClass]["videos"][i]["name"] + '</option>';
			});

			$('#videoSelect').empty().append(videoOptions);

			$('#videoLabel').fadeIn();
			$('#videoSelect').fadeIn();
		}
	});
	//End of Populate Video Dropdown when class chosen ==========================================================================

	//Draw the charts with based on the selected subject/class/video selected
	$('#videoSelect').change(function(){drawAllCharts()});

	drawAllCharts();

	function drawAllCharts()
	{
		//Get selected video's ID. Can be "Overall" as well.
		selectedVideoID = $('#videoSelect').val();
		selectedStudents = overall_data[selectedSubject]["groups"][selectedClass]["users"];
		allVideos = overall_data[selectedSubject]["groups"][selectedClass]["videos"];
		selectedClassName = overall_data[selectedSubject]["groups"][selectedClass]["name"];
		selectedClassID = overall_data[selectedSubject]["groups"][selectedClass]["id"];

		//The user chose a specific video
		if(selectedVideoID != "overall")
		{
			//Set the selectVideo array based on selected video id. Contains selected video's weights, strength and social links
			selectedVideo = overall_data[selectedSubject]["groups"][selectedClass]["videos"][selectedVideoID];
		}
		else //If the user did not choose a specific video, but instead chose the default "overall" option
		{
			//Class overall weights, strengths, social links are located here
			selectedVideo = overall_data[selectedSubject]["groups"][selectedClass];
		}

		//Reset Global Variables
		annotations = 0;
		comments = 0;
		replies = 0;
		//thinking skills
		ideate =0;
		justify =0;
		agree = 0;
		disagree = 0
		inquire = 0

		total_students_count = Object.keys(selectedStudents).length;

		google.charts.setOnLoadCallback(drawAttitudeChart);
		google.charts.setOnLoadCallback(drawDesiresChart);
		google.charts.setOnLoadCallback(draw21ccChart);
		google.charts.setOnLoadCallback(drawMindsetChart);

		google.charts.setOnLoadCallback(drawTSChart); //Thinking Skills Chart
		drawSNAChart(); //Social Network Chart
	}
});//end of document ready

google.charts.load('current', {'packages':['corechart', 'bar']});
// Set BAR chart options
var options = {
			   colors:['#00ACEC','#FF1655'],
			   theme: 'material',
			   legend: {
					position: 'bottom',
					cursor: 'pointer',
					textStyle: {fontSize: 11}
				},
				animation:{
					startup: true,
					duration: 1000,
					easing: 'out',
				},

				hAxis: {
					gridlines: {
						count: 0,
						color: 'transparent'
					},
					scaleType: 'linear',
					minValue: 0,
					baselineColor: 'transparent'
				},
				vAxis: {
					gridlines: {
						color: 'transparent'
					},
					scaleType: 'linear',
					 format: '#,###',
					minValue: 0,
					baselineColor: 'transparent',

				},
				annotations: {
					alwaysOutside: false,
				  textStyle: {
					  color: '#FFFFFF',
					  fontSize: 12,
					  bold: true,
					  auraColor: 'none',
					  offset: 100

					}
				}
			  };
function drawAttitudeChart()
{


	if (!cal){
	SelfEfficacy = 0;
	TaskValues = 0;
	Behave_Cog_EmoEngagement = 0;
	AcadSelfConcept = 0;

	}else{
	SelfEfficacy = parseFloat(cal["SelfEfficacy"]);
	TaskValues = parseFloat(cal["TaskValues"]);
	Behave_Cog_EmoEngagement = parseFloat(cal["Behave_Cog_EmoEngagement"]);;
	AcadSelfConcept = parseFloat(cal["AcadSelfConcept"]);
	}
	if (!postcal){
	PostSelfEfficacy = 0;
	PostTaskValues = 0;
	PostBehave_Cog_EmoEngagement = 0;
	PostAcadSelfConcept = 0;

	}else{
	PostSelfEfficacy = parseFloat(postcal["SelfEfficacy"]);
	PostTaskValues = parseFloat(postcal["TaskValues"]);
	PostBehave_Cog_EmoEngagement = parseFloat(postcal["Behave_Cog_EmoEngagement"]);
	PostAcadSelfConcept = parseFloat(postcal["AcadSelfConcept"]);
	}



	Highcharts.chart('attitudes_chart', {
	    chart: {
	        polar: true,
	        type: 'line'
	    },
		exporting: {
			enabled: false
		},
		credits: {
			enabled: false
		},
	    title: {
			text: 'MY LEARNING ATTITUDES',
			style: {fontWeight: 'bold', "fontSize": "12px" }
			//x: -80
		},
		/*subtitle: {
			text: 'How much I appreciate SS\'s importance and usefulness...<br>How confident am I in doing well... <br>How attentive and interested am I...<br>How much of a critical reader am I...<br>How satisfied am I with myself in EL...',
			style: {fontWeight: 'bold', align: 'right'}
		},*/
	    pane: {
	        center: ['50%', '50%'],
	        size: 150
	    },
	    xAxis: {
	        categories: ['Confidence', 'Appreciation for SS', 'Self-Belief', 'Attention & Interest'],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	    yAxis: {
			gridLineInterpolation: 'polygon',
			lineWidth: 0,
			allowDecimals: false,
			min: 0,
			max: 7,
			startOnTick: true,
			endOnTick: true,
			tickInterval: 1
		},
	    tooltip: {
	        shared: true,
	        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.2f}</b><br/>'
	    },
	    legend: {
	        align: 'center',
			y: 23
	    },
	    series: [{
	        name: 'Me (Before)',
			color: '#b36aff',
	        data: [SelfEfficacy, TaskValues, AcadSelfConcept, Behave_Cog_EmoEngagement],
	        pointPlacement: 'on',
			visible:  role=='O' ? true : false,
			showInLegend: role=='O' ? true : false
	    },{
	        name: 'Me (Now)',
			color: '#7d3ae8',
	        data: [PostSelfEfficacy, PostTaskValues, PostAcadSelfConcept, PostBehave_Cog_EmoEngagement],
	        pointPlacement: 'on',
			visible:  role=='O' ? true : false,
			showInLegend: role=='O' ? true : false
	    },{
	        name: 'Class (Before)',
			color: '#ffca69',
	         data: [AvgSelfEfficacy, AvgTaskValues, AvgAcadSelfConcept, AvgBehave_Cog_EmoEngagement],
	        pointPlacement: 'on',
	        visible: role=='O' ? false : true,
			showInLegend: role=='O' ? false : true
	    },{
	        name: 'Class (Now)',
			color: '#e8993a',
	         data: [PostAvgSelfEfficacy, PostAvgTaskValues, PostAvgAcadSelfConcept, PostAvgBehave_Cog_EmoEngagement],
	        pointPlacement: 'on',
	        visible: role=='O' ? false : true,
			showInLegend: role=='O' ? false : true
	    },{
	        name: 'Top Peer (Before)',
			color: '#79b8ff',
	         data: [TopSelfEfficacy, TopTaskValues, TopAcadSelfConcept, TopBehave_Cog_EmoEngagement],
	        pointPlacement: 'on',
	        visible:role=='O' ? false : false,
			showInLegend: true
	    },{
	        name: 'Top Peer (Now)',
			color: '#4188cc',
	         data: [PostTopSelfEfficacy, PostTopTaskValues, PostTopAcadSelfConcept, PostTopBehave_Cog_EmoEngagement],
	        pointPlacement: 'on',
	        visible: role=='O' ? false : false,
			showInLegend: true
	    }]
	});
}


function drawDesiresChart()
{

	if(!cal)
	{
		PerformanceGoals =0;
		LearningGoals = 0;
		SocialGoal = 0;
	}
	else{
	PerformanceGoals = parseFloat(cal["PerformanceGoals"]);
	LearningGoals = parseFloat(cal["LearningGoals"]);
	SocialGoal = parseFloat(cal["SocialGoal"]);
	}

	if(!postcal)
	{
		PostPerformanceGoals =0;
		PostLearningGoals = 0;
		PostSocialGoal = 0;
	}
	else{
	PostPerformanceGoals = parseFloat(postcal["PerformanceGoals"]);
	PostLearningGoals = parseFloat(postcal["LearningGoals"]);
	PostSocialGoal = parseFloat(postcal["SocialGoal"]);
	}
	Highcharts.chart('feel_chart', {
	    chart: {
	        polar: true,
	        type: 'line'
	    },
		exporting: {
			enabled: false
		},
		credits: {
			enabled: false
		},
	    title: {
			text: 'MY LEARNING MOTIVATIONS',
			style: {fontWeight: 'bold', "fontSize": "12px" }
			//x: -80
		},
		/*subtitle: {
			text: 'How often did I apply the different thinking skills...?',
			style: {fontWeight: 'bold'}
		},*/
	    pane: {
	        center: ['50%', '65%'],
	        size: 150
	    },
	    xAxis: {
	        categories: ['Prove Competence To Others<br>(Looking "Smart")', 'Master Skills & Knowledge<br>(Becoming "Expert")', 'Be Helpful & Responsible<br>(Being "Good")'],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	    yAxis: {
			gridLineInterpolation: 'polygon',
			lineWidth: 0,
			allowDecimals: true,
			min: 0,
			max: 7,
			tickInterval: 1
		},
	    tooltip: {
	        shared: true,
	        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.2f}</b><br/>'
	    },
	    legend: {
	        align: 'center',
			y: 5,
			margin: 5
	    },
	    series: [{
	        name: 'Me (Before)',
			color: '#b36aff',
	        data: [PerformanceGoals, LearningGoals, SocialGoal],
	        pointPlacement: 'on',
			visible:  role=='O' ? true : false,
			showInLegend: role=='O' ? true : false
	    },{
	        name: 'Me (Now)',
			color: '#7d3ae8',
	        data: [PostPerformanceGoals, PostLearningGoals, PostSocialGoal],
	        pointPlacement: 'on',
			visible:  role=='O' ? true : false,
			showInLegend: role=='O' ? true : false
	    },{
	        name: 'Class (Before)',
			color: '#ffca69',
	         data: [AvgPerformanceGoals, AvgLearningGoals, AvgSocialGoal],
	        pointPlacement: 'on',
	        visible: role=='O' ? false : true,
			showInLegend: role=='O' ? false : true
	    },{
	        name: 'Class (Now)',
			color: '#e8993a',
	         data: [PostAvgPerformanceGoals, PostAvgLearningGoals, PostAvgSocialGoal],
	        pointPlacement: 'on',
	        visible: role=='O' ? false : true,
			showInLegend: role=='O' ? false : true
	    },{
	        name: 'Top Peer (Before)',
			color: '#79b8ff',
	         data: [TopPerformanceGoals, TopLearningGoals, TopSocialGoal],
	        pointPlacement: 'on',
	        visible:role=='O' ? false : false,
			showInLegend: true
	    },{
	        name: 'Top Peer (Now)',
			color: '#4188cc',
	         data: [PostTopPerformanceGoals, PostTopLearningGoals, PostTopSocialGoal],
	        pointPlacement: 'on',
	        visible: role=='O' ? false : false,
			showInLegend: true
	    }]
	});
}

function draw21ccChart()
{

	if(!cal){
		Creativity = 0;
		Curiosity = 0;
		CriticalThinking = 0;
		Collaboration = 0;
		SelfDirectedLearning =0;
	}else{
		Creativity = parseFloat(cal["Creativity"]);
		Curiosity = parseFloat(cal["Curiosity"]);
		CriticalThinking = parseFloat(cal["CriticalThinking"]);
		Collaboration = parseFloat(cal["Collaboration"]);
		SelfDirectedLearning =parseFloat(cal["SelfDirectedLearning"]);
	}

	if(!postcal){
		PostCreativity = 0;
		PostCuriosity = 0;
		PostCriticalThinking = 0;
		PostCollaboration = 0;
		PostSelfDirectedLearning =0;
	}else{
	PostCreativity = parseFloat(postcal["Creativity"]);
	PostCuriosity = parseFloat(postcal["Curiosity"]);
	PostCriticalThinking = parseFloat(postcal["CriticalThinking"]);
	PostCollaboration = parseFloat(postcal["Collaboration"]);
	PostSelfDirectedLearning =parseFloat(postcal["SelfDirectedLearning"]);
	}


	Highcharts.chart('21cc_chart', {
	    chart: {
	        polar: true,
	        type: 'line'
	    },
		exporting: {
			enabled: false
		},
		credits: {
			enabled: false
		},
	    title: {
			text: 'MY 21ST CENTURY SKILLS PROFILE',
			style: {fontWeight: 'bold', "fontSize": "12px" }
			//x: -80
		},
		/*subtitle: {
			text: 'How often did I apply the different critical lenses...?',
			style: {fontWeight: 'bold'}
		},*/
	    pane: {
	        center: ['50%', '50%'],
	        size: 150
	    },
	    xAxis: {
	        categories: ['Creativity', 'Curiosity', 'Critical Thinking', 'Collaboration',
	                'Self Directed Learner'],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	    yAxis: {
			gridLineInterpolation: 'polygon',
			lineWidth: 0,
			allowDecimals: false,
			min: 0,
			tickInterval: 1
		},
	    tooltip: {
	        shared: true,
	        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.2f}</b><br/>'
	    },
	    legend: {
	        align: 'center',
			y: 20
	    },
	   series: [{
	        name: 'Me (Before)',
			color: '#b36aff',
	        data: [Creativity, Curiosity, CriticalThinking, Collaboration, SelfDirectedLearning],
	        pointPlacement: 'on',
			visible:  role=='O' ? true : false,
			showInLegend: role=='O' ? true : false
	    },{
	        name: 'Me (Now)',
			color: '#7d3ae8',
	        data: [PostCreativity, PostCuriosity, PostCriticalThinking, PostCollaboration, PostSelfDirectedLearning],
	        pointPlacement: 'on',
			visible:  role=='O' ? true : false,
			showInLegend: role=='O' ? true : false
	    },{
	        name: 'Class (Before)',
			color: '#ffca69',
	         data: [AvgCreativity, AvgCuriosity, AvgCriticalThinking, AvgCollaboration, AvgSelfDirectedLearning],
	        pointPlacement: 'on',
	        visible: role=='O' ? false : true,
			showInLegend: role=='O' ? false : true
	    },{
	        name: 'Class (Now)',
			color: '#e8993a',
	         data: [PostAvgCreativity, PostAvgCuriosity, PostAvgCriticalThinking, PostAvgCollaboration, PostAvgSelfDirectedLearning],
	        pointPlacement: 'on',
	        visible: role=='O' ? false : true,
			showInLegend: role=='O' ? false : true
	    },{
	        name: 'Top Peer (Before)',
			color: '#79b8ff',
	         data: [TopCreativity, TopCuriosity, TopCriticalThinking, TopCollaboration, TopSelfDirectedLearning],
	        pointPlacement: 'on',
	        visible:role=='O' ? false : false,
			showInLegend: true
		},{
	        name: 'Top Peer (Now)',
			color: '#4188cc',
	         data: [PostTopCreativity, PostTopCuriosity, PostTopCriticalThinking, PostTopCollaboration, PostTopSelfDirectedLearning],
	        pointPlacement: 'on',
	        visible: role=='O' ? false : false,
			showInLegend: true

	    }]
	});
}

var hidden_me_before = false;
var hidden_class_before = true;
var hidden_topPeer_before = true;
var hidden_me_now = false;
var hidden_class_now = true;
var hidden_topPeer_now = true;

function drawMindsetChart()
{


	/*var learning_goals_val = parseFloat(cal["PerformanceGoals"]);
	var surface_goals_val = parseFloat(cal["LearningGoals"]);
	var social_goals_val = parseFloat(cal["DeepLearning"]);*/

	if(!cal){
		DeepLearning = 0;
		SurfaceLearning = 0;

	}else{
	DeepLearning = parseFloat(cal["DeepLearning"]);
	SurfaceLearning = parseFloat(cal["SurfaceLearning"]);
	}

	if(!postcal){
		PostDeepLearning = 0;
		PostSurfaceLearning = 0;

	}else{
	PostDeepLearning = parseFloat(postcal["DeepLearning"]);
	PostSurfaceLearning = parseFloat(postcal["SurfaceLearning"]);
	}
	/*var learning_goals_pct = (learning_goals_val/7 * 100).toFixed(0);
	var surface_goals_pct = (surface_goals_val/7 * 100).toFixed(0);
	var social_goals_pct = (social_goals_val/7 * 100).toFixed(0);
	var deep_learning_pct = (deep_learning_val/7 * 100).toFixed(0);
	var surface_learning_pct = (surface_learning_val/7 * 100).toFixed(0);*/
	$('#surfacedeeptable').remove();
	$('#learningStrategiesLegend').remove();



	$("<table id='surfacedeeptable' class='slider-chart'></table>").appendTo("#learning_mindset_container");
	$('#surfacedeeptable').append("<tr><th></th><th style='text-align:left;font-size:11px;padding-right:20px;line-height:14px;'>Focus on surface<br>understanding<br>and scoring in exams</th><th style='text-align:right;font-size:11px;line-height:14px;'>Focus on deep<br>understanding<br>and learning for life</th></tr>");
	$('#surfacedeeptable tr:last').after("<tr><td></td></tr>");
	$('#surfacedeeptable tr:last').after("<tr><th></th><th style='font-size:10px;text-align:left'>(Less Healthy)</th><th style='font-size:10px;text-align:right'>(More Healthy)</th></tr>");
	if (role =='O'){
		$('#surfacedeeptable tr:last').after("<tr id='tr_me_before' class='two-column-comp-me-before'><td style='font-size:10.7px;font-weight:bold'>Me (Before)</td><td style='font-size:12px;font-weight:bold'>"+round(SurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(DeepLearning,2)+"</td></tr>");
	$('#surfacedeeptable tr:last').after("<tr><td></td></tr>");
	$('#surfacedeeptable tr:last').after("<tr id='tr_me_now' class='two-column-comp-me-now'><td style='font-size:10.7px;font-weight:bold'>Me (Now)</td><td style='font-size:12px;font-weight:bold'>"+round(PostSurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(PostDeepLearning,2)+"</td></tr>");
	$('#surfacedeeptable tr:last').after("<tr><td></td></tr>");
	$('#surfacedeeptable tr:last').after("<tr id='tr_topPeer_before' class='two-column-comp-top-before'><td style='font-size:10.7px;font-weight:bold'>Top Peer (Before)</td><td style='font-size:12px;font-weight:bold'>"+round(TopSurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(TopDeepLearning,2)+"</td></tr>");
	$('#surfacedeeptable tr:last').after("<tr><td></td></tr>");
	$('#surfacedeeptable tr:last').after("<tr id='tr_topPeer_now' class='two-column-comp-top-now'><td style='font-size:10.7px;font-weight:bold'>Top Peer (Now)</td><td style='font-size:12px;font-weight:bold'>"+round(PostTopSurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(PostTopDeepLearning,2)+"</td></tr>");
	}else{
	$('#surfacedeeptable tr:last').after("<tr id='tr_class_before' class='two-column-comp-class-before'><td style='font-size:10.7px;font-weight:bold'>Class (Before)</td><td style='font-size:12px;font-weight:bold'>"+round(AvgSurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(AvgDeepLearning,2)+"</td></tr>");
	$('#surfacedeeptable tr:last').after("<tr><td></td></tr>");
	$('#surfacedeeptable tr:last').after("<tr id='tr_class_now' class='two-column-comp-class-now'><td style='font-size:10.7px;font-weight:bold'>Class (Now)</td><td style='font-size:12px;font-weight:bold'>"+round(PostAvgSurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(PostAvgDeepLearning,2)+"</td></tr>");
	$('#surfacedeeptable tr:last').after("<tr><td></td></tr>");
	$('#surfacedeeptable tr:last').after("<tr id='tr_topPeer_before' class='two-column-comp-top-before'><td style='font-size:10.7px;font-weight:bold'>Top Peer (Before)</td><td style='font-size:12px;font-weight:bold'>"+round(TopSurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(TopDeepLearning,2)+"</td></tr>");
	$('#surfacedeeptable tr:last').after("<tr><td></td></tr>");
	$('#surfacedeeptable tr:last').after("<tr id='tr_topPeer_now' class='two-column-comp-top-now'><td style='font-size:10.7px;font-weight:bold'>Top Peer (Now)</td><td style='font-size:12px;font-weight:bold'>"+round(PostTopSurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(PostTopDeepLearning,2)+"</td></tr>");
	}

	twoColComp.init();

	if (role =='O'){

		$('<div id="learningStrategiesLegend"><div id="square_me_before" class="square"></div><label id="label_me_before">Me (Before)</label><div id="square_me_now" class="square"></div><label id="label_me_now">Me (Now)</label><div id="square_topPeer_before" class="square"></div><label id="label_topPeer_before">Top Peer (Before)</label><div id="square_topPeer_now" class="square"></div><label id="label_topPeer_now">Top Peer (Now)</label></div>').appendTo("#learning_mindset_container");
	}else{
		$('<div id="learningStrategiesLegend"><div id="square_class_before" class="square"></div><label id="label_class_before">Class (Before)</label><div id="square_class_now" class="square"></div><label id="label_class_now">Class (Now)</label><div id="square_topPeer_before" class="square"></div><label id="label_topPeer_before">Top Peer (Before)</label><div id="square_topPeer_now" class="square"></div><label id="label_topPeer_now">Top Peer (Now)</label></div>').appendTo("#learning_mindset_container");

	}
	if (role=='O')
	{

		/*$('#label_me').css("color", "lightgrey");
		$('#tr_me').css("opacity", 0);
		$('#label_class').css("color", "black");
		$('#tr_class').css("opacity", 1);
		$('#label_topPeer').css("color", "black");
		$('#tr_topPeer').css("opacity", 1);
		hidden_me = true;
		hidden_class = false;
		hidden_topPeer = false;*/

		$('#label_me_before').css("color", "black");
		$('#tr_me_before').css("opacity", 1);
		$('#label_me_now').css("color", "black");
		$('#tr_me_now').css("opacity", 1);
		$('#label_topPeer_before').css("color", "lightgrey");
		$('#tr_topPeer_before').css("opacity", 0);
		$('#label_topPeer_now').css("color", "lightgrey");
		$('#tr_topPeer_now').css("opacity", 0);
		hidden_me_before = false;
		hidden_me_now = false;
		hidden_topPeer_before = true;
		hidden_topPeer_now = true;
	}else{
		$('#label_topPeer_before').css("color", "lightgrey");
		$('#tr_topPeer_before').css("opacity", 0);
		$('#label_topPeer_now').css("color", "lightgrey");
		$('#tr_topPeer_now').css("opacity", 0);
		$('#label_class_before').css("color", "black");
		$('#tr_class_before').css("opacity", 1);
		$('#label_class_now').css("color", "black");
		$('#tr_class_now').css("opacity", 1);
		hidden_topPeer_before = true;
		hidden_topPeer_now = true;
		hidden_class_before = false;
		hidden_class_now = false;

	}

	$('#label_me_before').click(function(){
		if(hidden_me_now)
		{
			$('#label_me_before').css("color", "black");
			$('#tr_me_before').css("opacity", 1);
			hidden_me_before = false;
		}
		else
		{
			$('#label_me_before').css("color", "lightgrey");
			$('#tr_me_before').css("opacity", 0);
			hidden_me_before = true;
		}
	});
	$('#label_me_now').click(function(){
		if(hidden_me_now)
		{
			$('#label_me_now').css("color", "black");
			$('#tr_me_now').css("opacity", 1);
			hidden_me_now = false;
		}
		else
		{
			$('#label_me_now').css("color", "lightgrey");
			$('#tr_me_now').css("opacity", 0);
			hidden_me_now = true;
		}
	});


	$('#label_class_before').click(function(){
		if(hidden_class_before)
		{
			$('#label_class_before').css("color", "black");
			$('#tr_class_before').css("opacity", 1);
			hidden_class_before = false;
		}
		else
		{
			$('#label_class_before').css("color", "lightgrey");
			$('#tr_class_before').css("opacity", 0);
			hidden_class_before = true;
		}
	});
	$('#label_class_now').click(function(){
		if(hidden_class_now)
		{
			$('#label_class_now').css("color", "black");
			$('#tr_class_now').css("opacity", 1);
			hidden_class_now = false;
		}
		else
		{
			$('#label_class_now').css("color", "lightgrey");
			$('#tr_class_now').css("opacity", 0);
			hidden_class_now = true;
		}
	});

	$('#label_topPeer_before').click(function(){
		if(hidden_topPeer_before)
		{
			$('#label_topPeer_before').css("color", "black");
			$('#tr_topPeer_before').css("opacity", 1);
			hidden_topPeer_before = false;
		}
		else
		{
			$('#label_topPeer_before').css("color", "lightgrey");
			$('#tr_topPeer_before').css("opacity", 0);
			hidden_topPeer_before = true;
		}
	});
	$('#label_topPeer_now').click(function(){
		if(hidden_topPeer_now)
		{
			$('#label_topPeer_now').css("color", "black");
			$('#tr_topPeer_now').css("opacity", 1);
			hidden_topPeer_now = false;
		}
		else
		{
			$('#label_topPeer_now').css("color", "lightgrey");
			$('#tr_topPeer_now').css("opacity", 0);
			hidden_topPeer_now = true;
		}
	});
}

function drawTSChart()
{
	var selectedClassVideo = overall_data[selectedSubject]["groups"][selectedClass]["users"][selectedVideoID];
	var topIdeate = 0;
	var topJustify = 0;
	var topAgree = 0;
	var topDisagree = 0;
	var topInquire = 0;

	var avgIdeate = 0;
	var avgJustify = 0;
	var avgAgree = 0;
	var avgDisagree = 0;
	var avgInquire = 0;

	var length =0;
	//alert(length);
	$.each(selectedClassVideo, function( i, l ){
		if(selectedClassVideo[i]["role"] == 'O'){
			length++;
			if (selectedClassVideo[i]["ideate"] > topIdeate){
				topIdeate = selectedClassVideo[i]["ideate"];
			}
			if (selectedClassVideo[i]["justify"] > topJustify){
				topJustify = selectedClassVideo[i]["justify"];
			}
			if (selectedClassVideo[i]["agree"] > topAgree){
				topAgree = selectedClassVideo[i]["agree"];
			}
			if (selectedClassVideo[i]["disagree"] > topDisagree){
				topDisagree = selectedClassVideo[i]["disagree"];
			}
			if (selectedClassVideo[i]["inquire"] > topInquire){
				topInquire = selectedClassVideo[i]["inquire"];
			}

			avgIdeate += selectedClassVideo[i]["ideate"];
			avgJustify += selectedClassVideo[i]["justify"];
			avgAgree += selectedClassVideo[i]["agree"];
			avgDisagree += selectedClassVideo[i]["disagree"];
			avgInquire += selectedClassVideo[i]["inquire"];
		}

	});


		avgIdeate = avgIdeate/length;
		avgJustify = avgJustify/length;
		avgAgree = avgAgree/length;
		avgDisagree = avgDisagree/length;
		avgInquire = avgInquire/length;
		//alert(avgIdeate);

	ideate = selectedStudents[selectedVideoID][userID]["ideate"];
	justify = selectedStudents[selectedVideoID][userID]["justify"];
	agree = selectedStudents[selectedVideoID][userID]["agree"];
	disagree = selectedStudents[selectedVideoID][userID]["disagree"];
	inquire = selectedStudents[selectedVideoID][userID]["inquire"];

	Highcharts.chart('TS_chart', {
	    chart: {
	        polar: true,
	        type: 'line',
	    },
			exporting: {
					enabled: false
				},
				credits: {
					enabled: false
				},
	    title: {
					text: 'MY DISCUSSION FRAMES USAGE',
					style: {fontWeight: 'bold', "fontSize": "12px" }
					//x: -80
				},
				subtitle: {
					text: 'How often did I apply the different discussion frames...?',
					style: {fontWeight: 'bold', "fontSize": "10"}
				},
	    pane: {
			center: ['50%', '50%'],
	        size: 270
	    },
	    xAxis: {
	        categories: ['Ideate', 'Justify', 'Agree', 'Disagree',
	                'Inquire'],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	    yAxis: {
			gridLineInterpolation: 'polygon',
			lineWidth: 0,
			allowDecimals: false,
			min: 0,
			tickInterval: 1
		},
	    tooltip: {
	        shared: true,
	        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
	    },
	    legend: {
	        align: 'center',
	        margin: 1
	    },
	    series: [{
	        name: 'Me',
			color: '#7d3ae8',
	        data: [ideate, justify, agree, disagree, inquire],
	        pointPlacement: 'on',
					visible:  role=='O' ? true : false
	    },{
	        name: 'Class',
			color: '#e8993a',
	        data: [avgIdeate, avgJustify, avgAgree, avgDisagree, avgInquire],
	        pointPlacement: 'on',
	        visible:  role=='O' ? false : true
	    },{
	        name: 'Top Peer',
			color: '#3a89e8',
	        data: [topIdeate, topJustify, topAgree, topDisagree, topInquire],
	        pointPlacement: 'on',
	        visible:  role=='O' ? false : true
	    }]
	});
}

function drawSNAChart()
{
	/*console.log("SNA Data");
	console.log(selectedVideoID);*/
	if(selectedVideoID == "overall")
	{
		//SNA drawing setup
		if(overall_data[selectedSubject]["groups"][selectedClass]["weights"]["top_weight"] == 0)//Check that the selected video has at least 1 comment. By checking the top weight is not zero. To prevent error when drawing sna
			$('#sna_container').html("<h3 style='text-align:center;margin:0 0;position: relative;top: 50%;transform: translateY(-50%); '>Start posting a comment to see yourself in the network.</h3>");
		else
		{
			$('#sna_container').empty();
			drawSNA(selectedVideo["weights"],
					selectedVideo["strengths"],
					overall_data[selectedSubject]["groups"][selectedClass]["users"][selectedVideoID],
					selectedVideo["social_links"]);
		}
	}
	else
	{
		if(overall_data[selectedSubject]["groups"][selectedClass]["videos"][selectedVideoID]["weights"]["top_weight"] == 0)//Check that the selected video has at least 1 comment. By checking the top weight is not zero. To prevent error when drawing sna
			$('#sna_container').html("<h3 style='text-align:center;margin:0 0;position: relative;top: 50%;transform: translateY(-50%); '>Start posting a comment to see yourself in the network.</h3>");
		else
		{
			$('#sna_container').empty();
			drawSNA(selectedVideo["weights"],
					selectedVideo["strengths"],
					overall_data[selectedSubject]["groups"][selectedClass]["users"][selectedVideoID],
					selectedVideo["social_links"]);
		}
	}
	//End of SNA drawing setup
}
