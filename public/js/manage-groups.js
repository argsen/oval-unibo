// let groups;    //hold current course's groups info. (id, name, members)
function groupWithId(gid) {
    return $.grep(groups, function(val) {
                return val.id===gid;
            })[0];
}

function showMsg(message) {
    $(".msg").text(message);
    $(".msg").show();
    $("html, body").animate({scrollTop:0}, 'fast');
}

$(document).ready(function(){

    $(".msg").hide();
    $("#loading-hud").hide();

    $("#member-select").multiSelect({
        selectableHeader: "<div class=''>Available</div>",
        selectionHeader: "<div class=''>Selected</div>"
    });

    $("#edit-grp-members-select").multiSelect({
        selectableHeader: "<div class=''>Available</div>",
        selectionHeader: "<div class=''>Selected</div>"
    });

    $("#course-select").on("change", function() {
        let course_id = this.value;
        let select = $("#member-select");
        select.empty();
        select.multiSelect('refresh');

        $.ajax({
            type: "POST",
            url: "/get_students_in_course",
            data:{course:course_id},
            success: function(data){
                course_members = data;
                $.each(data, function(i, d) {
                    select.append('<option value="'+d.id+'">'+d.first_name+' '+d.last_name+'</option>');
                    select.multiSelect('refresh');
                })
            },
            error: function(req, status, err) {
                console.log("error /get_students_in_course - "+err);////////
            }
        });
    });

    $("#new-group-form").on("keydown", function(e) {
		if (e.which == 13) {
			e.preventDefault();
		}
	});
    $("#add-group-button").on("click", function(e) {
        
		if($("#new-group-form").validator('validate').has('.has-error').length) {
            e.preventDefault();
			return false;
		}
        $("#loading-hud").show();
        $(".msg").hide();
        
        let course = $("#course-select").val();
        let name = $("#group-name").val();
        let members = $("#member-select").val();

        $.ajax({
            type: "POST",
            url: "/add_group",
            data: {
                course: course,
                name: name,
                members: members
            },
            success: function(data) {
                $("#loading-hud").hide();
                if(data.msg) {
                    showMsg(data.msg);
                }
                if(data==='finished'){
                    showMsg('Group was successfully added');
                    $("#table-filter").trigger('change', course);
                    $("#new-group-form").trigger('reset');
                    $("#member-select").multiSelect('deselect_all').multiSelect("refresh");
                }
            },
            error: function(req, status, error) {
                console.log("error /add_group - "+error);/////
                showMsg('There was an error. Group could not be added.');
                $("#loading-hud").hide();
            }
        });

    });

    $("#table-filter").on("change", function() {
        let course_id = this.value;
        let tbody = $("#groups-table tbody");
        tbody.empty();

        $.ajax({
            type: "POST",
            url: "/get_groups_info_for_course",
            data: {course_id: course_id},
            success: function(data) {
                groups = [];
                $.each(data, function(i, d) {
                    let grp = {};
                    let tr = $("<tr/>").appendTo(tbody);
                   
                    //-- id --
                    grp.id = d.id;

                    //-- name --
                    tr.append('<td><span>'+d.name+'</span><br /><button class="btn btn-link edit-name-button" data-toggle="modal" data-target="#edit-name-modal" data-gid="'+d.id+'" title="Edit group name">Edit&nbsp;<i class="fas fa-pen-square"></i></button></td>');   
                    grp.name = d.name;

                    //-- members --
                    let member_td_html = '<td>';
                    let mbrs = [];
                    $.each(d.members, function(i, m){
                        let fullname = m.first_name+' '+m.last_name;
                        member_td_html += fullname+"<br />";
                        mbrs.push({id:m.id, name:fullname});
                    });
                    member_td_html += '<button class="btn btn-link edit-members-button" data-toggle="modal" data-target="#edit-members-modal" data-gid="'+d.id+'" title="Edit group members">Edit&nbsp;<i class="fas fa-pen-square"></i></button>';
                    member_td_html += '</td>';
                    tr.append(member_td_html);
                    grp.members = mbrs;
                    
                    tr.append('<td><button class="btn btn-link delete-button" data-gid="'+d.id+'" title="Delete"><i class="far fa-trash-alt"></i></button></td>')
                    groups.push(grp);
                });
            },
            error: function(req, status, error) {
                console.log("error /get_groups_info_for_course- "+error);/////
            }
        });
    });

    $("#edit-name-modal").on("show.bs.modal", function(e) {
        let group_id =  $(e.relatedTarget).data('gid');
        let group = groupWithId(group_id);
        $("#edit-grp-name-textbox").val(group.name);
        $("#update-group-name").attr("data-gid", group_id);
    });

    $("#update-group-name").on("click", function(e) {
        e.preventDefault();
        let group_id = $(this).attr('data-gid');
        let name = $("#edit-grp-name-textbox").val();
        $.ajax({
            type: "POST",
            url: "/edit_group_name",
            data: {
                group_id: group_id,
                name: name
            },
            success: function(data) {
                $("#edit-name-modal").modal('hide');
                if(data.name){
                    $.each(groups, function(i, val) {
                        if(val.id==group_id){
                            val.name = data.name;
                            return false;
                        }
                    });
                    $(".edit-name-button[data-gid="+group_id+"]").prev().prev().text(data.name);
                }
                else {
                    showMsg("Oops.. Something went wrong and Group Name wasn't saved.");
                }
            },
            error: function(req, status, err) {
                console.log("error /edit_group_name - "+err);//////
            }
        });
    });

    $("#edit-members-modal").on("show.bs.modal", function(e) {
        let group_id =  $(e.relatedTarget).data('gid');
        let group = groupWithId(group_id);

        $(this).find("span.group-being-edited").text(group.name);
        let sel = $("#edit-grp-members-select");
        sel.empty();
        sel.multiSelect('refresh');
        $.each(course_members, function(i, member) {
            if($.inArray(member.id, group.members.map(x=>x.id)) != -1) {
                sel.append('<option value="'+member.id+'" selected>'+member.first_name+' '+member.last_name+'</option>');
            }
            else {
                sel.append('<option value="'+member.id+'">'+member.first_name+' '+member.last_name+'</option>');
            }
            sel.multiSelect('refresh');
        });

        $("#update-group-members").attr("data-gid", group_id);
    });

    $("#update-group-members").on("click", function(e){
        e.preventDefault();
        let group_id = $(this).attr("data-gid");
        let member_ids = $("#edit-grp-members-select").val();
        $.ajax({
            type: "POST",
            url: "/edit_group_members",
            data: {group_id:group_id, member_ids:member_ids},
            success: function(data) {
                $("#edit-members-modal").modal("hide");
                $.each(groups, function(i, val) {
                    if(val.id==group_id){
                        val.members = data;
                        return false;
                    }
                });
                let div = $(".edit-members-button[data-gid="+group_id+"]").prev();
                div.empty();
                $.each(data, function(i, val) {
                    div.append(val.name+"<br />");
                });
            },
            error: function(req, status, err) {
                console.log("error /edit_group_members - "+err);/////
            }
        });
    });

    $("#groups-table").on("click", ".delete-button", function(e) {
        let button = $(this);
        let group_id = button.attr("data-gid");
        let group = groupWithId(Number(group_id));
        
        if(confirm('Are you sure to group "'+group.name+'"?')) {
            $.ajax({
                type: "POST",
                url: "/delete_group",
                data: {group_id: group_id},
                success: function(data){
                    button.parent().parent().remove();
                    if (data.result==="true"){
                        groups = $.grep(groups, function(x) {
                            return x.id === group_id;
                        });
                    }
                },
                error: function(req, status, error) {
                    console.log("error /delete_group - "+error);////////
                }
            });
        }
    });


});//end document.ready