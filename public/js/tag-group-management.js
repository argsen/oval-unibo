var defaultCovaaTagsArray = [{
    "id": 1,
    "text": "I THINK that...",
    "status": true,
    "item": [{
        "id": 1,
        "text": "In my opinion...",
        "status": true
    }, {
        "id": 2,
        "text": "My view is...",
        "status": true
    }, {
        "id": 3,
        "text": "I believe...",
        "status": true
    }, {
        "id": 4,
        "text": "I conclude...",
        "status": true
    }]
}, {
    "id": 2,
    "text": "I think so BECAUSE...",
    "status": true,
    "item": [{
        "id": 1,
        "text": "My opinion is support by...",
        "status": true
    }, {
        "id": 2,
        "text": "The evidence of my view is...",
        "status": true
    }, {
        "id": 3,
        "text": "The data/information that supports my conclusion is...",
        "status": true
    }]

}, {
    "id": 3,
    "text": "I AGREE...",
    "status": true,
    "item": [{
        "id": 1,
        "text": "I concur...",
        "status": true
    }, {
        "id": 2,
        "text": "I have the same idea...",
        "status": true
    }, {
        "id": 3,
        "text": "I share your/the authors view...",
        "status": true
    }, {
        "id": 4,
        "text": "That is a good point...",
        "status": true
    }]

}, {
    "id": 4,
    "text": "I DISAGREE...",
    "status": true,
    "item": [{
        "id": 1,
        "text": "I have a different idea...",
        "status": true
    }, {
        "id": 2,
        "text": "I dont share you/the author view...",
        "status": true
    }, {
        "id": 3,
        "text": "I beg to differ...",
        "status": true
    }]

}, {
    "id": 5,
    "text": "I need to ASK...",
    "status": true,
    "item": [{
        "id": 1,
        "text": "Why do you say that...?",
        "status": true
    }, {
        "id": 2,
        "text": "I am not sure I understand...",
        "status": true
    }, {
        "id": 3,
        "text": "Are you sure about...?",
        "status": true
    }, {
        "id": 4,
        "text": "What makes you say that...?",
        "status": true
    }, {
        "id": 5,
        "text": "How did you come to this conclusion...?",
        "status": true
    }]

}];
var covaaTagsArrayGlobal;

/**
 * @description: add covaa tag management functions
 */

function createCovaaTagManagement(covaaTagsArray, user_id) {

    // convert covaaTagsArray data type
    for(var i in covaaTagsArray){
        covaaTagsArray[i]['id'] = parseInt(covaaTagsArray[i]['id']);
        covaaTagsArray[i]['status'] = typeof(covaaTagsArray[i]['status']) === "string" ? (covaaTagsArray[i]['status'] === "true" ? true : false) : covaaTagsArray[i]['status'];
        
        for(var j in covaaTagsArray[i]['item']){
            covaaTagsArray[i]['item'][j]['id'] = parseInt(covaaTagsArray[i]['item'][j]['id']);
            covaaTagsArray[i]['item'][j]['status'] = typeof(covaaTagsArray[i]['item'][j]['status']) === "string" ? (covaaTagsArray[i]['item'][j]['status'] === "true" ? true : false) : covaaTagsArray[i]['item'][j]['status'];   
        }
    }

    var data = covaaTagsArray;

    var $ul = $('<ul style="cursor: pointer;"></ul>');

    for (var i in data) {
        if (data[i]['status']) {
            var $li = $('<li covaa-tag-group-id="' + data[i]['id'] + '"><span class="badge badge-info">' + data[i]['text'] + '</span>&nbsp;&nbsp;&nbsp;<i class="far fa-trash-alt"></i></li>');
            bindGroupClick($li.find('span'),user_id, data, data[i]['id']);
            bindGroupRemove($li.find('i'), user_id, data, data[i]['id']);
            $ul.append($li);
        }
    }

    $("#covaa-set-tag-group").empty();
    $("#covaa-set-tag-label").empty();
    $("#covaa-set-tag-group").append($ul);

}

function bindGroupClick($element, user_id, covaaTagsArray, tagGroupId) {

    var data;

    for (var i in covaaTagsArray) {

        if (covaaTagsArray[i]['id'] === tagGroupId) {
            data = covaaTagsArray[i]['item']
            break;
        }
    }

    $element.off('click');
    $element.on('click', function () {
        $("#covaa-set-tag-label").empty();
        var $ul = $('<ul style="cursor: pointer;"></ul>');
        for (var i in data) {
            if (data[i]['status']) {
                var $li = $('<li covaa-tag-group-label-id="' + data[i]['id'] + '"><span class="badge badge-success">' + data[i]['text'] + '</span>&nbsp;&nbsp;&nbsp;<i class="far fa-trash-alt"></i></li>');
                bindLabelRemove($li.find('i'), user_id, covaaTagsArray, tagGroupId, data[i]['id']);
                $ul.append($li);
            }
        }
        $("#covaa-set-tag-label").append($ul);

        //-- fill textbox with tag-group name --
        var $input = $("#covaa-set-tag-group-input >input");
        $input.tagsinput('removeAll');
        // $input.tagsinput('refresh');
        $input.tagsinput('add', $element.text());
        $input.tagsinput('refresh');

    });

}

function bindGroupRemove($element, user_id, covaaTagsArray, tagGroupId) {

    $element.off('click');
    $element.on('click', function () {

        var r = window.confirm("Are you sure you want to delete this tag group?");
        if (r == true) {
            // console.log('Tag group successfully deleted!');

            for (var i in covaaTagsArray) {
                var id = parseInt(covaaTagsArray[i]['id']);
                if (parseInt(tagGroupId) === id) {
                    covaaTagsArray[i]['status'] = false;
                }
            }

            update(user_id, covaaTagsArray, function(data){
                $("#covaa-tag-loading").hide();
                covaaTagsArrayGlobal = JSON.parse(data['covaa_tag_array']);
                createCovaaTagManagement(covaaTagsArrayGlobal, user_id);
            });


        } else {
            alert('You cancel action!');
        }

    });

}

function bindLabelRemove($element, user_id, covaaTagsArray, tagGroupId, tagGroupLabelId) {

    $element.off('click');
    $element.on('click', function () {

        var r = window.confirm("Are you sure you want to delete this label?");
        if (r == true) {
            // console.log('Label successfully deleted!');

            for (var i in covaaTagsArray) {
                var groupId = parseInt(covaaTagsArray[i]['id']);
                if (parseInt(tagGroupId) === groupId) {
                    for (var j in covaaTagsArray[i]['item']) {
                        var labelId = covaaTagsArray[i]['item'][j]['id'];
                        if (parseInt(tagGroupLabelId) == labelId) {
                            covaaTagsArray[i]['item'][j]['status'] = false;
                        }
                    }
                }
            }

            update(user_id, covaaTagsArray, function(data){
                $("#covaa-tag-loading").hide();
                covaaTagsArrayGlobal = JSON.parse(data['covaa_tag_array']);
                createCovaaTagManagement(covaaTagsArrayGlobal, user_id);
            });

        } else {
            alert('You cancel action!');
        }

    });
}

function update(user_id, covaaTagsArray, cb){

    $.post('/api/annonation/updateOrCreate', {
        user_id: user_id,
        covaa_tag_array: covaaTagsArray
    })
    .done(function (data) {
        cb(data);
    });

}

$(document).ready(function () {

    var user_id = parseInt($('.greetings').attr('userid'));

    $("#covaa-set-tag-group").empty();
    //get tag group info from server, if not use default
    $.get('/api/annonation/get',{
        user_id : user_id
    })
    .done(function(data){
        if(data){
            covaaTagsArrayGlobal = JSON.parse(data['covaa_tag_array']);
            createCovaaTagManagement(covaaTagsArrayGlobal, user_id);
        }else{
            covaaTagsArrayGlobal = defaultCovaaTagsArray;
            update(user_id, covaaTagsArrayGlobal, function(data){
                $("#covaa-tag-loading").hide();
                covaaTagsArrayGlobal = JSON.parse(data['covaa_tag_array']);
                createCovaaTagManagement(covaaTagsArrayGlobal, user_id);
            })
        }
    });



    $('#covaa-set-tag-group-input input').tagsinput({
        maxTags: 1,
        confirmKeys: [13, 44],
        trimValue: true
    });

    $('#covaa-set-tag-label-input input').tagsinput({
        confirmKeys: [13, 44],
        trimValue: true
    });

    $('#covaa-set-tag-group-input input').on('itemAdded', function (event) {
        $(this).attr('covaa-tag-group', event.item);
    });

    $('#covaa-set-tag-group-input input').on('itemRemoved', function (event) {
        $(this).attr('covaa-tag-group', '');
    });

    $('#covaa-set-tag-label-input input').on('itemAdded', function (event) {

        var attr = $(this).attr('covaa-tag-group-label');
        if (attr.length > 0) {
            var temp = attr.split(',');
            temp.push(event.item);
            $(this).attr('covaa-tag-group-label', temp.join(','));
        } else {
            $(this).attr('covaa-tag-group-label', event.item);
        }

    });

    $('#covaa-set-tag-label-input input').on('itemRemoved', function (event) {
        var temp = $(this).attr('covaa-tag-group-label').split(',');

        for (var i in temp) {
            if (temp[i] === event.item) {
                temp.splice(i, 1);
            }
        }

        $(this).attr('covaa-tag-group-label', temp.join(','));
    });


    $("#covaa-add-tag-group").on('click', function (event) {
        event.preventDefault();

        var covaaTagGroup = $('#covaa-set-tag-group-input > input').attr('covaa-tag-group');
        var covaaTagGroupLabel = $('#covaa-set-tag-label-input > input').attr('covaa-tag-group-label');

        $('#covaa-tag-loading').css({
            'display': 'inline-block'
        });

        if (covaaTagGroup && covaaTagGroupLabel) {

            //update covaaTagsArrayGlobal
            for (var i = 0; i < covaaTagsArrayGlobal.length; i++) {

                if (covaaTagsArrayGlobal[i]['text'] === covaaTagGroup) {

                    covaaTagsArrayGlobal[i]['status'] = true;
                    //update exist group
                    var covaaTagGroupLabelArray = covaaTagGroupLabel.split(',');

                    for (var j in covaaTagGroupLabelArray) {

                        for (var q = 0; q < covaaTagsArrayGlobal[i]['item'].length; q++) {

                            if (covaaTagsArrayGlobal[i]['item'][q]['text'] === covaaTagGroupLabelArray[j]) {

                                covaaTagsArrayGlobal[i]['item'][q]['status'] = true;

                                break;

                            } else if (q === covaaTagsArrayGlobal[i]['item'].length - 1) {

                                //add new lable
                                var temp = {
                                    "id": covaaTagsArrayGlobal[i]['item'].length + 1,
                                    "text": covaaTagGroupLabelArray[j],
                                    "status": true
                                }

                                covaaTagsArrayGlobal[i]['item'].push(temp);

                            }

                        }

                    }

                    break;

                } else if (i === covaaTagsArrayGlobal.length - 1) {
                    //add new group
                    var temp = {
                        "id": covaaTagsArrayGlobal.length + 1,
                        "text": covaaTagGroup,
                        "status": true,
                        "item": []
                    }

                    var covaaTagGroupLabelArray = covaaTagGroupLabel.split(',');

                    for (var j = 0; j < covaaTagGroupLabelArray.length; j++) {
                        var tempItem = {
                            "id": j + 1,
                            "text": covaaTagGroupLabelArray[j],
                            "status": true
                        }

                        temp['item'].push(tempItem);
                    }

                    covaaTagsArrayGlobal.push(temp);

                    break;
                }
            }

            update(user_id, covaaTagsArrayGlobal, function(data){
                $("#covaa-tag-loading").hide();
                covaaTagsArrayGlobal = JSON.parse(data['covaa_tag_array']);
                createCovaaTagManagement(covaaTagsArrayGlobal, user_id);
            });        

        } else {
            alert('please input all group & labels.');
        }

    });
});
