/**
 * @description: gloabal variables
 */

var viewAllAnnotations = true; //should default to true because students should see instructor's and TA's comment and vice versa.

var annotations = [];
var comments = [];
var item; //used to hold annotation/comment item being edited

var previewLen = 200; //val from common.inc
var previewOffsetX = -10; //val from common.inc
var previewOffsetY = 3; //val from common.inc

/**
 * @description: tracking global variables
 */

var previous_user_id = window.localStorage.getItem('user_id') || 0;
var trackings = [];

var item_start_time = null;          //start_time used in modal-form 

/**
 * @description: tags comment global variables
 */
var PAGE_NUMBER = 5;
var usersArray = [];
var commentsArray = [];
var covaaTagsArray = [];
var covaaBadgeArray = [];
var defaultCovaaBadgeArray = [];
var defaultCovaaTagsArray =[];

/**
 * @description: utility functions
 */

function init() {

	//-- hide plain comments
	$("#right-side-tabs").find("li:first").hide();
	$("#discussion-tab").click();
	$("#plain-comments").hide();


	$('#close-nav').on('click', function(){
		$(".canvas").toggleClass('canvas-off',1000);
	});

	//init font awesome 
	FontAwesomeConfig = {
		searchPseudoElements: true
	};

	if (previous_user_id != user_id) {
		window.localStorage.setItem('trackings', JSON.stringify(trackings));
	} else {
		trackings = JSON.parse(window.localStorage.getItem('trackings'));
	}

	//init annotation
	getAllAnnotations();
	getComments();
	// paddingTop();

	//init covaa tag comment user array
	for (var i in group_members) {
		var item = {};
		item['id'] = group_members[i]['id'];
		item['fullname'] = group_members[i]['name'];
		item['email'] = '';
		item['profile_picture_url'] = '';
		usersArray.push(item);
	}

	//init covaa tag comment label
	defaultCovaaTagsArray = [{
		"id": 1,
		"text": "I THINK that...",
		"status": true,
		"item": [{
			"id": 1,
			"text": "In my opinion...",
			"status": true
		}, {
			"id": 2,
			"text": "My view is...",
			"status": true
		}, {
			"id": 3,
			"text": "I believe...",
			"status": true
		}, {
			"id": 4,
			"text": "I conclude...",
			"status": true
		}]
	}, {
		"id": 2,
		"text": "I think so BECAUSE...",
		"status": true,
		"item": [{
			"id": 1,
			"text": "My opinion is support by...",
			"status": true
		}, {
			"id": 2,
			"text": "The evidence of my view is...",
			"status": true
		}, {
			"id": 3,
			"text": "The data/information that supports my conclusion is...",
			"status": true
		}]

	}, {
		"id": 3,
		"text": "I AGREE...",
		"status": true,
		"item": [{
			"id": 1,
			"text": "I concur...",
			"status": true
		}, {
			"id": 2,
			"text": "I have the same idea...",
			"status": true
		}, {
			"id": 3,
			"text": "I share your/the authors view...",
			"status": true
		}, {
			"id": 4,
			"text": "That is a good point...",
			"status": true
		}]

	}, {
		"id": 4,
		"text": "I DISAGREE...",
		"status": true,
		"item": [{
			"id": 1,
			"text": "I have a different idea...",
			"status": true
		}, {
			"id": 2,
			"text": "I dont share you/the author view...",
			"status": true
		}, {
			"id": 3,
			"text": "I beg to differ...",
			"status": true
		}]

	}, {
		"id": 5,
		"text": "I need to ASK...",
		"status": true,
		"item": [{
			"id": 1,
			"text": "Why do you say that...?",
			"status": true
		}, {
			"id": 2,
			"text": "I am not sure I understand...",
			"status": true
		}, {
			"id": 3,
			"text": "Are you sure about...?",
			"status": true
		}, {
			"id": 4,
			"text": "What makes you say that...?",
			"status": true
		}, {
			"id": 5,
			"text": "How did you come to this conclusion...?",
			"status": true
		}]

	}]

	defaultCovaaBadgeArray = [{
		name: "reward",
		count: 0,
		iconURL: '/img/medal_1.svg',
		userHasVoted: false
	}, {
		name: "trophy",
		count: 0,
		iconURL: '/img/trophy.svg',
		userHasVoted: false
	}]



	$.get('/api/annonation/getlatestbygroupvideoid',{
		group_video_id: group_video_id
	})
	.done(function (data) {
		if(data){
			var temp = JSON.parse(data['covaa_tag_array']);
			for(var i in temp){
				if(temp[i]['is_choose'] === 'true'){
					covaaTagsArray.push(temp[i]);
				}
			}

			// convert covaaTagsArray data type
			for(var i in covaaTagsArray){
				covaaTagsArray[i]['id'] = parseInt(covaaTagsArray[i]['id']);
				covaaTagsArray[i]['status'] = typeof(covaaTagsArray[i]['status']) === "string" ? (covaaTagsArray[i]['status'] === "true" ? true : false) : covaaTagsArray[i]['status'];
				
				for(var j in covaaTagsArray[i]['item']){
					covaaTagsArray[i]['item'][j]['id'] = parseInt(covaaTagsArray[i]['item'][j]['id']);
					covaaTagsArray[i]['item'][j]['status'] = typeof(covaaTagsArray[i]['item'][j]['status']) === "string" ? (covaaTagsArray[i]['item'][j]['status'] === "true" ? true : false) : covaaTagsArray[i]['item'][j]['status'];   
				}
			}

			createCovaaTagComment(group_video_id, null, null, null);

		}else{
			alert('Tag group has not been assigned, system use default tag group, please inform instructor to set.')
			covaaTagsArray = defaultCovaaTagsArray;
			createCovaaTagComment(group_video_id, null, null, null);
		}

	})
	.fail(function (jqXHR, textStatus) {
		window.location.href = '/';
	});

}

function dateStringFromSqlTimestamp(timestamp) {
	var formattedDate = "";
	if (timestamp != undefined) {
		var months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];

		// Split timestamp into [ Y, M, D, h, m, s ]
		var t = timestamp.split(/[- :]/);
		// Apply each element to the Date function
		var dateObj = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
		//formattedDate = months[(dateObj.getMonth())] + " " + dateObj.getDate() + ", " + dateObj.getFullYear();
		// formattedDate = dateObj.getDate()+"-"+months[(dateObj.getMonth())]+"-"+ dateObj.getFullYear();
		// formattedDate = dateObj.toLocaleDateString()+" "+dateObj.toLocaleTimeString();
		var hour = dateObj.getHours();
		var ampm = hour > 12 ? "PM" : "AM"
		hour = hour > 12 ? hour - 12 : hour;
		var min = dateObj.getMinutes();
		min = min > 10 ? min : "0" + min;
		formattedDate = hour + ":" + min + ampm + " " + dateObj.getDay() + " " + months[(dateObj.getMonth())] + ", " + dateObj.getFullYear();
	}
	return formattedDate;
}

function secondsToMinutesAndSeconds(seconds) {
	var h = Math.floor(seconds / 3600);
	var m = Math.floor(seconds / 60);
	var s = Math.floor(seconds % 60);

	h = (h > 0) ? h + ":" : "";
	m = (m < 10) ? "0" + m : m;
	s = (s < 10) ? "0" + s : s;
	var retVal = h + m + ":" + s;

	return retVal;
}

function commaDelimitedToArray(commaDelimited) {
	//--remove extra spaces first
	var str = commaDelimited.replace(/\s+/g, ' ').replace(/\s*,\s*/g, ',');
	var arr = str.split(',');
	return arr;
}

function unescapeHtml(safe) {
	return safe ? safe.replace(/&amp;/g, '&')
		.replace(/&lt;/g, '<')
		.replace(/&gt;/g, '>')
		.replace(/&quot;/g, '"')
		.replace(/&#039;/g, "'") :
		"";
}

function compareY(a, b) {
	var retVal = 0;
	if (a.y > b.y) {
		retVal = 1;
	} else if (a.y < b.y) {
		retVal = -1;
	}
	return retVal;
}

/**
 *@description: annotations & comments
 */

function getAllAnnotations() {
	annotations.splice(0, annotations.length);
	$.ajax({
		type: "POST",
		url: "/get_annotations",
		data: {
			course_id: course_id,
			group_id: group_id,
			video_id: video_id
		},
		success: function (data) {
			annotations = data;
			//--sort annotations by id of author
			annotations.sort((a,b)=>a.author_id-b.author_id);
			layoutAnnotations();
			trackingInitial({
				event: 'click',
				target: '#annotations-list .annotation-button',
				info: 'View an annotation'
			}, trackings);
		},
		error: function (request, status, error) {
			console.log("error get_annotations - " + error); /////
		}
	});
}

function generateTrendline() {
	var canvas = document.getElementById("trends");
	if (canvas.getContext) {
		var ctx = canvas.getContext('2d');
		canvas.width = $("#annotations-list").width() - 2;
		canvas.height = 25;
		var y = 25;
		var w = $("#annotations-list").width();
		ctx.lineWidth = 3;
		ctx.strokeStyle = "#F9B200";
		ctx.beginPath();
		for (i = 0; i < annotations.length; i++) {
			var x = annotations[i].start_time / video_duration * w;
			x = (x == w) ? x - 1.5 : x; ///////dodgey fix.. so annotation at end of video shows
			ctx.moveTo(x, 0);
			ctx.lineTo(x, y);
		}
		ctx.stroke();
	} else {
		//--canvas isn't supported--
		console.log("canvas not supported"); ///////////
	}
}

function saveTracking(record) {
	var data = [record];
	$.ajax({
		type: "POST",
		url: "/add_trackings",
		data: {
			data: data,
			group_video_id: group_video_id
		},
		success: function (data) {},
		error: function (request, status, error) {
			console.log("saveTracking error: " + error); /////
		}
	});
}

function trackingInitial(record, trackings) {
	$(record.target).on(record.event, function () {
		if (typeof record.info == 'function') {
			trackings.push({
				target: record.target,
				event: record.event,
				info: record.info(),
				event_time: Date.now()
			});
		} else {
			trackings.push({
				target: record.target,
				event: record.event,
				info: record.info,
				event_time: Date.now()
			});
		}

		if (trackings.length < 3) {
			window.localStorage.setItem('trackings', JSON.stringify(trackings));
		} else {
			var temp = trackings;
			trackings = [];
			$.ajax({
				type: "POST",
				url: "/add_trackings",
				data: {
					data: temp,
					group_video_id: group_video_id
				},
				success: function (data) {
					window.localStorage.setItem('trackings', JSON.stringify(trackings));
				},
				error: function (request, status, error) {
					trackings = trackings.concat(temp);
					window.localStorage.setItem('trackings', JSON.stringify(trackings));
				},
				async: false
			});
		}
	});
}

function layoutAnnotations(mode) {
	if (!mode) {
		mode = ALL;
	}
	var anno_list = $("#annotations-list");

	if (annotations.length == 0) {
		var noAnnotationText = "<br/>There is no annotation for this video yet.<br/>Add annotations as you find points of interest so you can review them later!</br> ";
		anno_list.html("<div class=\"no-annotation\">" + noAnnotationText + "</div>");
		$("#instructor-annotations").remove();
		$("#student-annotations").remove();
	} else {
		if($(".no-annotation").length) {
			$(".no-annotation").remove();
			$("#annotations-list").append("<div id=\"instructor-annotations\"></div>");
			$("#annotations-list").append("<div id=\"student-annotations\"></div>");
		}
		

		var x = 0;
		var y = 0;
		var iconsize = 32;
		var placed = [];
		var actual_width = anno_list.width() - iconsize / 2;
		var paddingX = 7;

		var $inst_list = $("#instructor-annotations");
		var $st_list = $("#student-annotations");
		$inst_list.empty().append("<div class='section'></div>");
		$st_list.empty().append("<div class='section'></div>");

		var icon_tag;
		var last_user = annotations[0].author_id;
		var $current_user_section = null;
		let $current_list = null;
		

		$.each(annotations, function (i, a) {
			if ((mode == MINE && !a.mine) || (mode == STUDENTS && a.by_instructor) || (mode == INSTRUCTORS && !a.by_instructor)) {
				return 1;
			}
			if (a.mine) {
				icon_tag = '<i class="far fa-dot-circle" aria-hidden="true"></i>';
			} else if (a.by_instructor) {
				icon_tag = '<i class="fas fa-info-circle blue" aria-hidden="true"></i>';
			} else {
				icon_tag = '<i class="far fa-user-circle pink" aria-hidden="true"></i>';
			}

			//--annotations is sorted by user_id, so go through checking for user_id 
			//--to see if it's same as last one and add section to instructor/student area
			if(i==0) {
				$current_user_section = a.by_instructor ? $inst_list.find(".section:first") : $st_list.find(".section:first");
				$current_user_section.append(`<button class="btn btn-link annotation-list-toggler" type="button" 
												data-target="#annotations-list-${a.author_id}">
												${a.name}
												</button>`);
				$current_list = $(`<div id="annotations-list-${a.author_id}" class="position-relative"></div>`)
				$current_user_section.append($current_list);
			}
			else if (a.author_id!=last_user) {
				var max_y = 0;
				$current_list.find(".annotation-icon").each(function () {
					max_y = Math.max($(this).position().top, max_y);
				});
				var height = max_y + $(".annotation-icon").height();
				$current_list.height(height);

				if(a.by_instructor) {
					$inst_list.append(`<div class='section'></div>`);
					$current_user_section = $inst_list.find(".section:last");	
				}
				else {
					$st_list.append("<div class='section'></div>");
					$current_user_section = $st_list.find(".section:last");
				}
				
				$current_user_section.append(`<button class="btn btn-link annotation-list-toggler" type="button" 
													data-target="#annotations-list-${a.author_id}">
												${a.name}
												</button>`);
				$current_list = $(`<div id="annotations-list-${a.author_id}" class="position-relative"></div>`);
				$current_user_section.append($current_list);
				placed = [];
				last_user = a.author_id;
			}
			var start_ratio = a.start_time / video_duration;
			x = Math.floor(actual_width * start_ratio) - paddingX;
			y = 0;
				
			$.each(placed, function (j, val) {
				if ((x >= val.x && x <= val.x + iconsize && y == val.y) || (x + iconsize >= val.x && x <= val.x + iconsize && y == val.y)) {
					y += iconsize;
				}
			});
			placed.push({
				x: x,
				y: y
			});
			placed.sort(compareY);

			var style = "left:" + x + "px; top:" + y + "px;";
			$current_list.append('<div class="annotation-icon" style="' + style + '"><button type="button" class="btn btn-link annotation-button" data-id="' + a.id + '">' + icon_tag + '</button></div>');
		});
		//--set height of last section
		var max_y = 0;
		$current_list.find(".annotation-icon").each(function () {
			max_y = Math.max($(this).position().top, max_y);
		});
		var height = max_y + $(".annotation-icon").height();
		$current_list.height(height);

		$("#trends").width($("#annotations").width() - 3);
		
	}
	$("#annotations").on("click", ".annotation-list-toggler", function() {
		let target = $(this).attr("data-target");
		$(this).toggleClass("is_hidden");
		$(target).toggle();
	});
	generateTrendline();
}

function getNominatedStudentList(itemType, itemId) {
	if (!itemId) {
		populateNominatedStudentList(null);
	} else {
		$.ajax({
			type: "POST",
			url: "/get_nominated_students_ids",
			data: {
				item: itemType,
				item_id: itemId
			},
			success: function (data) {
				populateNominatedStudentList(data.nominated);
			},
			error: function (request, status, error) {
				console.log("error get_nominated_students_ids: " + error); ///////
			}
		});
	}
}

function populateNominatedStudentList(nominated) {
	var list = $("#nominated-students-list");
	list.html("");
	// list.append('<option value="" disabled></option>');
	var selectedItems = [];
	$.each(group_members, function (i, member) {
		list.append('<option value="' + member.id + '">' + member.name + '</option>');
		if (nominated && nominated.length > 0) {

			if ($.inArray(member.id, nominated) != -1) {
				selectedItems.push(member.id);
			}
		}
	});
	list.val(selectedItems);
	list.multiSelect('refresh');

}

function saveFeedbacksAndConfidenceLevel(comment) {
	item = comment;
	var level = $("select[name=confidence-level]").val();
	var confidence_level = level > 0 ? $("select[name=confidence-level]").val() : 0;
	var answers = [];
	$('input[id^="point"]').each(function () {
		var a = new Object();
		var i = $(this).attr('id');
		var regex = /point([0-9]*)-(yes)?(no)?/g;
		var point_id = regex.exec(i)[1];
		a['point_id'] = point_id;
		var b = $(this).prop('checked');
		a['answer'] = b ? 1 : 0;
		answers.push(a);
	});
	$.ajax({
		type: "POST",
		url: "/save_feedback",
		data: {
			comment_id: item.id,
			answers: answers,
			confidence_level: confidence_level
		},
		error: function (request, status, error) {
			console.log("request.status: " + request.status + " error " + error + "<error>" + request.responseText + "</error>"); /////
		},
		async: false
	});
}

function paddingTop() {
	var navbarHeight = $('.canvas > .navbar').height();
	var coursebarHeight = $('.canvas > .courses-bar').height();
	var windowWidth = $(window).width();

	$('.main-content').css({
		// 'margin-top': navbarHeight + coursebarHeight + 'px',
		'width': windowWidth -40 + 'px',
	})

}

function createCovaaTagComment(group_video_id, annotation_id, annotation_time, annotation_title){

	if(annotation_id != null){
		var anno = unescapeHtml(annotation_title);

		var $commentsHeader = $('#comments-header');
		$commentsHeader.empty();
		$commentsHeader.append(`
		<div class="row">
			<div class="col-md-6 time-label">${annotation_time}</div>
			<button class="btn btn-outline-info col-md-6 float-right"><i class="fas fa-plus-circle"></i>&nbsp;Reply</button>
		</div><!-- row -->
		<div class="row">
			<div class="col">${anno}</div>
		</div><!-- row -->
		`);

		$.get('/api/comments/count', {
			group_video_id: group_video_id,
			annotation_id: annotation_id, 
		})
		.done(function (data) {
	
			var totalPages = Math.ceil(parseInt(data['total']) / PAGE_NUMBER);
			var $pagination = $('#covaa-pagination');
	
			$pagination.twbsPagination('destroy');
	
			$pagination.twbsPagination({
				totalPages: totalPages === 0 ? 1 : totalPages,
				visiblePages: 10,
				first: '<i class="fas fa-step-backward"></i>',
				prev: '<i class="fas fa-chevron-left"></i>',
				next: '<i class="fas fa-chevron-right"></i>',
				last: '<i class="fas fa-step-forward"></i>',
				onPageClick: function (event, page) {
	
					//scroll to top
					//$('html').scrollTop(0);
	
					$.get('/api/comments/get', {
							group_video_id: group_video_id,
							annotation_id: annotation_id, 
							number: PAGE_NUMBER,
							skip: (page - 1) * PAGE_NUMBER
						})
						.done(function (data) {
		
							for(var i in data['allPost']){
								if(data['allPost'][i]['parent'] === 0){
									data['allPost'][i]['parent'] = null;
								}
							}
							commentsArray = data['allPost'];


							//set uservote value to false
							for (var i in commentsArray) {
								var covaa_badge_array = JSON.parse(commentsArray[i]['covaa_badge_array']);
								for (var j in covaa_badge_array) {
									covaa_badge_array[j].count = parseInt(covaa_badge_array[j].count);
									covaa_badge_array[j].userHasVoted = false;
								}

								commentsArray[i]['covaa_badge_array'] = JSON.stringify(covaa_badge_array);

							}
	
							//order it
							function compare(property){
								return function(obj1,obj2){
									var value1 = obj1[property];
									var value2 = obj2[property];
									return value1 - value2;     // Desc
								}
							   }	
							
							commentsArray.sort(compare('id'));
	
							//pre-process date formate
							for(var i in commentsArray){
								commentsArray[i]['created'] = commentsArray[i]['created_at'];
								commentsArray[i]['modified'] = commentsArray[i]['updated_at'];
							}
							
							var saveComment = function (data) {
	
								// Convert pings to human readable format
								$(data.pings).each(function (index, id) {
									var user = usersArray.filter(function (user) {
										return user.id == id
									})[0];
									data.content = data.content.replace('@' + id, '@' + user.fullname);
								});
						
								return data;
							}
	
							$('#comments-container').comments({
								enableDeleting: false,
								covaa_badge_array: defaultCovaaBadgeArray,
								covaaTags: covaaTagsArray,
								profilePictureURL: '/img/user.svg',
								currentUserId: user_id,
								roundProfilePictures: true,
								textareaRows: 4,
								enableAttachments: false,
								enableHashtags: false,
								enablePinging: true,
								getUsers: function (success, error) {
									setTimeout(function () {
										success(usersArray);
									}, 500)
								},
								getComments: function (success, error) {

									for(var i in commentsArray){
										if(parseInt(commentsArray[i]['creator']) === user_id){
											commentsArray[i]['created_by_current_user'] = true;
										}else{
											commentsArray[i]['created_by_current_user'] = false;
										}
									}

									setTimeout(function () {
										success(commentsArray);
									}, 500);
								},
								postComment: function (commentJSON, success, error) {
						
									var tempCommentJSON = JSON.parse(JSON.stringify(commentJSON));
						
									//re-structure json data
									tempCommentJSON['group_video_id'] = group_video_id;
									tempCommentJSON['annotation_id'] = annotation_id;
									delete tempCommentJSON['id'];
									tempCommentJSON['creator'] = user_id;
									tempCommentJSON['fullname'] = user_fullname;
									tempCommentJSON['created_by_admin'] = false;
									tempCommentJSON['created_by_current_user'] = false;
									tempCommentJSON['pings'] = JSON.stringify(commentJSON['pings']);
									delete tempCommentJSON['created'];
									delete tempCommentJSON['modified'];
									var tempStr = tempCommentJSON['content'].replace('value="@You"', 'value="@' + user_fullname + '" data-value="' + user_id + '"');
									tempCommentJSON['content'] = tempStr;

									var verifiedData = verified(tempStr);

									$.post('/api/comments/create', tempCommentJSON)
									.done(function (data) {
										commentJSON['id'] = data['id'];
										success(commentJSON);
									});
								},
								putComment: function (data, success, error) {
						
									//re-structure json data
									data['pings'] = JSON.stringify(data['pings']);

									var tempStr = data['content'];

									var verifiedData = verified(tempStr);

										$.post('/api/comments/edit', data)
											.done(function(){
												data['pings'] = JSON.parse(data['pings']);
												success(saveComment(data));
											});
								},
								deleteComment: function (data, success, error) {
						
									//delete function 
									$.post('/api/comments/delete', data)
									.done(function(){
										success();
									});
						
								},
								cancelComment: function (data, success, error){
									success(data);
								},
								badgeComment: function (data, success, error){									

									$.post('/api/annonation/updateBadge',{
										id: data['id'],
										covaa_badge_array: data['covaa_badge_array']
									})
									.done(function(){
										//console.log(data);
										success(data);
									})

								},
								upvoteComment: function (data, success, error) {

									$.post('/api/annonation/updateVote',{
										id: data['id'],
										upvote_count: data['upvote_count']
									})
									.done(function(){
										success(data);
									})

								},
								// rewardComment: function (data, success, error) {
								// 	$.post('/api/annonation/updateReward',{
								// 		id: data['id'],
								// 		reward_count: data['reward_count']
								// 	})
								// 	.done(function(){
								// 		success(data);
								// 	})
								// },
								// trophyComment: function (data, success, error){

								// 	$.post('/api/annonation/updateTrophy',{
								// 		id: data['id'],
								// 		trophy_count: data['trophy_count']
								// 	})
								// 	.done(function(){

								// 		success(data);
								// 	})
								// }
								// uploadAttachments: function (dataArray, success, error) {
								// 	setTimeout(function () {
								// 		success(dataArray);
								// 	}, 500);
								// },
							}, function(){
								$('#comments-container .main').hide();
							});
	
						})
						.fail(function (jqXHR, textStatus) {
							window.location.href = '/';
						});
				}
			});
	
			//-- make it same height as left side
			$('#comments-container .data-container').css({'height': $('#left-side').height() - 143, 'overflow-y':'scroll'})
		})
		.fail(function (jqXHR, textStatus) {
			window.location.href = '/';
		});
	
	}else{


		var $commentsHeader = $('#comments-header');
		var $commentsContainer = $('#comments-container');
		var placeholder = '<div class="covaa-placeholder"><i class="far fa-comments"></i><p>Please click annotations to discuss!</p></div>';
		
		$commentsHeader.empty();
		$commentsContainer.empty();
		$commentsContainer.append(placeholder);

	}



}
/**
 * 
 * @param {*} root 
 * @param {*} covaaTags 
 * @param {*} existing_annotation object in format {"id": <number>, "html":<html text>}
 */
function addTagGrouptoAddAnnotation(root, covaaTags, existing_annotation) {

	var textarea = $("#annotation-instruction-textarea");
	textarea.html("");

	var covaaTags = covaaTags;
	let covaaTagarea = $("<div/>", {
		"class": "covaa_tag_comment_fields_container"
	});
	var covaa_tag_accordion = $("<div id='add_annotation_covaa_tag_accordion' class='accordion covaa-tag-accordion'></div>");
	covaaTagarea.append(covaa_tag_accordion);

	for (var i in covaaTags) {
		var filter = covaaTags[i]['status'];
		if (!filter) {
			continue;
		}
		covaa_tag_accordion.append(`
		<div class="card covaa-tag-card">
			<div class="m-1" class="header${covaaTags[i].id}">

					<button class="btn covaa-tag collapsed collapse_trigger" type="button" data-toggle="collapse" aria-expanded="false">
					${covaaTags[i].text}
					</button>

			</div>
			<div class="collapse" data-parent="#add_annotation_covaa_tag_accordion">
				<div class="card-body covaa-tag-card-body card-body-${covaaTags[i].id}">
				</div>
			</div>
		</div>

		`);
		var card_body = covaaTagarea.find(".card-body-"+covaaTags[i].id);
		for(var j in covaaTags[i].item) {
			card_body.append(`
					<button type="button" class="btn d-block covaa-tag-toggle-button" data-toggle="button" aria-pressed="false" autocomplete="off" data-covaa-group-id="${covaaTags[i].id}" data-covaa-group-sub-id="${covaaTags[i].item[j].id}">
						${covaaTags[i].item[j].text}
					</button>
			`);
		}

		card_body.append(`
			<textarea class="form-control covaa-comment-textarea" data-covaa-group-id="${covaaTags[i].id}" />
		`);
	}//end for (i in covaaTag)

	root.empty();
	root.append(covaaTagarea);

	root.off("click", ".collapse_trigger");
	root.on("click", ".collapse_trigger", function() {
		let state = $(this).attr("aria-expanded");
		let new_state = state=="true" ? "false" : "true";
		$(this).parent().next().collapse("toggle");
		$(this).attr("aria-expanded", new_state);
		if(new_state == "true") {
			$(this).parent().parent().siblings().find("button").attr("aria-expanded", state);
		}
	});
	 //-- when tag is selected and/or comment is written, put it in hidden textarea div
	 root.off("click", ".covaa-tag-toggle-button");
	 root.on("click", ".covaa-tag-toggle-button", function() {
		let group_id = $(this).data("covaa-group-id");
		let group_name = $(this).parent().parent().siblings().find("button").text().trim();
		let tag_id = $(this).data("covaa-group-sub-id");
		let tag_name = $(this).text().trim();
		let textarea_content = textarea.html();
		let the_div = textarea.find(`[data-tag-group-id="${group_id}"]`);
		
		if($(this).hasClass("active")) {//-- remove
			$(the_div[0]).find(`.covaa-comment-selected-tag-div[data-tag-id='${tag_id}']`).remove();
		}
		else {//-- add
			let color = $(this).addClass("active").css("color");
			$(this).removeClass("active");
			if(the_div.length){
				$(the_div[0]).find(".covaa-comment-for-tag-group").before(`
					<div class="covaa-comment-selected-tag-div" data-tag-id="${tag_id}" style="color:${color}">${tag_name}</div>`);
			}                
			else {
				textarea.append(`
					<div class="covaa-comment-tag-group-div" data-tag-group-id="${group_id}" data-tag-group-name="${group_name}">
						<span class="covaa-tag">${group_name}</span>
						<div class="covaa-comment-selected-tag-div" data-tag-id="${tag_id}" style="color:${color}">${tag_name}</div>
						<div class="covaa-comment-for-tag-group">${$(this).siblings().find("textarea").val() ? $(this).siblings().find("textarea").val() : ""}</div>
					</div>`);
			}
		}
		//-- make it to show "save" button
		textarea.click().trigger("change");
		
	});
	root.off("change", ".covaa-comment-textarea");
	root.on("change", ".covaa-comment-textarea", function() {
		let group_id = $(this).data("covaa-group-id");
		let group_name = $(this).parent().parent().siblings().find("button").text().trim();
		let comment_text = $(this).val() ? $(this).val() : "";
		let group_div = textarea.find(`.covaa-comment-tag-group-div[data-tag-group-id=${group_id}]`);
		let comment_div = group_div.find(".covaa-comment-for-tag-group");
		comment_div.text(comment_text).click().trigger("change");
	});

	//-- if existing annotation, set up the controls to match 
	if(existing_annotation) {
		let $dom = $(existing_annotation.html);
		for(let i=0; i<$dom.length; i++) {
			tag_group = $dom[i];
			if(tag_group.nodeType==3) {
				//--ignore empty text nodes...
				continue;
			}
			let tag_group_id = $(tag_group).data("tag-group-id");
			let tags = $(tag_group).children();
			for(let j=0; j<tags.length; j++) {
				$node = $(tags[j]);
				if($node.hasClass("covaa-comment-selected-tag-div")) {
					covaaTagarea.find(`button[data-covaa-group-id="${tag_group_id}"][data-covaa-group-sub-id="${$node.data("tag-id")}"]`).click();
				}
				else if ($node.hasClass("covaa-comment-for-tag-group")) {
					//-- comment text
					covaaTagarea.find(`textarea.covaa-comment-textarea[data-covaa-group-id="${tag_group_id}"]`).val($node.text());
				}
			}
		}
	}
	//-- click the first tag group to expand that card
	covaaTagarea.find(`button.covaa-tag`).first().click();
}

function isAllTagGroupExist(covaaTags, customData){

	var tempCovaaTags = JSON.parse(JSON.stringify(covaaTags));
	for(var i in customData){
		var covaaGroupId = parseInt(customData[i]['id'].split('-')[0]);
		for(var j in tempCovaaTags){
			if(tempCovaaTags[j]['id'] === covaaGroupId){
				var index = tempCovaaTags.indexOf(tempCovaaTags[j]);
				if (index > -1) {
					tempCovaaTags.splice(index, 1);
				}
			}
		}
	}

	if(tempCovaaTags.length === 0){
		return true;
	}else{
		return false;
	}
}

function verified(data){

	var verifiedData = [];

	$(data).find('li').each(function(){
		if($(this).text().length != 0){
			var item = {id: "", label:""};
			item.id = $(this).attr("covaa-group-id") + '-' + $(this).attr("covaa-group-sub-id");
			item.label = "";
			verifiedData.push(item);
		}
	});

	return verifiedData;
}

function getComments() {
	comments = [];
	$.ajax ({
		type: "POST",
		url: "/get_comments",
		data: {group_video_id: group_video_id, course_id: course_id},
		success: function (data) {
			if (data) {
				comments = data.slice();
				$("#plain-comments .comments-box").empty().html(formatComments());
				$(".comments-box").getNiceScroll().resize();
				trackingInitial({event: 'click', target: '.edit-comment-button', info: 'Edit comment'}, trackings);
			}
		},
		error: function(request, status, error) {
			console.log("home.js, getComments() ajax error: "+request.status+", error: "+error+"<error>"+request.responseText+"</error>");
			if(status=="401") {
				window.location = "/logout";
			}
		},
		async: false
	});
}

function formatComments() {
	var html = "";	//return val
    if (comments == null || comments.length <= 0) {
    	// Display a single, uneditable entry telling people to post something.
    	html = "<div id=\"no-comment-msg\" class=\"comment-text\">There is no comment for this video yet. You can add yours and review them later!</div>";

    } else {
		var list = "";
	    $.each(comments, function(index, value) {
	        html += formatOneComment(value);
	    });
    }
    return html;
}//end formatComments
function formatOneComment(comment) {
	var description = comment.description;
	description = $("<div/>").text(unescapeHtml(description)).html();
		
	// var commentDate = dateStringFromSqlTimestamp(value.updated_at);
	var commentID = comment.id;
	var divClass = (description.length > 100) ? " comment-summary" : "";

	var html = "";
	html += "\n<div class=\"comment\" data-commentid=\""+commentID+"\">";
	html += "\n\t<div class=\"comment-header\">";
	if (comment.is_mine) {
		html += "\n\t\t<button type=\"button\" data-id=\""+ commentID +"\" class=\"btn btn-link edit-comment-button\" title=\"Edit comment\"><i class=\"far fa-edit\"></i></button>";
	}
	html += "\n\t\t<div class=\"username\">" + comment.name;
	if (comment.by_instructor) {
		html += "<span class=\"instructor\">instructor</span>";
	}
	html += "</div>";
	if ((comment.privacy === "private") || (comment.privacy === "nominated")) {
		html += "\n\t\t<div class=\"privacy-icon\"><i class=\"fa fa-eye-slash\"></i></div>";
	}
	else if (comment.privacy === "all") {
		html += "\n\t\t<div class=\"privacy-icon\"><i class=\"fa fa-eye\"></i></div>";
	}
	html += "\n\t\t<div class=\"date\">"+comment.updated_at+"</div>";
	html += "\n\t\t<div class=\"tags\">";
	if (comment.tags && comment.tags.length > 0) {
		$.each(comment.tags, function(i, v) {
			html += "\n\t\t\t<span class=\"tag comment-tag\">"+v+"</span>";
		});
	}
	html += "\n\t\t</div><!-- .tags -->";
	html += "\n\t</div><!-- .comment-header -->";
	html += "\n\t<div class=\"comment-text"+divClass+"\" id=\"comment-text-"+commentID+"\">";
	html += "\n\t\t"+description;
	html += "\n\t</div><!-- .comment-text -->";
	html += "\n</div><!-- .comment -->";
	return html;
}

function showAnnotationModal(start_time) {
	item_start_time = start_time;

	var show = true;
	for (i = 0; i < annotations.length; i++) {
		var mine = annotations[i].mine;
		var start = annotations[i].start_time;
		if (mine && (start == item_start_time)) {
			alert("You already have annotation at " + start + " seconds. Please edit it instead.");
			show = false;
			break;
		}
	}

	var modal = $("#annotation-modal");
	if (show) {
		modal.find("#modalLabel").text("ADD ANNOTATION");
		item_start_time_text = secondsToMinutesAndSeconds(item_start_time);
		$("#time-slider").val(item_start_time);
		modal.find("#time-label").html(item_start_time_text);
		$(".time-edit-controls").show(); 
		modal.find(".edit-annotation-time").show();
		modal.find(".meta-data").hide();
		modal.find(".edit-instruction").hide();
		modal.find("#annotation-instruction").hide();
		modal.find("input[name='privacy-radio'][value='all']").prop("checked", true);
		modal.find("#nominated-selection").hide();
		$("#annotation-covaa-tag").show();
		$("#comment-tags-div").hide();
		$("#comment-textbox-div").hide();
		$("#annotation-textbox-div").show();
		$("#annotation-form").validator('update');

		$("#annotation-instruction-textarea").empty();
		//get team covaa tags array
		addTagGrouptoAddAnnotation($("#annotation-covaa-tag"), covaaTagsArray, null);

		modal.modal("show");
	}
}


/**
 *@description: on ready  
 */

(function ($) {

	var modal = $("#annotation-modal"); //DOM for modal-form
	var item_start_time_text = null; //human readable start_time text used in modal-form
	var item = null; //annotation or comment item used in modal-form
	
	init();

	$("#course-name").text(course_name);
	$("#group-name").text(group_name);
	$("#video-name").text(unescapeHtml(video_name));

	$(document).on("click", ".comment-summary", function() {
		$(this).removeClass('comment-summary');
		$(this).addClass("comment-full");
		$(".comments-box").getNiceScroll().resize();
	});
	$(document).on("click", ".comment-full", function() {
		$(this).removeClass("comment-full");
		$(this).addClass("comment-summary");
		$(".comments-box").getNiceScroll().resize();
	});

	$(window).resize(function () {
		layoutAnnotations();
	});

	$(".vertical-scroll").niceScroll({
		cursorcolor: "#c8c8c8",
		cursorborder: "1px solid transparent",
		autohidemode: "true"
	});
	$(".horizontal-scroll").niceScroll({
		cursorcolor: "#c8c8c8",
		cursorborder: "1px solid transparent",
		autohidemode: "true"
	});

	$("#term-select").val(course_term_year);

	$(".navbar").on("change", "#term-select", function () {
		var opt = $("#term-select").children(":selected");
		var year = opt.data('year');
		var term = opt.data('term');
		window.location.href = domain + "/year-and-term/" + year + "/" + term;
	});

	$("#video-segment-select").on("change", function() {
		var seg_id = $(this).val();
		var seg = segments.find(s=>s.id==seg_id);
		playSegment(seg.start, seg.end);//<-- function to be implemented for each type of video in js/media/ files
	});

	$(".add-annotation").on("click", function (e) {
		e.preventDefault();
		item = null;

		if(media_type === 'vimeo'){
			currentVideoTime(function(temp_start_time){
				showAnnotationModal(temp_start_time);
			});
		}	
		else{
			showAnnotationModal(currentVideoTime());
		}
	});

	
	$(".add-comment").on("click", function (e) {
		e.preventDefault();
		item = null;
		item_start_time = null;
		modal.find("#modalLabel").text("ADD COMMENT");
		if (is_instructor) {
			modal.find(".edit-instruction").show();
		} else {
			modal.find(".edit-instruction").hide();
		}
		modal.find(".edit-annotation-time").hide();
		modal.find(".meta-data").hide();
		if (comment_instruction) {
			modal.find("#annotation-instruction").text(unescapeHtml(comment_instruction));
			modal.find("#annotation-instruction").show();
		} else {
			modal.find("#annotation-instruction").hide();
		}
		$("#annotation-covaa-tag").hide();
		$("#comment-tags-div").show();
		$("#comment-textbox-div").show();
		$("#comment-textbox").val("");
		$("#annotation-textbox-div").hide();
		modal.find("input[name='privacy-radio'][value='all']").prop("checked", true);
		modal.find("#nominated-selection").hide();
		$("#annotation-form").validator('update');
		$("#annotation-modal").modal("show");
	}); 
	$(".play-annotation-button").on("click", function (e) {
		e.preventDefault();
		var startTime = $("#preview .time-label").prop('start-time');
		goTo(startTime);
	});
	$(".edit-annotation-button").on("click", function (e) {
		e.preventDefault();
		if ($("#preview").is(":visible")) {
			$("#preview").hide();
		}
		modal.find("#modalLabel").text("EDIT ANNOTATION");
		var annotation_id = $(this).attr("data-id");
		var match = $.grep(annotations, function (e) {
			return e.id == annotation_id;
		});
		item = null;
		if (match.length == 1) {
			item = match[0];
		}
		item_start_time = item.start_time;
		// $("#time-slider").val(item_start_time); 
		$(".time-edit-controls").hide(); 
		item_start_time_text = secondsToMinutesAndSeconds(item.start_time);
		modal.find("#time-label").html(item_start_time_text);
		$("#annotation-description").val(unescapeHtml(item.description));
		modal.find(".username").text(item.name);
		modal.find("#annotation-instruction").hide();
		if (item.privacy === "all") {
			modal.find(".privacy-icon").html("<i class=\"fas fa-eye\"></i>");
		} else {
			modal.find(".privacy-icon").html("<i class=\"fas fa-eye-slash\"></i>");
		}
		if (item.privacy == "nominated") {
			getNominatedStudentList("annotation", annotation_id);
			modal.find("#nominated-selection").show();
		} else {
			populateNominatedStudentList(null);
			modal.find("#nominated-selection").hide();
		}
		annotationDate = item.date;
		modal.find(".date").html(annotationDate);
		modal.find(".edit-annotation-time").show();
		modal.find(".meta-data").show();
		modal.find(".edit-instruction").hide();
		$("#annotation-covaa-tag").show();
		$("#comment-tags-div").hide();
		$("#comment-textbox-div").hide();
		$("#annotation-textbox-div").show();

		// var tags = "";

		// if (item.tags.length > 0) {
		// 	$.each(item.tags, function (i, val) {
		// 		tags += unescapeHtml(val) + ", ";
		// 	});
		// 	tags = tags.slice(0, -2);
		// }
		// $("#tags").val(tags);

		//get team covaa tags array
		let existing_annotation = {"id":annotation_id, "html":unescapeHtml(item.description)};
		addTagGrouptoAddAnnotation($("#annotation-covaa-tag"), covaaTagsArray, existing_annotation);

		//input exist descripton
		$("#annotation-instruction-textarea").empty();
		$("#annotation-instruction-textarea").append(unescapeHtml(item.description));

		modal.find("input[name='privacy-radio'][value='" + item.privacy + "']").prop("checked", true);
		if (item.privacy == "nominated") {
			getNominatedStudentList("annotation", annotation_id);
			modal.find("#nominated-selection").show();
		} else {
			populateNominatedStudentList(null);
			modal.find("#nominated-selection").hide();
		}
		$("#annotation-form").validator('update');
		$("#annotation-modal").modal("show");
	});
	$(".comments-box").on("click", ".edit-comment-button", function (e) {
		e.preventDefault();

		modal.find("#modalLabel").text("EDIT COMMENT");
		$("#annotation-textbox-div").hide();
		$("#comment-textbox-div").show();
		var comment_id = $(this).attr("data-id");
		var match = $.grep(comments, function (e) {
			return e.id == comment_id;
		});
		item = null;
		if (match.length == 1) {
			item = match[0];
		}
		$("#comment-textbox").val(unescapeHtml(item.description));
		$("#annotation-covaa-tag").hide();
		$("#comment-tags").show();
		if (item.tags.length > 0) {
			var t = "";
			$.each(item.tags, function (i, v) {
				t += unescapeHtml(v) + ", ";
			});
			t = t.slice(0, -2)
			modal.find("#tags").val(t);
		} else {
			$("#tags").val("");
		}
		if (is_instructor) {
			modal.find(".edit-instruction").show();
		} else {
			modal.find(".edit-instruction").hide();
		}
		modal.find(".edit-annotation-time").hide();
		modal.find(".meta-data").hide();
		if (comment_instruction) {
			modal.find("#annotation-instruction").text(unescapeHtml(comment_instruction));
			modal.find("#annotation-instruction").show();
		} else {
			modal.find("#annotation-instruction").hide();
		}

		modal.find("input[name='privacy-radio'][value='" + item.privacy + "']").prop("checked", true);
		if (item.privacy == "nominated") {
			getNominatedStudentList("comment", comment_id);
			modal.find("#nominated-selection").show();
		} else {
			populateNominatedStudentList(null);
			modal.find("#nominated-selection").hide();
		}
		$("#annotation-form").validator('update');
		$("#annotation-modal").modal("show");

	});

	$("#annotation-modal").on("show.bs.modal", function (e) {
		pauseVideo();
		$("#annotation-description").niceScroll({
			cursorcolor: "#585858",
			cursorborder: "1px solid transparent",
			autohidemode: "false"
		});
		$("#nominated-students-list").multiSelect({
			selectableHeader: "<div class=''>Available</div>",
			selectionHeader: "<div class=''>Nominated</div>"
		});
		modal.find("#annotation-form").validator("destroy");
		modal.find("#annotation-form").validator("update");
		$("#time-slider").attr('max', video_duration);

		var save_button = modal.find("#save");
		var title = modal.find("#modalLabel").text();
		if (points.length > 0) {
			if (title === "ADD COMMENT" || title === "EDIT COMMENT") {
				save_button.addClass("modal-text-button");
				save_button.html('Next<i class="fa fa-chevron-right" aria-hidden="true"></i>');
			} else {
				save_button.removeClass("modal-text-button");
				save_button.html('<i class="fa fa-save" aria-hidden="true"></i>');
			}
		}

	});

	modal.on("change", "input[name=privacy-radio]", function () {
		type = "";
		if ($("#modalLabel:contains('ANNOTATION')").length > 0) {
			type = "annotation";
		} else if ($("#modalLabel:contains('COMMENT')").length > 0) {
			type = "comment";
		}
		if ($(this).val() === "nominated") {
			var itemId = item ? item.id : null;
			getNominatedStudentList(type, itemId);
			modal.find("#nominated-selection").show();
		} else {
			populateNominatedStudentList(null);
			modal.find("#nominated-selection").hide();
		}
		modal.find("#annotation-form").validator("destroy");
		modal.find("#annotation-form").validator("update");
	});

	modal.on("change", "#time-slider", function () {
		item_start_time = Number($(this).val());
		modal.find("#time-label").html(secondsToMinutesAndSeconds(item_start_time));
	});

	modal.on("click", "#save", function () {
		var description = "";
		var privacy = $('input[name="privacy-radio"]:checked', '#annotation-form').val();
		var title = $("#modalLabel").text();
		var nominated = null;
		if (privacy === "nominated") {
			nominated = $("#nominated-students-list").val();
		}

		if (title === "ADD ANNOTATION") {
			var start = 0;
			var saveIt = true;
			for (i = 0; i < annotations.length; i++) {
				var mine = annotations[i].mine;
				start = annotations[i].start_time;
				if (mine && (start == item_start_time)) {
					saveIt = false;
					break;
				}
			}
			if(!saveIt) {
				alert("You already have annotation at " + start + " seconds. Please edit it instead.");
				modal.modal("hide");
				return false;
			}
			//add judge if choose all tags
			description = $('#annotation-instruction-textarea').html();
			// var verifiedData = verified(description);
			if(description.length==0) {
				alert("Annotation doesn't have any content. Please write something before saving.");
				return false;
			}

			// var isTagGroupExist = isAllTagGroupExist(covaaTagsArray, verifiedData);
			// if (isTagGroupExist) {
				$.ajax({
					type: "POST",
					url: "/add_annotation",
					data: {
						group_video_id: group_video_id,
						start_time: item_start_time,
						description: description,
						privacy: privacy,
						nominated_students_ids: nominated
					},
					success: function (data) {
						modal.modal("hide");
						getAllAnnotations();
					},
					error: function (request, status, error) {
						console.log("request.status: " + request.status + " error " + error + "<error>" + request.responseText + "</error>"); /////
					},
					async: false
				});
			// } else {
			// 	alert("Sorry, you need to choose tags from all tag groups, Thanks!");
			// }

		} 
		else if (title === "ADD COMMENT") {
			if ((modal.find("#comment-textbox").data('bs.validator.errors').length > 0) ||
				((privacy === "nominated") && modal.find("#nominated-students-list").data('bs.validator.errors').length > 0)) {
				return false;
			}

			description = $("#comment-textbox").val();
			var tags = commaDelimitedToArray($('#tags').val());

			$.ajax({
				type: "POST",
				url: "/add_comment",
				beforeSend: function (request) {
					request.setRequestHeader("Authorization", "Bearer " + api_token);
				},
				data: {
					group_video_id: group_video_id,
					tags: tags,
					description: description,
					privacy: privacy,
					nominated_students_ids: nominated
				},
				success: function (data) {
					if (points.length > 0) {
						modal.modal('hide');
						item = data;
						$("#feedback").modal('show');
					} else {
						modal.modal("hide");
					}
					comments.push(data);
					var html_str = formatOneComment(data);
					if ($("#no-comment-msg").length) {
						$("#no-comment-msg").remove();
						$(".comments-box").append(html_str)
					}
					else {
						$(html_str).insertBefore(".comment:first");
					}
					
					$(".comments-box").getNiceScroll().resize().show();
				},
				error: function (request, status, error) {
					console.log("request.status: " + request.status + " error " + error + "<error>" + request.responseText + "</error>"); /////
				},
				async: false
			});
		} 
		else if (title === "EDIT ANNOTATION") {
			//add judge if choose all tags
			description = $('#annotation-instruction-textarea').html();
			// var verifiedData = verified(description); 

			// var isTagGroupExist = isAllTagGroupExist(covaaTagsArray, verifiedData);
			// if (isTagGroupExist) {
				$.ajax({
					type: "POST",
					url: "/edit_annotation",
					data: {
						annotation_id: item.id,
						start_time: item.start_time,
						description: description,
						privacy: privacy,
						nominated_students_ids: nominated
					},
					success: function (data) {
						modal.modal("hide");
						getAllAnnotations();
					},
					error: function (request, status, error) {
						console.log("request.status: " + request.status + " error " + error + "<error>" + request.responseText + "</error>"); /////
					},
					async: false
				});
			// } else {
			// 	alert("Sorry, you need to choose tags from all tag groups, Thanks!");
			// }

		} 
		else if (title === "EDIT COMMENT") {
			if ((modal.find("#comment-textbox").data('bs.validator.errors').length > 0) ||
				((privacy === "nominated") && modal.find("#nominated-students-list").data('bs.validator.errors').length > 0)) {
				return false;
			}

			description = $("#comment-textbox").val();
			var tags = commaDelimitedToArray($('#tags').val());
			$.ajax({
				type: "POST",
				url: "/edit_comment",
				beforeSend: function (request) {
					request.setRequestHeader("Authorization", "Bearer " + api_token);
				},
				data: {
					comment_id: item.id,
					tags: tags,
					description: description,
					privacy: privacy,
					nominated_students_ids: nominated
				},
				success: function (data) {
					if (points.length > 0) {
						modal.modal('hide');
						item = data;
						$("#feedback").modal('show');
					} else {
						modal.modal("hide");
					}
					
					$(".comment[data-commentid='"+data.old_id+"']").remove();
					var html_str = formatOneComment(data);
					$(html_str).insertBefore(".comment:first");
					$(".comments-box").getNiceScroll().resize();
				},
				error: function (request, status, error) {
					console.log("request.status: " + request.status + " error " + error + "<error>" + request.responseText + "</error>"); /////
				},
				async: false
			});
		}
	});
	modal.on("click", "#delete", function () {
		var title = $("#modalLabel").text();
		if ((title === "ADD ANNOTATION") || (title === "ADD COMMENT")) {
			$("#annotation-modal .close").click();
			return;
		} else {
			if (confirm("Are you sure you want to delete?")) {
				if (title === "EDIT ANNOTATION") {
					$.ajax({
						type: "POST",
						url: "/delete_annotation",
						data: {
							annotation_id: item.id
						},
						success: function (data) {
							modal.modal("hide");
							// getAnnotations(ALL);
							getAllAnnotations();
						},
						error: function (request, status, error) {
							console.log("request.status: " + request.status + " error " + error + "<error>" + request.responseText + "</error>"); /////
						},
						async: false
					});
				} else if (title === "EDIT COMMENT") {
					$.ajax({
						type: "POST",
						url: "/delete_comment",
						data: {
							comment_id: item.id
						},
						success: function (data) {
							if(data.comment_id) {
								comments = comments.filter(x=>x.id!=data.comment_id);
								$(".comment[data-commentid='"+data.comment_id+"']").remove();
								if($(".comment").length===0) {
									$(".comments-box").append("<div id=\"no-comment-msg\" class=\"comment-text\">There is no comment for this video yet. You can add yours and review them later!</div>");
								}
							}
							modal.modal("hide");
						},
						error: function (request, status, error) {
							console.log("request.status: " + request.status + " error " + error + "<error>" + request.responseText + "</error>"); /////
						},
						async: false
					});
				}
			}
		}
	});
	modal.on("click", "#rewind-button", function () {
		item_start_time = (item_start_time > 1) ? item_start_time - 1 : item_start_time;
		modal.find("#time-label").html(secondsToMinutesAndSeconds(item_start_time));
	});
	modal.on("click", "#forward-button", function () {
		item_start_time = (item_start_time + 1 < video_duration) ? item_start_time + 1 : video_duration;
		modal.find("#time-label").html(secondsToMinutesAndSeconds(item_start_time));
	});
	modal.on("click", "#edit-instruction-button", function () {
		modal.modal("hide");
		$("#comment-instruction-modal").modal("show");
	});
	modal.on("hidden.bs.modal", function () {
		var scrollbar = $("#annotation-description").getNiceScroll().hide();
		var modal = $(this);
		item_start_time = null;
		item_start_time_text = null;
		modal.find("#tags").val("");
		modal.find("#annotation-description").val("");
		modal.find(".privacy-icon").html("");
		modal.find("#annotation-form").validator("destroy");
	});

	$("#comment-instruction-modal").on("show.bs.modal", function () {
		if (comment_instruction) {
			$("#comment-instruction-description").val(comment_instruction);
		}
	});
	$("#comment-instruction-modal").on("click", "#save-comment-instruction", function () {
		var description = $('#comment-instruction-description').val();
		$.ajax({
			type: "POST",
			url: "/edit_comment_instruction",
			data: {
				group_video_id: group_video_id,
				description: description
			},
			success: function (data) {
				comment_instruction = data;
				$("#comment-instruction-modal").modal('hide');
			},
			erorr: function (request, status, error) {
				console.log("error edit_comment_instruction : " + error); //////
			}
		});
	});
	$("#comment-instruction-modal").on("click", "#delete-comment-instruction", function () {
		if (!comment_instruction) {
			// $("#comment-instruction-modal .close").click();
			$("#comment-instruction-modal").modal('hide');
			return;
		}
		if (confirm("Are you sure you want to delete this instruction?")) {
			$.ajax({
				type: "POST",
				url: "/delete_comment_instruction",
				data: {
					group_video_id: group_video_id
				},
				success: function (data) {
					comment_instruction = null;
					$("#comment-instruction-modal").modal('hide');
				},
				erorr: function (request, status, error) {
					console.log("error delete_comment_instruction : " + error); //////
				}
			});
		}
	});

	$("#feedback").on("show.bs.modal", function () {
		$("#point-instruction").text(unescapeHtml(point_instruction));
		var div = $("#feedback-content");
		for (i = 0; i < points.length; i++) {
			var radio_div = $('<div/>', {
				'class': '',
			}).appendTo(div);
			$('<label/>', {
				'for': 'point' + points[i].id + '-yes',
				'html': '<span class="left-indent circle-radio">' + points[i].description + '</span>'
			}).prepend(
				$('<input/>', {
					'type': 'checkbox',
					'name': 'point' + points[i].id + '-feedback',
					'id': 'point' + points[i].id + '-yes',
					'class': 'checkbox pull-right',
					'value': '1'
				})
			).appendTo(div);
		} //end for
	});
	$("#feedback").on("hide.bs.modal", function () {
		$("#feedback-content").empty();
		$("#confidence-level option:eq(0)").prop("selected", true);
	});
	$("#feedback").on("click", "#re-enter-comment", function () {
		saveFeedbacksAndConfidenceLevel(item);

		modal.find("#modalLabel").text("EDIT COMMENT");
		$("#annotation-description").val(unescapeHtml(item.description));
		var tags = "";
		if (item.tags.length > 0) {
			tags = unescapeHtml(item.tags[0]);
			var i;
			for (i = 1; i < item.tags.length; i++) {
				tags += ", " + unescapeHtml(item.tags[i]);
			}
		}
		modal.find("#tags").val(tags);

		if (is_instructor) {
			modal.find(".edit-instruction").show();
		} else {
			modal.find(".edit-instruction").hide();
		}
		modal.find(".edit-annotation-time").hide();
		modal.find(".meta-data").hide();
		if (comment_instruction) {
			modal.find("#annotation-instruction").text(unescapeHtml(comment_instruction));
			modal.find("#annotation-instruction").show();
		} else {
			modal.find("#annotation-instruction").hide();
		}

		modal.find("input[name='privacy-radio'][value='" + item.privacy + "']").prop("checked", true);
		if (item.privacy == "nominated") {
			getNominatedStudentList("comment", comment_id);
			modal.find("#nominated-selection").show();
		} else {
			populateNominatedStudentList(null);
			modal.find("#nominated-selection").hide();
		}
		$("#feedback").modal("hide");
		$("#annotation-modal").modal("show");
	});
	$("#feedback").on("click", "#save-points", function () {
		saveFeedbacksAndConfidenceLevel(item);
		$("#feedback").modal("hide");
	});

	$(".annotations-buttons").on("click", ".download-comments", function (e) {

		//create data table
		$('#covaa-thread-table').DataTable().destroy();

		var table = $('#covaa-thread-table').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": function (data, callback, settings) {

				//10 columns for each pages
				var skip = data['start'] / data['length'];
				var search = data['search']['value'];

				$.get('/api/thread/get', {
						group_video_id: group_video_id,
						course_id:course_id,
						skip: skip,
						search: search
					})
					.done(function (res) {

						var data = res['data'];
						var total = res['total'];
						var tag = res['tag'] ? JSON.parse(res['tag']['covaa_tag_array']) : null;

						var temp = [];

						for(var i = 0; i < data.length; i++){
							var item = [];

							//--annotation id
							item.push(data[i]['anno_id']);
							
							//--annotation start time
							// item.push(data[i]['start_time'] + 's');
							item.push(secondsToMinutesAndSeconds(data[i]['start_time']));

							//--annotation desc
							var description = $(unescapeHtml(data[i]['annotation'])).text().trim()
							item.push(description);

							//--annotation author
							item.push(data[i]['anno_fullname']);

							//--annotation author role
							// item.push(data[i]['anno_by_instructor'] === true ? 'Instructor' : 'Student');

							//--annotation tag group &tag label
							var anno_tag_groups = "";
							var anno_tag_labels = "";
							var covaaGroupIdArrayNew = $(unescapeHtml(data[i]['annotation'])).find('li').toArray();
							var t_groups = [];
							var t_labels = [];
							if(tag) {
								for(var a in covaaGroupIdArrayNew){
									var covaaGroupId = $(covaaGroupIdArrayNew[a]).attr('covaa-group-id');
									var covaaGroupSubId = $(covaaGroupIdArrayNew[a]).attr('covaa-group-sub-id');
									for(var x in tag){
										if(tag[x]['id'] === covaaGroupId){
											if(!t_groups.includes(tag[x]['text'])) {
												t_groups.push(tag[x]['text']);
											}
											for(var y in tag[x]['item']){
												if( tag[x]['item'][y]['id'] === covaaGroupSubId){
													if(!t_labels.includes(tag[x]['item'][y]['text'])) {
														t_labels.push(tag[x]['item'][y]['text']);
													}
												}
											}
										}
									}
								}
								$.each(t_groups, function(i,v){
									anno_tag_groups += v+", ";
								});
								$.each(t_labels, function(i, v) {
									anno_tag_labels += v+", ";
								})
							}
							
							item.push(anno_tag_groups);
							item.push(anno_tag_labels);

							//--annotation_post id
							item.push(data[i]['id']);

							//--annotation_post desc
							var content = '';
							if(data[i]['content']) {
								content = data[i]['content'].indexOf('covaa-comment-label')>=0 ? $(data[i]['content']).text().trim() : data[i]['content'];
							}
							item.push(content);

							//--annotation_post author
							item.push(data[i]['fullname']);

							//--annotation_post author role
							// item.push(data[i]['post_by_instructor'] === true ? 'Instructor' : 'Student');

							//--annotation_post tag group & tag label
							var post_tag_groups = "";
							var post_tag_labels = "";
							t_groups = [];
							t_labels = [];
							var covaaGroupIdArray = $(data[i]['content']).find('li').toArray();

							if(tag) {
								for(var a in covaaGroupIdArray){
									var covaaGroupId = $(covaaGroupIdArrayNew[a]).attr('covaa-group-id');
									var covaaGroupSubId = $(covaaGroupIdArrayNew[a]).attr('covaa-group-sub-id');
									for(var x in tag){
										if(tag[x]['id'] === covaaGroupId){
											if(!t_groups.includes(tag[x]['text'])) {
												t_groups.push(tag[x]['text']);
											}
											for(var y in tag[x]['item']){
												if( tag[x]['item'][y]['id'] === covaaGroupSubId){
													if(!t_labels.includes(tag[x]['item'][y]['text'])) {
														t_labels.push(tag[x]['item'][y]['text']);
													}
												}
											}
										}
									}
								}
								$.each(t_groups, function(i,v){
									post_tag_groups += v+", ";
								});
								$.each(t_labels, function(i, v) {
									post_tag_labels += v+", ";
								})
								console.log("tags="+anno_tag_groups+"     labels="+anno_tag_labels);////////
							}
							
							item.push(post_tag_groups);
							item.push(post_tag_labels);

							//--annotation_post word count
							var word_count = data[i]['content'] ? data[i]['content'].replace(/\#/, "").replace(/\s+/g, " ").trim().split(/\s/).length : "";
							item.push(word_count);

							// item.push(data[i]['upvote_count']);

							// // calculator
							// var customiseBadge = JSON.parse(data[i]['covaa_badge_array']);
							// var customiseResult = "";
							// for(var x in customiseBadge){
							// 	customiseResult += customiseBadge[x].name  + ':' + '(' + customiseBadge[x].count + ') '; 
							// }
							// item.push(customiseResult);

							temp.push(item);	
						}

						callback({
							data: temp,
							recordsTotal: total,
							recordsFiltered: total
						});

					})
					.fail(function (jqXHR, textStatus) {
						window.location.href = '/';
					});

			},
			"dom": 'Bfrtip',
			"buttons": [
				'copy', 'csv', 'excel', 'pdf', 'print'
			],
			"oLanguage": {
				"sSearch": "Search by keywords:"
			},
			"bSort" : false
		});

		$("#annotation-detail-modal").modal('show');

	});
	$("#annotation-filter input").on("change", function (e) {
		var mode = $('input[name=filter]:checked').val();
		layoutAnnotations(parseInt(mode));
	});

	$('#annotations-list').on('mouseenter', '.annotation-button', function (e){
		//console.log('mouseover');

		var preview = $("#preview");

		if (preview.is(':visible')) {
			preview.hide();
		}

		var annotationID = $(this).data('id');
		var matches = $.grep(annotations, function (e) {
			return e.id == annotationID;
		});
		var annotation = matches[0];
		var startTime = annotation.start_time;
		var userName = annotation.name;

		var description = annotation.description;
		var creationDate = annotation.date;
		var privacyIcon = annotation.privacy === "all" ? "<i class=\"fa fa-eye\"></i>" : "<i class=\"fa fa-eye-slash\"></i>";
		
		var $desc = $(unescapeHtml(description));

		preview.find(".time-label").prop('start-time', startTime);
		preview.find(".time-label").text(secondsToMinutesAndSeconds(startTime));
		preview.find(".annotation-div").html($desc);
		preview.find(".privacy-icon").html(privacyIcon);
		preview.find(".username").text(userName);
		preview.find(".date").text(creationDate);

		// preview.find(".preview-tags").text(unescapeHtml(tags));
		preview.find(".preview-tags").html("");
		// $.each(annotation.tags, function (i, val) {
		// 	preview.find(".preview-tags").append('<span class="tag annotation-tag">' + val + "</span> ")
		// });
		if (annotation.mine) {
			preview.find(".edit-annotation-button").attr("data-id", annotationID);
			preview.find(".edit-annotation-button").show();
		} else {
			preview.find(".edit-annotation-button").hide();
		}

		var posX = e.pageX + previewOffsetX;
		var posY = e.pageY + previewOffsetY;
		var previewWidth = preview.width();
		var windowWidth = $(window).width();
		if (posX + previewWidth > windowWidth) {
			posX = windowWidth - previewWidth;
		}
		preview.css({
			'top': posY - 200 + "px",
			'left': posX + 20 + "px",
		});
		var footer = $(".footer").parent();
		var windowBottom = footer.position().top + footer.height();
		var previewBottom = posY + preview.height();
		if (windowBottom < previewBottom) {
			$(".canvas").height(previewBottom);
		}

		//add comment thread, ajax to get three popular comments
		$("#loading-hud").show();
		preview.find(".preview-comment").empty();

		$.get('/api/comments/getTop', {
			group_video_id: group_video_id,
			annotation_id: annotationID
		})
		.done(function (data) {
			var ul = $('<ul class="covaa-annotation-top-preview"></ul>');

			for(var i in data['recentPost']){
				var temp = data['recentPost'][i];

				var name = temp['fullname'];
				var content = temp['content'].indexOf('<li class="covaa-comment-label"')>=0 ? $(temp['content']).text() : temp['content'];

				var li = $('<li></li>');

				var nameSpan = '<p class="name">' + name + ' : ' + '</p>';
				var contentSpan = '<p class="content">' + content + '</p>';

				li.append(nameSpan);
				li.append(contentSpan);

				ul.append(li);

			}

			preview.find(".preview-comment").empty();
			preview.find(".preview-comment").append(ul);
			$("#loading-hud").hide();

		})
		.fail(function (jqXHR, textStatus) {
			window.location.href = '/';
		});

		preview.show();

		//end

		//bind close event
		$("#close-preview-button").click(function (e) {
			$("#preview").hide();
		});

		//when mouse over then show left comment thread
		// var annotationID = $(this).data('id');
		// var matches = $.grep(annotations, function (e) {
		// 	return e.id == annotationID;
		// });
		// var annotation = matches[0];
		// var startTime = annotation.start_time;
		// var description = annotation.description;
		// createCovaaTagComment(group_video_id, annotationID, secondsToMinutesAndSeconds(startTime) + ' - ' + unescapeHtml(description));

	});

	$('#annotations-list').on('mouseout', '.annotation-button', function (e){
		//console.log('mouseout');
		//$("#preview").hide();
	});

	$('#annotations-list').on('click', '.annotation-button', function (e) {
		saveTracking({
			event: "click",
			target: '.annotation-button',
			info: 'View an annotation',
			event_time: Date.now()
		});
		$("#discussion-tab").click();


		// var preview = $("#preview");

		// if (preview.is(':visible')) {
		// 	preview.hide();
		// }

		var annotationID = $(this).data('id');
		var matches = $.grep(annotations, function (e) {
			return e.id == annotationID;
		});
		var annotation = matches[0];
		var startTime = annotation.start_time;
		// var userName = annotation.name;

		var description = annotation.description;
		// var creationDate = annotation.date;
		// var privacyIcon = annotation.privacy === "all" ? "<i class=\"fa fa-eye\"></i>" : "<i class=\"fa fa-eye-slash\"></i>";

		//add covaa tag comment 
		createCovaaTagComment(group_video_id, annotationID, secondsToMinutesAndSeconds(startTime), unescapeHtml(description));

		//bind close event
		$("#close-preview-button").click(function (e) {
			$("#preview").hide();
		});

	});

	$(document).on("click", function (e) {
		if ($("#preview").is(":visible")) {
			var target = $(e.target);
			if (!target.hasClass("annotation-button") && !target.hasClass("far fa-dot-circle") && !target.hasClass("far fa-user-circle") && !target.hasClass("fas fa-info-circle")) {
				if (target.id != "preview" && !$('#preview').find(e.target).length) {
					/*------ trigger close click to record ------*/
					//$("#preview").hide();
					$("#close-preview-button").trigger("click");
				}
			}
		}
	});

	if (text_analysis && text_analysis.length > 0) {
		function setNoLinksMsg() {
			if ($("#related-ul").children().length == 0) {
				$("#no-links-msg").text("There are no related videos.");
				$("#no-links-msg").show();
			} else {
				$("#no-links-msg").hide();
			}
			$("#related-links").getNiceScroll().resize();
		}

		var filled_icon = '<i class="fa fa-heart"></i>';
		var unfilled_icon = '<i class="fa fa-heart-o"></i>';

		/**
		 * Method to empty out existing #related-ul and populate with data passed in
		 * @param {array} relatedItemsArray Array of objects with properties: word, type, video_id, title, url
		 */
		function fillRelatedUl(relatedItemsArray, isGlobal) {
			var related_ul = $("#related-ul");
			related_ul.html("");

			$.each(relatedItemsArray, function (i, item) {
				var icon = item.liked ? filled_icon : unfilled_icon;
				var action = item.liked ? "unlike" : "like";
				var like_button = "";
				if (!isGlobal && group_video.enable_recommended_resources_rating) {
					like_button = '<button type="button" class="btn btn-link white-text like-toggle" data-vid="' + item.video_id + '" data-action="' + action + '">' + icon + '</button>';
				}
				var html = '<span class="video-link">' + item.title + '</span>' + like_button;
				$('<li/>', {
					html: html,
					"data-url": item.url,
				}).appendTo(related_ul);
			})
			setNoLinksMsg();
		}

		function populateRelatedResources(searchTermArray) {
			// console.log("populateRelatedResoruces - "+searchTermArray);	/////
			// console.table(searchTermArray);/////
			var v_ids = []; //to check for duplicate links, because multiple words may have same link
			var relatedItems = [];

			$.each(searchTermArray, function (index, searchTerm) {
				$.each(text_analysis, function (i, word) {
					if ((word.text.indexOf(searchTerm) == 0) && (word['related'])) {
						$.each(word['related'], function (j, related) {
							if ($.inArray(related.video_id, v_ids) == -1) {
								v_ids.push(related.video_id);
								relatedItems.push(related);
							}
						})
					}
				});
			});
			//--sort in desc order of rating
			relatedItems.sort(function (a, b) {
				return b.rating - a.rating;
			});
			fillRelatedUl(relatedItems, false);
		} //end populateRelatedResources

		function populateSearchedResources(term) {
			$.ajax({
				type: "POST",
				url: "/search_related_videos",
				data: {
					group_video_id: group_video_id,
					searchTerm: term
				},
				success: function (data) {
					// console.log("success /search_related_video");////
					// console.table(data);/////
					fillRelatedUl(data, true);
				},
				error: function (req, status, error) {
					console.log("error /search_related_video - " + error); /////
				}
			});
			setNoLinksMsg();
		} //end populateSearchedResources
		populateRelatedResources(['']);

		function checkIfGlobalSearch() {
			if ($(".radio-group").find('input#scope-global').prop('checked')) {
				return true;
			}
			return false;
		}
		// var xhr; //to hold ajax request object for below method so it can be canceled
		$("#topic-search-textbox").autoComplete({
			minChars: 1,
			source: function (term, suggest) {
				term = term.toLowerCase();
				var choices = [];
				var matches = [];
				// if(checkIfGlobalSearch()) {//search from all keywords
				try {
					if (xhr) {
						xhr.abort();
					}
				} catch (e) {
					console.log("error canceling xhr for search auto complete"); /////
				}
				xhr = $.getJSON('/get_keyword_autocomplete_list', {
					q: term,
					s: checkIfGlobalSearch(),
					gvid: group_video_id
				}, function (data) {
					// console.table(data);/////
					suggest(data);
					// $.each(data, function(i, word) {
					// 	matches.push(word);
					// });
				});
				// }
				// else {//search within related 
				// $.each(text_analysis, function(i, v) {
				// 	choices.push(v.text);
				// });
				// for (i=0; i<choices.length; i++) {
				// 	// if (~choices[i].toLowerCase().indexOf(term)) {
				// 	if (choices[i].toLowerCase().indexOf(term)==0) {
				// 		matches.push(choices[i]);
				// 	}
				// }
				// }//end else
				// suggest(matches);
				// console.log("matches=");/////
				// console.table(matches);/////
			},
			onSelect: function (e, term, item) {
				if (checkIfGlobalSearch()) { //search from all keywords
					populateSearchedResources(term);
				} else { //search within related 
					populateRelatedResources([term]);
				}
			},
			menuClass: 'auto-complete-list'
		});
		$("#topic-search-textbox").on("keyup", function () {
			if (checkIfGlobalSearch()) { //search from all keywords
				if ($(this).val()) {
					populateSearchedResources($("#topic-search-textbox").val());
				}
			} else { //search within related 
				if (!$(this).val()) {
					populateRelatedResources(['']);
				} else {
					populateRelatedResources([$("#topic-search-textbox").val()]);
				}
			}
		});
		$("#topic-search-button").on("click", function () {
			if (checkIfGlobalSearch()) { //search from all keywords
				populateSearchedResources($("#topic-search-textbox").val());
			} else { //search within related 
				populateRelatedResources([$("#topic-search-textbox").val()]);
			}
		});
		$("#topic-search-form").on("submit", function (e) {
			e.preventDefault();
			if (checkIfGlobalSearch()) { //search from all keywords
				populateSearchedResources($("#topic-search-textbox").val());
			} else { //search within related 
				populateRelatedResources([$("#topic-search-textbox").val()]);
			}
		});
		$("input[type=radio][name=scope-status]").on("change", function () {
			if ($(this).attr('id') === "scope-global") {
				populateSearchedResources($("#topic-search-textbox").val());
			} else {
				populateRelatedResources([$("#topic-search-textbox").val()]);
			}
		});


		//--like/un-like button for related video--
		$(".like-toggle").on('click', function () {
			var vid = $(this).data('vid');
			var action = $(this).attr('data-action');
			var button = $(this);
			// console.log("toggle like - action= "+action);/////
			$.ajax({
				type: "POST",
				url: '/edit_recommended_video_like',
				data: {
					group_video_id: group_video_id,
					video_id: vid,
					action: action
				},
				// context: this,
				success: function (data) {
					// console.log("success /edit_recommended_video_like - "+data);/////
					var btn_action = action === "like" ? "unlike" : "like";
					button.attr('data-action', btn_action);
					if (btn_action === "like") {
						button.html(unfilled_icon)
					} else {
						button.html(filled_icon)
					}
					button.blur();
				},
				error: function (req, status, error) {
					console.log("error /edit_recommended_video_like - " + error); /////
				}
			});

		});

		//--keyword list for the current video--
		$(".no-keyword-msg").text("");
		var keyword_ul = $("#keyword-ul");
		$.each(text_analysis, function (i, word) {
			var html = word['text'];
			if (word['occurrences']) {
				$.each(word['occurrences'], function (j, val) {
					html += '<span class="related-time" data-time="' + val + '">@' + secondsToMinutesAndSeconds(val) + '</span>';
				});
				$("<li/>", {
					html: html
				}).appendTo(keyword_ul);
			}
		});
		$("#keyword-list").getNiceScroll().resize();

		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var target = $(e.target).attr("href");
			if (target === "#content-analysis") {
				$("#keyword-list").getNiceScroll().resize();
			} else if (target === "#annotations") {
				$("#keyword-list").getNiceScroll().hide();
			} else if (target === "#comments") {
				$(".comments-box").getNiceScroll().show();
				$("#related-links").getNiceScroll().hide();
			} else if (target === "#related-videos") {
				$(".comments-box").getNiceScroll().hide();
				$("#related-links").getNiceScroll().resize();
			}
		});
	} else { //no text analysis for this video
		$(".no-keyword-msg").text("There are no keywords.");
		$("#related-links").text("There are no related videos.");
	}

	// $(".video-link").on("click", function(e) {
	// 	pauseVideo();		
	// 	$("#modal-iframe").attr('src', $(this).data("url"));
	// 	$("#video-modal").modal('show');
	// });
	$("#related-videos").on("click", ".video-link", function () {
		pauseVideo();
		$("#modal-iframe").attr('src', $(this).parent().data("url"));
		$("#video-modal").modal('show');
	});
	$("#related-videos").on("click", ".related-time", function () {
		pauseVideo();
		$("#modal-iframe").attr('src', $(this).parent().data("time-url"));
		$("#video-modal").modal('show');
	});
	$("#video-modal").on('show.bs.modal', function () {

	});
	$("#video-modal").on('hide.bs.modal', function () {
		$("#modal-iframe").attr('src', '');
	});
	$("#keyword-ul").on("click", ".related-time", function () {
		var time = $(this).data("time");
		goTo(time);
	});
	$("#plain-comments").on("click", ".comment-tag", function () {
		var tag = $(this).text();
		$.ajax({
			type: "POST",
			url: "/get_comments_for_tag",
			data: {
				tag: tag,
				group_video_id: group_video_id
			},
			success: function (data) {
				var tag_modal = $("#same-tag-modal");
				tag_modal.find("#same-tag-modal-title").text('COMMENTS WITH TAG "' + tag + '"');
				var body = tag_modal.find(".modal-body");
				body.html("");
				var private_icon = '<i class="fa fa-eye-slash"></i>';
				var public_icon = '<i class="fa fa-eye"></i>';
				$.each(data, function (i, v) {
					var comment_div = $("<div/>", {
						'class': 'comment'
					}).appendTo(body);
					var comment_header = $("<div/>", {
						'class': 'comment-header',
					}).appendTo(comment_div);
					$("<div/>", {
						'class': 'username',
						'html': v.name
					}).appendTo(comment_header);
					var privacy_icon = (v.privacy === 'all') ? public_icon : private_icon;
					$("<div/>", {
						'class': 'privacy-icon',
						'html': privacy_icon
					}).appendTo(comment_header);
					$("<div/>", {
						'class': 'date',
						'html': v.updated_at
					}).appendTo(comment_header);

					$('<div/>', {
						'class': 'comment-text',
						'html': v.description
					}).appendTo(comment_div);
				});
				tag_modal.modal("show");
			},
			error: function (req, status, error) {
				console.log("error /get_comments_for_tag - " + error); /////
			}
		});
	});
	$("#preview").on("click", ".annotation-tag", function () {
		var tag = $(this).text();
		$.ajax({
			type: "POST",
			url: "/get_annotations_for_tag",
			data: {
				tag: tag,
				group_video_id: group_video_id
			},
			success: function (data) {
				var tag_modal = $("#same-tag-modal");
				tag_modal.find("#same-tag-modal-title").text('ANNOTATIONS WITH TAG "' + tag + '"');
				var body = tag_modal.find(".modal-body");
				body.html("");
				var private_icon = '<i class="fa fa-eye-slash"></i>';
				var public_icon = '<i class="fa fa-eye"></i>';
				$.each(data, function (i, v) {
					var comment_div = $("<div/>", {
						'class': 'comment'
					}).appendTo(body);
					var comment_header = $("<div/>", {
						'class': 'comment-header',
					}).appendTo(comment_div);
					$("<div/>", {
						'class': 'username',
						'html': v.name
					}).appendTo(comment_header);
					var privacy_icon = (v.privacy === 'all') ? public_icon : private_icon;
					$("<div/>", {
						'class': 'privacy-icon',
						'html': privacy_icon
					}).appendTo(comment_header);
					$("<div/>", {
						'class': 'date',
						'html': v.updated_at
					}).appendTo(comment_header);
					$("<div/>", {
						'class': 'video-time',
						'html': v.start_time
					}).appendTo(comment_header);

					$('<div/>', {
						'class': 'comment-text',
						'html': v.description
					}).appendTo(comment_div);
				});
				$("#preview").hide();
				tag_modal.modal("show");
			},
			error: function (req, status, error) {
				console.log("error /get_annotations_for_tag - " + error); /////
			}
		});
	});

	//Tracking all meaningful events
	var trackingsArr = [{
			event: 'click',
			target: '.add-annotation',
			info: 'Add Annotation'
		},
		{
			event: 'click',
			target: '.download-comments',
			info: 'Download Annotations'
		},
		{
			event: 'click',
			target: '#rewind-button',
			info: 'Edit annotation time (back)'
		},
		{
			event: 'click',
			target: '#forward-button',
			info: 'Edit annotation time (forward)'
		},
		{
			event: 'click',
			target: '#private',
			info: function () {
				return 'Set ' + $("#modalLabel").text().split(' ')[1].toLowerCase() + ' to private'
			}
		},
		{
			event: 'click',
			target: '#public',
			info: function () {
				return 'Set ' + $("#modalLabel").text().split(' ')[1].toLowerCase() + ' to public'
			}
		},
		{
			event: 'click',
			target: '#save',
			info: function () {
				return 'Save ' + $("#modalLabel").text().split(' ')[1].toLowerCase()
			}
		},
		{
			event: 'click',
			target: '#delete',
			info: function () {
				return 'Delete ' + $("#modalLabel").text().split(' ')[1].toLowerCase()
			}
		},
		{
			event: 'click',
			target: '#annotation-modal .close',
			info: 'Close modal'
		},
		{
			event: 'click',
			target: '#annotations-list .annotation-button',
			info: 'View an annotation'
		},
		{
			event: 'click',
			target: '.play-annotation-button',
			info: 'Play from annotation point'
		},
		{
			event: 'click',
			target: '.edit-annotation-button',
			info: 'Edit annotation'
		},
		{
			event: 'click',
			target: '#close-preview-button',
			info: 'Close annotation preview'
		},
		{
			event: 'change',
			target: '#annotation-filter input',
			info: 'Change annotation filter'
		},
		{
			event: 'click',
			target: '.add-comment',
			info: 'Add Comment'
		},
		// {event: 'click', target: '.edit-comment-button', info: 'Edit comment'}
	];

	for (var i = 0; i < trackingsArr.length; i++) {
		trackingInitial(trackingsArr[i], trackings);
	}

})(jQuery);
