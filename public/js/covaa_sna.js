 /**
 * CoVAA Learning Dashboard - Social Network Diagram generation
 * Author - Simon Yang <simon.yang@nie.edu.sg>
 * Since - 13 Feb 2018
 * File Type - Javascript with Cytoscape visualization <http://www.cytoscape.org/>
 **/


/* --------------------------------------------------------------------------------------- 
This function performs the drawing of the sna chart based on the selected option. 
Declares the cytoscape dom with stated settings and provide data through arguments.
   --------------------------------------------------------------------------------------- */
function drawSNA(weights, strengths, students, socialLinks)
{
	//Creating an array to exclude admin users in SNA
	var exclude_user_array = [];

	//Creating a designated array for all users nodes with the required data format.
	var user_array = [];

	$.each(students, function(i, l)
	{
		if(students[i]["role"] != 5)
		{
			user_array.push({ 
				data: { 
					id: students[i]["id"], 
					name: students[i]["name"], 
					weight: students[i]["weight"], 
					reply: students[i]["reply"], 
					faveShape: students[i]["faveShape"], 
					nameColor: students[i]["nameColor"] 
				} 
			});	
		}
		else
			exclude_user_array.push(students[i]["id"]);
		
	});

	//Creating a designated array for social links with the required data format.
	var socialLinks_array = [];

	$.each(socialLinks, function(i, l)
	{
		if($.inArray(socialLinks[i]["source"], exclude_user_array) == -1 && $.inArray(socialLinks[i]["target"], exclude_user_array) == -1)
		{
			socialLinks_array.push({
				data: { 
						source: socialLinks[i]["source"], 
						direction: socialLinks[i]["direction"], 
						target: socialLinks[i]["target"], 
						faveColor: socialLinks[i]["faveColor"], 
						strength: socialLinks[i]["strength"]
					}
			});
		}
	});

	setTimeout(function() { 
	    $('#sna_container').cytoscape({
		  layout: {
			name: 'concentric',
			padding: 30,
			flow: { axis: 'x', minSeparation: 50 }, // use DAG/tree flow layout if specified, e.g. { axis: 'y', minSeparation: 30 }
			minNodeSpacing: 20, // min spacing between outside of nodes (used for radius adjustment)
			animate: false,
			//animationDuration: 2000,
			levelWidth: function( nodes ){ // the variation of concentric values in each level
			  return nodes.maxDegree() / 5;
			  }
		  },
		  
		  style: cytoscape.stylesheet()
			.selector('node')
			  .css({
				'shape': 'data(faveShape)',
				//'shape': 'roundrectangle',
				'width': 'mapData(weight, '+weights["bottom_weight"]+',' +weights["top_weight"]+', 20, 60)',
				'height': 'mapData(weight, '+weights["bottom_weight"]+',' +weights["top_weight"]+', 20, 60)',
				'content': 'data(name)',
				'text-valign': 'top',
				'text-outline-width': 2,
				'text-outline-color': '#fff',
				//'text-outline-color': 'mapData(weight, 0, 100, blue, red)', 
				'background-color': 'mapData(reply, '+weights["bottom_reply"]+',' +weights["top_reply"]+', #00ACEC, #3f51b5)', //#6abf69, #00600f
				//'background-color': 'mapData(reply, this.nodes.minDegree(), this.nodes.maxDegree(), #3100E4, #FF9101)', #3100E4, #FF9101
				'color': 'data(nameColor)',
				//'font-weight': 'bold',
				'font-size': 20,
				'border-width': 1,
				'border-color': '#333'
			  })
			.selector(':selected')
			  .css({
				'border-width': 5,
				'border-color': '#333'
			  })
			.selector('edge')
			  .css({
				'opacity': 0.666,
				//'width': 'mapData(strength, 70, 100, 2, 6)',
				'width': 'mapData(strength, '+strengths['bottom_strength']+',' +strengths['top_strength']+', 2, 8)',
				/* 'target-arrow-shape': 'triangle',
				'source-arrow-shape': 'circle', */
				'line-color': 'data(faveColor)',
				'source-arrow-color': 'data(faveColor)',
				'target-arrow-color': 'data(faveColor)'
			  })
			.selector('edge.questionable')
			  .css({
				'line-style': 'dotted',
				'target-arrow-shape': 'diamond'
			  })
			.selector('.faded')
			  .css({
				'opacity': 0.25,
				'text-opacity': 0
			  }),
		  
		  elements: {
			nodes: user_array,
			edges: socialLinks_array
		  },
		  
		  ready: function(){
			window.cy = this;
			
			cy.one("load", function(){
				
				cy.nodes().pageRank();
				
				cy.nodes().forEach(function( ele ){
					if(ele.data("weight") == 0)
						ele.style( 'background-color', '#A9A9A9' );
				});
				
				  cy.edges().forEach(function( ele ){
				  if(ele.data("direction") == "1" || ele.data("direction") == "0")
				  {
					  ele.style( 'target-arrow-shape', 'triangle' );
				  }
				  if(ele.data("direction") == "2")
				  {
					  ele.style( 'target-arrow-shape', 'triangle' );
					  ele.style( 'source-arrow-shape', 'triangle' );
				  }
				});
				
				//console.log("Arrows drawn.");
			});
			
			cy.on('tap', 'node', function(evt){

			  resetColors();
			  
			  var node = evt.cyTarget; //Get target node
			  //console.log( 'tapped ' + node.id() );

			  var connected_edges = node.connectedEdges(); //Get all edges connected to node
			  connected_edges.style( 'line-color', 'red' );
			  connected_edges.style( 'source-arrow-color', 'red' );
			  connected_edges.style( 'target-arrow-color', 'red' ); 
			  
			  /*connected_edges.forEach(function( ele ){
				  if(ele.data("direction") == "0")
				  {
					  ele.style( 'line-color', 'red' );
					  //ele.style( 'source-arrow-color', 'red' );
					  ele.style( 'target-arrow-color', 'red' );
				  }
				  if(ele.data("direction") == "1")
				  {
					  ele.style( 'line-color', 'blue' );
					  //ele.style( 'source-arrow-color', 'blue' );
					  ele.style( 'target-arrow-color', 'blue' );
				  }
				  if(ele.data("direction") == "2")
				  {
					  ele.style( 'line-color', 'green' );
					  ele.style( 'source-arrow-color', 'green' );
					  ele.style( 'target-arrow-color', 'green' );
				  }
				});
			  
			  console.log(node);*/
			  
			});
			
			cy.on('tap', 'edge', function(evt){
				
				resetColors();
				
				var edge = evt.cyTarget; //Get target edge
				
				edge.style( 'line-color', 'black' );
				edge.style( 'source-arrow-color', 'black' );
				edge.style( 'target-arrow-color', 'black' );
			});
			// giddy up
			
			
		  },
		  
		  zoomingEnabled: true,
		  //userZoomingEnabled: true,	  
		  minZoom: 0.2,
		  maxZoom: 2.0,
		  wheelSensitivity: 0.1,
		  motionBlur: true
		});   
    },750);

	
	
}

/* --------------------------------------------------------------------------------------- 
This function resets the colours of all highlighted lines on the sna chart.
   --------------------------------------------------------------------------------------- */
function resetColors(){
	var edges = cy.edges(); //Get all edges in chart

	edges.style( 'line-color', '#848484' ); //Reset all edges to default color
	edges.style( 'source-arrow-color', '#848484' );
	edges.style( 'target-arrow-color', '#848484' );
}