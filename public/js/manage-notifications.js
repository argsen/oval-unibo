$(document).ready(function(){
    $("#mask-loading").hide();

    $(document).ajaxStart(function(){
        $("#mask-loading").show();
    });

    $(document).ajaxStop(function(){
        $("#mask-loading").hide();
    });
});

$('document').ready(function () {
    //--setup tooltips-- 
    $(".icon-for-tooltip").tooltip({
        template: '<div class="tooltip q-tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
    });

    //--hide hud 
    $("#loading-hud").hide();

    //--setup wysiwyg editor (trumbowyg) 
    $.trumbowyg.svgPath = '../img/trumbowyg-icons.svg';
    $("#new-notification-content").trumbowyg({
        // btns: [['viewHTML'], ['strong', 'em', 'del'], ['link']] 
        btns: trumbowyg_btns
    });

    //--when course is selected, load groups dl 
    $("#new-notification-course").on("change", function () {
        $.ajax({
            type: "POST",
            url: "/get_groups",
            data: {
                course_id: $(this).find('option:selected').val()
            },
            success: function (data) {
                var group_dl = $("#new-notification-group");
                group_dl.find('option').remove();
                $.each(data.groups, function (i, val) {
                    group_dl.append("<option value='" + val.id + "'>" + val.name + "</option>")
                });
                group_dl.find('option:first').prop('selected', 'selected');
            },
            error: function (req, status, error) {
                console.log("error /get_groups -" + error); ///// 
            }
        });
    });

    //--when page loads, select first course so the groups load.... 
    $("#new-notification-course option[selected=selected]").removeProp('selected', 'selected');
    $("#new-notification-course option:first").prop('selected', 'selected').change();


    var new_notification_form = $("#new-notification-form");

    //--hide deadline fields when first loads 
    $("fieldset#deadline-fieldset").hide();
    //-- show/hide field(s) depending on radio value... 
    new_notification_form.on("change", function () {
        //--if type is task, show deadline fieldset, otherwise hide 
        if ($("input[name=type-radio]:checked").val() === "task") {
            $("fieldset#deadline-fieldset").show();
        }
        if ($("input[name=type-radio]:checked").val() === "message") {
            $("fieldset#deadline-fieldset").hide();
        }

        //--when no for deadline is selected, hide date input field 
        if ($("input[name=deadline-radio]:checked").val() === "true") {
            $("#new-notification-deadline").show();
        }
        if ($("input[name=deadline-radio]:checked").val() === "false") {
            $("#new-notification-deadline").hide();
        }
    });


    //--save button 
    $("#save-new-notification-button").on("click", function () {
        new_notification_form.validator('validate');
        if (new_notification_form.find('.has-error').length) {
            return false;
        }
        $("#loading-hud").show();
        var type = $('input[name=type-radio]:checked').val();
        var msg = $("#new-notification-content").val();
        var issue_date = $("#new-notification-issue").val();
        var group_id = $("#new-notification-group").val();
        var deadline = null;
        if (type === "task" && $('input[name=deadline-radio]:checked').val() == 'true') {
            deadline = $("#new-notification-deadline").val();
        }
        $.ajax({
            type: "POST",
            url: "/add_notification",
            data: {
                type: type,
                msg: msg,
                author_id: user_id,
                issue_date: issue_date,
                deadline: deadline,
                group_id: group_id
            },
            success: function (data) {
                $("#new-notification-form").trigger("reset");
                location.reload();
            },
            error: function (req, status, error) {
                console.log("error /add_notification -" + error); ///// 
                $("#loading-hud").hide();
            }
        });
    });

    //--modal for editing  
    var edit_modal = $("#edit-notification-modal");
    var edit_notification_form = $("#edit-notification-form");
    edit_modal.on("show.bs.modal", function (e) {
        //--populate fields 
        var tr = $(e.relatedTarget).parent().parent();
        edit_modal.find("input[name=edit-type-radio][value=" + tr.find(".type-td").text() + "]").prop("checked", true);
        $("#edit-notification-issue").val(tr.find(".issue-td").data('val'));
        var textarea = $("#edit-notification-msg"); 
        textarea.trumbowyg('destroy'); 
        textarea.text(tr.find(".msg-td").text()); 
        textarea.trumbowyg({ 
          btns: trumbowyg_btns 
        });
        $("#edit-notification-deadline").val(tr.find(".deadline-td").data('val'));
        //--hide deadline field if type is message 
        if (tr.find(".type-td").text() === 'message') {
            $("#edit-deadline-div").hide();
        }
        //--set notification-id attr for save button 
        $("#save-notification-button").attr("data-nid", $(e.relatedTarget).data('nid'));
    });
    edit_modal.on("hidden.bs.modal", function (e) {
        edit_notification_form.trigger('reset');
    });
    edit_notification_form.on("change", function () {
        if ($("input[name=edit-type-radio]:checked").val() === "task") {
            $("#edit-deadline-div").show();
        }
        if ($("input[name=edit-type-radio]:checked").val() === "message") {
            $("#edit-deadline-div").hide();
        }
    });
    edit_modal.on("click", "#save-notification-button", function (e) {
        edit_notification_form.validator('validate');
        if (edit_notification_form.find('.has-error').length) {
            return false;
        }
        $.ajax({
            type: "POST",
            url: "/edit_notification",
            data: {
                notification_id: $(this).attr('data-nid'),
                type: edit_modal.find("input[name=edit-type-radio]:checked").val(),
                issue_at: $("#edit-notification-issue").val(),
                msg: $("#edit-notification-msg").val(),
                deadline: $("#edit-notification-deadline").val()
            },
            success: function (data) {
                location.reload();
            },
            error: function (req, status, error) {
                console.log("error /edit_notification - " + error); ///// 
                edit_modal.modal('hide');
            }
        });
    });


}); //end document ready