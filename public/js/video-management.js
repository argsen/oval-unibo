var modal_course_id;
var modal_group_id;
var modal_course_name;
var modal_group_name;
var modal_video_id;
var modal_groups;
var covaa_tag_array = [];


function saveVideo (v_id, u_id, media_type, point_instruction, points, c_id, request_analysis, custom_title, video_password, segments) {
	$.ajax({
		type: "POST",
		url: "/add_video",
		data: {
			video_id:v_id, 
			media_type:media_type, 
			point_instruction:point_instruction, 
			points:points, 
			course_id:c_id, 
			request_analysis:request_analysis,
			custom_title: custom_title, 
			video_password:video_password, 
			segments:segments
		},
		success: function(data) {
			if (data.action == "redirect") {
				window.location.href = data.url;
			}
			else if (data.msg) {
				$(".msg").text(data.msg).show();
			}
			else if(data.course_id){
				var protocol = window.location.protocol;
				var host = window.location.host;
				// window.location.href = protocol+"//"+host+"/video-management/"+data.course_id+"?"+Math.random()+"#assigned";
				window.location.href = protocol+"//"+host+"/video-management/"+c_id+"?"+Math.random()+"#v-"+data.video_id;
			}
			else {
				window.location.hash = 'unassigned';
				window.location.reload(true);
			}
			
		},
		error: function(req, status, error) {
			$("#loading-hud").hide();
			console.log("error /add_video - "+error);	/////
			alert("Oops, something went wrong...");	////////
		}
	});
}

function modalGetGroups() {
	$("#modal-course-name").text(modal_course_name);
	$.ajax({
		type: "POST",
		url: "/get_group_info_for_video",
		data: {course_id: modal_course_id, video_id: modal_video_id, user_id: user_id},
		success: function(data) {
			var groups = data.unassigned_groups;
			var list = $("#modal-group-list");
			list.html("");
			if (groups.length == 0) {
				list.append("<option disabled>No available groups</option>");
			}
			else {
				$.each(groups, function(i, group) {
					list.append('<option value="'+group.id+'">'+group.name+'</option>');
				});
				list.focus();
			}
		},
		error: function(req, status, err) {
			console.log("error get_groups - "+status+": "+err);	///////
		},
		async: false
	});
}

function assignVideoToGroups(group_ids, copy_from_group_id, copy_comment_instruction, copy_points, copy_quiz) {
	if (group_ids.length == 0) {
		alert("Please select groups to assign this video to");/////
		return;
	}
	$.ajax({
		type: "POST",
		url: "/save_video_group",
		data: {
			group_ids: group_ids, 
			course_id: modal_course_id, 
			video_id: modal_video_id,
			copy_from: copy_from_group_id,
			copy_comment_instruction: copy_comment_instruction,
			copy_points: copy_points,
			copy_quiz: copy_quiz
		},
		success: function () {
			alert("Video is assigned to the Group");	/////
			$("#modal-form").modal('hide');
			window.location.href = "/video-management/"+modal_course_id+"?"+Math.random()+"#assigned";
		},
		error: function(req, status, err) {
			console.log("error save_video_group - "+status+": "+err);	///////
		},
		async: false
	});
}


function checkIfCourseWidePoints (course_id, video_id) {
	$.ajax({
			type: "POST",
			url: "/check_if_course_wide_points",
			data: {course_id: modal_course_id, video_id: modal_video_id},
			success: function(data) {
				if (data.is_course_wide) {
					$("#same-points").prop("checked", true);
					$("#points-form-groups").hide();
				}
				else {
					$("#not-same-points").prop("checked", true);
					$("#points-form-groups").show();
				}
			},
			failure: function (request, status, error) {
				console.log(status+": error checking if course-wide points: "+error);	///////
			}
		});
}

function getGroupsForVideo() {
		$.ajax({
			type: "POST",
			url: "/get_groups_for_video",
			data: {video_id: modal_video_id},
			success: function(data) {
				var groups = data.groups;
				var ul = $("#points-form-groups-dropdown");
				ul.html("");
				if (groups.length > 0) {
					// modal_group_id = groups[0].id;
					$.each(groups, function(i, group) {
						ul.append('<li id="'+group.id+'">'+group.name+'</li>');
					});
					$("#points-form-group-name").text(groups[0].name);

					fillPointsInputs();
				}
			},
			error: function(request, status, error) {
				console.log(status+": error getting groups for video: "+error);	///////
			},
		});
}

function fillPointsInputs() {
	$.ajax({
		type: "POST",
		url: "/get_points_for_group_video",
		data: {group_id: modal_group_id, video_id: modal_video_id},
		success: function(data) {
			var instruction = unescapeHtml(data.point_instruction);
			$("#modal-point-instruction").val(instruction);
			$("#modal-points .row").remove();
			var points = data.points;
			if (points.length > 0) {
				$.each(points, function (index, point) {
					var row = $('<div>', {'class':'row'}).appendTo("#modal-points");
					var col10 = $('<div>', {
									'class': 'col-9',
									'html':'<input class="form-control" id="'+point.id+'" type="text" value="'+point.description+'">'
								}).appendTo(row);
					var col2 = $('<div>', {
									'class':'col-3',
									'html':'<button type="button" class="modal-delete-point outline-button btn-sm full-width" title="delete"><span class="hidden-sm-down">Delete</span><i class="fa fa-minus-circle"></i></button>'
								}).appendTo(row);
				});
			}
			else {
				var row = $('<div>', {'class':'row'}).appendTo("#modal-points");
				var col10 = $('<div>', {
									'class': 'col-9',
									'html':'<input class="form-control new-point" type="text" placeholder="Point text here...">'
								}).appendTo(row);
					var col2 = $('<div>', {
									'class':'col-3',
									'html':'<button type="button" class="modal-delete-point outline-button btn-sm full-width" title="delete"><span class="hidden-sm-down">Delete</span><i class="fa fa-minus-circle"></i></button>'
								}).appendTo(row);
			}
		},
		error: function(request, status, error) {
			console.log(status+": error getting points: "+error);	///////
		}
	});
}

function hostname(url) {
    var match = url.match(/:\/\/(www[0-9]?\.)?(.[^\/:]+)/i);
    if ( match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0 ) return match[2];
}

function unescapeHtml(safe) {
	return safe ? safe.replace(/&amp;/g, '&')
		.replace(/&lt;/g, '<')
		.replace(/&gt;/g, '>')
		.replace(/&quot;/g, '"')
		.replace(/&#039;/g, "'")
		:
		"";
}

function first_key_of_array(arr) {
	for (var key in arr) {
		return key;
	}
}

$(document).ready(function(){

	var modal = $("#modal-form");
	var points_form = $("#modal-points-form");

	$("#course-name").text(course_name);
	$("#group-name").text(group_name);
	$("#term-name").text(course_term_year);

	if ($.trim($(".msg").text()).length > 0) {
		$(".msg").show();
	}
	else {
		$(".msg").hide();
	}

	$("#points-controls").hide();
	$("#text-analysis").hide();
	$("#loading-hud").hide();

	$("#course-to-assign").val(course_id);

	//--hide analysis request controls
	$("#text-analysis").hide();

	//--ellipsise video description if long (uses plugin clamp.js https://github.com/josephschmitt/Clamp.js) 
	$('.clamp-this').each(function (i, element) {
		$clamp(element, {
			clamp: 5
		});
	});
	$(".admin-page-section").on('click', '.clamp-this', function () {
		$(this).removeClass("clamp-this");
		$(this).addClass("full-text");
		$(this).text($(this).data('full'));
	});
	$(".admin-page-section").on('click', '.full-text', function () {
		$(this).removeClass("full-text");
		$(this).addClass("clamp-this");
		$clamp($(this)[0], {
			clamp: 5
		});
	});


	//-- show/hide corresponding fields when radio value changed --
	$("#add-video-form input").on("change click", function(e) {
		var radio = $("input[name=source-radio]:checked");
		$("#open-link").prop("title", "Open "+radio.parent().text()+" page in new window");
		$("#open-link").prop("href", radio.attr('data-url'));
		if (radio.attr('id')==="vimeo") {
			$(".vimeo-only").show();
		}
		else {
			$(".vimeo-only").hide();
		}

		// if($("input[name=group-radio]:checked", "#add-video-form").val() === "true") {
		// 	$("#points").show();
		// }
		// else {
		// 	$("#points").hide();
		// }
		if ($("input[name=points-radio]:checked", "#add-video-form").val() === "true") {
			$("#points-controls").show();
		}
		if ($("input[name=points-radio]:checked", "#add-video-form").val() === "false") {
			$("#points-controls").hide();
		}
		if ($("input[name=segments-radio]:checked", "#add-video-form").val() === "true") {
			$("#segments-controls").show();
			$(".segment-start").attr("required", true);
			$(".segment-end").attr("required", true);
			$("#add-video-form").validator('update');
		}
		if ($("input[name=segments-radio]:checked", "#add-video-form").val() === "false") {
			$("#segments-controls").hide();
			$(".segment-start").attr("required", false);
			$(".segment-end").attr("required", false);
			$("#add-video-form").validator('update');
		}

	});//end add-video-form radio on change

	
	$("input:radio[name=source-radio]:first").attr("checked", true).trigger("click");
	$("[name=course-radio]").val(["true"]);
	$("[name=points-radio]").val(["false"]);
	$("[name=segments-radio]").val(["false"]).trigger("click");

	//--setup tooltips--
	$(".icon-for-tooltip").tooltip({
		template: '<div class="tooltip q-tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
	});
	$('.gray-tooltip[data-toggle="tooltip"]').tooltip({
		template: '<div class="tooltip b-tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
	});



	$("#another-point-button").on("click", function() {
		var row = $('<div>', {'class':'row space-bottom'}).appendTo("#points-inputs");
		var col10 = $('<div>', {
						'class':'col-9',
						'html':'<input class="form-control gray-textbox" type="text" placeholder="Point text here...">'
					}).appendTo(row);
		var col2 = $('<div>', {
						'class':'col',
						'html':'<button type="button" class="delete-point outline-button btn-sm full-width" title="delete"><span class="d-none d-sm-inline-block">Delete</span><i class="fa fa-minus-circle"></i></button>'
					}).appendTo(row);
		$("#points-inputs :text").last().focus();
	});
	$("#points").on("click", '.delete-point', function(e) {
		$(this).parent().parent().remove();
	});



	$("#another-segment-button").on("click", function() {
		var row = $('<fieldset>', {'class':'row form-group mb-2 no-border'}).appendTo("#segments-inputs");
		$('<div>', {
			'class':'col-12 col-md-4 mb-1',
			'html':'<legend>Title</legend><input class="form-control gray-textbox segment-title" type="text" placeholder="Title">'
		}).appendTo(row);
		$('<div>', {
			'class':'col-5 col-md-3',
			'html':'<legend>Start Time</legend><input class="form-control gray-textbox segment-start" type="text" placeholder="start" data-start="start" required data-required-error="Please fill in start time.">'
		}).appendTo(row);
		$('<div>', {
			'class':'col-5 col-md-3',
			'html':'<legend>End Time</legend><input class="form-control gray-textbox segment-end" type="text" placeholder="end" data-end="end" required data-required-error="Please fill in end time.">'
		}).appendTo(row);
		$('<div>', {
			'class':'col-2 mt-4',
			'html':'<button type="button" class="delete-segment outline-button btn-sm full-width" title="delete"><span class="d-none d-md-inline-block">Delete</span><i class="fa fa-minus-circle"></i></button>'
		}).appendTo(row);
		$('<div>', {
			'class': 'col-12 help-block with-errors'
		}).appendTo(row);
		$("#add-video-form").validator('update');
	});
	$("#segments").on("click", '.delete-segment', function(e) {
		$(this).parent().parent().remove();
	});

	//-- show/hide text analysis field --> disable
	/*
	$("#video-url").on("focusout", function() {
		//--HELIX--
		if ($("input[name=source-radio]:checked", "#add-video-form").val() === "helix") {
			$("#text-analysis").hide();
		}
		//--YouTube--
		if ($("input[name=source-radio]:checked", "#add-video-form").val() === "youtube") {
			form_media_type = "youtube";
			var url = $("#video-url").val();
			var host = hostname(url);
			var vid;

			if (host == "youtube.com") {
				vid = url.substr(url.lastIndexOf('watch?v=')+8, 11);
			} else if (host == "youtu.be") {
				vid = url.substr(url.lastIndexOf('/')+1, 11);
			}
			if (vid) {
				$.ajax({
					type: "POST",
					url: "/check_youtube_caption",
					data: {video_id:vid},
					success: function(data) {
						if (data.caption_available) {
							$("#text-analysis").show();
						}
						else {
							$("#text-analysis").hide();
						}
					},
					error: function(req, status, error) {
						console.log("error /check_youtube_caption - "+error);//////
					}
				});
			}
		}
	});
	*/

	/**
	 * Function to take video_time in "00:00:00" or "00:00" format string, 
	 * and return seconds. 
	 * If the string isn't formatted correctly, it returns null.
	 * @param {String} video_time 
	 * @return {Number} seconds calculated from string param or null if param in wrong pattern
	 */
	function calculateVideoTimeToSeconds(video_time) {
		var regex = "^([0-9]{1,3}):([0-9]{2})(:([0-9]{2}))?$";
		var matches = video_time.match(regex);
		if(matches===null) {
			return null;
		}
		var sec = 0;
		if(matches[4]) {
			sec = Number(matches[1])*60*60 + Number(matches[2])*60 + Number(matches[4]);
		}
		else {
			sec = Number(matches[1])*60 + Number(matches[2]);
		}
		return sec;
	}


	$().validator({
			custom: {
				'start': function($e) {
					if(!$e.val()) {
						return "Please fill in start time";
					}
					var sec = calculateVideoTimeToSeconds($e.val());
					if(sec===null) {
						return "Please enter start time between 0:00 and end of video.";
					}

					var end = $e.parent().next('div').find('input:text').val();
					if(end) {
						var end_sec = calculateVideoTimeToSeconds(end);
						if(sec >= end_sec) {
							return "End time must be greater than Start time.";
						}
					}//if end
				},//end 'start':function
				'end': function($e){
					if(!$e.val()) {
						return "Please fill in end time";
					}
					var sec = calculateVideoTimeToSeconds($e.val());
					if (sec===null) {
						return "Please enter end time between start time and end of video.";
					}

					var start = $e.parent().prev('div').find('input:text').val();
					if(start) {
						var start_sec = calculateVideoTimeToSeconds(start);
						if(start_sec >= sec) {
							return "End time must be greater than Start time.";
						}
					}//if start


				}//end 'end':function
			}
			
	});
	
	//-- add video --
	$("#add-video-form input").on("keydown", function(e) {
		if (e.which == 13) {
			e.preventDefault();
		}
	});
	$("#add-video-button").on("click", function(e) {
		$("#add-video-form").validator('validate');
		if($("#add-video-form").find('.has-error').length > 0) {
			return false;
		}
		$("#loading-hud").show();
		var video_id;
		var media_type;
		var video_password = null;

		var u_id = $("#UID").val();
		var c_id = $("#CID").val();
		var g_id = $("#GID").val();
		var url = $("#video-url").val();
		var  course_id = $("select[name=course-to-assign]").val();

		var v_title = $("#custom-title").val();

		var segments = [];
		if($('input[name=segments-radio]:checked', '#add-video-form')) {
			$("#segments-inputs fieldset").each(function(index, fieldset) {
				var seg_title = $(fieldset).find('.segment-title').val();
				var seg_start = calculateVideoTimeToSeconds($(fieldset).find('.segment-start').val());
				var seg_end = calculateVideoTimeToSeconds($(fieldset).find('.segment-end').val());
				if(seg_start && seg_end) {
					segments.push({'title':seg_title, 'start':seg_start, 'end':seg_end});
				}
			});
		}

		var points = [];
		var point_instruction = null;
		if($('input[name=points-radio]:checked', '#add-video-form')) {
			point_instruction = $("#point-instruction-textbox").val();
			$('#points-inputs :text').each(function(index, item) {
				if($(item).val()!=="") {
					points.push($(item).val());
				}
			});
		}

		var req_analysis = false;
		if ($("#request-analysis").is(':checked')) {
			req_analysis = true;
		}


		//--HELIX--
		if ($("input[name=source-radio]:checked", "#add-video-form").val() === "helix") {
			media_type = "helix";
			var helix_host = helix_js_host.split('://').pop();
			if (hostname(url) != helix_host) {
				alert('Please check your UniSA Media Library URL is valid. (It starts with ' + helix_js_host + '/)');
				$("#loading-hud").hide();
				return false;
			}

			video_id = url.substr(url.lastIndexOf('/')+1);
			video_id = video_id.replace(/(_|\.)(\S+)?/g, '');
		}
		//--YouTube--
		if ($("input[name=source-radio]:checked", "#add-video-form").val() === "youtube") {
			media_type = "youtube";
			var host = hostname(url);

			if (host == "youtube.com") {
				video_id = url.substr(url.lastIndexOf('watch?v=')+8, 11);
			} else if (host == "youtu.be") {
				video_id = url.substr(url.lastIndexOf('/')+1, 11);
			} else {
				alert('Please check your YouTube URL is valid. (It starts with https://www.youtube.com/ or https://youtu.be/)');
				$("#loading-hud").hide();
				return false;
			}
		}
		//--Vimeo--
		if ($("input[name=source-radio]:checked", "#add-video-form").val() === "vimeo") {
			media_type = "vimeo";
			if(hostname(url) != "vimeo.com") {
				alert("Please check if your Vimeo URL is valid. (It starts with 'https://vimeo.com'");
				$("#loading-hud").hide();
				return false;
			}
			video_password = $("#video-password").val();
			// console.log("url="+url);/////
			// console.log("match = "+url.match(/https?:\/\/(?:www\.|player\.)?vimeo\.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/)?(\d+)(?:$|\/|\?)/)[3]);/////
			video_id = url.match(/https?:\/\/(?:www\.|player\.)?vimeo\.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/)?(\d+)(?:$|\/|\?)/)[3];
		}

		saveVideo (video_id, u_id, media_type, point_instruction, points, course_id, req_analysis, v_title, video_password, segments);
		$("#add-video-form").trigger("reset");
		return false;
	});//end add-video-form submit

	//-- delete video (unassigned to groups) --
	$(".delete-button").click(function() {
		var video_id = $(this).data("id");
		if (confirm("Are you sure you want to delete?")) {
			$.ajax({
				type: "POST",
				url: "/delete_video",
				data: {video_id: video_id},
				success: function() {
					location.reload();
				},
				error: function(request, status, error) {
					console.log("error with deleting video: "+error);	///////
				},
				async: false
			});
		}
	});

	//-- archive/un-archive --
	$(".archive-button").on("click", function() {
		var group_video_id = $(this).data('id');

		//--check if student activity & confirm to archive
		$.ajax({
			type: "POST",
			url: "/check_student_activity",
			data: {group_video_id: group_video_id},
			success: function(data) {
				if (data.has_activity && confirm("There are student activities on this video for the group. Are you sure you would like to archive it?")) {
					$.ajax({
						type:"POST",
						url: "/archive_group_video",
						data: {group_video_id:group_video_id},
						success: function(data) {
							window.location.reload();
						},
						error(req, status, error) {
							console.log("error /archive_group_video - "+error);/////
						}
					});
				}
				if (!data.has_activity && confirm("Are you sure you would like to make this video unavailable to the group?")) {
					$.ajax({
						type:"POST",
						url: "/delete_group_video",
						data: {group_video_id:group_video_id},
						success: function(data) {
							window.location.reload();
						},
						error(req, status, error) {
							console.log("error /delete_group_video - "+error);/////
						}
					});
				}
			},
			error: function(req, status, error) {
				console.log("error /check_student_activity - "+error);/////
			}
		});
	});

	modal.on("show.bs.modal", function(e) {
		var button = $(e.relatedTarget); // Button that triggered the modal
  		modal_video_id = button.data('id');
		modal_course_id = course_id;
		modal_course_name = course_name;
		// modal_group_id = group_id;
		modal_group_name = group_name;

		//--populate thumbnail and title for video--
		$.ajax({
			type: "POST",
			url: "/get_video_info",
			data: {video_id: modal_video_id},
			success: function (data) {
				$("#modal-video-thumbnail").attr("src", data.thumbnail_url);
				$("#modal-video-title").text(data.title);
			},
			error: function (req, status, error) {
				console.log("error /get_video_info - "+error);/////
			}
		});

		$("#copy-from").hide();

		//-- populate course dropdown
		$("#course-dropdown li").first().click();
		// $("#modal-group-list").focus();
	});


	$("#course-dropdown li").on('click', function() {
		modal_course_id = $(this).attr("id");
		modal_course_name = $(this).text();
		$("#assign-video-to-group-form").trigger('reset');
		modalGetGroups();
		// $("#modal-group-list").focus();
	});


	//-- when copy-contents option changed, fetch groups that has same video & configure form
	$("input[type=radio][name=copy-contents]").on("change", function() {
		if($(this).val() === "true") {
			$("#copy-from-course").html("");
			$("#copy-from-course").prop("disabled", false);
			$("#copy-from-group").html("");
			$("#copy-from-group").prop("disabled", false);
			$("#copy-from").show();
			modal_groups = [];

			$.ajax({
				type: "POST",
				url: "/get_groups_with_video",
				data: {video_id: modal_video_id},
				success: function(data) {
					if (data.length == 0) {
						$("#copy-from-course").append('<option>No available courses</option>');
						$("#copy-from-course").prop("disabled", "disabled");
						$("#copy-from-group").append('<option>No available groups</option>');
						$("#copy-from-group").prop("disabled", "disabled");
						$("#comment-instruction-cb").prop("disabled", true);
						$("#points-cb").prop("disabled", true);
						$("#quiz-cb").prop("disabled", true);
					}
					else {
						modal_groups = data;
						var selected_course_id = first_key_of_array(modal_groups);
						for(var key in modal_groups) {
							$("#copy-from-course").append('<option value="'+key+'">'+modal_groups[key][0].course_name+'</option>');
						}
						$("#copy-from-course").val(selected_course_id);
						$("#copy-from-course").trigger("change");
					}
				},
				error: function(request, status, error) {
					console.log("error /get_other_groups_with_video -"+error);/////
				}
			});
		}
		else {
			$("#copy-from").hide();
		}
	});

	//-- when course selected, populate group dropdown
	$("#copy-from-course").on('change', function(e) {
		var selected = $(this).find("option:selected");
		var selected_course_id = selected.val();
		var groups = modal_groups[selected_course_id];
		$("#copy-from-group").html("");
		$("#copy-from-group").prop("disabled", false);
		$.each (groups, function(i, val) {
			if(i==0) {
				$("#copy-from-group").append('<option value="'+val.id+'" selected>'+val.name+'</option>');
			}
			else {
				$("#copy-from-group").append('<option value="'+val.id+'">'+val.name+'</option>');
			}
		});
		$("#copy-from-group").val(groups[0].id);
		$("#copy-from-group").trigger("change");
	});

	//-- when group selected, configure checkboxes for option to match
	$("#copy-from-group").on('change', function(e) {
		var selected = $(this).find("option:selected");
		var selected_group_id = selected.val();
		var selected_course = $("#copy-from-course option:selected").val();
		var groups = modal_groups[selected_course];

		$.each(groups, function(i, val) {
			if (val.id == selected_group_id) {
				if (val.has_comment_instruction == true) {
					$("#copy-comment-instruction-checkbox").removeClass("disabled");
					$("#comment-instruction-cb").prop("disabled", false);
				}
				else {
					$("#copy-comment-instruction-checkbox").addClass("disabled");
					$("#comment-instruction-cb").prop("disabled", true);
				}
				if (val.has_points == true) {
					$("#copy-points-checkbox").removeClass("disabled");
					$("#points-cb").prop("disabled", false);
				}
				else {
					$("#copy-points-checkbox").addClass("disabled");
					$("#points-cb").prop("disabled", true);
				}
				if (val.has_quiz == true) {
					$("#copy-quiz-checkbox").removeClass("disabled");
					$("#quiz-cb").prop("disabled", false);
				}
				else {
					$("#copy-quiz-checkbox").addClass("disabled");
					$("#quiz-cb").prop("disabled", true);
				}
				return false;
			}
		});
	});


	modal.on("click", "#assign-to-group", function(e) {
		e.preventDefault();
		$("#assign-video-to-group-form").validator('validate');
		if($("#assign-video-to-group-form").find('.has-error').length) {
			return false;
		}

		var group_ids = [];
		$("#modal-group-list :selected").each(function() {
			group_ids.push($(this).val());
		});
		var copy = $("#copy-contents-yes").is(":checked") ? true : false;
		var copy_from_group_id = -1;
		var copy_comment_instruction = false;
		var copy_points = false;
		var copy_quiz = false;
		if (copy) {
			copy_from_group_id = $("#copy-from-group option:selected").val();
			copy_comment_instruction = $("#comment-instruction-cb").is(":checked") ? true : false;
			copy_points = $("#points-cb").is(":checked") ? true : false;
			copy_quiz = $("#quiz-cb").is(":checked") ? true : false;
		}
		assignVideoToGroups(group_ids, copy_from_group_id, copy_comment_instruction, copy_points, copy_quiz);
	});
	
	modal.on("hidden.bs.modal", function () {
		modal_course_id = null;
		modal_course_name = null;
		modal_group_id = null;
		modal_group_name = null;

		$("#modal-video-thumbnail").attr("src", "");
		$("#modal-video-title").text("");
		$("#assign-video-to-group-form").trigger('reset');
	});

	points_form.on("show.bs.modal", function(e) {
		var button = $(e.relatedTarget);
		modal_video_id = button.data('id');
		var title = button.data('title');
		modal_course_id = course_id;
		modal_group_id = group_id;
		$("#points-form-course-name").text(course_name);
		// $("#points-form-video-title").text(title);
		$("#points-form-group-name").text(group_name);

		//--populate thumbnail and title for video--
		$.ajax({
			type: "POST",
			url: "/get_video_info",
			data: {video_id: modal_video_id},
			success: function (data) {
				$("#points-form-thumbnail-img").attr("src", data.thumbnail_url);
				$("#points-form-video-title").text(data.title);
			},
			error: function (req, status, error) {
				console.log("error /get_video_info - "+error);/////
			}
		});

		// checkIfCourseWidePoints(modal_course_id, modal_video_id);
		// getGroupsForVideo();
		fillPointsInputs();
	});

	points_form.on("click", "#modal-another-point", function() {
		var row = $('<div>', {'class':'row'}).appendTo("#modal-points");
		var col10 = $('<div>', {
						'class':'col-9',
						'html':'<input class="form-control new-point" type="text" placeholder="Point text here...">'
					}).appendTo(row);
		var col2 = $('<div>', {
						'class':'col-3',
						'html':'<button type="button" class="modal-delete-point outline-button btn-sm full-width" title="delete"><span class="hidden-sm-down">Delete</span><i class="fa fa-minus-circle"></i></button>'
					}).appendTo(row);
		$("#modal-points :text").last().focus();
	});

	$("input[name=course-wide-points-radio]").on("change", function() {
		if ($(this).val() === "yes") {
			$("#points-form-groups").hide();
		}
		if ($(this).val() === "no") {
			$("#points-form-groups").show();
		}
	});

	points_form.on("click", ".modal-delete-point", function() {
		$(this).parent().parent().remove();
	});

	points_form.on("click", "#points-form-course-dropdown li", function() {
		modal_course_id = $(this).attr("id");
		$("#points-form-course-name").text($(this).text());
		$.ajax({
				type: "POST",
				url: "/get_videos_for_course",
				data: {course_id: modal_course_id},
				success: function(data) {
					var videos = data.videos;
					var ul = $("#points-form-video-dropdown");
					ul.html("");
					$.each(videos, function(i, video) {
						ul.append('<li id="'+video.id+'">'+video.title+'</li>');
					});
					$("#points-form-video-dropdown li").first().click();
				},
				error: function(request, status, error) {
					console.log(status+": error getting videos for course: "+error);	///////
				},
				async: false
			});
	});

	points_form.on("click", "#points-form-video-dropdown li", function() {
		$("#points-form-video-title").text($(this).text());
		modal_video_id = $(this).attr("id");

		checkIfCourseWidePoints(modal_course_id, modal_video_id);
		getGroupsForVideo();
	});

	points_form.on("click", "#points-form-groups-dropdown li", function() {
		$("#points-form-group-name").text = $(this).text();
		modal_group_id = $(this).attr("id");
	});

	points_form.on("click", "#save-points", function() {
		var is_course_wide = false;
		if ($("input[name=course-wide-points-radio]").val() === "yes") {
			is_course_wide = true;
		}
		var point_instruction = $("#modal-point-instruction").val();
		var point_ids = [];
		var points = [];
		$('#modal-points :text').each(function(index, item) {
			if($(item).val()!=="") {
				var point_id = $(this).hasClass('new-point') ? -1 : Number($(this).attr('id'));
				point_ids.push(point_id);
				points.push($(item).val());
			}
		});

		$.ajax ({
			type: "POST",
			url: "/save_points",
			data: {course_id: modal_course_id, group_id: modal_group_id, video_id: modal_video_id, is_course_wide: is_course_wide, point_instruction: point_instruction, point_ids: point_ids, points: points},
			success: function(data) {
				alert("Points were saved");
				points_form.modal("hide");
			},
			error: function(request, status, error) {
				console.log(status+": error saving points: "+error);	///////
			}
		});
	});

	points_form.on("click", "#delete-points", function() {
		if (confirm('Are you sure to delete points and instruction for selected video for the group/course?')) {
			var is_course_wide = false;
			if ($("input[name=course-wide-points-radio]").val() === "yes") {
				is_course_wide = true;
			}
			$.ajax({
				type:"POST",
				url:"/delete_points",
				data:{video_id: modal_video_id, group_id: modal_group_id, is_course_wide: is_course_wide},
				success: function(data) {
					points_form.modal('hide');
				},
				error: function(request, status, error) {
					console.log(status+": error deleting points: "+error);	///////
				}
			});
		}
	});

	points_form.on("hidden.bs.modal", function () {
		modal_course_id = null;
		modal_group_id = null;
		modal_video_id = null;
		$("#modal-points-form :text").val('');

	});

	var visibility_modal = $("#visibility-modal");
	$(".visibility-button").on("click", function() {
		visibility_modal.find("#group-video-id").val($(this).data('id'));
		var hidden = $(this).data('hidden');
		if (hidden) {
			visibility_modal.find('#hidden-radio').prop('checked', true);
		}
		else {
			visibility_modal.find('#visible-radio').prop('checked', true);
		}
	});
	visibility_modal.find("#save-visibility").on("click", function(e) {
		e.preventDefault();
		var group_video_id = $('#group-video-id').val();
		var visibility = $('input[name="visibility-radio"]:checked').val();
		$.ajax({
			type:"POST",
			url: "/edit_visibility",
			data: {group_video_id:group_video_id , visibility:visibility },
			success: function() {
				location.reload();
			},
			error: function(requst, status, error) {
				console.log("error /edit_visibility - "+error);	/////
			}
		});
	});

	sortable(".sortable-list", {
		forcePlaceholderSize: true,
		placeholderClass: 'sortable-placeholder'
	});
	$("#order-form").on("submit", function(e) {
		e.preventDefault();
		var order = [];
		$('#video-order-list li').each(function(i) {
			order.push($(this).data('id'));
		});
		$.ajax({
			type: "POST",
			url: "/edit_video_order",
			data: {group_video_ids: order},
			success: function() {
				$("#order-modal").modal('hide');
				location.reload();
			},
			error: function(req, status, err) {
				console.log("error /edit_video_order: "+err);	/////
			}
		});
	});

	var keywords_modal = $("#text-analysis-modal");

	function setToggleModalRadioLabels(instruction, label1, label2) {
		keywords_modal.find(".instruction").text(instruction);
		keywords_modal.find("#show-radio").parent().find("span").text(label1);
		keywords_modal.find("#hide-radio").parent().find("span").text(label2);
	}

	keywords_modal.on("show.bs.modal", function(e) {
		//on show, get the related button's title to be the modal title
		//& change text 
		//& change attribute of save button so it can save different things
		var button = $(e.relatedTarget);
		var type;
		if (button.hasClass("text-analysis-button")){
			type = "analysis";
			setToggleModalRadioLabels("Set visibility to", "Show", "Hide");
		}
		else if (button.hasClass("recommended-resources-button")) {
			type = "resource";
			setToggleModalRadioLabels("Set visibility to", "Show", "Hide");
		}
		else {
			type = "rating";
			setToggleModalRadioLabels("Set ability to rate recommended resources", "Enable", "Disable");
		}
		keywords_modal.find("#text-analysis-title").text(button.prop("title").toUpperCase());
		keywords_modal.find("#save-analysis-vis").attr("data-type", type);
	});
	$(".toggle-visibility").on("click", function() {
		keywords_modal.find(".group-video-id").val($(this).data('id'));
		var show = $(this).data('visible');
		if (show) {
			keywords_modal.find('#show-radio').prop('checked', true);
		}
		else {
			keywords_modal.find('#hide-radio').prop('checked', true);
		}
	});
	keywords_modal.find("#save-analysis-vis").on("click", function(e) {
		e.preventDefault();
		var group_video_id = $('.group-video-id').val();
		var show = $('input[name="analysis-vis-radio"]:checked').val();
		var type = $(this).attr("data-type");
		if (type==="analysis") {
			$.ajax({
				type:"POST",
				url: "/edit_text_analysis_visibility",
				data: {group_video_id:group_video_id , visibility:show },
				success: function() {
					location.reload();
				},
				error: function(requst, status, error) {
					console.log("error /edit_text_analysis_visibility - "+error);	/////
				}
			});
		}
		else if (type==="resources") {
			$.ajax({
				type:"POST",
				url: "/edit_recommended_resources_visibility",
				data: {group_video_id:group_video_id , visibility:show },
				success: function() {
					location.reload();
				},
				error: function(requst, status, error) {
					console.log("error /edit_text_analysis_visibility - "+error);	/////
				}
			});
		}
		else {
			$.ajax({
				type:"POST",
				url:"/toggle_recommended_resources_rating_ability",
				data: {group_video_id: group_video_id, enable:show},
				success:function(result) {
					console.log("success/toggle_recommended_resources_rating_ability")
					if(result.success==true)
						location.reload();
				},
				error: function(request, status, error) {
					console.log("error /toggle_recommended_resources_rating_ability - "+error);
				}
			});
		}
		
	});
	$('#upload-transcript-form').validator({
		custom: {
			filetype: function($el) {
				var acceptable = $el.data('filetype').split(',');
				var filename = $('#upload-transcript-form').find("#transcript-file").val();
				var extension = filename.replace(/^.*\./, '');
				if (extension == filename) {
					extension = '';
				} 
				else {
					extension = extension.toLowerCase();
				}
				if ($.inArray(extension, acceptable) == -1) {
					return "Invalid file type. Please select .srt file";
				}
			}
		}
	});
	var transcript_form = $("#transcript-form");
	transcript_form.on("show.bs.modal", function(e){
		transcript_form.find("input[name='video_id']").val($(e.relatedTarget).data('id'));
	});

	$(".request-analysis").on("click", function(e) {
		var video_id = $(this).data('id');
		$.ajax({
			type: "POST",
			url: "/add_analysis_request",
			data: {video_id:video_id, user_id:user_id},
			success: function(data) {
				$(".msg").text(data.msg);
				$(".msg").show();
				$("html, body").animate({
					scrollTop:0
				}, 200);
			},
			error: function(request, status, error) {
				console.log("error request-analysis: "+error);	//////
			}
		});
	});

	var edit_keywords_modal = $("#edit-keywords-modal");
	edit_keywords_modal.on("show.bs.modal", function(e){
		var group_video_id = $(e.relatedTarget).data('gvid');
		$("#update-keywords").attr('data-gvid', group_video_id);

		$.ajax({
			type: "POST",
			url: "/get_keywords_for_group_video",
			data: {group_video_id: group_video_id},
			success: function(data) {
				console.log("success get_keywords");/////
				console.table(data);////
				var keywords = $.map(data, function(v, i) {
					return v;
				})
				keywords.sort(function(a, b) {
					return (a.keyword > b.keyword) ? 1 : ((b.keyword > a.keyword) ? -1 : 0);
				});

				$("#edit-keywords-modal table tbody").empty();

				$.each(keywords, function(i, kw) {
					var button = "";
					if (kw.hide==1) {
						button = '<button type="button" class="btn btn-link center-block toggle-keyword" data-word="'+kw.keyword+'" data-visible="0"><i class="fa fa-square-o"></i></button>';
					}
					else {
						button = '<button type="button" class="btn btn-link center-block toggle-keyword" data-word="'+kw.keyword+'" data-visible="1"><i class="fa fa-check-square-o"></i></button>';
					}
					$("#edit-keywords-modal table tbody").append('<tr><td>'+kw.keyword+'</td><td>'+button+'</td></tr>');
				});
			},
			error: function (request, status, error) {
				console.log("error /get_keywords_for_group_video - "+error);/////
			}
		});
	});

	edit_keywords_modal.on("click", '.toggle-keyword', function(e) {
		if($(this).hasClass('red')) {
			$(this).removeClass('red');
		}
		else {
			$(this).addClass('red');
		}
		if($(this).attr('data-visible') == "1") {
			$(this).attr('data-visible', "0");
			$(this).find("i").removeClass('fa-check-square-o');
			$(this).find("i").addClass('fa-square-o');
		}
		else {
			$(this).attr('data-visible', "1");
			$(this).find("i.fa").removeClass('fa-square-o');
			$(this).find("i").addClass('fa-check-square-o');
		}
	});

	edit_keywords_modal.on("click", "#update-keywords", function(e) {
		e.preventDefault();
		var visible = [];
		var hide = [];
		var group_video_id = $(this).data('gvid');
		$.each($("#edit-keywords-modal table tbody .btn.red"), function() {
			if($(this).attr('data-visible') == 1) {
				visible.push($(this).data('word'));
			}
			else {
				hide.push($(this).data('word'));
			}
		});

		$.ajax({
			type: "POST",
			url: "/update_keywords_visibility",
			data: {group_video_id: group_video_id, visible: visible, hide: hide},
			success: function(data) {
				edit_keywords_modal.modal('hide');
			},
			error: function(request, status, error){
				console.log("error /update_keywords_visibility - "+error);//////
				edit_keywords_modal.modal('hide');
				alert("Something went wrong..");
			}
		});
	});
	edit_keywords_modal.on("hide.bs.modal", function() {
		$("#edit-keywords-modal table tbody .btn.red").removeClass('red');
	});

	var groups_modal = $("#modal-assigned-group-list");
	groups_modal.on("show.bs.modal", function(e) {

		var button = $(e.relatedTarget); // Button that triggered the modal
  		modal_video_id = button.data('id');
		modal_course_id = course_id;
		modal_course_name = course_name;
		modal_groups = [];
		$.ajax({
			type: "POST",
			url: "/get_groups_with_video",
			data: {video_id: modal_video_id},
			success: function(data) {
				modal_groups = data;
				$('#assigned-groups-course-ul li[data-id="'+modal_course_id+'"]').click();
			},
			error: function(req, status, error) {
				console.log("error /get_groups_with_video");/////
			}
		});
	});

	$("#assigned-groups-course-ul li").on("click", function() {
		modal_course_id = $(this).data("id");
		modal_course_name = $(this).text();
		$("#assigned-groups-course-name").text($(this).text());

		var table = $("#assigned-group-table tbody");
		table.html("");
		
		var minus = '<i class="fa fa-minus" aria-hidden="true"></i>';
		if(modal_groups[modal_course_id]) {
			var tick = '<i class="fa fa-check" aria-hidden="true"></i>';
			$.each(modal_groups[modal_course_id], function(i, val) {
				var tr = $("<tr>").appendTo(table);
				$("<td>", {
							"class":"col-xs-6",
							"html":"<a href='/view/"+val.group_video_id+"'>"+val.name+"</a>"
						}).appendTo(tr);
				$("<td>", {
							"class":"col-xs-2 text-center",
							"html":val.has_comment_instruction ? tick : minus
						}).appendTo(tr);
				$("<td>", {
							"class":"col-xs-2 text-center",
							"html":val.has_points ? tick : minus
						}).appendTo(tr);
				$("<td>", {
							"class":"col-xs-2 text-center",
							"html":val.has_quiz ? tick : minus
						}).appendTo(tr);
			});
		}
		else {
			var tr = $("<tr>", {"class":"warning"}).appendTo(table);
			tr.append("<td>No assigned groups in this course</td>");
			$("<td>", {
				"class":"col-xs-2 text-center",
				"html": minus
				}).appendTo(tr);
			$("<td>", {
					"class":"col-xs-2 text-center",
					"html": minus
				}).appendTo(tr);
			$("<td>", {
				"class":"col-xs-2 text-center",
				"html": minus
				}).appendTo(tr);
		}
	});

	groups_modal.on("hidden.bs.modal", function() {
		modal_course_id = null;
		modal_course_name = null;
		modal_group_id = null;
		modal_group_name = null;
		$("#assigned-group-list-ul").html("");
	});



	// var covaa_tag_array = [];//declared at top
	$('#my-select').multiSelect({
		selectableHeader: "<div class='custom-header'>Available tag group</div>",
		selectionHeader: "<div class='custom-header'>Selected tag group</div>",
		keepOrder: true,
		afterSelect: function (values) {
			for (var i in covaa_tag_array) {
				if (values[0] === covaa_tag_array[i]['id']) {
					covaa_tag_array[i]['is_choose'] = 'true';
				} 
			}
		},
		afterDeselect: function (values) {
			for (var i in covaa_tag_array) {
				if (values[0] === covaa_tag_array[i]['id']) {
					covaa_tag_array[i]['is_choose'] = 'false';
				} 
			}
		}
	});
	$('#save-tag-group').on('click', function () {
		var group_video_id = $(this).attr('data-gvid');
		$.post('/api/annonation/savebygroupvideoid', {
				user_id: user_id,
				group_video_id: group_video_id,
				covaa_tag_array: covaa_tag_array
			})
			.done(function (data) {
				//hide modal
				$("#covaa-dialog").modal('hide');
			});
	});
	$('#delete-tag-group').on('click', function(){
		if(confirm("This will unassign currently selected Tag Groups. Are you sure to start over?")) {
			var group_video_id = $(this).attr('data-gvid');
			$.post('/api/annonation/deletebygroupvideoid',{
				user_id: user_id,
				group_video_id: group_video_id
			})
			.done(function(data){
				//hide modal
				// $("#covaa-dialog").modal('hide');
				// window.location.reload();
				$.get('/api/annonation/get',{
					user_id: user_id,
				})
				.done(function(data){
					if(!data){
						alert('Please set your tag group in tag management page!');
					}else{
						covaa_tag_array = JSON.parse(data['covaa_tag_array']);

						setupCovaaTagSelect();
						$("#my-select").prop('disabled', false);
						$("#my-select").multiSelect("refresh");
						$('#save-tag-group').show();
					}
				});
			});
		}
		
	});
	function setupCovaaTagSelect(){
		var select = $("#my-select");
		select.empty();
		$.each(covaa_tag_array, function(i,tag) {
			if (tag['status']==='true') {
				if(tag['is_choose']==='true') {
					select.append("<option value='"+tag['id']+"' selected>"+tag['text']+"</option>");
				}
				else {
					select.append("<option value='"+tag['id']+"'>"+tag['text']+"</option>");
				}
			}
		});
		select.prop('disabled', true);
		select.multiSelect('refresh');
	}
	
	$('.covaa-set-tag-group').each(function(){

		// $(this).off("click");
		$(this).on("click", function(){

			//todo:  change modal body, then select
			var group_video_id = $(this).attr('data-group-video-id');;
			$('#save-tag-group').attr('data-gvid', group_video_id);
			$('#delete-tag-group').attr('data-gvid', group_video_id)

			$.get('/api/annonation/getbygroupvideoid',{
				user_id: user_id,
				group_video_id: group_video_id
			})
			.done(function(data){
				$('#save-tag-group').hide();
				var select = $("#my-select");
				if(data.length > 0){
					//todo: show current tag group
					data = data[0]

					covaa_tag_array = JSON.parse(data['covaa_tag_array']);
					setupCovaaTagSelect(covaa_tag_array);
					$("#covaa-dialog").modal('show');

				}else{
					//show default tag group bind to this user
					$.get('/api/annonation/get',{
						user_id: user_id,
					})
					.done(function(data){
						if(!data){
							alert('Please set your tag group in tag management page!');
						}else{
							covaa_tag_array = JSON.parse(data['covaa_tag_array']);

							setupCovaaTagSelect();
							$("#covaa-dialog").modal('show');
						}
					});
				}
			});

		});
	});

	var title_modal = $("#edit-title-modal"); 
  title_modal.on("show.bs.modal", function(e) { 
    var button = $(e.relatedTarget); 
    modal_video_id = button.data('id'); 
    var title = button.attr('title'); 
    modal_course_id = course_id; 
    modal_group_id = group_id; 
    $("#title-modal-course-name").text(course_name); 
    var group_video_id = button.data('gvid'); 
    $("#save-title").attr('data-gvid', group_video_id); 
 
    //--populate thumbnail and title for video-- 
    $.ajax({
    	type: "POST",
    	url: "/get_video_info",
    	data: {
    		video_id: modal_video_id
    	},
    	success: function (data) {
    		$("#title-modal-thumbnail-img").attr("src", data.thumbnail_url);
    		$("#title-modal-video-title").text(data.title);
    	},
    	error: function (req, status, error) {
    		console.log("error /get_video_info - " + error); ///// 
    	}
    });

    $("#edit-custom-title").val(button.attr('data-val'));
    });
    title_modal.on("click", "#save-title", function (e) {
    	e.preventDefault();
    	var group_video_id = $(this).attr('data-gvid');
    	var custom_title = $("#edit-custom-title").val();
    	$.ajax({
    		type: "POST",
    		url: "/update_gv_custom_title",
    		data: {
    			group_video_id: group_video_id,
    			title: custom_title
    		},
    		success: function (data) {
    			var button = $(".gv-title-button[data-gvid='" + group_video_id + "']");
				button.find('span').text(unescapeHtml(data.title)); 
				button.attr('data-val', unescapeHtml(data.title));
    			title_modal.modal('hide');
    		},
    		error: function (req, status, error) {
    			console.log("error save title - " + status + ":" + error); ///// 
    		}
    	});
		});
		
		var segments_form = $("#modal-segments-form");
		function addFieldsetForNewSegmentToModal() {
			var row = $("<fieldset>", {'class':'row form-group mb-2 no-border new-segment'}).appendTo("#m-segments-inputs");
			$("<div>", {
				'class':'col-12 col-md-4 mb-1',
				'html':'<legend>Title</legend><input class="form-control m-segment-title" type="text" placeholder="Title">'
			}).appendTo(row);
			$("<div>", {
				'class':'col-5 col-md-3',
				'html':'<legend>Start Time</legend><input class="form-control m-segment-start" type="text" placeholder="0:00" data-start="start" required data-required-error="Please fill in start time.">'
			}).appendTo(row);
			$("<div>", {
				'class':'col-5 col-md-3',
				'html':'<legend>End Time</legend><input class="form-control m-segment-end" type="text" placeholder="1:10:00" data-end="end" required data-required-error="Please fill in end time.">'
			}).appendTo(row);
			$("<div>", {
				'class':'col-2 mt-4',
				'html':'<button type="button" class="m-delete-segment outline-button btn-sm full-width" title="delete"><i class="fa fa-minus-circle"></i></button>'
			}).appendTo(row);
			$("<div>", {
				'class':'col-12 help-block with-errors'
			}).appendTo(row);
		}//end addFieldsetForNewSegmentToModal()

		segments_form.on("show.bs.modal", function(e) {
			var button = $(e.relatedTarget);
			modal_video_id = button.data('vid');
			var gvid = button.data('gvid');
			$("#save-segments").attr('data-gvid', gvid);
			var segment_id = button.data('gvsid');
			modal_course_id = course_id;
			modal_group_id = group_id;
			$("#segments-form-course-name").text(course_name);
			$("#segments-form-group-name").text(group_name);

			//--populate thumbnail and title for video--
			$.ajax({
				type: "POST",
				url: "/get_video_info",
				data: {video_id: modal_video_id},
				success: function (data) {
					$("#segments-form-thumbnail-img").attr("src", data.thumbnail_url);
					$("#segments-form-video-title").text(data.title);
				},
				error: function (req, status, error) {
					console.log("error /get_video_info - "+error);/////
				}
			});

			$.ajax({
				type:"POST",
				url: "/get_segments_for_group_video",
				data: {gvid: gvid},
				success: function(data) {
					$("#m-segments-inputs fieldset").remove();
					if(data.length > 0) {
						$.each(data, function(i, seg) {
							var row = $("<fieldset>", {
								'class':'row form-group mb-2 no-border',
								'data-gvsid':seg.id 
							}).appendTo("#m-segments-inputs");
							$("<div>", {
								'class':'col-12 col-md-4 mb-1',
								'html':'<legend>Title</legend><input class="form-control m-segment-title" type="text" placeholder="Title" value="'+seg.title+'">'
							}).appendTo(row);
							$("<div>", {
								'class':'col-5 col-md-3',
								'html':'<legend>Start Time</legend><input class="form-control m-segment-start" type="text" placeholder="0:00" data-start="start" required data-required-error="Please fill in start time." value="'+seg.start_time+'">'
							}).appendTo(row);
							$("<div>", {
								'class':'col-5 col-md-3',
								'html':'<legend>End Time</legend><input class="form-control m-segment-end" type="text" placeholder="1:10:00" data-end="end" required data-required-error="Please fill in end time." value="'+seg.end_time+'">'
							}).appendTo(row);
							$("<div>", {
								'class':'col-2 mt-4',
								'html':'<button type="button" class="m-delete-segment outline-button btn-sm full-width" title="delete"><i class="fa fa-minus-circle"></i></button>'
							}).appendTo(row);
							$("<div>", {
								'class':'col-12 help-block with-errors'
							}).appendTo(row);
						});
					}
					else {
						addFieldsetForNewSegmentToModal();
					}
					$("#m-segments-form").validator('update');
				},
				error: function (req, status, error) {
					console.log("error /get_segments_for_group_video - "+error);/////
				}
			});
		});//end segment_form bs.modal.show
		
		segments_form.on("click", '.m-delete-segment', function() {
			$(this).parent().parent().remove();
		});//end click m-delete-segment

		segments_form.on("click", "#m-another-segment", function() {
			addFieldsetForNewSegmentToModal();
			$("#m-segments-inputs fieldset").last().find('input:text:first').focus();
		});

		segments_form.on("click", "#save-segments", function() {
			$("#m-segments-form").validator('validate');
			if($("#m-segments-form").find('.has-error').length > 0) {
				return false;
			}
			var group_video_id = $(this).attr('data-gvid');
			var segs = [];
			$.each($("#m-segments-inputs fieldset"), function() {
				var s_id = ($(this).hasClass("new-segment")) ? null :  $(this).data('gvsid');
				var s_title = $(this).find(".m-segment-title").val();
				var s_start = $(this).find(".m-segment-start").val();
				var s_end = $(this).find(".m-segment-end").val();
				segs.push({"id":s_id, "title":s_title, "start":calculateVideoTimeToSeconds(s_start), "end":calculateVideoTimeToSeconds(s_end)});
			});
			$.ajax({
				type: "POST",
				url: "/save_segments",
				data: {
					group_video_id: group_video_id,
					segments: segs
				},
				success: function(data) {
					alert("Segments are saved");
					segments_form.modal("hide");
				},
				error: function(req, status, err) {
					console.log("error /save_segments - "+err);/////
				}
			})
		});//end click #save-segments

		segments_form.on('hidden.bs.modal', function() {
			modal_course_id= null;
			modal_group_id=null;
			modal_video_id = null;
		});

});//doc ready



