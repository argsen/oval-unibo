function save_status(received_notification_id, is_completed, success_function) {
    $.ajax({
        type: "POST",
        url: "/save_notification_status",
        data: {
            received_notification_id: received_notification_id,
            status: is_completed,
        },
        success: function (data) {
            success_function();
        },
        error: function (req, status, error) {
            console.log("error /save_notification_status - " + error); ///// 
        }
    });
}

$('document').ready(function () {

    $("#covaa-notification-show").on("click", function(){
		$(".canvas").toggleClass('canvas-off');
	})

    $(".navmenu").on("click", ".status-button", function () {
        if ($(this).hasClass("task-not-done")) {
            var button = $(this);
            var func = function () {
                button.addClass("task-done");
                button.removeClass("task-not-done");
                button.html("done&nbsp;<i class='fa fa-check-square-o'></i>")
            };
            save_status(
                $(this).data('rnid'),
                true,
                func
            );
        } else if ($(this).hasClass("task-done")) {
            var button = $(this);
            var func = function () {
                button.addClass("task-not-done");
                button.removeClass("task-done");
                button.html("complete&nbsp;<i class='fa fa-square-o'></i>")
            };
            save_status(
                $(this).data('rnid'),
                false,
                func
            );
        }
    });
    $(".navmenu").on("click", ".read-button", function () {
        var button = $(this);
        var func = function () {
            button.parent().remove();
        };
        save_status(
            $(this).data('rnid'),
            true,
            func
        );
    });
    $(".navmenu").on("click", "#refresh-notification-button", function () {
        $("div#notifications-container").fadeOut();
        // $("div.navmenu").load("/refresh_notifications", function() { 
        //     $("div.navmenu").fadeIn(); 
        // }); 
        $.ajax({
            type: "POST",
            url: "/refresh_notifications",
            success: function (data) {
                $("div#notifications-container").html(data.notifications);
                $("div#notifications-container").fadeIn();
                $("#menu-button > .icon-wrap").html(data.icon);
            },
            error: function (req, status, err) {
                console.log("error /refresh_notifications - " + err); ///// 
                $("div#notifications-container").fadeIn();
            }
        });
    });

    $(".navmenu").on("shown.bs.collapse", ".collapse", function() { 
        $(".navmenu").niceScroll().resize(); 
    }); 
    $(".navmenu").on("hidden.bs.collapse", ".collapse", function() { 
        $(".navmenu").niceScroll().resize(); 
    });


}); //end document ready