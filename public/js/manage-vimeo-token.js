
$('document').ready(function(){

    $(".delete-vimeo-cred-button").on("click", function() {
        var cred_id = $(this).data('id');
        $.ajax({
            type: 'POST',
            url: '/delete_vimeo_cred',
            data: {cred_id: cred_id},
            success: function(data) {
                location.reload();
            },
            error: function(req, status, err) {
                console.log("error /delete_vimeo_cred - "+err);/////
            }
        });
    });


})//document ready