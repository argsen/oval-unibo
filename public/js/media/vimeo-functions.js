
var player;
var pauseAt = null;

function onVimeoIframeAPIReady() {
    var options = {
        id: video_identifier, //vidoe_identifier should be either the id or the url of the video.
        width: 640,
        loop: false,
        title: false,
        transparent: false
    };

    player = new Vimeo.Player('player', options);

    load_quiz(group_video_id);
}

function pauseVideo() {
    player.pause().then(function () {
        // the video was paused
    }).catch(function (error) {
        switch (error.name) {
            case 'PasswordError':
                // the video is password-protected and the viewer needs to enter the
                // password first
                break;

            case 'PrivacyError':
                // the video is private
                break;

            default:
                // some other error occurred
                break;
        }
    });
}

function playVideo() {
    player.play().then(function () {
        // the video was played
    }).catch(function (error) {
        switch (error.name) {
            case 'PasswordError':
                // the video is password-protected and the viewer needs to enter the
                // password first
                break;

            case 'PrivacyError':
                // the video is private
                break;

            default:
                // some other error occurred
                break;
        }
    });
}

function goTo(time) {
    player.setCurrentTime(time).then(function (seconds) {
        // seconds = the actual time that the player seeked to
    }).catch(function (error) {
        switch (error.name) {
            case 'RangeError':
                // the time was less than 0 or greater than the video’s duration
                break;

            default:
                // some other error occurred
                break;
        }
    });
}

function currentVideoTime(cb) {
    player.getCurrentTime().then(function (seconds) {
        // seconds = the current playback position
        return cb(Math.floor(seconds));
    }).catch(function (error) {
        // an error occurred
        return cb(null);
    });
}

function onTime() {
    $.each(current_keywords, function (i, val) {
        
        currentVideoTime(function(seconds){
            if(seconds > i){
                $("#current-keywords").text(val);
            }
        })

    });
    currentVideoTime(function(sec) {
        if(pauseAt && sec > pauseAt) {
            pauseVideo();
            pauseAt = null;
        }
    })
}

function onPlayerReady() {

    player.ready().then(function () {
        // the player is ready
        pauseVideo();
        window.setInterval(onTime, 1000);

    });
}

function onPlayerStateChange() {

    player.on('play', function (data) {
        // data is an object containing properties specific to that event
        var videoTime = data.seconds.toFixed(1);
        var event_time = Date.now();
        var action = "Play";
        saveTracking({
            event: action,
            target: 'vimeo-tracking',
            info: videoTime,
            event_time: event_time
        });
    });

    player.on('pause', function (data) {
        // data is an object containing properties specific to that event
        var videoTime = data.seconds.toFixed(1);
        var event_time = Date.now();
        var action = "Paused";
        saveTracking({
            event: action,
            target: 'vimeo-tracking',
            info: videoTime,
            event_time: event_time
        });
    });

    player.on('ended', function (data) {
        // data is an object containing properties specific to that event
        var videoTime = data.seconds.toFixed(1);
        var event_time = Date.now();
        var action = "Ended";
        saveTracking({
            event: action,
            target: 'vimeo-tracking',
            info: videoTime,
            event_time: event_time
        });
        $("#current-keywords").html("&nbsp;");
    });

    player.on('bufferstart', function (data) {
        // data is an object containing properties specific to that event
        var videoTime = data.seconds.toFixed(1);
        var event_time = Date.now();
        var action = "Buffering";
        saveTracking({
            event: action,
            target: 'vimeo-tracking',
            info: videoTime,
            event_time: event_time
        });
    });

    player.on('loaded', function () {
        var frame = document.querySelector('iframe');
        if (frame !== null) {
            frame.style.width = '100%'
        }
    });

}

onVimeoIframeAPIReady();
onPlayerReady();
onPlayerStateChange();

function playSegment(start, end) {
	pauseAt = end;	
	goTo(start);
	playVideo();
}

/*------ quiz client plugin for Vimeo ------*/
var quiz_meta = [];
var is_visable = true;
var flag = false; //true for modal fired, false for not fired

function load_quiz(group_video_id) {
    $.ajax({
        type: "GET",
        url: "/get_quiz",
        data: {
            group_video_id: group_video_id
        },
        success: function (res) {

            if (res.quiz != null) {
                quiz_meta = JSON.parse(res.quiz.quiz_data);

                is_visable = res.quiz.visable;

            }

        },
        error: function (request, status, error) {
            alert("error get quiz - " + error);
        }
    });
}

function show_quiz(data) {
    pauseVideo();
    show_quiz_modal(data)
}

function show_quiz_modal(data, cb) {

    $("#quiz_modal").on("show.bs.modal", function () {
        pauseVideo();
    });
    $("#feedback_dialog").on("hidden.bs.modal", function () {
        playVideo();
    });


    $("#quiz_modal").off("hidden.bs.modal");
    $(".client_question_list_wrap").empty();

    for (var i = 0; i < data.items.length; i++) {
        switch (data.items[i].type) {
            case "text":
                var $li = $("<li></li>");
                var div = "<h3> <i class='fa fa-question-circle-o' aria-hidden='true'></i> Question " + (parseInt(i) + 1) + " </h3>" +
                    "<h4><strong>" + data.items[i].title + "</strong></h4>" +
                    "<textarea name='' id='quiz_q" + (parseInt(i) + 1) + "' cols='20' rows='3' placeholder='Please input your answer'></textarea>"
                $li.append(div);
                $(".client_question_list_wrap").append($li);
                break;

            case "multiple_choice":
                var $li = $("<li></li>");
                var div = "<h3> <i class='fa fa-question-circle-o' aria-hidden='true'></i> Question " + (parseInt(i) + 1) + " </h3>" +
                    "<h4><strong>" + data.items[i].title + "</strong></h4>";

                var choice_list = $("<ul class='list'></ul>");
                for (var j = 0; j < data.items[i].list.length; j++) {
                    var choice = "<li class='list__item'>" +
                        "<input type='radio' class='radio-btn' name='choise_" + (parseInt(i) + 1) + "' id='choise_" + (parseInt(i) + 1) + "_" + (parseInt(j) + 1) + "'  />" +
                        "<label for='choise_" + (parseInt(i) + 1) + "_" + (parseInt(j) + 1) + "' class='label'>" + data.items[i].list[j] + "</label>" +
                        "</li>";
                    choice_list.append(choice);
                }

                $li.append(div);
                $li.append(choice_list);

                $(".client_question_list_wrap").append($li);

                break;
            default:
                break;
        }
    }

    $("#quiz_modal").modal({
        backdrop: 'static',
        keyboard: false
    })

    /*------ submit result ------*/
    $("#submit_result").off();
    $("#submit_result").on("click", function () {
        /*------ collect result data ------*/

        for (var i = 0; i < data.items.length; i++) {
            switch (data.items[i].type) {
                case "text":
                    var user_ans = $("#quiz_q" + (parseInt(i) + 1)).val();
                    data.items[i]["user_ans"] = encodeText(user_ans);

                    break;
                case "multiple_choice":
                    var user_ans = $(".client_question_list_wrap input[name='" + "choise_" + (parseInt(i) + 1) + "']:checked").siblings("label").text();
                    data.items[i]["user_ans"] = encodeText(user_ans);

                    break;
                default:
                    break;
            }
        }

        /*------ send data to server ------*/
        var user_id = $(".greetings").attr('userid');

        $.ajax({
            type: "POST",
            url: "/submit_quiz_result",
            data: {
                user_id: user_id, //int
                group_video_id: group_video_id,
                quiz_data: data //obj
            },
            success: function (res) {
                if (res.result === true) {

                    var feedback_arr = show_feedback_hint(data);

                    if (feedback_arr.length > 0) {
                        $("#quiz_modal").modal('hide');
                        show_feedback_modal(feedback_arr);
                    } else {

                        setTimeout(function () {
                            $("#quiz_modal").modal("hide");
                            playVideo();
                        }, 1000);

                    }

                }
            },
            error: function (request, status, error) {
                alert("error submit quiz result - " + error);
            }
        });


    });

}

function alert_modal_client(message) {
    $("#alert_dialog_content").empty();

    var content = "<h3>" + message + "</h3>";
    $("#alert_dialog_content").append(content);

    $("#alert_dialog").modal({
        backdrop: 'static',
        keyboard: false
    });

}

function show_feedback_hint(quiz_data) {

    var return_obj = [];

    for (var i = 0; i < quiz_data.items.length; i++) {
        var user_ans = quiz_data.items[i].user_ans;
        var title = quiz_data.items[i].title;
        var feedback_position = 0;

        if (quiz_data.items[i].type === "multiple_choice") {

            /*------ compare feedback ------*/
            for (var j = 0; j < quiz_data.items[i].list.length; j++) {
                if (user_ans === quiz_data.items[i].list[j]) {
                    feedback_position = j;
                }
            }

            /*------ structure return obj ------*/
            var temp = {
                "type": "Multiple Choice",
                "question": "",
                "answer": "",
                "is_correct": false,
                "feedback": ""
            };

            temp.question = title;
            temp.answer = user_ans;
            temp.feedback = quiz_data.items[i].feedback[feedback_position];

            if (quiz_data.items[i].ans[0] === quiz_data.items[i].user_ans) {
                temp.is_correct = true;
            } else {
                temp.is_correct = false;
            }

            return_obj.push(temp);

        } else {

            /*------ type is short answer ------*/
            var temp = {
                "type": "Short Answer",
                "question": "",
                "answer": "",
                "is_correct": "",
                "feedback": ""
            };

            temp.question = title;
            temp.answer = user_ans;
            temp.feedback = quiz_data.items[i].ans;

            return_obj.push(temp);
        }
    }

    return return_obj;

}

function show_feedback_modal(feedback_array) {

    $("#feedback_dialog_content_table_head").nextAll('tr').remove();

    for (var i = 0; i < feedback_array.length; i++) {

        var $tr = $("<tr></tr>");

        for (var item in feedback_array[i]) {

            if (item === "is_correct") {

                if (typeof (feedback_array[i][item]) === "boolean") {
                    switch (feedback_array[i][item]) {
                        case true:
                            var th = "<td style='text-align:center;'>" + "<img src='../../img/tick.png' alt='' style='width:32px; height:auto;'>" + "</td>";
                            break;
                        case false:
                            var th = "<td style='text-align:center;'>" + "<img src='../../img/cancel.png' alt='' style='width:32px; height:auto;'>" + "</td>";
                            break;
                        default:
                            break;
                    }

                } else {
                    var th = "<td>" + "please check example answer" + "</td>";
                }

                $tr.append(th);

            } else {
                var th = "<td>" + feedback_array[i][item] + "</td>";
                $tr.append(th);
            }

        }

        $("#feedback_dialog_content table tbody").append($tr);

    }

    $("#feedback_dialog").modal({
        backdrop: 'static',
        keyboard: false
    });
}

function encodeText(txt) {
    return txt.replace(/\&/g, '&amp;').replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace(/\"/g, '&quot;').replace(/\'/g, '&apos;');
}

var intervalID_youtubeplayer = window.setInterval(
    function () {

        currentVideoTime(function(seconds){

            if (quiz_meta.length > 0 && is_visable) {

                var current_video_time = parseInt(seconds);
                var trigger = true;
                var meta_position = 0;
    
                for (var i = 0; i < quiz_meta.length; i++) {
                    if (trigger && current_video_time <= parseInt(quiz_meta[i].stop)) {
                        meta_position = i;
                        trigger = false
                    }
                }
    
                if (meta_position > 0) {
                    var quiz_stop = parseInt(quiz_meta[meta_position].stop);
                } else {
                    var quiz_stop = parseInt(quiz_meta[0].stop);
                }
    
                if (current_video_time === quiz_stop) {
                    setTimeout(function () {
                        show_quiz(quiz_meta[meta_position]);
                    }, 1000);
                }
    
            }

        })



    },
    1000
)

/*------ end quiz client plugin for Viemo ------*/
