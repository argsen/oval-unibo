<?php 
 
namespace oval; 
 
use Illuminate\Database\Eloquent\Model; 
 
/**
 * Model class for table 'received_notifications'
 */
class ReceivedNotification extends Model 
{ 
    protected $table = 'received_notifications'; 
    public $timestamps = false; 
    protected $fillable = ['recipient_id', 'notification_id']; 
 
    /** 
     * One-to-Many relationship (inverse) 
     *  
     * Returns the User this UserNotification is for. 
     * @return User User object 
     */ 
    public function recipient() { 
        return $this->belongsTo('oval\User', 'user_id'); 
    } 
 
    /** 
     * One-to-Many relationship (inverse) 
     *  
     * Returns the Notification 
     * @return Notification Notification object 
     */ 
    public function notification() { 
        return $this->belongsTo('oval\Notification'); 
    } 
} 