<?php

namespace oval;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table 'adf_courses'
 */
class AdfCourse extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'adf_courses';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid', 'code', 'subject', 'level', 'course_id'
    ];
}
