<?php

namespace oval\Providers;

use Illuminate\Support\ServiceProvider;
use oval\Classes\UniboLtiHelper;

class LtiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('oval\Classes\LtiHelperInterface', function($app) {
            return new UniboLtiHelper;
        });
    }
}
