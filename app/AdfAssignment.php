<?php

namespace oval;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table 'adf_assignments'
 */
class AdfAssignment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'adf_assignments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid', 'title', 'type', 'start', 'end'
    ];
}
