<?php

namespace oval;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table 'recommended_video_likes'
 */
class RecommendedVideoLike extends Model
{
    protected $table = 'recommended_video_likes';

    /**
     * One-to-Many relationship (inverce)
     * @return GroupVideo GroupVideo object this vote is related to
     */
    public function group_video() {
        return $this->belongsTo('oval\GroupVideo');
    }

    /**
     * One-to-Many relationship (inverse)
     * @return User User object that "liked" recommended video
     */
    public function user() {
        return $this->belongsTo('oval\User');
    }

    /**
     * One-to-Many relationship (inverse)
     * @return Video Video object that was "liked"
     */
    public function liked_video() {
        return $this->belongsTo('oval\Video');
    }


}//end class
