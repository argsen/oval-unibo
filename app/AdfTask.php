<?php

namespace oval;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table 'adf_tasks'
 */
class AdfTask extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'adf_tasks';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid', 'title', 'subject', 'start', 'end', 'status'
    ];
}
