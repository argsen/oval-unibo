<?php

namespace oval;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for 'annotation_posts'
 */
class AnnotationPost extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'annotation_posts';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    // protected $dateFormat = 'U';

    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'modified';

    /** 
     * The attributes that are mass assignable.
     * 
     * @var array 
     */
    protected $fillable = [
        'group_video_id',
        'annotation_id',
        'parent',
        'content',
        'pings',
        'creator',
        'fullname',
        'profile_picture-url',
        'created_by_admin',
        'created_by_current_user',
        'upvote_count',
        'user_has_upvoted',
        'covaaTags'];

}
