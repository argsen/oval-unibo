<?php

namespace oval;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table 'keywords'
 * 
 * The table is populated based on values in 'transcripts' table's 'analysis' column 
 * and the time values in json value of 'transcripts' table's 'transcript' column.
 */
class Keyword extends Model
{
    protected $table = "keywords";
    public $timestamps = false;

    /**
    *   One-to-Many relationship (Inverse)
    *   @return GroupVideo object 
    **/
    public function group_video() {
        return $this->belongsTo('oval\GroupVideo');
    }

    /**
     * Method to get Keyword objects that has same word for different video.
     * 
     * It returns array of keywords with video url, video url for when the keyword appears, and video time.
     * 
     * @return array Array of array with keys: title, url, time_url, time
     */
    public function related() {
        $video = $this->group_video->video();
        $skip = $video->allGroupVideos()->pluck('id')->all();
        $related = Keyword::where([
                        ['keyword', '=', $this->keyword],
                        ['type', '=', $this->type]
                    ])
                    ->whereNotIn('group_video_id', $skip)
                    ->get();
        $video_ids = [];//for checking for dupes
        $list = [];
        if(!empty($related)) {
            foreach ($related as $r) {
                $video = GroupVideo::find($r->group_video_id)->video();
                if(!in_array($video->id, $video_ids)) {
                    $video_ids[] = $video->id;
                    $list[] = [
                        'word'=>$r->keyword,
                        'type'=>$r->type,
                        'video_id'=>$video->id,
                        'title'=>$video->title,
                        'url'=>$video->video_url(),
                        'rating'=>$this->group_video->ratingForLikedVideo($video->id)
                    ];
                }
            }
        }
        usort($list, function($a, $b) {
			return ($a['rating'] < $b['rating']);
		});
        return $list;
    }

    public function relatedWithUserLike(int $user_id) {
        $list = $this->related();
        $group_video_id = $this->group_video->id;
        $retval = [];
        foreach ($list as $item) {
            $like = RecommendedVideoLike::where([
                ['group_video_id', '=', $group_video_id],
                ['user_id', '=', $user_id],
                ['liked_video_id', '=', $item['video_id']]
            ])->first();
            $item['liked'] = !empty($like);
            $retval[] = $item;
        }
        return $retval;
    }


    /**
     * Method to get time values when the keyword appears in video.
     * @return array Array
     */
    public function occurrences() {
        $occurrences = Keyword::where([
            ['keyword', '=', $this->keyword],
            ['type', '=', $this->type],
            ['group_video_id', '=', $this->group_video_id],
        ])->get();
        $list = null;
        if(!empty($occurrences)) {
            foreach ($occurrences as $o) {
                $list[] = intval(floor($o->startTime));
            }
        }
        return $list;
    }
}
