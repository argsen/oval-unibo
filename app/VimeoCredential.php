<?php

namespace oval;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table 'vimeo_credentials'
 */
class VimeoCredential extends Model
{
    protected $table = "vimeo_credentials";

    protected $fillable = ['vimeo_user_uri', 'access_token', 'user_id'];

    /**
     * One-to-Many relationship (inverse)
     * @return User User that owns this cred
     */
    public function owner () {
        return $this->belongsTo('oval\User', 'user_id');
    }
}
