<?php 
 
namespace oval; 
 
use Illuminate\Database\Eloquent\Model; 
 
/**
 * Model class for table 'notifications'
 */
class Notification extends Model 
{ 
 
 
    protected $table = 'notifications'; 
 
    public static function boot() { 
        self::saving(function($model){ 
            //when updating 'message', reset ReceivedNotification's 'completed' to false and 'completed_date' to null 
            if($model->isDirty()) { 
                foreach ($model->received_notifications as $rn) { 
                    $rn->completed = false; 
                    $rn->completed_at = null; 
                    $rn->save(); 
                } 
            } 
        }); 
    } 
 
    /** 
     * One-to-Many relationship (inverse) 
     *  
     * User that wrote this notification 
     * @return User User object 
     */ 
    public function author() { 
        return $this->belongsTo('oval\User', 'author_id'); 
    } 
 
    /** 
     * One-to-Many relationship 
     *  
     * Returns all ReceivedNotification objects that references this Notification. 
     * @return collection Collection of Notification objects. 
     */ 
    public function received_notifications () { 
        return $this->hasMany('oval\ReceivedNotification'); 
    } 
 
    /** 
     * One-to-Many relationship (inverse) 
     *  
     * Returns the group this Notification is sent to 
     * @return Group Group object 
     */ 
    public function recipient_group() { 
        return $this->belongsTo('oval\Group', 'recipient_group_id'); 
    } 
 
    /** 
     * Returns users that this notification is sent to. 
     * @return collection Collection of User objects 
     */ 
    public function recipients() { 
        $recipients = collect(); 
        $rec = $this->received_notifications; 
        foreach($rec as $r) { 
            $u = User::find($r->recipient_id); 
            $recipients->push($u); 
        } 
        return $recipients; 
    } 
 
}