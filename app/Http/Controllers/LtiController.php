<?php

namespace oval\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use IMSGlobal\LTI\ToolProvider;
use IMSGlobal\LTI\ToolProvider\DataConnector;

use oval\User;
use oval\Course;
use oval\GroupVideo;
use oval\Classes\LtiHelperInterface;


const LTI_PASSWORD = 'xFSD7BykF5R_mg5PZ+tY,]NG';


/**
 * Class inheriting ToolProvider from IMSGlobal's LIT library.
 * 
 * This method runs as part of $tool->handleRequest() in LtiController class.
 * Use LtiHelper class specific to installation to sync/import data.
 * Note: to change binding of LtiHelperInterface, create class implimenting it 
 * and set the binding in app\Providers\LtiServiceProvider.
 * 
 * @uses IMSGlobal\LTI\ToolProvider
 * @see https://github.com/IMSGlobal/LTI-Tool-Provider-Library-PHP/wiki/Usage
 */
class OvalLtiProvider extends ToolProvider\ToolProvider {

  function onLaunch() {
    // Insert code here to handle incoming connections - use the user,
    // context and resourceLink properties of the class instance
    // to access the current user, context and resource link.
    $this->user->save();

    //--find user with email address
    //--leave it as is if already exists except for role
    $user = User::where('email', '=', $this->user->email)->first();
    if (empty($user)) {
      $user = new User;
      $user->email = $this->user->email;
      $user->first_name = $this->user->firstname;
      $user->last_name = $this->user->lastname;
      $user->password = bcrypt(LTI_PASSWORD);
      $user->role = $this->getOvalUserRole();
      $user->save();
    }
    else {//--if user exists, and not OVAL admin but supposed to be, update it
      if ($this->getOvalUserRole() =='A') {
        $user->role = $this->getOvalUserRole();
        $user->save();
      }
    }
    Auth::login($user);

    $consumer_id = $this->consumer->getRecordId();
    $moodle_user_id = $this->user->getId();

    //--instantiate installation specific LTI helper class
    //--and let it sync data with Moodle
    $lti_helper = resolve('oval\Classes\LtiHelperInterface');
    $lti_helper->handleEnrollmentData($consumer_id, $user, $this->isInstructor(), $moodle_user_id);


  }

  /**
   * Method to return the value for oval.user table's role column.
   * 
   * @return string 'A' if admin, 'O' if not
   */
  function getOvalUserRole() {
		if ($this->user->isAdmin()) return 'A';
		return 'O';
	}

  /**
   * Method to check if the user is an instructor' in OVAL
   * 
   * @return boolean true if Moodle role is admin or staff, false for others. 
   */
  function isInstructor() {
		if ($this->user->isAdmin()) return true;
  	if ($this->user->isStaff()) return true;
		return false;
	}
}

/**
 * This class handles LTI connection.
 * @author Ken
 */
class LtiController extends Controller
{

    public function __construct() {

    }

    /**
     * Method called from route /lti - the route used when user clicks on LTI link on Moodle
     * 
     * This method uses LTI library to check authentication,
     * saves info coming from LTI request,
     * then redirects instructor to /select-video page,
     * student to /view (or /course/{course_id}).
     * 
     * @see https://github.com/IMSGlobal/LTI-Tool-Provider-Library-PHP/wiki/Usage
     * @uses IMSGlobal\LTI\ToolProvider\ToolConsumer
     * @uses OvalLtiProvider::handleRequest()
     * 
     * @param Request $req
     * @return Illuminate\Http\RedirectResponse
     * 
     */
    public function launch(Request $req) {
      global $_POST;
      $_POST = $req->all();

      try {
        $db_config = DB::getConfig();
        $conn_str = $db_config['driver'] . ':host=' . $db_config['host'] . ';port=' . $db_config['port'] . ';dbname=' . $db_config['database'];
        $pdo = new \PDO($conn_str, $db_config['username'], $db_config['password']);
      } catch (PDOException $e) {
        return 'Connection failed: ' . $e->getMessage();
      }
      $db_connector = DataConnector\DataConnector::getDataConnector('', $pdo);

      $tool = new OvalLtiProvider($db_connector);
      $tool->setParameterConstraint('oauth_consumer_key', TRUE, 50, array('basic-lti-launch-request', 'ContentItemSelectionRequest', 'DashboardRequest'));
      $tool->setParameterConstraint('resource_link_id', TRUE, 50, array('basic-lti-launch-request'));
      $tool->setParameterConstraint('user_id', TRUE, 50, array('basic-lti-launch-request'));
      $tool->setParameterConstraint('roles', TRUE, NULL, array('basic-lti-launch-request'));
      $tool->handleRequest();

      // close PDO connection
      $pdo = null;

      $lti_user = Auth::user();

      $link_id = $req->resource_link_id;
      $group_video = GroupVideo::where([
                        ['moodle_resource_id', '=', $link_id],
                        ['status', '=', 'current']
                    ])
                    ->whereNotNull('moodle_resource_id')
                    ->first();
      $course = Course::where([
                  ['consumer_id', '=', $tool->consumer->getRecordId()],
                  ['moodle_course_id', '=', intval($req->context_id)]
                ])
                ->first();

      if(empty($course)) {
        return view('pages.message-page', ['title'=>'ERROR', 'message'=>'Oops, something is wrong. Please try again later.']);
      }
      if($lti_user->isInstructorOf($course)){
        //--if instructor, redirect to select video page 
        return redirect()->secure('/select-video/'.$link_id.'/'.$course->id.(!empty($group_video) ? '/'.$group_video->id : ""));
      }
      elseif(!empty($group_video)) {
        //--if student and lti-resource-id is linked to group_video, redirect to it
        return redirect()->secure('/view/'.$group_video->id);
      }
      else {
        //--else redirect to course if student & no group_video associated to link
        return redirect()->secure('/course/'.$course->id);
      }
      
    }
}
