<?php
namespace oval\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use oval;
// namespace App\Http\Controllers;
//
// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
// use App\Http\Controllers\Controller;


class StudentdashController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
	}
  public function index()
  {
  		$user = Auth::user();

    	$current_logged_in_user_id = $user->id;

		$ownPreData = $this->getOwnPreData($current_logged_in_user_id);
		$ownPreData = $this->string2KeyedArray($ownPreData);

		$ownPostData = $this->getOwnPostData($current_logged_in_user_id);
		$ownPostData = $this->string2KeyedArray($ownPostData);

		$allPreData = $this->getAllPreData();

		$allPostData = $this->getAllPostData();

		$overall_data = $this->getParticipationData($current_logged_in_user_id);

		if ($user->role == 'A'){
			$is_admin = true;
		}else{
			$is_admin = false;
		}

  	return view('pages.student-dash', compact('ownPreData', 'ownPostData', 'allPreData', 'allPostData', 'overall_data', 'is_admin', '$current_logged_in_user_id'));
  }

  public function getParticipationData($current_logged_in_user_id)
  {
    //Init final array to be returned
    $overall_data = array();

    //Query all course ID and Name that user is enrolled in
    $coursesList = DB::table('enrollments')
                    ->join('courses', 'enrollments.course_id', '=', 'courses.id')
                    ->select('courses.id', 'courses.name')
                    ->where('enrollments.user_id', '=', $current_logged_in_user_id)
                    ->get();

    //Loop through result and init array for each course in $overall_data containing id, name and groups
    foreach($coursesList as $course)
    {
      $overall_data[$course->id] = array("id"=>$course->id,	"name"=>$course->name, "groups"=>array());
    }

    //Query all groups ID and Name that each Course has
    $subjects_list = array();
		foreach($overall_data as $course)
		{
		   $subjects_list[] = $course["id"];
		}

    $groupsList = DB::table('groups')
                  ->join('group_members', 'group_members.group_id', '=', 'groups.id')
                  ->join('courses', 'courses.id', '=', 'groups.id')
                  ->select('groups.id', 'groups.name', 'groups.course_id')
                  ->where('group_members.user_id', '=', $current_logged_in_user_id)
                  ->whereIn('courses.id', $subjects_list)
                  ->get();

    foreach($groupsList as $group)
    {
			$overall_data[$group->course_id]["groups"][$group->id] = array(
																	"id"=>$group->id,
																	"name"=>$group->name,
																	"users"=>array(),
																	"videos"=>array(),
																	"social_links"=>array()
																);
    }

    //To populate all Groups with student objects
    foreach($overall_data as $course)
    {
      $course_id = $course["id"];

      foreach($course["groups"] as $group)//Group
			{
				$group_id = $group["id"];

				//Retrieve all videos from current Group
        $videosList = DB::table('videos')
                      ->join('group_videos', 'videos.id', '=', 'group_videos.video_id')
                      ->select('videos.id', 'videos.title', 'group_videos.custom_title')
                      ->where('group_videos.group_id', '=', $group_id)
                      ->get();

        foreach($videosList as $video)
				{
					$video_id = $video->id;
					$video_title = $video->title;
					$video_custom_title = $video->custom_title;
					if($video_custom_title == "") $video_custom_title = $video_title;

					$overall_data[$course_id]["groups"][$group_id]["videos"][$video_id] = array(
																						"id"=>$video_id,
																						"name"=>$video_custom_title,
																						"full_name"=>$video_title,
																						"social_links" => array()
																					);
				}//End of videos loop

				//Fetch all annotations from current group
				$annotationsList = DB::table('annotations')
                            ->join('group_videos', 'annotations.group_video_id', '=', 'group_videos.id')
                            ->where('group_videos.group_id', '=', $group_id)
                            ->select('annotations.user_id', 'group_videos.video_id')
                            ->get();

        //Fetch all replies to annotations from current group
        $annotationPostsList = DB::table('annotation_posts')
                                ->join('group_videos', 'annotation_posts.group_video_id', '=', 'group_videos.id')
                                ->where('group_videos.group_id', '=', $group_id)
                                ->select('annotation_posts.id', 'group_videos.video_id', 'annotation_posts.annotation_id', 'annotation_posts.parent', 'annotation_posts.creator')
                                ->get();

        //Fetch all comments and replies to annotations from current group
        $submissionsList = DB::table('submissions')
                        ->join('group_videos', 'submissions.video_id', '=', 'group_videos.video_id')
                        ->where('group_videos.group_id', '=', $group_id)
                        ->select('submissions.id', 'group_videos.video_id', 'submissions.annotation_id', 'submissions.parent_id', 'submissions.user_id', 'submissions.tag_labels')
                        ->get();


				//Grab all users from current class
        $usersList = DB::table('users')
                      ->join('group_members', 'users.id', '=', 'group_members.user_id')
                      ->where('group_members.group_id', '=', $group_id)
                      ->select('users.id', 'users.first_name', 'users.last_name', 'users.role')
                      ->get();

				//Setup array of users
        foreach($usersList as $user)//Looping through all students to populate dataset.
				{
					$user_id = $user->id;
					$user_name = $user->first_name . " " . $user->last_name;
					$role = $user->role;

					//Create the student data array used for Overall Group level (Regardless of videos)
					$overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id] = array(
																		"id"=>$user_id,
																		"name"=>$user_name,
																		"role"=>$role,
																		"annotations"=>0,
																		"comments"=>0,
																		"reply"=>0,
																		"ideate" =>0,
																		"justify" =>0,
																		"agree" => 0,
																		"disagree" => 0,
																		"inquire" => 0,
                                    "topics_started" => array(),
																		"submissions"=> array(),
																		"weight" => 0,
																		"faveShape" => "rectangle",
																		"nameColor" => "black"
																	);

					//Create the student data array used for Video level
					foreach($overall_data[$course_id]["groups"][$group_id]["videos"] as $video_array)
					{
						$video_id = $video_array["id"];

						$overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id] = array(
																		"id"=>$user_id,
																		"name"=>$user_name,
																		"role"=>$role,
																		"annotations"=>0,
																		"comments"=>0,
																		"reply"=>0,
																		"ideate" =>0,
																		"justify" =>0,
																		"agree" => 0,
																		"disagree" => 0,
																		"inquire" => 0,
                                    "topics_started" => array(),
																		"submissions"=> array(),
																		"weight" => 0,
																		"faveShape" => "rectangle",
																		"nameColor" => "black"
																	);
					}

					if($user_id == $current_logged_in_user_id)//If current looped user is current logged in user, change SNA nameColor to unique color
					{
						$overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["nameColor"] = "#37c4a1";
						$overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["nameColor"] = "#37c4a1";
					}

          foreach($annotationsList as $annotation)//Loop through all annotations (Starting point of each "discussion thread")
					{
						if($annotation->user_id == $user_id)//If current submission belongs to current looped user
						{
							array_push($overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["topics_started"], $annotation);//Add annotation entry to overall level topics_started list.

							$video_id = $annotation->video_id;
							array_push($overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["topics_started"], $annotation);//Add annotation entry to video level topics_started list.

							//Set the user SNA shape as ellipse because got at least 1 submission
							$overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["faveShape"] = "ellipse";
							$overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["faveShape"] = "ellipse";

              //Add 1 counter for annotation made
              $overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["annotations"]++;
              $overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["annotations"]++;

						}
					}//End of annotationsList loop

					foreach($submissionsList as $submission)//$submissionsList is the class level. Loop through all submissions
					{
						if($submission->user_id == $user_id)//If current submission belongs to current looped user
						{
							array_push($overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["submissions"], $submission);//Add submission entry to overall level submissions list.

							$video_id = $submission->video_id;
							array_push($overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["submissions"], $submission);//Add submission entry to video level submissions list.

							//Add 1 counter for each comment or reply to the weight of student. Used for SNA
							$overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["weight"]++;
							$overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["weight"]++;

							//Set the user SNA shape as ellipse because got at least 1 submission
							$overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["faveShape"] = "ellipse";
							$overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["faveShape"] = "ellipse";

							if($submission->parent_id == 0)//If its a parent comment
							{
								//Add 1 counter for comment made
								$overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["comments"]++;
								$overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["comments"]++;
							}
							else//theres parent_id, so this is a reply.
							{
								//Special condition that if this user has at least 1 reply, but 0 parent comments, they will have a different shape.
								if($overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["comments"] == 0)
									$overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["faveShape"] = "rhomboid";

								if($overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["comments"] == 0)
									$overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["faveShape"] = "rhomboid";

								//Add 1 counter for reply made
								$overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["reply"]++;
								$overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["reply"]++;

                //***WHEN EVERYTHING WORKS, CHANGE THE WAY THE TRAVERSING OF DATA IS DONE USING ARRAY_SEARCH RATHER THAN LOOPING THE WHOLE ARRAY MANUALLY.***
                // $key = array_search($submission["parent_id"], array_column($annotationPostsList, 'id'));
                // if($key != false)//Found parent_id
                // {
                //   $parent_submission = $annotationPostsList[$key];
                //
                //   &$social_links = $overall_data[$course_id]["groups"][$group_id]["videos"][$video_id]["social_links"];
                //
                //   $findasSource = array_search($submission->user_id, array_column($social_links, 'source'));
                //   $findasTarget = array_search($submission->user_id, array_column($social_links, 'target'));
                // }


								foreach($submissionsList as $parent_comment)//Find the parent comment for this reply
								{
									if($parent_comment->id == $submission->parent_id)//If parent comment found
									{
										//Populating VIDEO level Social links
										$found = false;

										//Loop through current video social links to see if existing link exist for these 2 users
										foreach($overall_data[$course_id]["groups"][$group_id]["videos"][$video_id]["social_links"] as &$video_sl)
										{
											if($video_sl["target"] == $parent_comment->user_id && $video_sl["source"] == $submission->user_id)//If found social link of the same direction, add one to strength.
											{
												$video_sl["strength"] += 1;
												$found = true;
												break;
											}
											else if($video_sl["target"] == $submission->user_id && $video_sl["source"] == $parent_comment->user_id)//If found social link of opposite direction, means there is 2 way connection, so update both strength and direction.
											{
												$video_sl["strength"] += 1;
												$video_sl["direction"] = 2;
												$found = true;
												break;
											}
										}

										if(!$found)//If SLA doesnt exist yet for this target user, create the SLA
										{
											array_push($overall_data[$course_id]["groups"][$group_id]["videos"][$video_id]["social_links"], array(
																"source" => $submission->user_id,
																"target" => $parent_comment->user_id,
																"strength" => 1,
																"direction" => "1",
																"faveColor" => "#848484"
															));
										}
										//End of Populating VIDEO level Social links

										//Populating CLASS (Overall) level Social links
										$found = false;

										//Loop through class overall social links to see if existing link exist for these 2 users
										foreach($overall_data[$course_id]["groups"][$group_id]["social_links"] as &$class_sl)
										{
											if($class_sl["target"] == $parent_comment->user_id && $class_sl["source"] == $submission->user_id)//If found social link of the same direction, add one to strength.
											{
												$class_sl["strength"] += 1;
												$found = true;
												break;
											}
											else if($class_sl["target"] == $submission->user_id && $class_sl["source"] == $parent_comment->user_id)//If found social link of opposite direction, means there is 2 way connection, so update both strength and direction.
											{
												$class_sl["strength"] += 1;
												$class_sl["direction"] = 2;
												$found = true;
												break;
											}
										}

										if(!$found)//If SLA doesnt exist yet for this target user, create the SLA
										{
											array_push($overall_data[$course_id]["groups"][$group_id]["social_links"], array(
																"source" => $submission->user_id,
																"target" => $parent_comment->user_id,
																"strength" => 1,
																"direction" => "1",
																"faveColor" => "#848484"
															));
										}
										//End of Populating CLASS level Social links
									}
								}
							}

							//check thinking skills used and add counters for each skill used
              $tags_used_in_submission = $submission->tag_labels;
							if(strpos($tags_used_in_submission, "I think that") !== false){
								$overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["ideate"]++;
								$overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["ideate"]++;
							}
							else if(strpos($tags_used_in_submission, "I think so because") !== false){
								$overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["justify"]++;
								$overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["justify"]++;
							}
							else if(strpos($tags_used_in_submission, "I agree") !== false){
								$overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["agree"]++;
								$overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["agree"]++;
							}
							else if(strpos($tags_used_in_submission, "I disagree") !== false){
								$overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["disagree"]++;
								$overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["disagree"]++;
							}
							else if(strpos($tags_used_in_submission, "I need to ask") !== false){
								$overall_data[$course_id]["groups"][$group_id]["users"]["overall"][$user_id]["inquire"]++;
								$overall_data[$course_id]["groups"][$group_id]["users"][$video_id][$user_id]["inquire"]++;
							}
						}//End of If current submission belongs to current looped user
					}//End of submissions loop
				}//End of student loop

				//Populating VIDEO level Top/Bottom Weights and Strength.
				//Weights and Replies are used for SNA visualization, to have a balanced proportions (size(weight) and color gradient(reply) of each node) in the drawings.
				foreach($overall_data[$course_id]["groups"][$group_id]["videos"] as $video_array)//for each video in the class
				{
					//Setup weight and replies variables
					$bottom_weight = 0;
					$top_weight = 0;
					$bottom_reply = 0;
					$top_reply = 0;

					$video_id = $video_array["id"]; //Track video ID

					//Loop through all users for this video, track the highest and lowest weight and reply
					foreach($overall_data[$course_id]["groups"][$group_id]["users"][$video_id] as $user)
					{
						if($user["weight"] <= $bottom_weight)
							$bottom_weight = $user["weight"];

						if($user["weight"] >= $top_weight)
							$top_weight = $user["weight"];

						if($user["reply"] <= $bottom_reply)
							$bottom_reply = $user["reply"];

						if($user["reply"] >= $top_reply)
							$top_reply = $user["reply"];
					}

					//Once highest and lowest weight and replies are identified, set the values in the data array for this video.
					$overall_data[$course_id]["groups"][$group_id]["videos"][$video_id]["weights"] = array("bottom_weight"=>$bottom_weight, "top_weight"=>$top_weight, "bottom_reply"=>$bottom_reply, "top_reply"=>$top_reply);


					//Strengths are used for SNA visualization, to have a balanced proportion (thickness of connection lines) between each node in the drawings.
					$bottom_strength = 0;
					$top_strength = 0;

					//Loop through all social links for this video, track highest and lowest strengths
					foreach($overall_data[$course_id]["groups"][$group_id]["videos"][$video_id]["social_links"] as $social_link)
					{
						if($social_link["strength"] <= $bottom_strength)
							$bottom_strength = $social_link["strength"];

						if($social_link["strength"] >= $top_strength)
							$top_strength = $social_link["strength"];
					}

					//Once highest and lowest strength are identified, set the values in the data array for this video.
					$overall_data[$course_id]["groups"][$group_id]["videos"][$video_id]["strengths"] = array("bottom_strength"=>$bottom_strength, "top_strength"=>$top_strength);
				}
				//End of Populating VIDEO level Top/Bottom Weights and Strength

				//Same as above, but for Class level.
				//Populating CLASS level Top/Bottom Weights and Strength

				//Weights and Replies are used for SNA visualization, to have a balanced proportions (size(weight) and color gradient(reply) of each node) in the drawings.

				//Setup weight and replies variables
				$class_bottom_weight = 0;
				$class_top_weight = 0;
				$class_bottom_reply = 0;
				$class_top_reply = 0;

				//Loop through all users for this class, track the highest and lowest weight and reply
				foreach($overall_data[$course_id]["groups"][$group_id]["users"]["overall"] as $user)
				{
					if($user["weight"] <= $class_bottom_weight)
						$class_bottom_weight = $user["weight"];

					if($user["weight"] >= $class_top_weight)
						$class_top_weight = $user["weight"];

					if($user["reply"] <= $class_bottom_reply)
						$class_bottom_reply = $user["reply"];

					if($user["reply"] >= $class_top_reply)
						$class_top_reply = $user["reply"];
				}

				//Once highest and lowest weight and replies are identified, set the values in the data array for this class.
				$overall_data[$course_id]["groups"][$group_id]["weights"] = array("bottom_weight"=>$class_bottom_weight, "top_weight"=>$class_top_weight, "bottom_reply"=>$class_bottom_reply, "top_reply"=>$class_top_reply);

				//Strengths are used for SNA visualization, to have a balanced proportion (thickness of connection lines) between each node in the drawings.
				$class_bottom_strength = 0;
				$class_top_strength = 0;

				//Loop through all social links for this class, track highest and lowest strengths
				foreach($overall_data[$course_id]["groups"][$group_id]["social_links"] as $class_social_link)
				{
					if($class_social_link["strength"] <= $class_bottom_strength)
						$class_bottom_strength = $class_social_link["strength"];

					if($class_social_link["strength"] >= $class_top_strength)
						$class_top_strength = $class_social_link["strength"];
				}

				//Once highest and lowest strength are identified, set the values in the data array for this class.
				$overall_data[$course_id]["groups"][$group_id]["strengths"] = array("bottom_strength"=>$class_bottom_strength, "top_strength"=>$class_top_strength);
				//End of Populating CLASS level Top/Bottom Weights and Strength

			}//End of groups loop
    }//End of courses loop

    return $overall_data;
  }

  public function string2KeyedArray($string, $delimiter = '&', $kv = '=')
  {
  	if ($string == null)
  	{
  		return null;
  	}
    if ($a = explode($delimiter, $string)) { // create parts
  	foreach ($a as $s) { // each part
  	  if ($s) {
  		if ($pos = strpos($s, $kv)) { // key/value delimiter
  		  $ka[trim(substr($s, 0, $pos))] = trim(substr($s, $pos + strlen($kv)));
  		} else { // key delimiter not found
  		  $ka[] = trim($s);
  		}
  	  }
  	}
  	return $ka;
    }
  }

	public function getOwnPreData($current_logged_in_user_id)
	{
		$surveydata = DB::table('surveys')->where('userid', $current_logged_in_user_id)->where('isCompleted', true)->where('surveyType', 'Pre')->value('calResult');
		return $surveydata;

	}
	public function getOwnPostData($current_logged_in_user_id)
	{
		$surveydata = DB::table('surveys')->where('userid', $current_logged_in_user_id)->where('isCompleted', true)->where('surveyType', 'Post')->value('calResult');
		return $surveydata;

	}
	public function getAllPreData()
	{
		$allsurvey = array();
		$surveydata = DB::table('surveys')->where('isCompleted', true)->where('surveyType', 'Pre')->get();
		foreach ($surveydata as $data) {
		    $userid = $data->userid;
			$trial = $data->trialNo;
			$type = $data->surveyType;
			// $username = $data->studentname;
			$class = $data->class;
			$cal1 = $this->string2KeyedArray($data->calResult);

			$allcal = array(
						"id"=>$userid,
						// "name"=>$username,
						"trial"=>$trial,
						"type"=>$type,
						"class"=>$class,
						"cal" => $cal1);

			array_push($allsurvey,$allcal);
		}
		return $allsurvey;

	}
	public function getAllPostData()
	{
		$allsurvey = array();
		$surveydata = DB::table('surveys')->where('isCompleted', true)->where('surveyType', 'Post')->get();
		foreach ($surveydata as $data) {
		    $userid = $data->userid;
			$trial = $data->trialNo;
			$type = $data->surveyType;
			// $username = $data->studentname;
			$class = $data->class;
			$cal1 = $this->string2KeyedArray($data->calResult);

			$allcal = array(
						"id"=>$userid,
						// "name"=>$username,
						"trial"=>$trial,
						"type"=>$type,
						"class"=>$class,
						"cal" => $cal1);

			array_push($allsurvey,$allcal);
		}
		return $allsurvey;

	}
}
