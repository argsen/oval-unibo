<?php

namespace oval\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\RequestException;

use oval\AdfUser;
use oval\AdfCourse;
use oval\User;
use oval\Course;
use oval\Enrollment;
use oval\Group;
use oval\LtiConsumer;
use oval\Jobs\SendEmail;

class AdfController extends Controller
{
    private $adfUrl = '';
    private $adfTokenUrl = '';
    private $adfClientId = '';
    private $adfClientSecret = '';
    private $adfConsumerId = 0;

    public $client = NULL;
    public $token = NULL;

    public function __construct() {
        // Load ADF settings from the environment
        $this->adfUrl = env('ADF_URL', 'https://adf.dev.sls.ufinity.com/apis/v1/graphql');
        $this->adfTokenUrl = env('ADF_TOKEN_URL', 'https://adf.dev.sls.ufinity.com/apis/v1/token');
        $this->adfConsumerId = env('ADF_CONSUMER_ID', 999);
        $lti_consumer = LtiConsumer::where('consumer_pk', '=', $this->adfConsumerId)->first();
        $this->adfClientId = $lti_consumer->consumer_key256;
        $this->adfClientSecret = $lti_consumer->secret;

        $this->client = new Client(); //GuzzleHttp\Client
        $this->token = $this->__getToken();
    }

    // --------------- Public Functions --------------- //
    /**
     * Verify ADF request is valid
     */
    public function verify(Request $req) {
        $contextId = $req->input('contextId');

        $context = $this->__getContextUser($contextId);

        if (is_object($context)) {
            $context_user = $context->user;
            $context_event = $context->event;

            // check and create user
            $user = User::where('email', '=', $context_user->email)->first();
            if (empty($user)) {
                $names = explode(" ", $context_user->name);
                $user = new User;
                $user->email = $context_user->email;
                $user->first_name = $names[0];
                $user->last_name = $names[1];
                $default_password = $this->__generatePassword(10);
                $user->password = bcrypt($default_password);
                $user->role = 'O';  // treat all ADF users as normal user
                $user->save();

                $this->__sendEmail($user->email, $default_password);
            }
            Auth::login($user);

            $adfuser = AdfUser::firstOrNew(['email' => $context_user->email]);
            $adfuser->uuid = $context_user->uuid;
            $adfuser->email = $context_user->email;
            $adfuser->name = $context_user->name;
            $adfuser->role = $context_user->role;
            $adfuser->level = $context_user->level;
            $adfuser->class_serial_no = $context_user->classSerialNo;
            $adfuser->user_id = $user->id;
            $adfuser->save();

            // check and create course
            if ($context_event->type == 'LAUNCH_APP') {
                $context_course = $this->__getCourseDetail($context_event->typeId);
                if (is_object($context_course)) {
                    $adfcourse = AdfCourse::firstOrNew(['uuid' => $context_course->uuid]);
                    $adfcourse->uuid = $context_course->uuid;
                    $adfcourse->code = $context_course->code;
                    $adfcourse->subject = $context_course->subject;
                    $adfcourse->level = $context_course->level;
                    $adfcourse->save();

                    // Create or update Course 
                    $course = Course::firstOrNew(['consumer_id' => $this->adfConsumerId, 'moodle_course_id' => $adfcourse->id]);
                    $course->name = $context_course->subject;
                    $course->consumer_id = $this->adfConsumerId;
                    $course->moodle_course_id = $adfcourse->id;
                    $course->save();

                    // Create or update Enrollment
                    $enrollment = Enrollment::firstOrNew(['course_id' => $course->id, 'user_id' => $user->id]);
                    $enrollment->course_id = $course->id;
                    $enrollment->user_id = $user->id;
                    $enrollment->is_instructor = ($adfuser->role == 'TEACHER' ? TRUE : FALSE);
                    $enrollment->save();

                    // Create or update default Course Group
                    $g = Group::firstOrNew(['moodle_group_id' => NULL, 'course_id' => $course->id]);
                    $g->moodle_group_id = NULL;
                    $g->name = 'Course Group';
                    $g->course_id = $course->id;
                    $g->save();
            
                    //--add user to the default group
                    $g->addMember($user);
                }
            }
        }


        if (empty($course)) {
            return view('pages.message-page', ['title'=>'ERROR', 'message'=>'Oops, something is wrong. Please try again later.']);
        } else {
            return redirect()->secure('/course/'.$course->id);
        }
    }


    // --------------- Private Functions --------------- //
    /**
     * Get access token for following GraphQL queries
     */
    private function __getToken() {
        $client = $this->client;

        try {
            $response = $client->request('POST', $this->adfTokenUrl, [
                RequestOptions::HEADERS => [
                    'Content-Type' => 'application/json',
                    'Accept'       => 'application/json'
                ],
                RequestOptions::JSON => [
                    'clientId'     => $this->adfClientId,
                    'clientSecret' => $this->adfClientSecret,
                    'grantType'    => 'client_credentials',
                    'scope'        => 'all'
                ]
            ]);
        } catch (Exception $e) {
            error_log($e->getMessage() . ': ' . $e->getTraceAsString());
            return NULL;
        }

        $result = json_decode($response->getBody());
        if ($result && property_exists($result, 'data') && property_exists($result->data, 'token')) {
            return $result->data->token;
        } else {
            return NULL;
        }
    }

    private function __getContextUser($contextId) {
        $client = $this->client;

        $queryStr = 'query { context(uuid:"' . $contextId . '") { user { uuid name role level email classSerialNo } event { type typeId } } }';
        try {
            $response = $client->request('POST', $this->adfUrl, [
                RequestOptions::HEADERS => [
                    'Authorization' => 'Bearer ' . $this->token,
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json'
                ],
                RequestOptions::JSON => ['query' => $queryStr]
            ]);
        } catch (Exception $e) {
            error_log($e->getMessage() . ': ' . $e->getTraceAsString());
            return FALSE;
        }

        $result = json_decode($response->getBody());

        if (!empty($result) && property_exists($result, 'errors') && count($result->errors) > 0) {
            if ($result->errors[0]->message == 'Authorization header is invalid') {
                $this->token = $this->__getToken();
                if (is_null($this->token)) {
                    return FALSE;
                } else {
                    return $this->__getContextUser($contextId);
                }
            } else {
                // return $result->errors[0];
                error_log(json_encode($result->errors[0]));
                return FALSE;
            }
        }
        if (!empty($result) && property_exists($result, 'data') && property_exists($result->data, 'context')) {
            return $result->data->context;
            /**
             * Example context data
             * {
             *   "user": {
             *     "uuid": "4a3486ca-8002-497b-8600-ae246062f1a4",
             *     "name": "Tester xxx",
             *     "role": "TEACHER",
             *     "level": null,
             *     "email": "abc@mail.dev.sls.ufinity.com",
             *     "classSerialNo": null
             *   },
             *   "event": {
             *     "type": "LAUNCH_APP",
             *     "typeId": "90c632fa-92e1-11e7-b105-0800279a327c"
             *   }
             * }
             * 
             * 1. LAUNCH_APP: Launch App from <Subject Group> page
             *    typeId should be corresponding SubjectGroup uuid
             * 2. LAUNCH_ASSIGNMENT: Open <Assignment> page (from Assignment Listing page)
             *    typeId should be corresponding Assignment uuid
             * 3. LAUNCH_TASK: Open <Task> page (from either Dashboard or Notification)
             *    typeId should be corresponding Task uuid
             */
        } else {
            return FALSE;
        }
    }

    private function __getUserCourses($userId) {
        $client = $this->client;

        $queryStr = 'query { user(uuid:"' . $userId . '") { subjectGroups(first:50) { uuid code level subject } } }';
        try {
            $response = $client->request('POST', $this->adfUrl, [
                RequestOptions::HEADERS => [
                    'Authorization' => 'Bearer ' . $this->token,
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json'
                ],
                RequestOptions::JSON => ['query' => $queryStr]
            ]);
        } catch (Exception $e) {
            error_log($e->getMessage() . ': ' . $e->getTraceAsString());
            return FALSE;
        }

        $result = json_decode($response->getBody());

        if (!empty($result) && property_exists($result, 'errors') && count($result->errors) > 0) {
            if ($result->errors[0]->message == 'Authorization header is invalid') {
                $this->token = $this->__getToken();
                if (is_null($this->token)) {
                    return FALSE;
                } else {
                    return $this->__getUserCourses($userId);
                }
            } else {
                // return $result->errors[0];
                error_log(json_encode($result->errors[0]));
                return FALSE;
            }
        }
        if (!empty($result) && property_exists($result, 'data') && property_exists($result->data, 'user')) {
            return $result->data->user;
            /**
             * Example data
             * 
             * {
             *   "subjectGroups":[{
             *     "uuid":"90c632fa-92e1-11e7-b105-0800279a327c",
             *     "code":"101",
             *     "level":"SECONDARY_1",
             *     "subject":"ADDITIONAL MATHEMATICS"
             *   }]
             * }
             */
        } else {
            return FALSE;
        }
    }

    private function __getCourseDetail($courseId) {
        $client = $this->client;

        $queryStr = 'query { subjectGroup(uuid:"' . $courseId . '") { uuid code level subject } }';
        try {
            $response = $client->request('POST', $this->adfUrl, [
                RequestOptions::HEADERS => [
                    'Authorization' => 'Bearer ' . $this->token,
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json'
                ],
                RequestOptions::JSON => ['query' => $queryStr]
            ]);
        } catch (Exception $e) {
            error_log($e->getMessage() . ': ' . $e->getTraceAsString());
            return FALSE;
        }

        $result = json_decode($response->getBody());

        if (!empty($result) && property_exists($result, 'errors') && count($result->errors) > 0) {
            if ($result->errors[0]->message == 'Authorization header is invalid') {
                $this->token = $this->__getToken();
                if (is_null($this->token)) {
                    return FALSE;
                } else {
                    return $this->__getCourseDetail($courseId);
                }
            } else {
                // return $result->errors[0];
                error_log(json_encode($result->errors[0]));
                return FALSE;
            }
        }
        if (!empty($result) && property_exists($result, 'data') && property_exists($result->data, 'subjectGroup')) {
            return $result->data->subjectGroup;
            /**
             * Example data
             * 
             * {
             *   "uuid": "90c632fa-92e1-11e7-b105-0800279a327c",
             *   "code": "101",
             *   "level": "SECONDARY_1",
             *   "subject": "ADDITIONAL MATHEMATICS"
             * }
             */
        } else {
            return FALSE;
        }
    }

    private function __getCourseAssignments($courseId) {
        $client = $this->client;

        $queryStr = 'query { subjectGroup(uuid:"' . $courseId . '") { assignments(first:50) { uuid type title start end } } }';
        try {
            $response = $client->request('POST', $this->adfUrl, [
                RequestOptions::HEADERS => [
                    'Authorization' => 'Bearer ' . $this->token,
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json'
                ],
                RequestOptions::JSON => ['query' => $queryStr]
            ]);
        } catch (Exception $e) {
            error_log($e->getMessage() . ': ' . $e->getTraceAsString());
            return FALSE;
        }

        $result = json_decode($response->getBody());

        if (!empty($result) && property_exists($result, 'errors') && count($result->errors) > 0) {
            if ($result->errors[0]->message == 'Authorization header is invalid') {
                $this->token = $this->__getToken();
                if (is_null($this->token)) {
                    return FALSE;
                } else {
                    return $this->__getCourseAssignments($courseId);
                }
            } else {
                // return $result->errors[0];
                error_log(json_encode($result->errors[0]));
                return FALSE;
            }
        }
        if (!empty($result) && property_exists($result, 'data') && property_exists($result->data, 'subjectGroup')) {
            return $result->data->subjectGroup;
            /**
             * Example data
             * 
             * {
             *   "assignments":[{
             *     "uuid":"d57aa443-43a6-4c58-a692-53865ecd1bca",
             *     "type":"101",
             *     "title":"app lesson1",
             *     "start":"2018-12-03T17:15:30+08:00",
             *     "end":"2018-12-13T17:15:30+08:00"
             *   }]
             * }
             */
        } else {
            return FALSE;
        }
    }

    private function __getAssignmentCourseAndTasks($assignmentId) {
        $client = $this->client;

        $queryStr = 'query { assignment(uuid:"' . $assignmentId . '") { subjectGroup { uuid type title start end } tasks(first:50) { uuid title start end subject status } } }';
        try {
            $response = $client->request('POST', $this->adfUrl, [
                RequestOptions::HEADERS => [
                    'Authorization' => 'Bearer ' . $this->token,
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json'
                ],
                RequestOptions::JSON => ['query' => $queryStr]
            ]);
        } catch (Exception $e) {
            error_log($e->getMessage() . ': ' . $e->getTraceAsString());
            return FALSE;
        }

        $result = json_decode($response->getBody());

        if (!empty($result) && property_exists($result, 'errors') && count($result->errors) > 0) {
            if ($result->errors[0]->message == 'Authorization header is invalid') {
                $this->token = $this->__getToken();
                if (is_null($this->token)) {
                    return FALSE;
                } else {
                    return $this->__getAssignmentCourseAndTasks($assignmentId);
                }
            } else {
                // return $result->errors[0];
                error_log(json_encode($result->errors[0]));
                return FALSE;
            }
        }
        if (!empty($result) && property_exists($result, 'data') && property_exists($result->data, 'assignment')) {
            return $result->data->assignment;
            /**
             * Example data
             * 
             * {
             *   "tasks":[{
             *     "uuid":"d57aa443-43a6-4c58-a692-53865ecd1bca",
             *     "title":"app lesson1",
             *     "start":"2018-12-03T17:15:30+08:00",
             *     "end":"2018-12-13T17:15:30+08:00",
             *     "subject":"ADDITIONAL MATHEMATICS",
             *     "status":"NEW"
             *   }]
             * }
             */
        } else {
            return FALSE;
        }
    }

    private function __generatePassword(
        $length,
        $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ) {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        if ($max < 1) {
            throw new Exception('$keyspace must be at least two characters long');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }

    private function __sendEmail($email, $password) {
        return dispatch(new SendEmail([
            'email'   => 'emails.new-adf-user',
            'subject' => 'Welcome to CoVAA',
            'to'      => $email,
            'params'  => [
                'password' => $password
            ]
        ]));
    }
}
