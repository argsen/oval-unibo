<?php

namespace oval\Http\Controllers\Auth;

use oval\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use oval\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showResetPasswordForm()
    {
        $user = Auth::user();

        return view('auth.passwords.reset-password', ['email' => $user->email]);
    }

    public function resetPassword(Request $req)
    {
        $method = $req->method();

        if ($req->isMethod('post')) {
            $form = $req->all();
            $this->validate($req, [
                'old_password' => 'bail|required|max:255',
                'password' => 'required|min:6|confirmed|max:255',
                'password_confirmation' => 'required|same:password|max:255'
            ]);
            $old_password = $form['old_password'];
            $password = $form['password'];
            // $password_confirmation = $form['password_confirmation'];

            $user = Auth::user();

            if (!password_verify($old_password, $user->password)) {
                $req->session()->flash('error-status', 'Your old password is incorrect!');
            } elseif ($old_password == $password) {
                $req->session()->flash('error-status', 'Your new password is the same with your old password!');
            } else {
                $user->password = bcrypt($password);
                $user->save();

                $req->session()->flash('status', 'Your password has been changed successfully!');
            }
            return view('auth.passwords.reset-password');
        } else {
            return 'Invalid request.';
        }
    }
}
