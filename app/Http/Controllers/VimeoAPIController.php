<?php

namespace oval\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Vinkla\Vimeo\Facades\Vimeo;
use oval\VimeoCredential;

/**
 * Controller class that handles requests to Vimeo API
 */
class VimeoAPIController extends Controller
{
    public function vimeo_auth_redirect (Request $req) {
        //-- check 'state' matches
        if (session('vimeo_state') != $req->input('state')) {
            $msg = "Something went wrong with Vimeo authentication. Please try again.";   
        }
        else {
            //-- get token
            $tokens = Vimeo::accessToken($req->input('code'), url('/vimeo_auth_redirect'));
            
            if ($tokens['status'] == 200) {
                $cred = VimeoCredential::where([
                            ['vimeo_user_uri', '=', $tokens['body']['user']['uri']],
                            ['user_id', '=', Auth::user()->id]
                        ])
                        ->first();
                if(!$cred) {
                    $cred = new VimeoCredential;
                    $cred->vimeo_user_uri = $tokens['body']['user']['uri'];
                    $cred->user_id = Auth::user()->id;
                }
                $cred->vimeo_user_name = $tokens['body']['user']['name'];
                $cred->access_token = $tokens['body']['access_token'];
                $cred->save();

                $msg = "Successfully authenticated with Vimeo. Please try adding video now.";
            }
            else {
                $msg = "Unsuccessful authentication with Vimeo. Please check your Vimeo credentials.";
            }
        }
        return redirect()->secure('/video-management')->with('msg', $msg);
    }

    /**
     * Method called when form is submitted from manage-vimeo-token page to add new token.
     */
    public function add_vimeo_token (Request $req) {
        
        $cred = VimeoCredential::where([
            ['access_token', '=', $req->token],
            ['user_id', '=', null]
        ])->first();
        if (empty($cred)) {
            $cred = new VimeoCredential;
            $cred->access_token = $req->token;
            $cred->save();
        }
        return back();
    }

    /**
     * Method called via Ajax when delete button is clicked on manage-vimeo-token page
     * 
     * Note: we are not sending delete request to vimeo API. It might be better if  
     * we did...
     */
    public function delete_vimeo_cred (Request $req) {
        VimeoCredential::destroy($req->cred_id);
    }
}
