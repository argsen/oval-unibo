<?php
namespace oval\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use JavaScript;
use oval;


class SurveyController extends Controller
{
	
	public function index()
	{

		return view('survey.index');
	}
	public function sendCompletedData(Request $request)
	{
		/*$req= $request->username;
		$trial = $request->trialNo;
		return $trial;*/
		$data= $request->all();
		$req= $data['request'];
		
		if($req == "insertSurveyEntry")
		{
			$user_login = $data['user_login'];
			$studentName = $data['studentname'];
			$studentclass = $data['studentclass'];
			if($user_login == "")
			{
				
				$user_login =3;	
			}
			
			
			
			$surveyEntry = $data['entryArray'];
			$trialNo = $data['trialNo'];
			$surveyType = $data['surveyType'];
			$dashboard_type = $data['dashboard'];
			$complete = $data['complete'];
			
			$status = "success";
			//$test = $this->getNextStatementId();
			//return $surveyEntry;
			if($surveyType == "Pre" && $complete=="true"){
				 
				$insertPre=$this->submitPreSurvey($user_login, $studentName, $studentclass, $trialNo, $surveyType, $surveyEntry,$complete);
				return $insertPre;
				//return view('survey.index', compact('insertPre'));
			}
			else if($complete=="false"){
				$insertNC= $this->submitNotCompleteSurvey($user_login, $studentName, $studentclass, $trialNo, $surveyType, $surveyEntry, $complete);
				return $insertNC;
			}
			else if($dashboard_type == 2 && $complete==1){

				//return submitPostSurveyNoDashboard($user_login, $studentName, $trialNo, $surveyType, $surveyEntry,$complete);
			}
			else if($dashboard_type == 3 && $complete==1){
				//return submitPostSurveySelfPeerDashboard($user_login, $studentName, $trialNo, $surveyType, $surveyEntry,$complete);
			}
		}

		//return $test;
		
		
	}
	public function submitNotCompleteSurvey($user_login, $studentName, $studentClass, $trialNo, $surveyType, $surveyEntry, $complete)
	{
		
		$values = json_encode($surveyEntry);
		$cal_values = null;
		//$studentClass = "SS-3A";
		$insert = $this->insertData ($user_login,$trialNo,$surveyType,$studentName,$studentClass,$values,$cal_values,$complete);

		if (!$insert){
			return "failed";
		}
		else {
			return "success";}
			
		}
	public function submitPreSurvey($user_login, $studentName, $studentClass, $trialNo, $surveyType, $surveyEntry, $complete)
		{
			//$studentClass = "SS-3A";
			$PG1 = $surveyEntry['surveyQuestions']['PG1'];
			$PG2 = $surveyEntry['surveyQuestions']['PG2'];
			$PG3 = $surveyEntry['surveyQuestions']['PG3'];
			$PG4 = $surveyEntry['surveyQuestions']['PG4'];
			$PG5 = $surveyEntry['surveyQuestions']['PG5'];
			$PG6 = $surveyEntry['surveyQuestions']['PG6'];
			$PG7 = $surveyEntry['surveyQuestions']['PG7'];
			$PG8 = $surveyEntry['surveyQuestions']['PG8'];
			
			$PerformanceGoals = round(($PG1 + $PG2 + $PG3 + $PG4 + $PG5 + $PG6 + $PG7 + $PG8)/8, 2);
			
	//echo "PerformanceGoals: " . $PerformanceGoals . "<br>";

			$LG1 = $surveyEntry['surveyQuestions']['LG1'];
			$LG2 = $surveyEntry['surveyQuestions']['LG2'];
			$LG3 = $surveyEntry['surveyQuestions']['LG3'];
			$LG4 = $surveyEntry['surveyQuestions']['LG4'];
			$LG5 = $surveyEntry['surveyQuestions']['LG5'];
			$LG6 = $surveyEntry['surveyQuestions']['LG6'];
			$LG7 = $surveyEntry['surveyQuestions']['LG7'];
			$LG8 = $surveyEntry['surveyQuestions']['LG8'];

			$LearningGoals = round(($LG1 + $LG2 + $LG3 + $LG4 + $LG5 + $LG6 + $LG7 + $LG8)/8 , 2);
			
	//echo "LearningGoals: " . $LearningGoals . "<br>";
			
			$SE1 = $surveyEntry['surveyQuestions']['SE1'];
			$SE2 = $surveyEntry['surveyQuestions']['SE2'];
			$SE3 = $surveyEntry['surveyQuestions']['SE3'];
			$SE4 = $surveyEntry['surveyQuestions']['SE4'];
			$SE5 = $surveyEntry['surveyQuestions']['SE5'];
			
			$SelfEfficacy = round(($SE1 + $SE2 + $SE3 + $SE4 + $SE5)/5, 2);
			
	//echo "SelfEfficacy: " . $SelfEfficacy . "<br>";

			$TV1 = $surveyEntry['surveyQuestions']['TV1'];
			$TV2 = $surveyEntry['surveyQuestions']['TV2'];
			$TV3 = $surveyEntry['surveyQuestions']['TV3'];
			$TV4 = $surveyEntry['surveyQuestions']['TV4'];
			$TV5 = $surveyEntry['surveyQuestions']['TV5'];
			
			$TaskValues = round(($TV1 + $TV2 + $TV3 + $TV4 + $TV5)/5, 2);
			
	//echo "TaskValues: " . $TaskValues . "<br>";
			
			$SelfEfficacy_TaskValues = round(($SE1 + $SE2 + $SE3 + $SE4 + $SE5 + $TV1 + $TV2 + $TV3 + $TV4 + $TV5)/10, 2);

			$Alien1 = $surveyEntry['surveyQuestions']['Alien1'];
			$Alien3 = $surveyEntry['surveyQuestions']['Alien3'];
			$Alien4 = $surveyEntry['surveyQuestions']['Alien4'];
			
			$Alienation = round(($Alien1 + $Alien3 + $Alien4 )/3, 2);

			$Closeness1 = 0;
			$Closeness3 = 0;
			$Closeness4 = 0;
			
			switch($Alien1){
				case 1:	$Closeness1 = 7; break;
				case 2: $Closeness1 = 6; break;
				case 3: $Closeness1 = 5; break;
				case 4: $Closeness1 = 4; break;
				case 5: $Closeness1 = 3; break;
				case 6: $Closeness1 = 2; break;
				case 7: $Closeness1 = 1; break;
			}
			
			switch($Alien3){
				case 1:	$Closeness3 = 7; break;
				case 2: $Closeness3 = 6; break;
				case 3: $Closeness3 = 5; break;
				case 4: $Closeness3 = 4; break;
				case 5: $Closeness3 = 3; break;
				case 6: $Closeness3 = 2; break;
				case 7: $Closeness3 = 1; break;
			}
			
			switch($Alien4){
				case 1:	$Closeness4 = 7; break;
				case 2: $Closeness4 = 6; break;
				case 3: $Closeness4 = 5; break;
				case 4: $Closeness4 = 4; break;
				case 5: $Closeness4 = 3; break;
				case 6: $Closeness4 = 2; break;
				case 7: $Closeness4 = 1; break;
			}
			
			$Closeness = round(($Closeness1 + $Closeness3 + $Closeness4 )/3, 2);

			$Trust1 = $surveyEntry['surveyQuestions']['Trust1'];
			$Trust2 = $surveyEntry['surveyQuestions']['Trust2'];
			$Trust3 = $surveyEntry['surveyQuestions']['Trust3'];
			$Trust4 = $surveyEntry['surveyQuestions']['Trust4'];
			$Trust5 = $surveyEntry['surveyQuestions']['Trust5'];
			
			$Trust = round(($Trust1 + $Trust2 + $Trust3 + $Trust4 + $Trust5 )/5, 2);
			
	//$Comm_Closeness_Trust = round(($Comm1 + $Comm2 + $Comm3 + $Comm5 + $Closeness1 + $Closeness3 + $Closeness4 + $Trust1 + $Trust2 + $Trust3 + $Trust4 + $Trust5)/12, 2);
			
			$TAS1 = $surveyEntry['surveyQuestions']['TAS1'];
			$TAS2 = $surveyEntry['surveyQuestions']['TAS2'];
			$TAS3 = $surveyEntry['surveyQuestions']['TAS3'];
			$TAS4 = $surveyEntry['surveyQuestions']['TAS4'];
			
			$AutonomySupport = round(($TAS1 + $TAS2 + $TAS3 + $TAS4 )/4, 2);

			$TCS1 = $surveyEntry['surveyQuestions']['TCS1'];
			$TCS2 = $surveyEntry['surveyQuestions']['TCS2'];
			$TCS3 = $surveyEntry['surveyQuestions']['TCS3'];
			$TCS4 = $surveyEntry['surveyQuestions']['TCS4'];
			
			$CompetenceSupport = round(($TCS1 + $TCS2 + $TCS3 + $TCS4 )/4, 2);
			
			$Autonomy_CompetenceSupport = round(($TAS1 + $TAS2 + $TAS3 + $TAS4 + $TCS1 + $TCS2 + $TCS3 + $TCS4)/8, 2);

			$BENG1 = $surveyEntry['surveyQuestions']['BENG1'];
			$BENG2 = $surveyEntry['surveyQuestions']['BENG2'];
			$BENG3 = $surveyEntry['surveyQuestions']['BENG3'];
			$BENG4 = $surveyEntry['surveyQuestions']['BENG4'];
			
			$BehaviouralEngagement = round(($BENG1 + $BENG2 + $BENG3 + $BENG4)/4, 2);

			$CENG9 = $surveyEntry['surveyQuestions']['CENG9'];
			$CENG10 = $surveyEntry['surveyQuestions']['CENG10'];
			$CENG11 = $surveyEntry['surveyQuestions']['CENG11'];
			$CENG12 = $surveyEntry['surveyQuestions']['CENG12'];
			
			$CognitiveEngagement = round(($CENG9 + $CENG10 + $CENG11 + $CENG12)/4, 2);

			$EENG5 = $surveyEntry['surveyQuestions']['EENG5'];
			$EENG6 = $surveyEntry['surveyQuestions']['EENG6'];
			$EENG7 = $surveyEntry['surveyQuestions']['EENG7'];
			$EENG8 = $surveyEntry['surveyQuestions']['EENG8'];
			$EENG13 = $surveyEntry['surveyQuestions']['EENG13'];
			
			$EmotionalEngagement = round(($EENG5 + $EENG6 + $EENG7 + $EENG8 + $EENG13)/5, 2);
			
			$Behave_Cog_EmoEngagement = round(($BENG1 + $BENG2 + $BENG3 + $BENG4 + $CENG9 + $CENG10 + $CENG11 + $CENG12 + $EENG5 + $EENG6 + $EENG7 + $EENG8 + $EENG13)/13, 2);

			$GENG1 = $surveyEntry['surveyQuestions']['GENG1'];
			$GENG2 = $surveyEntry['surveyQuestions']['GENG2'];
			$GENG3 = $surveyEntry['surveyQuestions']['GENG3'];
			$GENG4 = $surveyEntry['surveyQuestions']['GENG4'];
			$GENG5 = $surveyEntry['surveyQuestions']['GENG5'];
			
			$Collaboration = round(($GENG1 + $GENG2 + $GENG3 + $GENG4 + $GENG5)/5, 2);

			$CPcr1 = $surveyEntry['surveyQuestions']['CPcr1'];
			$CPcr2 = $surveyEntry['surveyQuestions']['CPcr2'];
			$CPcr3 = $surveyEntry['surveyQuestions']['CPcr3'];
			$CPcr4 = $surveyEntry['surveyQuestions']['CPcr4'];
			$CPcr5 = $surveyEntry['surveyQuestions']['CPcr5'];
			$CPcr6 = $surveyEntry['surveyQuestions']['CPcr6'];
			
			$Creativity = round(($CPcr1 + $CPcr2 + $CPcr3 + $CPcr4 + $CPcr5 + $CPcr6)/6, 2);

			$CPcu2 = $surveyEntry['surveyQuestions']['CPcu2'];
			$CPcu3 = $surveyEntry['surveyQuestions']['CPcu3'];
			$CPcu4 = $surveyEntry['surveyQuestions']['CPcu4'];
			$CPcu5 = $surveyEntry['surveyQuestions']['CPcu5'];
			$CPcu6 = $surveyEntry['surveyQuestions']['CPcu6'];
			$CPcu7 = $surveyEntry['surveyQuestions']['CPcu7'];
			$CPcu8 = $surveyEntry['surveyQuestions']['CPcu8'];
			
			$Curiosity = round(($CPcu2 + $CPcu3 + $CPcu4 + $CPcu5 + $CPcu6 + $CPcu7 + $CPcu8)/7, 2);

			$CPct1 = $surveyEntry['surveyQuestions']['CPct1'];
			$CPct2 = $surveyEntry['surveyQuestions']['CPct2'];
			$CPct5 = $surveyEntry['surveyQuestions']['CPct5'];
			
			$CriticalThinking = round(($CPct1 + $CPct2 + $CPct5 )/3, 2);
			
			$Creativity_Curiosity = round(($CPcr1 + $CPcr2 + $CPcr3 + $CPcr4 + $CPcr5 + $CPcr6 + $CPcu2 + $CPcu3 + $CPcu4 + $CPcu5 + $CPcu6 + $CPcu7 + $CPcu8)/13, 2);
			
			$Creativity_Curiosity_CritThinking = round(($CPcr1 + $CPcr2 + $CPcr3 + $CPcr4 + $CPcr5 + $CPcr6 + $CPcu2 + $CPcu3 + $CPcu4 + $CPcu5 + $CPcu6 + $CPcu7 + $CPcu8 + $CPct1 + $CPct2 + $CPct5)/16, 2);

			$ASC1 = $surveyEntry['surveyQuestions']['ASC1'];
			$ASC2 = $surveyEntry['surveyQuestions']['ASC2'];
			$ASC3 = $surveyEntry['surveyQuestions']['ASC3'];
			
			switch($ASC3){
				case 1:	$ASC3 = 7; break;
				case 2: $ASC3 = 6; break;
				case 3: $ASC3 = 5; break;
				case 4: $ASC3 = 4; break;
				case 5: $ASC3 = 3; break;
				case 6: $ASC3 = 2; break;
				case 7: $ASC3 = 1; break;
			}
			
			$ASC4 = $surveyEntry['surveyQuestions']['ASC4'];
			$ASC5 = $surveyEntry['surveyQuestions']['ASC5'];
			
			switch($ASC5){
				case 1:	$ASC5 = 7; break;
				case 2: $ASC5 = 6; break;
				case 3: $ASC5 = 5; break;
				case 4: $ASC5 = 4; break;
				case 5: $ASC5 = 3; break;
				case 6: $ASC5 = 2; break;
				case 7: $ASC5 = 1; break;
			}
			
			$ASC6 = $surveyEntry['surveyQuestions']['ASC6'];
			$ASC7 = $surveyEntry['surveyQuestions']['ASC7'];
			$ASC8 = $surveyEntry['surveyQuestions']['ASC8'];
			
			switch($ASC8){
				case 1:	$ASC8 = 7; break;
				case 2: $ASC8 = 6; break;
				case 3: $ASC8 = 5; break;
				case 4: $ASC8 = 4; break;
				case 5: $ASC8 = 3; break;
				case 6: $ASC8 = 2; break;
				case 7: $ASC8 = 1; break;
			}
			
			$ASC9 = $surveyEntry['surveyQuestions']['ASC9'];
			
			switch($ASC9){
				case 1:	$ASC9 = 7; break;
				case 2: $ASC9 = 6; break;
				case 3: $ASC9 = 5; break;
				case 4: $ASC9 = 4; break;
				case 5: $ASC9 = 3; break;
				case 6: $ASC9 = 2; break;
				case 7: $ASC9 = 1; break;
			}
			
			$ASC10 = $surveyEntry['surveyQuestions']['ASC10'];
			
			switch($ASC10){
				case 1:	$ASC10 = 7; break;
				case 2: $ASC10 = 6; break;
				case 3: $ASC10 = 5; break;
				case 4: $ASC10 = 4; break;
				case 5: $ASC10 = 3; break;
				case 6: $ASC10 = 2; break;
				case 7: $ASC10 = 1; break;
			}
			
			$AcadSelfConcept = round(($ASC1 + $ASC2 + $ASC3 + $ASC4 + $ASC5 + $ASC6 + $ASC7 + $ASC8 + $ASC9 + $ASC10 )/10, 2);
			
			$DM1 = $surveyEntry['surveyQuestions']['DM1'];
			$DM2 = $surveyEntry['surveyQuestions']['DM2'];
			$DM3 = $surveyEntry['surveyQuestions']['DM3'];
			$DM4 = $surveyEntry['surveyQuestions']['DM4'];
			$DM5 = $surveyEntry['surveyQuestions']['DM5'];
			
			$DeepMotive = round(($DM1 + $DM2 + $DM3 + $DM4 + $DM5)/5, 2);

			$DS1 = $surveyEntry['surveyQuestions']['DS1'];
			$DS2 = $surveyEntry['surveyQuestions']['DS2'];
			$DS3 = $surveyEntry['surveyQuestions']['DS3'];
			$DS4 = $surveyEntry['surveyQuestions']['DS4'];
			$DS5 = $surveyEntry['surveyQuestions']['DS5'];
			
			$DeepStrategy = round(($DS1 + $DS2 + $DS3 + $DS4 + $DS5)/5, 2);
			
			$DeepLearning = round(($DM1 + $DM2 + $DM3 + $DM4 + $DM5 + $DS1 + $DS2 + $DS3 + $DS4 + $DS5)/10, 2);

			$SM1 = $surveyEntry['surveyQuestions']['SM1'];
			$SM2 = $surveyEntry['surveyQuestions']['SM2'];
			$SM3 = $surveyEntry['surveyQuestions']['SM3'];
			$SM4 = $surveyEntry['surveyQuestions']['SM4'];
			$SM5 = $surveyEntry['surveyQuestions']['SM5'];
			
			$SurfaceMotive = round(($SM1 + $SM2 + $SM3 + $SM4 + $SM5)/5, 2);

			$SS1 = $surveyEntry['surveyQuestions']['SS1'];
			$SS2 = $surveyEntry['surveyQuestions']['SS2'];
			$SS3 = $surveyEntry['surveyQuestions']['SS3'];
			$SS4 = $surveyEntry['surveyQuestions']['SS4'];
			$SS5 = $surveyEntry['surveyQuestions']['SS5'];
			
			$SurfaceStrategy = round(($SS1 + $SS2 + $SS3 + $SS4 + $SS5)/5, 2);

			$SurfaceLearning = round(($SM1 + $SM2 + $SM3 + $SM4 + $SM5 + $SS1 + $SS2 + $SS3 + $SS4 + $SS5)/10, 2);

			$SDLr1 = $surveyEntry['surveyQuestions']['SDLr1'];
			$SDLr2 = $surveyEntry['surveyQuestions']['SDLr2'];
			$SDLr3 = $surveyEntry['surveyQuestions']['SDLr3'];
			$SDLr4 = $surveyEntry['surveyQuestions']['SDLr4'];
			$SDLr5 = $surveyEntry['surveyQuestions']['SDLr5'];
			
			$Reflection = round(($SDLr1 + $SDLr2 + $SDLr3 + $SDLr4 + $SDLr5)/5, 2);

			$SDLg6 = $surveyEntry['surveyQuestions']['SDLg6'];
			$SDLg7 = $surveyEntry['surveyQuestions']['SDLg7'];
			$SDLg8 = $surveyEntry['surveyQuestions']['SDLg8'];
			$SDLg9 = $surveyEntry['surveyQuestions']['SDLg9'];
			
			$GoalSetting = round(($SDLg6 + $SDLg7 + $SDLg8 + $SDLg9)/4, 2);
			
			$SelfDirectedLearning = round(($SDLr1 + $SDLr2 + $SDLr3 + $SDLr4 + $SDLr5 + $SDLg6 + $SDLg7 + $SDLg8 + $SDLg9)/9, 2);

			$SGP1 = $surveyEntry['surveyQuestions']['SGP1'];
			$SGP2 = $surveyEntry['surveyQuestions']['SGP2'];
			$SGP3 = $surveyEntry['surveyQuestions']['SGP3'];
			$SGP4 = $surveyEntry['surveyQuestions']['SGP4'];
			$SGP5 = $surveyEntry['surveyQuestions']['SGP5'];
			
			$SocialGoalProsocial = round(($SGP1 + $SGP2 + $SGP3 + $SGP4 + $SGP5)/5, 2);
			
			$SGR1 = $surveyEntry['surveyQuestions']['SGR1'];
			$SGR2 = $surveyEntry['surveyQuestions']['SGR2'];
			$SGR3 = $surveyEntry['surveyQuestions']['SGR3'];
			$SGR4 = $surveyEntry['surveyQuestions']['SGR4'];
			
			$SocialGoalResponsibility = round(($SGR1 + $SGR2 + $SGR3 + $SGR4)/4, 2);
			
			$SocialGoal = round(($SGP1 + $SGP2 + $SGP3 + $SGP4 + $SGP5 + $SGR1 + $SGR2 + $SGR3 + $SGR4)/9, 2);
			
			$array_Insert = array( 
				"PG1" => (int)$PG1, "PG2" => (int)$PG2, "PG3" => (int)$PG3, "PG4" => (int)$PG4, "PG5" => (int)$PG5, "PG6" => (int)$PG6, "PG7" => (int)$PG7, "PG8" => (int)$PG8, 
				"LG1" => (int)$LG1, "LG2" => (int)$LG2, "LG3" => (int)$LG3, "LG4" => (int)$LG4, "LG5" => (int)$LG5, "LG6" => (int)$LG6, "LG7" => (int)$LG7, "LG8" => (int)$LG8, 
				"SE1" => (int)$SE1, "SE2" => (int)$SE2, "SE3" => (int)$SE3, "SE4" => (int)$SE4, "SE5" => (int)$SE5, 
				"TV1" => (int)$TV1, "TV2" => (int)$TV2, "TV3" => (int)$TV3, "TV4" => (int)$TV4, "TV5" => (int)$TV5, 
				"Alien1" => (int)$Alien1, "Alien3" => (int)$Alien3, "Alien4" => (int)$Alien4, 
				"Closeness1" => (int)$Closeness1, "Closeness3" => (int)$Closeness3, "Closeness4" => (int)$Closeness4, 
				"Trust1" => (int)$Trust1, "Trust2" => (int)$Trust2, "Trust3" => (int)$Trust3, "Trust4" => (int)$Trust4, "Trust5" => (int)$Trust5, 
				"TAS1" => (int)$TAS1, "TAS2" => (int)$TAS2, "TAS3" => (int)$TAS3, "TAS4" => (int)$TAS4, 
				"TCS1" => (int)$TCS1, "TCS2" => (int)$TCS2, "TCS3" => (int)$TCS3, "TCS4" => (int)$TCS4, 
				"BENG1" => (int)$BENG1, "BENG2" => (int)$BENG2, "BENG3" => (int)$BENG3, "BENG4" => (int)$BENG4, 
				"CENG9" => (int)$CENG9, "CENG10" => (int)$CENG10, "CENG11" => (int)$CENG11, "CENG12" => (int)$CENG12, 
				"EENG5" => (int)$EENG5, "EENG6" => (int)$EENG6, "EENG7" => (int)$EENG7, "EENG8" => (int)$EENG8, "EENG13" => (int)$EENG13, 
				"GENG1" => (int)$GENG1, "GENG2" => (int)$GENG2, "GENG3" => (int)$GENG3, "GENG4" => (int)$GENG4, "GENG5" => (int)$GENG5,
				"CPcr1" => (int)$CPcr1, "CPcr2" => (int)$CPcr2, "CPcr3" => (int)$CPcr3, "CPcr4" => (int)$CPcr4, "CPcr5" => (int)$CPcr5, "CPcr6" => (int)$CPcr6,
				"CPcu2" => (int)$CPcu2, "CPcu3" => (int)$CPcu3, "CPcu4" => (int)$CPcu4, "CPcu5" => (int)$CPcu5, "CPcu6" => (int)$CPcu6, "CPcu7" => (int)$CPcu7, "CPcu8" => (int)$CPcu8, 
				"CPct1" => (int)$CPct1, "CPct2" => (int)$CPct2, "CPct5" => (int)$CPct5, 
				"ASC1" => (int)$ASC1, "ASC2" => (int)$ASC2, "ASC3" => (int)$ASC3, "ASC4" => (int)$ASC4, "ASC5" => (int)$ASC5, "ASC6" => (int)$ASC6, "ASC7" => (int)$ASC7, "ASC8" => (int)$ASC8, "ASC9" => (int)$ASC9, "ASC10" => (int)$ASC10,
				"DM1" => (int)$DM1, "DM2" => (int)$DM2, "DM3" => (int)$DM3, "DM4" => (int)$DM4, "DM5" => (int)$DM5, 
				"DS1" => (int)$DS1, "DS2" => (int)$DS2, "DS3" => (int)$DS3, "DS4" => (int)$DS4, "DS5" => (int)$DS5, 
				"SM1" => (int)$SM1, "SM2" => (int)$SM2, "SM3" => (int)$SM3, "SM4" => (int)$SM4, "SM5" => (int)$SM5, 
				"SS1" => (int)$SS1, "SS2" => (int)$SS2, "SS3" => (int)$SS3, "SS4" => (int)$SS4, "SS5" => (int)$SS5, 
				"SDLr1" => (int)$SDLr1, "SDLr2" => (int)$SDLr2, "SDLr3" => (int)$SDLr3, "SDLr4" => (int)$SDLr4, "SDLr5" => (int)$SDLr5, 
				"SDLg6" => (int)$SDLg6, "SDLg7" => (int)$SDLg7, "SDLg8" => (int)$SDLg8, "SDLg9" => (int)$SDLg9, 
		//"LN1" => (int)$LN1, "LN2" => (int)$LN2, 
				"SGP1" => (int)$SGP1, "SGP2" => (int)$SGP2, "SGP3" => (int)$SGP3, "SGP4" => (int)$SGP4, "SGP5" => (int)$SGP5, 
				"SGR1" => (int)$SGR1, "SGR2" => (int)$SGR2, "SGR3" => (int)$SGR3, "SGR4" => (int)$SGR4
			);
			
			$array_cal = array(
				"PerformanceGoals" => $PerformanceGoals, "LearningGoals" => $LearningGoals, 
				"SelfEfficacy" => $SelfEfficacy, "TaskValues" => $TaskValues,
				"SelfEfficacy_TaskValues" => $SelfEfficacy_TaskValues, "Alienation" => $Alienation, 
				"Closeness" => $Closeness, "Trust" => $Trust,
				"AutonomySupport" => $AutonomySupport, "CompetenceSupport" => $CompetenceSupport, "Autonomy_CompetenceSupport" => $Autonomy_CompetenceSupport, 
				"BehaviouralEngagement" => $BehaviouralEngagement, "CognitiveEngagement" => $CognitiveEngagement, "EmotionalEngagement" => $EmotionalEngagement, 
				"Behave_Cog_EmoEngagement" => $Behave_Cog_EmoEngagement, "Collaboration" => $Collaboration, 
				"Creativity" => $Creativity, "Curiosity" => $Curiosity, "CriticalThinking" => $CriticalThinking, "Creativity_Curiosity" => $Creativity_Curiosity, 
				"Creativity_Curiosity_CritThinking" => $Creativity_Curiosity_CritThinking, "AcadSelfConcept" => $AcadSelfConcept,
				"DeepMotive" => $DeepMotive, "DeepStrategy" => $DeepStrategy, "DeepLearning" => $DeepLearning,
				"SurfaceMotive" => $SurfaceMotive, "SurfaceStrategy" => $SurfaceStrategy, "SurfaceLearning" => $SurfaceLearning, 
				"Reflection" => $Reflection, "GoalSetting" => $GoalSetting, "SelfDirectedLearning" => $SelfDirectedLearning,
				"SocialGoalProsocial" => $SocialGoalProsocial, "SocialGoalResponsibility" => $SocialGoalResponsibility, "SocialGoal" => $SocialGoal 
			);
			

			$values = 	http_build_query($array_Insert);
			$cal_values = 	http_build_query($array_cal);
			$insert = $this->insertData ($user_login,$trialNo,$surveyType,$studentName,$studentClass,$values,$cal_values,$complete);
//echo $values;
			if ($insert){
				return 'success';
			}
			else {
				return 'failed';}
			}
			public function getNextStatementId()
			{
			    $next_id = DB::select("select nextval('surveys_id_seq')");
				return intval($next_id['0']->nextval);
			}
			public function insertData ($user_login,$trialNo,$surveyType,$studentName,$studentClass,$values,$cal_values,$complete){
					

					$values = pg_escape_string($values);
					$cal_values = pg_escape_string($cal_values);
						$count = DB::table('survey')->where('userid', $user_login)->where('surveytype', 'Pre')->get()->count();
						if ($count>0){
							$sql = DB::table('survey')
						            ->where('userid', $user_login)
						            ->update(['iscompleted' => $complete, 'result'=>$values,'calresult'=>$cal_values]);

						}else{
							$id = $this->getNextStatementId();
							$sql =DB::table('survey')->insertGetId(
							['id'=>$id, 'userid' => $user_login,'studentname'=>$studentName,
							'class' => $studentClass, 'trialno' => $trialNo, 'surveytype' => $surveyType, 'iscompleted' => $complete, 'result' => $values, 'calresult' => $cal_values
						]
					);
						}

					return $sql;

				}
}


		


			
