<?php

namespace oval;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table 'quiz_createion'.
 */
class quiz_creation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'quiz_creation';

    protected $fillable = ['creator_id', 'identifier', 'media_type', 'quiz_data','visable'];
    
    /**
     * One-to-One relationship
     * @return User User that created this quiz
     */
    public function creator() {
        return $this->belongsTo('oval\User', 'creator_id', 'id');
    }

    /**
     * One-to-Many relationship (inverse)
     * @return GroupVideo GroupVideo this quiz belongs to
     */
    public function group_video() {
        return $this->belongsTo('oval\GroupVideo');
    }

}
