<?php

namespace oval;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table 'annotation_group_tags'
 */
class AnnotationGroupTag extends Model
{
  /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'annotation_group_tags';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /** 
     * The attributes that are mass assignable.
     * 
     * @var array 
     */
    protected $fillable = [
        'user_id',
        'covaa_tag_array'
    ];

}
