<?php

namespace oval;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

/**
 * Model class for table 'users'.
 */
class User extends Authenticatable
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'is_lti', 'api_token', 'created_at', 'updated_at'
    ];


    /**
     * Method to return full name of the user.
     * @return string
     */
    public function fullName() {
    	return $this->first_name." ".$this->last_name;
    }

    /**
     * Method to return id of the user.
     * TODO: check where it's used... $user->id does the same thing..
     */
    public function id() {
    	return $this->id;
    }

    /**
    *	Method to check if the user is an instructor of any courses
    *	@return boolean
    **/
    public function isAnInstructor() {
    	return DB::table('enrollments')
				->whereUserId($this->id)
				->whereIsInstructor(true)
				->count() >0;
    }

    /**
    *	Method to check if the user is an instructor of the course passed in as parameter
    *	@param Course $course
    *	@return boolean
    **/
    public function isInstructorOf($course) {
    	return DB::table('enrollments')
    			->whereUserId($this->id)
    			->whereCourseId($course->id)
    			->whereIsInstructor(true)
    			->count() >0;
    }

    public function isStudentOf($course) {
        $notInstructor = !$this->isInstructorOf($course);
        if ($notInstructor && $this->role=='O') {
            return true;
        }
        return false;
    }

    public function isAStudent() { 
        return ($this->role=="O" && !$this->isAnInstructor()); 
    }

    /**
    *	Method to check if the user is enrolled in the course passed in as parameter
    *	@param Course $course
    *	@return boolean true if enrolled, false if not.
    **/
    public function checkIfEnrolledIn($course) {
    	return DB::table('enrollments')
			->whereUserId($this->id)
			->whereCourseId($course->id)
			->count() > 0;
    }

    /**
    *	Many to many Relationship - Courses the user is enrolled in.
    *	@return collection of Course objects
    **/
    public function enrolledCourses() {
    	return $this->belongsToMany('oval\Course', "enrollments")->withPivot('is_instructor');
    }

    /**
     * Method to return Courses in the term & year passed in as parameter that this User is enrolled in.
     * @param string $term
     * @param int $year
     * @return collection Collection of Courses objects
     */
    public function enrolledCoursesForTermAndYear (string $term, int $year) {
        return $this->enrolledCourses->filter(function($value, $key) use($term, $year) {
            return ($value->term==$term && $value->year==$year);
        });
    }

    /**
     * Method to get the year and term combinations the user has enrollments in.
     * @return Collection Collection with keys: year, term, term_name
     */
    public function enrolledTerms() {
        $terms = DB::table('enrollments')
                    ->join('courses', 'enrollments.course_id', '=', 'courses.id')
                    ->select(DB::raw("year, term, concat(courses.term,' ',courses.year) as term_name"))
                    ->where('enrollments.user_id', '=', $this->id)
                    ->where('courses.year', '<>', null)
                    ->where('courses.term', '<>', null)
                    ->orderBy('term_name', 'desc')
                    ->distinct()
                    ->get();
        return $terms;
    }

    /**
    *	Get collection of Course objects the user teaches
    *	@return collection of Course objects
    **/
    public function coursesTeaching() {
    	$user_id = $this->id;
    	return Course::whereIn('id', function($q) use ($user_id) {
    					$q	->select('course_id')
    						->from('enrollments')
    						->where('user_id', '=', $user_id)
    						->where('is_instructor', '=', true);
					})
					->get();
    }

    public function coursesTeachingForTermAndYear (string $term, int $year) {
        return $this->coursesTeaching()->filter(function ($v, $i) use ($term, $year) {
            return ($v->term==$term && $v->year==$year);
        });
    }

    /**
    *	Add many-to-many relationship.
    *	Enroll this user into the course passed as parameter
    *	@param Course $course
    **/
    public function enrollIn($course) {
    	//check if user is enrolled in the course first...
		$alreadyEnrolled = $this->checkIfEnrolledIn($course);
		if (!$alreadyEnrolled) {
			$this->enrolledCourses()->attach($course);
		}
    }

    /**
    *	Add many-to-many relationship.
    *	Make this user an instructor of a Course
    *	@param Course $course
    **/
    public function makeInstructorOf($course) {
    	//first check if enrolled, and unenroll if enrolled
    	if($this->checkIfEnrolledIn($course)) {
    		echo "\nalready enrolled";
    		$this->enrolledCourses()->detach($course->id);
    	}
    	$this->enrolledCourses()->save($course, ['is_instructor'=>true]);
    }

    /**
    *	Get many-to-many relationship
    *	@return collection of Group objects
    **/
    public function groupMemberOf() {
    	return $this->belongsToMany('oval\Group', "group_members");
    }

    /**
    *	Method to check if the user belongs to the group passed in as parameter
    *	@param Group $group
    *	@return boolean true if in group, false if not.
    **/
    public function checkIfInGroup($group) {
    	return DB::table('group_members')
			->whereUserId($this->id)
			->whereGroupId($group->id)
			->count() > 0;
    }

	/**
	*	Add this user to the group passed in
	*	@param	Group $group
	**/
	public function addToGroup($group) {
		if (!$this->checkIfInGroup($group)) {
            $this->groupMemberOf()->attach($group);
            
            //-- create ReceivedNotification(s) if notifications exist for the group
            $notifications = $group->notifications_received;
            if($notifications->count() > 0) {
                foreach($notifications as $n) {
                    $rn = new ReceivedNotification;
                    $rn->notification_id = $n->id;
                    $rn->recipient_id = $this->id;
                    $rn->save();
                }
            }
		}
	}

    /**
    *	Remove this user from the Group passed in
    *	@param Group $group
    **/
    public function removeFromGroup($group) {
        $this->groupMemberOf->detach($group->id);
        
        //-- delete RecievedNotification(s)
        $notifications = $group->notifications_recieved;
        if($notifications->count() > 0) {
            foreach($notifications as $n) {
                $rn = ReceivedNotification::where([
                            ['notification_id', '=', $n->id],
                            ['recipient_id', '=', $this->id]
                        ])
                        ->delete();
            }
        }
    }

    /**
    *	Videos that are assigned to the groups this user belongs to
    *	@return collection of Video objects
    **/
    public function viewableGroupVideos() {
    	$group_videos = collect();
    	foreach ($this->groupMemberOf as $group) {
            $group_videos = $group_videos->merge($group->availableGroupVideosForUser($this));

    	}
    	return $group_videos;
    }

    /**
     * One-to-many relationship - Annotation created by the User
     *	@return collection of Annotation objects
     **/
    public function annotations() {
    	return $this->hasMany('oval\Annotation');
    }

    /**
    *	One-to-many relationship.
    *	Comments written by this user.
    *	@return collection of Comment objects
    **/
    public function comments() {
    	return $this->hasMany('oval\Comment');
    }

    /**
     * One to many relationship - Videos added by the User
     *	@return collection of Video objects
     **/
    public function videosAdded() {
    	return $this->hasMany('oval\Video', 'added_by');
    }

    /**
    *	Many-to-many relationship.
    *	Returns videos viewed by this user.
    *	@return collection of Video objects
    **/
    public function videosViewed() {
    	return $this->belongsToMany('oval\Video', 'videos_viewed_by');
    }

    /**
    *	Many-to-many relationship.
    *	Annotations viewed by this user.
    *	@return collection of Annotation objects
    **/
    public function annotationViewed() {
    	return $this->belongsToMany('oval\Annotation', 'annotation_viewed_by');
    }
    
    /**
    *	One-to-Many relationship.
    *	Feedbacks made by this user.
    *	@return collection of Feedback objects
    **/
    public function feedbacks() {
    	return $this->hasMany('oval\Feedback');
    }
    
    /**
    *	Method to get this user's ConfidenceLevel for GroupVideo whose id is passed in
    *	@param int group_video_id
    *	@return ConfidenceLevel object
    **/
    public function confidenceLevelForVideo($group_video_id) {
    	return DB::table('confidence_levels')
    			->where([
    				['user_id', '=', $this->id],
    				['group_video_id', '=', $group_video_id]
    			])
    			->first();
    }
    
    /**
    *	One-to-Many relationship
    *	@return collection of Tracking objects
    **/
    public function trackings() {
    	return $this->hasMany('oval\Tracking');
    }

    /**
    *   One-to-Many relationship
    *   @return collection of AnalysisRequest objects
    **/
    public function analysis_request() {
        return $this->hasMany('oval\AnalysisRequest');
    }


    /**
     * One-to-Many relationship
     * @return collection of quiz_creation objects
     */
    public function quizzes_created() {
        return $this->hasMany('oval\quiz_creation', 'creator_id', 'id');
    }

    /**
     * One-to-Many relationship
     * @return collection Collection of RecommendedVieoLike objects
     */
    public function liked_recommended_video() {
        return $this->hasMany('oval\RecommendedVideoLike');
    }

    /** 
     * One-to-Many relationship 
     *  
     * Returns collection of Notifications written by this user 
     * @return collection Collection of Notification objects 
     */ 
    public function authored_notification() { 
        return $this->hasMany('oval\Notification', 'author_id'); 
    } 
 
    /** 
     * One-to-Many relationship 
     *  
     * Notifications received by this user.(UserNotification objects) 
     * @return collection Collection of UserNotification objects 
     */ 
    public function received_notifications() { 
        return $this->hasMany('oval\ReceivedNotification', 'recipient_id'); 
    } 
 

    /**
     * One-to-Many relationship
     * @return collection VimeoCredential objects owned by this user
     */
    public function vimeo_credentials() {
        return $this->hasMany('oval\VimeoCredential', 'user_id', 'id');
    }

}//end class
