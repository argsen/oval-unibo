<?php

namespace oval;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table 'group_video_segments'
 */
class GroupVideoSegment extends Model
{
    protected $table = 'group_video_segments';

    /**
     * One-to-Many relationship (inverse)
     * @return GroupVideo The GroupVideo this segment belongs to
     */
    public function group_video() {
        return $this->belongsTo('oval\GroupVideo');
    }

    private function secondsToTimeString(int $secs) {
        $hour = intdiv($secs, 360);
        $rest = $secs % 360;
        $minute = intdiv($rest, 60);
        $sec = $rest % 60;
        $minute = $hour>0 && $minute<10 ? "0".$minute : $minute;
        $sec = $sec<10 ? "0".$sec : $sec;

        return $hour>0 ? $hour.":".$minute.":".$sec : $minute.":".$sec;
    }
    public function formattedStartTime() {
        return $this->secondsToTimeString($this->start);
    }
    public function formattedEndTime() {
        return $this->secondsToTimeString($this->end);
    }
}
