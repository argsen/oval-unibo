<?php

namespace oval;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table 'annotation_group_tag_links'
 */
class AnnotationGroupTagLink extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'annotation_group_tag_links';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /** 
     * The attributes that are mass assignable.
     * 
     * @var array 
     */
    protected $fillable = [
        'user_id',
        'group_video_id',
        'covaa_tag_array'
    ];
}
