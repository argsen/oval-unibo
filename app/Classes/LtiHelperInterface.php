<?php
namespace oval\Classes;

use oval\User;

/**
 * Interface for class called when landing from LTI connection.
 * 
 * As each institute may have different things to sync and/or customization of Moodle, 
 * we need to create a concrete class for each OVAL installation.
 * The binding needs to be updated when implementing for new installation.
 * Set this in app\Providers\LtiServiceProvider
 */
interface LtiHelperInterface {

    /**
     * Method to import or sync Courses, Enrollments, Groups information from LTI source
     */
    public function handleEnrollmentData(int $consumer_id, User $user, bool $is_instructor, int $moodle_user_id);

}

?>