<?php
namespace oval\Classes;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use oval\User;
use oval\LtiCredential;
use oval\Course;
use oval\Enrollment;
use oval\Group;


/**
 * Concrete class implementing LtiHelperInterface for Unibo
 * 
 * Get LtiCredential(credential of database where the LTI request originates from)
 * for the consumer_pk and import course and group info for OVAL.
 * This doesn't deal with any custom tables in Moodle
 */
class UniboLtiHelper implements LtiHelperInterface {

    public function handleEnrollmentData(int $consumer_id, User $user, bool $is_instructor, int $moodle_user_id) {

        //--set db credentials for the LTI source (DB::connection('moodle'))
        //--get values from DB and set the config
        $cred = LtiCredential::where('consumer_id', '=', $consumer_id)->first();
        if(!empty($cred)) {
          config(["database.connections.moodle" => [
            "driver" => $cred->db_type,
            "host" => $cred->host,
            "port" => $cred->port,
            "database" => $cred->database,
            "username" => $cred->username,
            "password" => $cred->password,
            "prefix" => $cred->prefix,
            "charset" => 'utf8'
          ]]);
        }
        
    
        //----if this is "Academic Developer", there's no enrollment entries in Moodle
        //----create or update enrollment on OVAL as instructor for the course and update group info 
        $moodle_course_id = intval($_POST['context_id']);
        if ($is_instructor) {
          $course = Course::where([
                        ['consumer_id', '=', $consumer_id],
                        ['moodle_course_id', '=', $moodle_course_id]
                    ])->first();
          if(empty($course)) {
            $course = new Course;
            $course->moodle_course_id = $moodle_course_id;
          }
          $course->name = $_POST['context_title'];
          $course->consumer_id = $consumer_id;
          $course->save();
          
          $enrollment = Enrollment::where([
            ['course_id', '=', $course->id],
            ['user_id', '=', $user->id]
          ])->first();
          if (empty($enrollment)) {
            $enrollment = new Enrollment;
            $enrollment->course_id = $course->id;
            $enrollment->user_id = $user->id;
          }
          $enrollment->is_instructor = true;
          $enrollment->save();
    
          // Create or update default Course Group 
          $g = Group::firstOrNew(['moodle_group_id' => NULL, 'course_id' => $course->id]);
          $g->moodle_group_id = NULL;
          $g->name = 'Course Group';
          $g->course_id = $course->id;
          $g->save();
    
          //--add user to the default group
          $g->addMember($user);
    
          //--if we can connect to Moodle DB, get group info
          try {
            //--check if we can connect to moodle db...
            DB::connection('moodle')->getPdo();
            //--if connection fails, goes to catch block below---->
    
            //--if we can connect, update group info
            //--add user to all groups of the course
            $groups = DB::connection('moodle')->table('groups')
                      -> select('id', 'name')
                      -> whereNotNull('id')
                      -> where([
                        ['courseid', '=', $moodle_course_id],
                      ])
                      ->get();

            foreach ($groups as $group) {
              $g = Group::firstOrNew(['moodle_group_id' => $group->id]);
              $g->moodle_group_id = $group->id;
              $g->name = $group->name;
              $g->course_id = $course->id;
              $g->save();
              
              $g->addMember($user);
            }
          }
          catch (\Exception $e) {
            Log::info("Couldn't connect to Moodle DB - ".$e);/////
            //--nothing to do if no connection to Moodle DB
          } 
        }//end if instructor
        else {//--if students--
          //--if we can connect to Moodle DB, get group info  
          try {
            //--check if we can connect to moodle db...
            DB::connection('moodle')->getPdo();
            //--if connection fails, goes to catch block--->
    
            //--if we can connect, update course & group info
            $enrollments = DB::connection('moodle')->table('user_enrolments')
                -> leftJoin('enrol', 'user_enrolments.enrolid', '=', 'enrol.id')
                -> leftJoin('course', 'enrol.courseid', '=', 'course.id')
                -> select('course.id', 'course.fullname', 'course.startdate', 'course.enddate')
                -> whereNotNull('course.id')
                -> where([
                  ['user_enrolments.userid', '=', $moodle_user_id],
                ])
                ->get();
            foreach ($enrollments  as $enrol) {
              set_time_limit(30);
              $course = Course::where([
                            ['consumer_id', '=', $consumer_id],
                            ['moodle_course_id', '=', $enrol->id],
                        ])->first();

            
              if (empty($course)) {
                $course = new Course;
                $course->moodle_course_id = $enrol->id;
              }
              $course->consumer_id = $consumer_id;
              $course->name = $enrol->fullname;
              $course->save();
      
              $enrollment = Enrollment::where([
                ['course_id', '=', $course->id],
                ['user_id', '=', $user->id]
              ])->first();
              if (empty($enrollment)) {
                $enrollment = new Enrollment;
                $enrollment->course_id = $course->id;
                $enrollment->user_id = $user->id;
              }
              $enrollment->is_instructor = false;
              $enrollment->save();
      
              $groups = DB::connection('moodle')->table('groups')
                        -> select('groups.id', 'groups.name')
                        -> leftJoin('groups_members', 'groups.id', '=', 'groups_members.groupid')
                        -> whereNotNull('groups.id')
                        -> where([
                          ['groups.courseid', '=', $moodle_course_id],
                          ['groups_members.userid', '=', $moodle_user_id],
                        ])
                        ->get();
              foreach ($groups as $group) {
                $g = Group::firstOrNew(['moodle_group_id' => $group->id]);
                $g->name = $group->name;
                $g->course_id = $course->id;
                $g->moodle_group_id = $group->id;
                $g->save();
                $g->addmember($user);
              }
    
              // Create a default Course Group 
              $g = Group::firstOrCreate(['moodle_group_id' => NULL, 'course_id' => $course->id], ['name'=>'Course Group']);
    
              // Add current user
              $g->addMember($user);
              
            }//end foreach enrollments
          }//end try
          catch (\Exception $e) {//--can't connect to Moodle DB
            Log::info("Couldn't connect to Moodle DB - ".$e);/////
            //-- enrol the current user in the current course 
            $course = Course::where([
                        ['consumer_id', '=', $consumer_id],
                        ['moodle_course_id', '=', $moodle_course_id]
                    ])->first();
            if(empty($course)) {
              $course = new Course;
              $course->consumer_id = $consumer_id;
              $course->moodle_course_id = $moodle_course_id;
              $course->name = $_POST['context_title'];
              $course->save();
            }
            $enrollment = Enrollment::where([
              ['course_id', '=', $course->id],
              ['user_id', '=', $user->id]
            ])->first();
            if (empty($enrollment)) {
              $enrollment = new Enrollment;
              $enrollment->course_id = $course->id;
              $enrollment->user_id = $user->id;
            }
            $enrollment->is_instructor = false;
            $enrollment->save();
    
            // Create or update default Course Group for the course
            $g = Group::firstOrCreate(['moodle_group_id' => NULL, 'course_id' => $course->id], ['name'=>'Course Group']);
            // Add currently logged in user
            $g->addMember($user);
          } //end catch   
        }//end else 
    }//end handleEnrollmentData

}//end class

?>