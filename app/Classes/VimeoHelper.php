<?php
namespace oval\Classes;

use oval;

/**
 * This is a helper class to handle Vimeo API reply
 */
class VimeoHelper {

    /**
     * Method to insert new Video into database from data obtained from Vimeo API
     * 
     * This method tries to fetch Video with vimeo video id included in the parameter passed in,
     * if it doesn't exist, inserts a new one,
     * and sets all fields, then returns the Video object.
     * The video object is not yet saved - the caller has to save.
     * @param $video_data Array containing video data from API
     * @return Video The newly created Video object
     */
    public static function video_from_metadata($metadata) {
        $identifier = explode('/', $metadata['body']['uri'])[2];
        $v = oval\Video::firstOrNew([
            'identifier' => $identifier,
            'media_type' => "vimeo"
        ]);
        $v->thumbnail_url = $metadata['body']['pictures']['sizes'][0]['link'];
        $v->title = $metadata['body']['name'];
        $v->description = $metadata['body']['description'];
        $v->duration = intval($metadata['body']['duration']);

        return $v;
    }
}