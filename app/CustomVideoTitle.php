<?php

namespace oval;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table 'custom_video_titles'
 */
class CustomVideoTitle extends Model
{
    protected $table = 'custom_video_titles';
}
