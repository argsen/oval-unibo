<?php

namespace oval;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table 'adf_users'
 */
class AdfUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'adf_users';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid', 'email', 'name', 'role', 'level', 'class_serial_no', 'user_id'
    ];
}
